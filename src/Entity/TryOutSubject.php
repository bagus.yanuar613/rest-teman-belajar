<?php


namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TryOutSubjectRepository")
 */
class TryOutSubject
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categories")
     */
    private $subject;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CategoriesParent", inversedBy="tryOutSubject")
     */
    private $mainSubject;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TryOut", inversedBy="subject")
     */
    private $tryout;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TryOutRegistrantAnswer", mappedBy="subject")
     */
    private $tryoutAnswer;

    /**
     * @ORM\Column(type="string")
     */
    private $icon;

    /**
     * @ORM\Column(type="integer")
     */
    private $duration;

    /**
     * @ORM\Column(type="integer")
     */
    private $quizNumber;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="integer")
     */
    private $sorting;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return TryOutSubject
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     * @return TryOutSubject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTryout()
    {
        return $this->tryout;
    }

    /**
     * @param mixed $tryout
     * @return TryOutSubject
     */
    public function setTryout($tryout)
    {
        $this->tryout = $tryout;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     * @return TryOutSubject
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuizNumber()
    {
        return $this->quizNumber;
    }

    /**
     * @param mixed $quizNumber
     * @return TryOutSubject
     */
    public function setQuizNumber($quizNumber)
    {
        $this->quizNumber = $quizNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     * @return TryOutSubject
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return TryOutSubject
     */
    public function setCreatedAt(\DateTime $createdAt): TryOutSubject
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return TryOutSubject
     */
    public function setUpdatedAt(\DateTime $updatedAt): TryOutSubject
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $icon
     * @return TryOutSubject
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param mixed $sorting
     * @return TryOutSubject
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMainSubject()
    {
        return $this->mainSubject;
    }

    /**
     * @param mixed $mainSubject
     * @return TryOutSubject
     */
    public function setMainSubject($mainSubject)
    {
        $this->mainSubject = $mainSubject;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTryoutAnswer()
    {
        return $this->tryoutAnswer;
    }

    /**
     * @param mixed $tryoutAnswer
     * @return TryOutSubject
     */
    public function setTryoutAnswer($tryoutAnswer)
    {
        $this->tryoutAnswer = $tryoutAnswer;
        return $this;
    }

}
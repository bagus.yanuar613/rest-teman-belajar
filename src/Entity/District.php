<?php

namespace App\Entity;

use App\Repository\DistrictRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DistrictRepository::class)
 */
class District
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City", inversedBy="district")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SubCity", inversedBy="district")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $subCity;

    /**
     *@ORM\Column(type="string")
     */
    private $districtName;

    /**
     *@ORM\Column(type="string")
     */
    private $code;

    /**
     *@ORM\Column(type="string")
     */
    private $slug;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="district")
     */
    private $user;



    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Member", mappedBy="district")
     */
    private $memberProfile;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cart", mappedBy="district")
     */
    private $cart;

    /**
     * District constructor.
     */
    public function __construct()
    {
        $this->user = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDistrictName()
    {
        return $this->districtName;
    }

    /**
     * @param mixed $districtName
     * @return District
     */
    public function setDistrictName($districtName)
    {
        $this->districtName = $districtName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return District
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return District
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMemberProfile()
    {
        return $this->memberProfile;
    }

    /**
     * @param mixed $memberProfile
     * @return District
     */
    public function setMemberProfile($memberProfile)
    {
        $this->memberProfile = $memberProfile;
        return $this;
    }

    public function addUser(User $item)
    {
        if ($this->user->contains($item)) {
            return;
        }
        $this->user->add($item);
        $item->addDistrict($this);
    }

    public function removeUser(User $item)
    {
        if (!$this->user->contains($item)) {
            return;
        }
        $this->user->removeElement($item);
        $item->removeDistrict($this);
    }

    /**
     * @return mixed
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param mixed $cart
     * @return District
     */
    public function setCart($cart)
    {
        $this->cart = $cart;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubCity()
    {
        return $this->subCity;
    }

    /**
     * @param mixed $subCity
     * @return District
     */
    public function setSubCity($subCity)
    {
        $this->subCity = $subCity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     * @return District
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return District
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }



}

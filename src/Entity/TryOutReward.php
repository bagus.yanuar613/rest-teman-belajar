<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * @ORM\Entity(repositoryClass="App\Repository\TryOutRewardRepository")
 */
class TryOutReward
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\TryOut", inversedBy="reward")
     */
    private $tryOut;

    /**
     * @ORM\Column(type="integer")
     */
    private $freeAccess;

    /**
     * @ORM\Column(type="integer")
     */
    private $minRegistrant;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return TryOutReward
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTryOut()
    {
        return $this->tryOut;
    }

    /**
     * @param mixed $tryOut
     * @return TryOutReward
     */
    public function setTryOut($tryOut)
    {
        $this->tryOut = $tryOut;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFreeAccess()
    {
        return $this->freeAccess;
    }

    /**
     * @param mixed $freeAccess
     * @return TryOutReward
     */
    public function setFreeAccess($freeAccess)
    {
        $this->freeAccess = $freeAccess;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMinRegistrant()
    {
        return $this->minRegistrant;
    }

    /**
     * @param mixed $minRegistrant
     * @return TryOutReward
     */
    public function setMinRegistrant($minRegistrant)
    {
        $this->minRegistrant = $minRegistrant;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     * @return TryOutReward
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return TryOutReward
     */
    public function setCreatedAt(\DateTime $createdAt): TryOutReward
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return TryOutReward
     */
    public function setUpdatedAt(\DateTime $updatedAt): TryOutReward
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }






}
<?php

namespace App\Entity;

use App\Repository\MethodRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MethodRepository::class)
 */
class Method
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;

    /**
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $slug;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default"=0})
     */
    private $price;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Subject", mappedBy="method")
     */
    private $subject;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Categories", mappedBy="method")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Pricing", mappedBy="method")
     */
    private $pricing;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cart", mappedBy="method")
     */
    private $cart;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CategoryToParentToMethod", mappedBy="method")
     */
    private $CategoryToMethodParent;


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\TentorSubject", mappedBy="method")
     */
    private $tentorSubject;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Promo", mappedBy="method")
     */
    private $promo;

    /**
     * Method constructor.
     */
    public function __construct()
    {
        $this->subject = new ArrayCollection();
        $this->category = new ArrayCollection();
        $this->tentorSubject = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Method
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return Method
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return Method
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getSubject(): ArrayCollection
    {
        return $this->subject;
    }

    /**
     * @param ArrayCollection $subject
     * @return Method
     */
    public function setSubject(ArrayCollection $subject): Method
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCategory(): ArrayCollection
    {
        return $this->category;
    }

    /**
     * @param ArrayCollection $category
     * @return Method
     */
    public function setCategory(ArrayCollection $category): Method
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPricing()
    {
        return $this->pricing;
    }

    /**
     * @param mixed $pricing
     * @return Method
     */
    public function setPricing($pricing)
    {
        $this->pricing = $pricing;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param mixed $cart
     * @return Method
     */
    public function setCart($cart)
    {
        $this->cart = $cart;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoryToMethodParent()
    {
        return $this->CategoryToMethodParent;
    }

    /**
     * @param mixed $CategoryToMethodParent
     * @return Method
     */
    public function setCategoryToMethodParent($CategoryToMethodParent)
    {
        $this->CategoryToMethodParent = $CategoryToMethodParent;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getPromo()
    {
        return $this->promo;
    }

    /**
     * @param mixed $promo
     * @return Method
     */
    public function setPromo($promo)
    {
        $this->promo = $promo;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function addSubject(Subject $item)
    {
        if ($this->subject->contains($item)) {
            return;
        }
        $this->subject->add($item);
        $item->addMethod($this);
    }

    public function removeSubject(Subject $item)
    {
        if (!$this->subject->contains($item)) {
            return;
        }
        $this->subject->removeElement($item);
        $item->removeMethod($this);
    }

    public function addTentorSubject(TentorSubject $item)
    {
        if ($this->tentorSubject->contains($item)) {
            return;
        }
        $this->tentorSubject->add($item);
        $item->addMethod($this);
    }

    public function removeTentorSubject(TentorSubject $item)
    {
        if (!$this->tentorSubject->contains($item)) {
            return;
        }
        $this->tentorSubject->removeElement($item);
        $item->removeMethod($this);
    }

    /**
     * @param Categories $item
     */
    public function addCategory(Categories $item)
    {
        if ($this->category->contains($item)) {
            return;
        }
        $this->category->add($item);
        $item->addMethod($this);
    }

    /**
     * @param Categories $item
     */
    public function removeCategory(Categories $item)
    {
        if (!$this->category->contains($item)) {
            return;
        }
        $this->category->removeElement($item);
        $item->removeMethod($this);
    }

    /**
     * @return mixed
     */
    public function getTentorSubject()
    {
        return $this->tentorSubject;
    }

    /**
     * @param mixed $tentorSubject
     * @return Method
     */
    public function setTentorSubject($tentorSubject)
    {
        $this->tentorSubject = $tentorSubject;
        return $this;
    }

}

<?php

namespace App\Entity;

use App\Repository\PaymentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * @ORM\Entity(repositoryClass=PaymentRepository::class)
 */
class Payment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="payment")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $user;


    /**
     * @ORM\Column(type="string")
     *
     */
    private $paymentMethod;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $paymentReference;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BankAccount", inversedBy="payment")
     */
    private $bank;


    /**
     * @ORM\Column(type="integer", nullable=true, options={"default"=0})
     */
    private $manualCode;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default"=0})
     */
    private $amount;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expiredPay;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $rekening;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $holderName;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $confirmedAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Cart", inversedBy="payment")
     * @ORM\JoinTable(name="payment_to_cart", joinColumns={@ORM\JoinColumn(name="payment_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="cart_id", referencedColumnName="id")})
     */
    private $cart;

    /**
     * Payment constructor.
     */
    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
        $this->cart = new ArrayCollection();

    }


    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @param mixed $paymentMethod
     *
     * @return Payment
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param mixed $paymentReference
     *
     * @return Payment
     */
    public function setPaymentReference($paymentReference)
    {
        $this->paymentReference = $paymentReference;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentReference()
    {
        return $this->paymentReference;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return Payment
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $updatedAt
     *
     * @return Payment
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $status
     *
     * @return Payment
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $bank
     *
     * @return Payment
     */
    public function setBank($bank)
    {
        $this->bank = $bank;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @return mixed
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param mixed $cart
     * @return Payment
     */
    public function setCart($cart)
    {
        $this->cart = $cart;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Payment
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getManualCode()
    {
        return $this->manualCode;
    }

    /**
     * @param mixed $manualCode
     * @return Payment
     */
    public function setManualCode($manualCode)
    {
        $this->manualCode = $manualCode;
        return $this;
    }


    public function addCart(Cart $item)
    {
        if ($this->cart->contains($item)) {
            return;
        }
        $this->cart->add($item);
        $item->addPayment($this);
    }

    public function removeCart(Cart $item)
    {
        if (!$this->cart->contains($item)) {
            return;
        }
        $this->cart->removeElement($item);
        $item->removePayment($this);
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     * @return Payment
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    public function getTotalCart()
    {
        $cart = $this->getCart();
        $summary = 0;
        foreach ($cart as $v) {
            $summary += ($v->getTotal() - $v->getDiscount());
        }
        return $summary;
    }

    /**
     * @return mixed
     */
    public function getExpiredPay()
    {
        return $this->expiredPay;
    }

    /**
     * @param mixed $expiredPay
     * @return Payment
     */
    public function setExpiredPay($expiredPay)
    {
        $this->expiredPay = $expiredPay;
        return $this;
    }

    public function getGeneratedExpiredPay()
    {
        setlocale(LC_ALL, 'IND');
        date_default_timezone_set("Asia/Jakarta");
        $start = $this->getExpiredPay()->format('Y-m-d H:i:s');
        return strftime('%A, %d %B %Y %H:%I:%S', strtotime($start));
    }

    /**
     * @return mixed
     */
    public function getRekening()
    {
        return $this->rekening;
    }

    /**
     * @param mixed $rekening
     * @return Payment
     */
    public function setRekening($rekening)
    {
        $this->rekening = $rekening;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHolderName()
    {
        return $this->holderName;
    }

    /**
     * @param mixed $holderName
     * @return Payment
     */
    public function setHolderName($holderName)
    {
        $this->holderName = $holderName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getConfirmedAt()
    {
        return $this->confirmedAt;
    }

    /**
     * @param mixed $confirmedAt
     * @return Payment
     */
    public function setConfirmedAt($confirmedAt)
    {
        $this->confirmedAt = $confirmedAt;
        return $this;
    }

    public function getGeneratedCreatedAt()
    {
        setlocale(LC_ALL, 'IND');
        date_default_timezone_set("Asia/Jakarta");
        $start = $this->getCreatedAt()->format('Y-m-d H:i:s');
        return strftime('%d %B %Y', strtotime($start));
    }
}

<?php

namespace App\Entity;

use App\Repository\CartRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=CartRepository::class)
 */
class Cart
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Method", inversedBy="cart")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $method;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $duration;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $encounter;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $attendees;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $firstMeet;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private $note;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $amount;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $total;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $discount;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $referenceId;


    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Transaction", inversedBy="cart")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $transaction;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subscription", inversedBy="cart")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $subscription;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expiredPay;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\District", inversedBy="cart")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $district;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private $address;


    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isFinish;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Attendance", mappedBy="cart")
     */
    private $attendance;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Payment", mappedBy="cart")
     */
    private $payment;

    /**
     * Cart constructor.
     */
    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
        $this->payment = new ArrayCollection();
        $this->isActive = false;
        $this->isFinish = false;
    }

    /**
     * @param mixed $id
     * @return Cart
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     * @return Cart
     */
    public function setTotal($total)
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     * @return Cart
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReferenceId()
    {
        return $this->referenceId;
    }

    /**
     * @param mixed $referenceId
     * @return Cart
     */
    public function setReferenceId($referenceId)
    {
        $this->referenceId = $referenceId;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     * @return Cart
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     * @return Cart
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEncounter()
    {
        return $this->encounter;
    }

    /**
     * @param mixed $encounter
     * @return Cart
     */
    public function setEncounter($encounter)
    {
        $this->encounter = $encounter;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttendees()
    {
        return $this->attendees;
    }

    /**
     * @param mixed $attendees
     * @return Cart
     */
    public function setAttendees($attendees)
    {
        $this->attendees = $attendees;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFirstMeet(): \DateTime
    {
        return $this->firstMeet;
    }

    /**
     * @param \DateTime $firstMeet
     * @return Cart
     */
    public function setFirstMeet(\DateTime $firstMeet): Cart
    {
        $this->firstMeet = $firstMeet;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     * @return Cart
     */
    public function setNote($note)
    {
        $this->note = $note;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     * @return Cart
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Cart
     */
    public function setCreatedAt(\DateTime $createdAt): Cart
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return Cart
     */
    public function setUpdatedAt(\DateTime $updatedAt): Cart
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param mixed $transaction
     * @return Cart
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExpiredPay()
    {
        return $this->expiredPay;
    }

    /**
     * @param mixed $expiredPay
     * @return Cart
     */
    public function setExpiredPay($expiredPay)
    {
        $this->expiredPay = $expiredPay;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param mixed $district
     * @return Cart
     */
    public function setDistrict($district)
    {
        $this->district = $district;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return Cart
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttendance()
    {
        return $this->attendance;
    }

    /**
     * @param mixed $attendance
     * @return Cart
     */
    public function setAttendance($attendance)
    {
        $this->attendance = $attendance;
        return $this;
    }

    public function getRest()
    {
        $encounter = $this->getEncounter();
        $presence = count($this->getAttendance());
        return $encounter - $presence;
    }

    public function getGeneratedFirstMeet()
    {
        setlocale(LC_ALL, 'IND');
        $strTime = $this->firstMeet->format('Y-m-d H:i:s');
        return strftime('%A, %d %B %Y %H', strtotime($strTime)) . ':00 WIB';
    }

    public function getEstimatedIncome()
    {
        $amount = $this->getAmount();
        return number_format((0.85 * $amount), 0, ',', '.');
    }

    /**
     * @return mixed
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param mixed $payment
     * @return Cart
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;
        return $this;
    }

    public function addPayment(Payment $item)
    {
        if ($this->payment->contains($item)) {
            return;
        }
        $this->payment->add($item);
        $item->addCart($this);
    }

    public function removePayment(Payment $item)
    {
        if (!$this->payment->contains($item)) {
            return;
        }
        $this->payment->removeElement($item);
        $item->removeCart($this);
    }

    public function getRestAttendance()
    {
        $currentAttendance = count($this->getAttendance());
        $encounter = $this->getEncounter();
        return $encounter - $currentAttendance;
    }

    public function getIsExpired()
    {
        if ($this->getTransaction() === null && $this->getExpiredPay() === null ) {
            return false;
        }elseif ($this->getTransaction() === null && $this->getExpiredPay() < new \DateTime()){
            return true;
        }
        return false;
    }

    public function getLastPaymentId()
    {
        if (count($this->getPayment()) > 0) {
            return $this->getPayment()->last();
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param mixed $subscription
     * @return Cart
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     * @return Cart
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsFinish()
    {
        return $this->isFinish;
    }

    /**
     * @param mixed $isFinish
     * @return Cart
     */
    public function setIsFinish($isFinish)
    {
        $this->isFinish = $isFinish;
        return $this;
    }


}

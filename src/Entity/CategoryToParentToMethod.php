<?php

namespace App\Entity;

use App\Repository\CategoryToParentToMethodRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoryToParentToMethodRepository::class)
 */
class CategoryToParentToMethod
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categories", inversedBy="CategoryToMethodParent")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Method", inversedBy="CategoryToMethodParent")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $method;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CategoriesParent", inversedBy="CategoryToMethodToParent")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $parentCategory;

    /**
     * @param mixed $category
     *
     * @return CategoryToParentToMethod
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $method
     *
     * @return CategoryToParentToMethod
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed|null $parentCategory
     *
     * @return CategoryToParentToMethod
     */
    public function setParentCategory($parentCategory)
    {
        $this->parentCategory = $parentCategory;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getParentCategory()
    {
        return $this->parentCategory;
    }
}

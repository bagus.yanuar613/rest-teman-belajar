<?php

namespace App\Entity;

use App\Repository\SubCityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SubCityRepository::class)
 */
class SubCity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Province", inversedBy="subCity")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $province;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City", inversedBy="subCity")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $city;

    /**
     *@ORM\Column(type="string")
     *
     */
    private $name;

    /**
     *@ORM\Column(type="string")
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\District", mappedBy="subCity")
     */
    private $district;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * @param mixed $province
     * @return SubCity
     */
    public function setProvince($province)
    {
        $this->province = $province;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return SubCity
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return SubCity
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param mixed $district
     * @return SubCity
     */
    public function setDistrict($district)
    {
        $this->district = $district;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return SubCity
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

}

<?php


namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TryOutRepository")
 */
class TryOut
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $banner;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isFree;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TryOutSubject", mappedBy="tryout")
     */
    private $subject;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $codeAccess;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $estimatedTime;

    /**
     *
     *
     *
     * @ORM\Column(type="boolean")
     */
    private $isIRT;

    /**
     *
     *
     *
     * @ORM\Column(type="boolean")
     */
    private $irtChecked;

    /**
     * @var \DateTime
     *
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startDay;

    /**
     * @var \DateTime
     *
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $finalDay;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Level", inversedBy="tryOut")
     */
    private $level;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categories")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TryOutRegistrant", mappedBy="tryOut")
     */
    private $registrant;

    /**
     * @ORM\Column(type="text")
     */
    private $rules;
    /**
     * @ORM\Column(type="text")
     */
    private $direction;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isStrict;


    /**
     * @ORM\Column(type="integer")
     */
    private $restTime;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TryOutPricing", mappedBy="tryOut")
     */
    private $pricing;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\TryOutReward", mappedBy="tryOut")
     */
    private $reward;


    public function __construct()
    {
        $this->irtChecked = false;

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return TryOut
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return TryOut
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBanner()
    {
        return $this->banner;
    }

    /**
     * @param mixed $banner
     * @return TryOut
     */
    public function setBanner($banner)
    {
        $this->banner = $banner;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsFree()
    {
        return $this->isFree;
    }

    /**
     * @param mixed $isFree
     * @return TryOut
     */
    public function setIsFree($isFree)
    {
        $this->isFree = $isFree;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeAccess()
    {
        return $this->codeAccess;
    }

    /**
     * @param mixed $codeAccess
     * @return TryOut
     */
    public function setCodeAccess($codeAccess)
    {
        $this->codeAccess = $codeAccess;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstimatedTime()
    {
        return $this->estimatedTime;
    }

    /**
     * @param mixed $estimatedTime
     * @return TryOut
     */
    public function setEstimatedTime($estimatedTime)
    {
        $this->estimatedTime = $estimatedTime;
        return $this;
    }


    public function getStartDay()
    {
        return $this->startDay ? $this->startDay->format("Y-m-d") : $this->startDay;
    }

    /**
     * @param  $startDay
     * @return TryOut
     */
    public function setStartDay($startDay): TryOut
    {

        $this->startDay = empty($startDay) ?  NULL : $startDay instanceof DateTime ? $startDay : new DateTime($startDay);
        return $this;
    }


    public function getFinalDay()
    {
        return $this->finalDay ? $this->finalDay->format("Y-m-d") : $this->finalDay;
    }

    /**
     * @param  $finalDay
     * @return TryOut
     */
    public function setFinalDay($finalDay): TryOut
    {
        $this->finalDay =empty($finalDay) ?  NULL :  $finalDay instanceof DateTime ? $finalDay : new DateTime($finalDay);
        $now = new DateTime();
        if($now > $this->finalDay){
            $this->startDay = NULL;
            $this->finalDay = NULL;
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     * @return TryOut
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive ?? false;
        return $this;
    }


    public function getCreatedAt()
    {
        return $this->createdAt ? $this->createdAt->format('Y-m-d H:i:s') : '';
    }

    /**
     * @param \DateTime $createdAt
     * @return TryOut
     */
    public function setCreatedAt(\DateTime $createdAt): TryOut
    {
        $this->createdAt = $createdAt;
        return $this;
    }


    public function getUpdatedAt()
    {
        return $this->updatedAt ? $this->updatedAt->format('Y-m-d H:i:s') : '';
    }

    /**
     * @param \DateTime $updatedAt
     * @return TryOut
     */
    public function setUpdatedAt(\DateTime $updatedAt): TryOut
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     * @return TryOut
     */
    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return TryOut
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * @param mixed $rules
     * @return TryOut
     */
    public function setRules($rules)
    {
        $this->rules = $rules;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param mixed $direction
     * @return TryOut
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return TryOut
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return TryOut
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsStrict()
    {
        return $this->isStrict;
    }

    /**
     * @param mixed $isStrict
     * @return TryOut
     */
    public function setIsStrict($isStrict)
    {
        $this->isStrict = $isStrict;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRestTime()
    {
        return $this->restTime;
    }

    /**
     * @param mixed $restTime
     * @return TryOut
     */
    public function setRestTime($restTime)
    {
        $this->restTime = $restTime;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegistrant()
    {
        return $this->registrant;
    }

    /**
     * @param mixed $registrant
     * @return TryOut
     */
    public function setRegistrant($registrant)
    {
        $this->registrant = $registrant;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPricing()
    {
        return $this->pricing;
    }

    /**
     * @param mixed $pricing
     * @return TryOut
     */
    public function setPricing($pricing)
    {
        $this->pricing = $pricing;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReward()
    {
        return $this->reward;
    }

    /**
     * @param mixed $reward
     * @return TryOut
     */
    public function setReward($reward)
    {
        $this->reward = $reward;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     * @return TryOut
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsIRT()
    {
        return $this->isIRT;
    }

    /**
     * @param mixed $isIRT
     * @return TryOut
     */
    public function setIsIRT($isIRT)
    {
        $this->isIRT = $isIRT;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIrtChecked()
    {
        return $this->irtChecked;
    }

    /**
     * @param mixed $irtChecked
     * @return TryOut
     */
    public function setIrtChecked($irtChecked)
    {
        $this->irtChecked = $irtChecked;
        return $this;
    }


}
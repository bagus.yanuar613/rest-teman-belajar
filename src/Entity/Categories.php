<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\CategoriesRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * Categories
 *
 * @ORM\Entity(repositoryClass="App\Repository\CategoriesRepository")
 */
class Categories
{
    protected $request;

    /**
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;

    /**
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $slug;

    /**
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $icon;

    /**
     *
     * @ORM\Column(type="boolean", nullable=true, options={"default"=false})
     */
    private $isDefault;

    /**
     *
     * @ORM\Column(type="boolean", nullable=true, options={"default"=false})
     */
    private $isReference;

    /**
     *
     * @ORM\Column(type="boolean", nullable=true, options={"default"=false})
     */
    private $isPopular;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default"=0})
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Subject", mappedBy="category")
     */
    private $subject;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TryOutSubscription", mappedBy="tryOut")
     */
    private $tryOutPricing;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TentorSubject", mappedBy="category")
     */
    private $tentorSubject;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProductCategory", inversedBy="tryOut")
     */
    private $product;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isTryOut;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isProduct;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Method", inversedBy="category")
     * @ORM\JoinTable(name="category_to_method", joinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="method_id", referencedColumnName="id")})
     */
    private $method;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Level", inversedBy="category")
     * @ORM\JoinTable(name="category_to_level", joinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="level_id", referencedColumnName="id")})
     */
    private $level;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\CategoriesParent", mappedBy="category")
     */
    private $parentCategory;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CategoryToParentToLevel", mappedBy="category")
     */
    private $CategoryTolevelToParent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CategoryToParentToMethod", mappedBy="category")
     */
    private $CategoryToMethodParent;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\MemberClass", mappedBy="category")
     */
    private $memberClass;

    /**
     * Categories constructor.
     */
    public function __construct()
    {
        $this->request = Request::createFromGlobals();
        $this->isTryOut = false;
        $this->isProduct = false;
        $this->method = new ArrayCollection();
        $this->level = new ArrayCollection();
        $this->parentCategory = new ArrayCollection();
        $this->memberClass = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Categories
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Categories
     */
    public function setSlug(string $slug): Categories
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     * @return Categories
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * @param mixed $isDefault
     * @return Categories
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $icon
     * @return Categories
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsReference()
    {
        return $this->isReference;
    }

    /**
     * @param mixed $isReference
     * @return Categories
     */
    public function setIsReference($isReference)
    {
        $this->isReference = $isReference;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return Categories
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }


    public function testReq()
    {
        return $this->request->query->get('coba');
    }

    /**
     * @return mixed
     */
    public function getIsPopular()
    {
        return $this->isPopular;
    }

    /**
     * @param mixed $isPopular
     * @return Categories
     */
    public function setIsPopular($isPopular)
    {
        $this->isPopular = $isPopular;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getTentorSubject()
    {
        return $this->tentorSubject;
    }

    /**
     * @param mixed $tentorSubject
     * @return Categories
     */
    public function setTentorSubject($tentorSubject)
    {
        $this->tentorSubject = $tentorSubject;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTryOut(): bool
    {
        return $this->isTryOut;
    }

    /**
     * @param bool $isTryOut
     * @return Categories
     */
    public function setIsTryOut(bool $isTryOut): Categories
    {
        $this->isTryOut = $isTryOut;
        return $this;
    }


    public function getRequest(): ?Request
    {
        return $this->request;
    }

    /**
     * @param Request $request
     * @return Categories
     */
    public function setRequest(Request $request): Categories
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @return bool
     */
    public function isProduct(): bool
    {
        return $this->isProduct;
    }

    /**
     * @param bool $isProduct
     * @return Categories
     */
    public function setIsProduct(bool $isProduct): Categories
    {
        $this->isProduct = $isProduct;
        return $this;
    }

    /**
     * @param mixed $method
     *
     * @return Categories
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $level
     *
     * @return Categories
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $parentCategory
     *
     * @return Categories
     */
    public function setParentCategory($parentCategory)
    {
        $this->parentCategory = $parentCategory;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getParentCategory()
    {
        return $this->parentCategory;
    }

    /**
     * @param Method $param
     */
    public function addMethod(Method $param)
    {
        if ($this->method->contains($param)) {
            return;
        }
        $this->method->add($param);
        $param->addCategory($this);
    }

    /**
     * @param Method $param
     */
    public function removeMethod(Method $param)
    {
        if (!$this->method->contains($param)) {
            return;
        }
        $this->method->removeElement($param);
        $param->removeCategory($this);
    }

    /**
     * @param Level $param
     */
    public function addLevel(Level $param)
    {
        if ($this->level->contains($param)) {
            return;
        }
        $this->level->add($param);
        $param->addCategory($this);
    }

    /**
     * @param Level $param
     */
    public function removeLevel(Level $param)
    {
        if (!$this->level->contains($param)) {
            return;
        }
        $this->level->removeElement($param);
        $param->removeCategory($this);
    }

    /**
     * @param CategoriesParent $param
     */
    public function addCategoryParent(CategoriesParent $param)
    {
        if ($this->parentCategory->contains($param)) {
            return;
        }
        $this->parentCategory->add($param);
        $param->addCategory($this);
    }

    /**
     * @param CategoriesParent $param
     */
    public function removeCategoryParent(CategoriesParent $param)
    {
        if (!$this->parentCategory->contains($param)) {
            return;
        }
        $this->parentCategory->removeElement($param);
        $param->removeCategory($this);
    }

    /**
     * @param mixed $CategoryTolevelToParent
     *
     * @return Categories
     */
    public function setCategoryTolevelToParent($CategoryTolevelToParent)
    {
        $this->CategoryTolevelToParent = $CategoryTolevelToParent;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoryTolevelToParent()
    {
        return $this->CategoryTolevelToParent;
    }


    /**
     * @param mixed $CategoryToMethodParent
     *
     * @return Categories
     */
    public function setCategoryToMethodParent($CategoryToMethodParent)
    {
        $this->CategoryToMethodParent = $CategoryToMethodParent;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoryToMethodParent()
    {
        return $this->CategoryToMethodParent;
    }

    /**
     * @return mixed
     */
    public function getMemberClass()
    {
        return $this->memberClass;
    }

    /**
     * @param mixed $memberClass
     * @return Categories
     */
    public function setMemberClass($memberClass)
    {
        $this->memberClass = $memberClass;
        return $this;
    }


    public function addMemberClass(MemberClass $param)
    {
        if ($this->memberClass->contains($param)) {
            return;
        }
        $this->memberClass->add($param);
        $param->addCategory($this);
    }


    public function removeMemberClass(MemberClass $param)
    {
        if (!$this->memberClass->contains($param)) {
            return;
        }
        $this->memberClass->removeElement($param);
        $param->removeCategory($this);
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     * @return Categories
     */
    public function setProduct($product)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTryOutPricing()
    {
        return $this->tryOutPricing;
    }

    /**
     * @param mixed $tryOutPricing
     * @return Categories
     */
    public function setTryOutPricing($tryOutPricing)
    {
        $this->tryOutPricing = $tryOutPricing;
        return $this;
    }




}

<?php

namespace App\Entity;

use App\Repository\TentorIdentityRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=TentorIdentityRepository::class)
 */
class TentorIdentity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $nik;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $certificate;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="tentorIdentity")
     */
    private $user;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $bank;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $rekening;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $holderName;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * TentorIdentity constructor.
     */
    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNik()
    {
        return $this->nik;
    }

    /**
     * @param mixed $nik
     *
     * @return TentorIdentity
     */
    public function setNik($nik)
    {
        $this->nik = $nik;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCertificate()
    {
        return $this->certificate;
    }

    /**
     * @param mixed $certificate
     *
     * @return TentorIdentity
     */
    public function setCertificate($certificate)
    {
        $this->certificate = $certificate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     *
     * @return TentorIdentity
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return TentorIdentity
     */
    public function setCreatedAt(\DateTime $createdAt): TentorIdentity
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     *
     * @return TentorIdentity
     */
    public function setUpdatedAt(\DateTime $updatedAt): TentorIdentity
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @param mixed $bank
     *
     * @return TentorIdentity
     */
    public function setBank($bank)
    {
        $this->bank = $bank;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @param mixed $rekening
     *
     * @return TentorIdentity
     */
    public function setRekening($rekening)
    {
        $this->rekening = $rekening;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRekening()
    {
        return $this->rekening;
    }

    /**
     * @param mixed $holderName
     *
     * @return TentorIdentity
     */
    public function setHolderName($holderName)
    {
        $this->holderName = $holderName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHolderName()
    {
        return $this->holderName;
    }

    public function getGeneratedCertificate()
    {
        if ($this->certificate !== null) {
            if (str_contains($this->certificate, 'https://')) {
                return $this->certificate;
            }
            $server = 'http';
            if (isset($_SERVER['HTTPS'])) {
                $server = $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http';
            }
            $sName = $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'];
            return $server . '://' . $sName  . $this->certificate;
        }
        return $this->certificate;
    }

}

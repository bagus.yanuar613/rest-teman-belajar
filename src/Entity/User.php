<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * User
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *
     * @ORM\Column(type="string", unique=true)
     */
    private $email;

    /**
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $password;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];
    /**
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $apiToken;

    /**
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $webFcmToken;

    /**
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $appFcmToken;

    /**
     *
     * @ORM\Column(type="boolean", nullable=true, options={"default"=false})
     */
    private $isEmailVerified;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $provider;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Admin", mappedBy="user")
     */
    private $adminProfile;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Member", mappedBy="user")
     * @SerializedName("profile")
     */
    private $memberProfile;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Tentor", mappedBy="user")
     * @SerializedName("profile")
     */
    private $tentorProfile;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TentorSubject", mappedBy="user", fetch="EXTRA_LAZY")
     */
    private $tentorSubject;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Schedule", mappedBy="user")
     */
    private $schedule;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\District", inversedBy="user")
     * @ORM\JoinTable(name="user_to_district", joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="district_id", referencedColumnName="id")})
     */
    private $district;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Rating", mappedBy="tentor")
     */
    private $ratings;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\TentorIdentity", mappedBy="user")
     */
    private $tentorIdentity;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction", mappedBy="user")
     */
    private $transaction;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Subscription", mappedBy="user")
     */
    private $subscription;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Payment", mappedBy="user")
     */
    private $payment;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TentorSaldo", mappedBy="user")
     */
    private $tentorSaldo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Withdraw", mappedBy="user")
     */
    private $withdraw;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->district = new ArrayCollection();
        $this->schedule = new ArrayCollection();
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        // TODO: Implement getRoles() method.
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        // TODO: Implement getPassword() method.
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        // TODO: Implement getUsername() method.
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }


    public function setEmail($email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt->format('Y-m-d H:i:s');
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt->format('Y-m-d H:i:s');
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMemberProfile()
    {
        return $this->memberProfile;
    }

    /**
     * @param mixed $memberProfile
     *
     * @return User
     */
    public function setMemberProfile($memberProfile)
    {
        $this->memberProfile = $memberProfile;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTentorProfile()
    {
        return $this->tentorProfile;
    }

    /**
     * @param mixed $tentorProfile
     *
     * @return User
     */
    public function setTentorProfile($tentorProfile)
    {
        $this->tentorProfile = $tentorProfile;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTentorSubject()
    {
        return $this->tentorSubject;
    }

    /**
     * @param mixed $tentorSubject
     *
     * @return User
     */
    public function setTentorSubject($tentorSubject)
    {
        $this->tentorSubject = $tentorSubject;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * @param mixed $schedule
     *
     * @return User
     */
    public function setSchedule($schedule)
    {
        $this->schedule = $schedule;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param mixed $district
     *
     * @return User
     */
    public function setDistrict($district)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRatings()
    {
        return $this->ratings;
    }

    /**
     * @param mixed $ratings
     *
     * @return User
     */
    public function setRatings($ratings)
    {
        $this->ratings = $ratings;

        return $this;
    }

    public function getAvgRatings()
    {
        $count = count($this->getRatings());
        $total = 0;
        $avg   = 0;
        if ($count > 0) {
            foreach ($this->getRatings() as $data) {
                $total += $data->getRating();
            }
            $avg = $total / $count;
        }

        return $avg;
    }

    /**
     * @return mixed
     */
    public function getTentorIdentity()
    {
        return $this->tentorIdentity;
    }

    /**
     * @param mixed $tentorIdentity
     *
     * @return User
     */
    public function setTentorIdentity($tentorIdentity)
    {
        $this->tentorIdentity = $tentorIdentity;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getApiToken()
    {
        return $this->apiToken;
    }

    /**
     * @param mixed $apiToken
     *
     * @return User
     */
    public function setApiToken($apiToken)
    {
        $this->apiToken = $apiToken;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWebFcmToken()
    {
        return $this->webFcmToken;
    }

    /**
     * @param mixed $webFcmToken
     *
     * @return User
     */
    public function setWebFcmToken($webFcmToken)
    {
        $this->webFcmToken = $webFcmToken;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAppFcmToken()
    {
        return $this->appFcmToken;
    }

    /**
     * @param mixed $appFcmToken
     *
     * @return User
     */
    public function setAppFcmToken($appFcmToken)
    {
        $this->appFcmToken = $appFcmToken;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdminProfile()
    {
        return $this->adminProfile;
    }

    /**
     * @param mixed $adminProfile
     *
     * @return User
     */
    public function setAdminProfile($adminProfile)
    {
        $this->adminProfile = $adminProfile;
        return $this;
    }

    public function addDistrict(District $item)
    {
        if ($this->district->contains($item)) {
            return;
        }
        $this->district->add($item);
        $item->addUser($this);
    }

    public function removeDistrict(District $item)
    {
        if (!$this->district->contains($item)) {
            return;
        }
        $this->district->removeElement($item);
        $item->removeUser($this);
    }

    public function addSchedule(Schedule $item)
    {
        if ($this->schedule->contains($item)) {
            return;
        }
        $this->schedule->add($item);
        $item->addUser($this);
    }

    public function removeSchedule(Schedule $item)
    {
        if (!$this->schedule->contains($item)) {
            return;
        }
        $this->schedule->removeElement($item);
        $item->removeUser($this);
    }

    /**
     * @return mixed
     */
    public function getIsEmailVerified()
    {
        return $this->isEmailVerified;
    }

    /**
     * @param mixed $isEmailVerified
     * @return User
     */
    public function setIsEmailVerified($isEmailVerified)
    {
        $this->isEmailVerified = $isEmailVerified;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param mixed $transaction
     * @return User
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param mixed $payment
     * @return User
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTentorSaldo()
    {
        return $this->tentorSaldo;
    }

    /**
     * @param mixed $tentorSaldo
     * @return User
     */
    public function setTentorSaldo($tentorSaldo)
    {
        $this->tentorSaldo = $tentorSaldo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWithdraw()
    {
        return $this->withdraw;
    }

    /**
     * @param mixed $withdraw
     * @return User
     */
    public function setWithdraw($withdraw)
    {
        $this->withdraw = $withdraw;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param mixed $provider
     * @return User
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param mixed $subscription
     * @return User
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;
        return $this;
    }

}

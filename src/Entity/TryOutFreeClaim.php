<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=TryOutFreeClaimRepository::class)
 */
class TryOutFreeClaim
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\TryOutFreeAccess")
     */
    private $freeAccess;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TryOutRegistrant")
     */
    private $registrant;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TryOut")
     */
    private $tryOut;

    /**
     * @ORM\Column(type="string")
     */
    private $codeAccess;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return TryOutFreeClaim
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFreeAccess()
    {
        return $this->freeAccess;
    }

    /**
     * @param mixed $freeAccess
     * @return TryOutFreeClaim
     */
    public function setFreeAccess($freeAccess)
    {
        $this->freeAccess = $freeAccess;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegistrant()
    {
        return $this->registrant;
    }

    /**
     * @param mixed $registrant
     * @return TryOutFreeClaim
     */
    public function setRegistrant($registrant)
    {
        $this->registrant = $registrant;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTryOut()
    {
        return $this->tryOut;
    }

    /**
     * @param mixed $tryOut
     * @return TryOutFreeClaim
     */
    public function setTryOut($tryOut)
    {
        $this->tryOut = $tryOut;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodeAccess()
    {
        return $this->codeAccess;
    }

    /**
     * @param mixed $codeAccess
     * @return TryOutFreeClaim
     */
    public function setCodeAccess($codeAccess)
    {
        $this->codeAccess = $codeAccess;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return TryOutFreeClaim
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return TryOutFreeClaim
     */
    public function setCreatedAt(\DateTime $createdAt): TryOutFreeClaim
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return TryOutFreeClaim
     */
    public function setUpdatedAt(\DateTime $updatedAt): TryOutFreeClaim
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

}
<?php

namespace App\Entity;

use App\Repository\AttendanceReportRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AttendanceReportRepository::class)
 */
class AttendanceReport
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Attendance", inversedBy="report")
     */
    private $attendance;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $finishAt;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $duration;

    /**
     * AttendanceReport constructor.
     */
    public function __construct()
    {
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAttendance()
    {
        return $this->attendance;
    }

    /**
     * @param mixed $attendance
     * @return AttendanceReport
     */
    public function setAttendance($attendance)
    {
        $this->attendance = $attendance;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartAt(): \DateTime
    {
        return $this->startAt;
    }

    /**
     * @param \DateTime $startAt
     * @return AttendanceReport
     */
    public function setStartAt(\DateTime $startAt): AttendanceReport
    {
        $this->startAt = $startAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFinishAt(): \DateTime
    {
        return $this->finishAt;
    }

    /**
     * @param \DateTime $finishAt
     * @return AttendanceReport
     */
    public function setFinishAt(\DateTime $finishAt): AttendanceReport
    {
        $this->finishAt = $finishAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     * @return AttendanceReport
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

}

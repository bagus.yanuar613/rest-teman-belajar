<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 *
 * @ORM\Entity(repositoryClass="App\Repository\MemberRepository")
 */
class Member
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $fullName;

    /**
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $parentName;

    /**
     *
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $phone;

    /**
     *
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $parentPhone;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateOfBirth;

    /**
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $school;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MemberClass", inversedBy="memberProfile")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $class;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\District", inversedBy="memberProfile")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $district;
    /**
     *
     * @ORM\Column(name="address", type="text", length=65535, nullable=true)
     */
    private $address;

    /**
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isActive;

    /**
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $deviceID;

    /**
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $deviceModel;
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $avatar;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="memberProfile")
     */
    private $user;

    /**
     *
     * @ORM\Column(type="boolean", nullable=true, options={"default"=true})
     */
    private $gender;

    /**
     *
     * @ORM\Column(type="integer", nullable=true, options={"default"=0})
     */
    private $medal;

    /**
     *
     * @ORM\Column(type="integer", nullable=true, options={"default"=0})
     */
    private $coin;

    /**
     * Member constructor.
     */
    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     * @return Member
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return Member
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * @param mixed $school
     * @return Member
     */
    public function setSchool($school)
    {
        $this->school = $school;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param mixed $class
     * @return Member
     */
    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return Member
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     * @return Member
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Member
     */
    public function setCreatedAt(\DateTime $createdAt): Member
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return Member
     */
    public function setUpdatedAt(\DateTime $updatedAt): Member
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param mixed $avatar
     * @return Member
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Member
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeviceID()
    {
        return $this->deviceID;
    }

    /**
     * @param mixed $deviceID
     * @return Member
     */
    public function setDeviceID($deviceID)
    {
        $this->deviceID = $deviceID;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeviceModel()
    {
        return $this->deviceModel;
    }

    /**
     * @param mixed $deviceModel
     * @return Member
     */
    public function setDeviceModel($deviceModel)
    {
        $this->deviceModel = $deviceModel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParentName()
    {
        return $this->parentName;
    }

    /**
     * @param mixed $parentName
     * @return Member
     */
    public function setParentName($parentName)
    {
        $this->parentName = $parentName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParentPhone()
    {
        return $this->parentPhone;
    }

    /**
     * @param mixed $parentPhone
     * @return Member
     */
    public function setParentPhone($parentPhone)
    {
        $this->parentPhone = $parentPhone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param mixed $district
     * @return Member
     */
    public function setDistrict($district)
    {
        $this->district = $district;
        return $this;
    }

    /**
     * @param $gender
     *
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @return mixed
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * @param mixed $dateOfBirth
     * @return Member
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
        return $this;
    }

    public function getGeneratedAvatar()
    {
        if ($this->getAvatar() !== null) {
            if (str_contains($this->getAvatar(), 'https://')) {
                return $this->getAvatar();
            } else {
                $server = 'http';
                if (isset($_SERVER['HTTPS'])) {
                    $server = $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http';
                }
                $sName = $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'];
                return $server . '://' . $sName . '/images/img-account/' . $this->getAvatar();
            }
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getMedal()
    {
        return $this->medal;
    }

    /**
     * @param mixed $medal
     * @return Member
     */
    public function setMedal($medal)
    {
        $this->medal = $medal;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoin()
    {
        return $this->coin;
    }

    /**
     * @param mixed $coin
     * @return Member
     */
    public function setCoin($coin)
    {
        $this->coin = $coin;
        return $this;
    }

    public function getMyQrCode()
    {
        return bin2hex($this->getUser()->getId());
    }
}

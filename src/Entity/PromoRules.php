<?php

namespace App\Entity;

use App\Repository\PromoRulesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PromoRulesRepository::class)
 */
class PromoRules
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PromoToRules", mappedBy="promoRules")
     */
    private $promoToRules;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return PromoRules
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPromoToRules()
    {
        return $this->promoToRules;
    }

    /**
     * @param mixed $promoToRules
     * @return PromoRules
     */
    public function setPromoToRules($promoToRules)
    {
        $this->promoToRules = $promoToRules;
        return $this;
    }

}

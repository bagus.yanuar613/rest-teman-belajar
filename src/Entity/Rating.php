<?php

namespace App\Entity;

use App\Repository\RatingRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=RatingRepository::class)
 */
class Rating
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="ratings")
     */
    private $tentor;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $user;

    /**
     * @ORM\Column(type="float")
     */
    private $rating;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $review;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default"=false})
     */
    private $isAnonymous;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Transaction", inversedBy="rating")
     */
    private $transaction;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Subscription", inversedBy="rating")
     */
    private $subscription;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * Rating constructor.
     */
    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTentor()
    {
        return $this->tentor;
    }

    /**
     * @param mixed $tentor
     * @return Rating
     */
    public function setTentor($tentor)
    {
        $this->tentor = $tentor;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Rating
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     * @return Rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * @param mixed $review
     * @return Rating
     */
    public function setReview($review)
    {
        $this->review = $review;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param mixed $transaction
     * @return Rating
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsAnonymous()
    {
        return $this->isAnonymous;
    }

    /**
     * @param mixed $isAnonymous
     * @return Rating
     */
    public function setIsAnonymous($isAnonymous)
    {
        $this->isAnonymous = $isAnonymous;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Rating
     */
    public function setCreatedAt(\DateTime $createdAt): Rating
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return Rating
     */
    public function setUpdatedAt(\DateTime $updatedAt): Rating
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function getGeneratedCreatedAt()
    {
        setlocale(LC_ALL, 'IND');
        date_default_timezone_set("Asia/Jakarta");
        $created = $this->getCreatedAt()->format('Y-m-d H:i:s');
        return strftime('%d %B %Y', strtotime($created));
    }

    /**
     * @return mixed
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param mixed $subscription
     * @return Rating
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;
        return $this;
    }

}

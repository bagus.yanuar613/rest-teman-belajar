<?php

namespace App\Entity;

use App\Repository\CategoryToParentToLevelRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoryToParentToLevelRepository::class)
 */
class CategoryToParentToLevel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categories", inversedBy="CategoryTolevelToParent")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Level", inversedBy="CategoryTolevelToParent")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $level;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CategoriesParent", inversedBy="CategoryTolevelToParent")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $parentCategory;

    /**
     * @param mixed $category
     *
     * @return CategoryToParentToLevel
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $parentCategory
     *
     * @return CategoryToParentToLevel
     */
    public function setParentCategory($parentCategory)
    {
        $this->parentCategory = $parentCategory;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getParentCategory()
    {
        return $this->parentCategory;
    }

    /**
     * @param mixed|null $level
     *
     * @return CategoryToParentToLevel
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
}

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }
}

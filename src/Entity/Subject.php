<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\MaxDepth;



/**
 * Subject
 *
 * @ORM\Entity(repositoryClass="App\Repository\SubjectRepository")
 */
class Subject
{
    /**
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categories", inversedBy="subject")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CategoriesParent", inversedBy="subject")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $parentCategory;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Level", inversedBy="subject")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $level;

    /**
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;

    /**
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $slug;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;




    /**
     * Subject constructor.
     */
    public function __construct()
    {
        $this->method = new ArrayCollection();
        $this->isTryOut = false;
    }


    /**
     * @ORM\Column(type="boolean")
     */
    private $isTryOut;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt->format('Y-m-d H:i:s');
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): string
    {
        return $this->updatedAt->format('Y-m-d H:i:s');
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCategory(): ?Categories
    {
        return $this->category;
    }

    public function setCategory(?Categories $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getLevel(): ?Level
    {
        return $this->level;
    }

    public function setLevel(?Level $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getPricingBy(){
        return 0;
    }

    /**
     * @return mixed
     */
    public function getParentCategory()
    {
        return $this->parentCategory;
    }

    /**
     * @param mixed $parentCategory
     * @return Subject
     */
    public function setParentCategory($parentCategory)
    {
        $this->parentCategory = $parentCategory;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     * @return Subject
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    public function addMethod(Method $item)
    {
        if ($this->method->contains($item)) {
            return;
        }
        $this->method->add($item);
        $item->addSubject($this);
    }

    public function removeMethod(Method $item)
    {
        if (!$this->method->contains($item)) {
            return;
        }
        $this->method->removeElement($item);
        $item->removeSubject($this);
    }
    /**
     * @return bool
     */
    public function isTryOut(): bool
    {
        return $this->isTryOut;
    }

    /**
     * @param bool $isTryOut
     * @return Subject
     */
    public function setIsTryOut(bool $isTryOut): Subject
    {
        $this->isTryOut = $isTryOut;
        return $this;
    }



}

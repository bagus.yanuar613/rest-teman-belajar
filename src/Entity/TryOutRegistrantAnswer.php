<?php


namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * @ORM\Entity(repositoryClass="App\Repository\TryOutRegistrantAnswerRepository")
 */
class TryOutRegistrantAnswer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TryOutQuiz")
     */
    private $quiz;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TryOutSubject", inversedBy="tryoutAnswer")
     */
    private $subject;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TryOutRegistrant", inversedBy="answers")
     */
    private $registration;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $user;

    /**
     * @var \DateTime
     *
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startedAt;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $answer;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $score;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isBegin;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnded;

    /**
     * @ORM\Column(type="boolean")
     */
    private $irtChecked;

    /**
     * @var \DateTime
     *
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endedAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;



    public function __construct()
    {
        $this->isEnded = false;
        $this->irtChecked = false;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return TryOutRegistrantAnswer
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuiz()
    {
        return $this->quiz;
    }

    /**
     * @param mixed $quiz
     * @return TryOutRegistrantAnswer
     */
    public function setQuiz($quiz)
    {
        $this->quiz = $quiz;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegistration()
    {
        return $this->registration;
    }

    /**
     * @param mixed $registration
     * @return TryOutRegistrantAnswer
     */
    public function setRegistration($registration)
    {
        $this->registration = $registration;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param mixed $answer
     * @return TryOutRegistrantAnswer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param mixed $score
     * @return TryOutRegistrantAnswer
     */
    public function setScore($score)
    {
        $this->score = $score;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return TryOutRegistrantAnswer
     */
    public function setCreatedAt(\DateTime $createdAt): TryOutRegistrantAnswer
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return TryOutRegistrantAnswer
     */
    public function setUpdatedAt(\DateTime $updatedAt): TryOutRegistrantAnswer
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return TryOutRegistrantAnswer
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartedAt(): \DateTime
    {
        return $this->startedAt;
    }

    /**
     * @param \DateTime $startedAt
     * @return TryOutRegistrantAnswer
     */
    public function setStartedAt(\DateTime $startedAt): TryOutRegistrantAnswer
    {
        $this->startedAt = $startedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     * @return TryOutRegistrantAnswer
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsBegin()
    {
        return $this->isBegin;
    }

    /**
     * @param mixed $isBegin
     * @return TryOutRegistrantAnswer
     */
    public function setIsBegin($isBegin)
    {
        $this->isBegin = $isBegin;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEnded(): bool
    {
        return $this->isEnded;
    }

    /**
     * @return bool
     */
    public function getIsEnded(): bool
    {
        return $this->isEnded;
    }

    /**
     * @param bool $isEnded
     * @return TryOutRegistrantAnswer
     */
    public function setIsEnded(bool $isEnded): TryOutRegistrantAnswer
    {
        $this->isEnded = $isEnded;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndedAt(): \DateTime
    {
        return $this->endedAt;
    }

    /**
     * @param \DateTime $endedAt
     * @return TryOutRegistrantAnswer
     */
    public function setEndedAt(\DateTime $endedAt): TryOutRegistrantAnswer
    {
        $this->endedAt = $endedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIrtChecked()
    {
        return $this->irtChecked;
    }

    /**
     * @param mixed $irtChecked
     * @return TryOutRegistrantAnswer
     */
    public function setIrtChecked($irtChecked)
    {
        $this->irtChecked = $irtChecked;
        return $this;
    }



}
<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * CategoriesParent
 *
 * @ORM\Entity(repositoryClass="App\Repository\CategoriesParentRepository")
 */
class CategoriesParent
{
    /**
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;

    /**
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $slug;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default"=0})
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Subject", mappedBy="parentCategory")
     */
    private $subject;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TryOutSubject", mappedBy="mainSubject")
     */
    private $tryOutSubject;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CategoryToParentToLevel", mappedBy="parentCategory")
     */
    private $CategoryTolevelToParent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CategoryToParentToMethod", mappedBy="parentCategory")
     */
    private $CategoryToMethodToParent;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isTryOut;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Categories", inversedBy="parentCategory")
     * @ORM\JoinTable(name="parent_to_category", joinColumns={@ORM\JoinColumn(name="parentCategory_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")})
     */
    private $category;



    public function __construct()
    {
        $this->isTryOut = false;
        $this->category = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     * @return CategoriesParent
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    public function getCategoriesGroup()
    {
        $temp = [];
        $subject = $this->getSubject()->filter(
            function (Subject $entry) use (&$temp) {
                if (!in_array($entry->getCategory()->getId(), $temp)) {
                    array_push($temp, $entry->getCategory()->getId());
                    return true;
                }
            }
        );
        $result = [];
        /**
         * @var Subject $data
         */
        foreach ($subject as $key => $data){
            $result[$key]['id'] = $data->getCategory()->getId();
            $result[$key]['name'] = $data->getCategory()->getName();
            $result[$key]['slug'] = $data->getCategory()->getSlug();
        }
        return $result;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return CategoriesParent
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTryOut(): bool
    {
        return $this->isTryOut;
    }

    /**
     * @param bool $isTryOut
     * @return CategoriesParent
     */
    public function setIsTryOut(bool $isTryOut): CategoriesParent
    {
        $this->isTryOut = $isTryOut;
        return $this;
    }

    /**
     * @param mixed $category
     *
     * @return CategoriesParent
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Categories $param
     */
    public function addCategory(Categories $param)
    {
        if ($this->category->contains($param)) {
            return;
        }
        $this->category->add($param);
        $param->addCategoryParent($this);
    }

    /**
     * @param Categories $param
     */
    public function removeCategory(Categories $param)
    {
        if (!$this->category->contains($param)) {
            return;
        }
        $this->category->removeElement($param);
        $param->removeCategoryParent($this);
    }

    /**
     * @return mixed
     */
    public function getTryOutSubject()
    {
        return $this->tryOutSubject;
    }

    /**
     * @param mixed $tryOutSubject
     * @return CategoriesParent
     */
    public function setTryOutSubject($tryOutSubject)
    {
        $this->tryOutSubject = $tryOutSubject;
        return $this;
    }
    /**
     * @param mixed $CategoryTolevelToParent
     *
     * @return CategoriesParent
     */
    public function setCategoryTolevelToParent($CategoryTolevelToParent)
    {
        $this->CategoryTolevelToParent = $CategoryTolevelToParent;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoryTolevelToParent()
    {
        return $this->CategoryTolevelToParent;
    }

    /**
     * @param mixed $CategoryToMethodToParent
     *
     * @return CategoriesParent
     */
    public function setCategoryToMethodToParent($CategoryToMethodToParent)
    {
        $this->CategoryToMethodToParent = $CategoryToMethodToParent;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoryToMethodToParent()
    {
        return $this->CategoryToMethodToParent;
    }

}

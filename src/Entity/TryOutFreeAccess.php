<?php

namespace App\Entity;

use App\Repository\TryOutFreeAccessRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * @ORM\Entity(repositoryClass=TryOutFreeAccessRepository::class)
 */
class TryOutFreeAccess
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TryOutRegistrant", inversedBy="rewards")
     */
    private $registration;

    /**
     * @ORM\Column(type="string")
     */
    private $referalCode;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param mixed $registration
     *
     * @return TryOutFreeAccess
     */
    public function setRegistration($registration)
    {
        $this->registration = $registration;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegistration()
    {
        return $this->registration;
    }

    /**
     * @param mixed $referalCode
     *
     * @return TryOutFreeAccess
     */
    public function setReferalCode($referalCode)
    {
        $this->referalCode = $referalCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReferalCode()
    {
        return $this->referalCode;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return TryOutFreeAccess
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $updatedAt
     *
     * @return TryOutFreeAccess
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}

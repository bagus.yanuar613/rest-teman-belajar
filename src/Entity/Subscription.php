<?php

namespace App\Entity;


use App\Repository\SubscriptionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=SubscriptionRepository::class)
 */
class Subscription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="subscription")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     * @SerializedName("member")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TentorSubject", inversedBy="subscription")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     * @SerializedName("skill")
     */
    private $tentorSubject;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $subscriptionCode;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default"=0})
     */
    private $remains;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"default"=0})
     */
    private $status;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isRead;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $confirmedAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cart", mappedBy="subscription")
     *
     */
    private $cart;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Attendance", mappedBy="subscription")
     */
    private $attendance;

//    /**
//     * @ORM\ManyToMany(targetEntity="App\Entity\Schedule", inversedBy="subscription")
//     * @ORM\JoinTable(name="subscription_to_schedule", joinColumns={@ORM\JoinColumn(name="subscription_id", referencedColumnName="id")},
//     *     inverseJoinColumns={@ORM\JoinColumn(name="schedule_id", referencedColumnName="id")})
//     */
//    private $schedule;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AttendancePlan", mappedBy="subscription")
     */
    private $attendancePlan;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Rating", mappedBy="subscription")
     */
    private $rating;

    /**
     * Subscription constructor.
     */
    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
//        $this->schedule = new ArrayCollection();
        $this->isRead = false;
        $this->remains = 0;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Subscription
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTentorSubject()
    {
        return $this->tentorSubject;
    }

    /**
     * @param mixed $tentorSubject
     * @return Subscription
     */
    public function setTentorSubject($tentorSubject)
    {
        $this->tentorSubject = $tentorSubject;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubscriptionCode()
    {
        return $this->subscriptionCode;
    }

    /**
     * @param mixed $subscriptionCode
     * @return Subscription
     */
    public function setSubscriptionCode($subscriptionCode)
    {
        $this->subscriptionCode = $subscriptionCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Subscription
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getConfirmedAt()
    {
        return $this->confirmedAt;
    }

    /**
     * @param mixed $confirmedAt
     * @return Subscription
     */
    public function setConfirmedAt($confirmedAt)
    {
        $this->confirmedAt = $confirmedAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Subscription
     */
    public function setCreatedAt(\DateTime $createdAt): Subscription
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return Subscription
     */
    public function setUpdatedAt(\DateTime $updatedAt): Subscription
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param mixed $cart
     * @return Subscription
     */
    public function setCart($cart)
    {
        $this->cart = $cart;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRead(): bool
    {
        return $this->isRead;
    }

    /**
     * @param bool $isRead
     * @return Subscription
     */
    public function setIsRead(bool $isRead): Subscription
    {
        $this->isRead = $isRead;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttendance()
    {
        return $this->attendance;
    }

    /**
     * @param mixed $attendance
     * @return Subscription
     */
    public function setAttendance($attendance)
    {
        $this->attendance = $attendance;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRemains()
    {
        return $this->remains;
    }

    /**
     * @param mixed $remains
     * @return Subscription
     */
    public function setRemains($remains)
    {
        $this->remains = $remains;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttendancePlan()
    {
        return $this->attendancePlan;
    }

    /**
     * @param mixed $attendancePlan
     * @return Subscription
     */
    public function setAttendancePlan($attendancePlan)
    {
        $this->attendancePlan = $attendancePlan;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     * @return Subscription
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
        return $this;
    }

    public function getCartActive()
    {
        if ($this->getRemains() <= 0) {
            return $this->getLastCart();
        }
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('isActive', 1))
            ->andWhere(Criteria::expr()->eq('isFinish', 0))
            ->orderBy(['id' => 'ASC'])
            ->setFirstResult(0)
            ->setMaxResults(1);
        if (count($this->getCart()->matching($criteria)) > 0) {
            return $this->getCart()->matching($criteria)[0];
        }
        return null;
    }

    public function getLastCart()
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('isActive', 1))
            ->orderBy(['id' => 'DESC'])
            ->setFirstResult(0)
            ->setMaxResults(1);
        if (count($this->getCart()->matching($criteria)) > 0) {
            return $this->getCart()->matching($criteria)[0];
        }
        return null;
    }

    public function getRequestCart()
    {
        if (count($this->getCart()) > 0) {
            return $this->getCart()[0];
        }
        return null;
    }

    public function getHasReview()
    {
        if ($this->getRating() !== null) {
            return true;
        }
        return false;
    }

    public function getHasRenewal()
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('isActive', false))
            ->andWhere(Criteria::expr()->eq('isFinish', false))
            ->andWhere(Criteria::expr()->gt('expiredPay', new \DateTime()))
            ->andWhere(Criteria::expr()->isNull('transaction'))
            ->setFirstResult(0)
            ->setMaxResults(1);
        if (count($this->getCart()->matching($criteria)) > 0) {
            return true;
        }
        return false;
    }

    public function getHasPayment(){
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('isActive', false))
            ->andWhere(Criteria::expr()->eq('isFinish', false))
            ->andWhere(Criteria::expr()->neq('transaction', null))
            ->setFirstResult(0)
            ->setMaxResults(1);
        if (count($this->getCart()->matching($criteria)) > 0) {
            $cartHasTransaction =  $this->getCart()->matching($criteria)[0];
            if($cartHasTransaction->getTransaction()->getIspaid() === null && $cartHasTransaction->getTransaction()->getPaymentExpiration() > new \DateTime()){
                return true;
            }
        }
        return false;
    }
}

<?php

namespace App\Entity;

use App\Repository\ProvinceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProvinceRepository::class)
 */
class Province
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *@ORM\Column(type="string")
     */
    private $provinceName;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\City", mappedBy="province")
     */
    private $city;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SubCity", mappedBy="province")
     */
    private $subCity;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getProvinceName()
    {
        return $this->provinceName;
    }

    /**
     * @param mixed $provinceName
     * @return Province
     */
    public function setProvinceName($provinceName)
    {
        $this->provinceName = $provinceName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return Province
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubCity()
    {
        return $this->subCity;
    }

    /**
     * @param mixed $subCity
     * @return Province
     */
    public function setSubCity($subCity)
    {
        $this->subCity = $subCity;
        return $this;
    }

}

<?php

namespace App\Entity;

use App\Repository\TransactionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=TransactionRepository::class)
 */
class Transaction
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $subscriptionCode;


    /**
     * @ORM\Column(type="smallint", nullable=true, options={"default"=0})
     */
    private $status;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $confirmedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="transaction")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     * @SerializedName("member")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TentorSubject", inversedBy="transaction")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     * @SerializedName("skill")
     */
    private $tentorSubject;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BankAccount", inversedBy="payment")
     */
    private $bank;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;


    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Rating", mappedBy="transaction")
     */
    private $rating;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cart", mappedBy="transaction")
     *
     */
    private $cart;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Attendance", mappedBy="transaction")
     */
    private $attendance;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TryOutRegistrant", mappedBy="transaction")
     */
    private $tryOutRegistrant;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $confirmationInfo;



    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $paymentExpiration;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default"=null})
     */
    private $isPaid;

    /**
     * Transaction constructor.
     */
    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
        $this->isPaid = null;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt->format('Y-m-d H:i:s');
    }

    /**
     * @param \DateTime $createdAt
     * @return Transaction
     */
    public function setCreatedAt(\DateTime $createdAt): Transaction
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->updatedAt->format('Y-m-d H:i:s');
    }

    /**
     * @param \DateTime $updatedAt
     * @return Transaction
     */
    public function setUpdatedAt(\DateTime $updatedAt): Transaction
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     * @return Transaction
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Transaction
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Transaction
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTentorSubject()
    {
        return $this->tentorSubject;
    }

    /**
     * @param mixed $tentorSubject
     * @return Transaction
     */
    public function setTentorSubject($tentorSubject)
    {
        $this->tentorSubject = $tentorSubject;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getSubscriptionCode()
    {
        return $this->subscriptionCode;
    }

    /**
     * @param mixed $subscriptionCode
     * @return Transaction
     */
    public function setSubscriptionCode($subscriptionCode)
    {
        $this->subscriptionCode = $subscriptionCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getConfirmedAt()
    {
        return $this->confirmedAt;
    }

    /**
     * @param mixed $confirmedAt
     * @return Transaction
     */
    public function setConfirmedAt($confirmedAt)
    {
        $this->confirmedAt = $confirmedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param mixed $cart
     * @return Transaction
     */
    public function setCart($cart)
    {
        $this->cart = $cart;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttendance()
    {
        return $this->attendance;
    }

    /**
     * @param mixed $attendance
     * @return Transaction
     */
    public function setAttendance($attendance)
    {
        $this->attendance = $attendance;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTryOutRegistrant()
    {
        return $this->tryOutRegistrant;
    }

    /**
     * @param mixed $tryOutRegistrant
     * @return Transaction
     */
    public function setTryOutRegistrant($tryOutRegistrant)
    {
        $this->tryOutRegistrant = $tryOutRegistrant;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return Transaction
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     * @return Transaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentExpiration()
    {
        return $this->paymentExpiration;
    }

    /**
     * @param mixed $paymentExpiration
     * @return Transaction
     */
    public function setPaymentExpiration($paymentExpiration)
    {
        $this->paymentExpiration = $paymentExpiration;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsPaid()
    {
        return $this->isPaid;
    }

    /**
     * @param mixed $isPaid
     * @return Transaction
     */
    public function setIsPaid($isPaid)
    {
        $this->isPaid = $isPaid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getConfirmationInfo()
    {
        return $this->confirmationInfo;
    }

    /**
     * @param mixed $confirmationInfo
     * @return Transaction
     */
    public function setConfirmationInfo($confirmationInfo)
    {
        $this->confirmationInfo = $confirmationInfo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @param mixed $bank
     * @return Transaction
     */
    public function setBank($bank)
    {
        $this->bank = $bank;
        return $this;
    }
}

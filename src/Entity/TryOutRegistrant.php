<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TryOutRegistrantRepository")
 */
class TryOutRegistrant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="integer")
     */
    private $basePrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $registrantNumber;

    /**
     * @ORM\Column(type="integer")
     */
    private $discount;

    /**
     * @ORM\Column(type="integer")
     */
    private $finalPrice;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BankAccount")
     */
    private $paymentMethod;

    /**
     * @ORM\Column(type="integer")
     */
    private $totalPrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $duration;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Promo")
     */
    private $promo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $promoDescription;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPaid;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $confirmationInfo;

    /**
     * @var \DateTime
     *
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expiredAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TryOut", inversedBy="registrant")
     */
    private $tryOut;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TryOutRegistrant", mappedBy="invitedBy")
     */
    private $invitation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TryOutRegistrantAnswer", mappedBy="registration")
     */
    private $answers;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TryOutRegistrant", inversedBy="invitation")
     */
    private $invitedBy;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TryOutPricing")
     */
    private $tryOutPricing;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Transaction", inversedBy="tryOutRegistrant")
     */
    private $transaction;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TryOutFreeAccess", mappedBy="registration")
     */
    private $rewards;

    /**
     * @ORM\Column(type="boolean")
     */
    private $irtChecked;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnded;

    /**
     * @var \DateTime
     *
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endedAt;

    /**
     *
     *
     *
     * @ORM\Column(type="boolean")
     */
    private $isActive;



    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
        $this->discount = 0;
        $this->duration = 0;
        $this->isEnded = 0;
        $this->isActive = 1;
        $this->irtChecked = 0;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return TryOutRegistrant
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBasePrice()
    {
        return $this->basePrice;
    }

    /**
     * @param mixed $basePrice
     * @return TryOutRegistrant
     */
    public function setBasePrice($basePrice)
    {
        $this->basePrice = $basePrice;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFinalPrice()
    {
        return $this->finalPrice;
    }

    /**
     * @param mixed $finalPrice
     * @return TryOutRegistrant
     */
    public function setFinalPrice($finalPrice)
    {
        $this->finalPrice = $finalPrice;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     * @return TryOutRegistrant
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpiredAt(): ?\DateTime
    {
        return $this->expiredAt;
    }

    /**
     * @param \DateTime $expiredAt
     * @return TryOutRegistrant
     */
    public function setExpiredAt(\DateTime $expiredAt): TryOutRegistrant
    {
        $this->expiredAt = $expiredAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTryOutPricing()
    {
        return $this->tryOutPricing;
    }

    /**
     * @param mixed $tryOutPricing
     * @return TryOutRegistrant
     */
    public function setTryOutPricing($tryOutPricing)
    {
        $this->tryOutPricing = $tryOutPricing;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return TryOutRegistrant
     */
    public function setCreatedAt(\DateTime $createdAt): TryOutRegistrant
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return TryOutRegistrant
     */
    public function setUpdatedAt(\DateTime $updatedAt): TryOutRegistrant
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPromo()
    {
        return $this->promo;
    }

    /**
     * @param mixed $promo
     * @return TryOutRegistrant
     */
    public function setPromo($promo)
    {
        $this->promo = $promo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPromoDescription()
    {
        return $this->promoDescription;
    }

    /**
     * @param mixed $promoDescription
     * @return TryOutRegistrant
     */
    public function setPromoDescription($promoDescription)
    {
        $this->promoDescription = $promoDescription;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return TryOutRegistrant
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTryOut()
    {
        return $this->tryOut;
    }

    /**
     * @param mixed $tryOut
     * @return TryOutRegistrant
     */
    public function setTryOut($tryOut)
    {
        $this->tryOut = $tryOut;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegistrantNumber()
    {
        return $this->registrantNumber;
    }

    /**
     * @param mixed $registrantNumber
     * @return TryOutRegistrant
     */
    public function setRegistrantNumber($registrantNumber)
    {
        $this->registrantNumber = $registrantNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     * @return TryOutRegistrant
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * @param mixed $totalPrice
     * @return TryOutRegistrant
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsPaid()
    {
        return $this->isPaid;
    }

    /**
     * @param mixed $isPaid
     * @return TryOutRegistrant
     */
    public function setIsPaid($isPaid)
    {
        $this->isPaid = $isPaid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param mixed $paymentMethod
     * @return TryOutRegistrant
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsEnded(): int
    {
        return $this->isEnded;
    }

    /**
     * @param int $isEnded
     * @return TryOutRegistrant
     */
    public function setIsEnded(int $isEnded): TryOutRegistrant
    {
        $this->isEnded = $isEnded;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndedAt(): \DateTime
    {
        return $this->endedAt;
    }

    /**
     * @param \DateTime $endedAt
     * @return TryOutRegistrant
     */
    public function setEndedAt(\DateTime $endedAt): TryOutRegistrant
    {
        $this->endedAt = $endedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInvitedBy()
    {
        return $this->invitedBy;
    }

    /**
     * @param mixed $invitedBy
     * @return TryOutRegistrant
     */
    public function setInvitedBy($invitedBy)
    {
        $this->invitedBy = $invitedBy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInvitation()
    {
        return $this->invitation;
    }

    /**
     * @param mixed $invitation
     * @return TryOutRegistrant
     */
    public function setInvitation($invitation)
    {
        $this->invitation = $invitation;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     * @return TryOutRegistrant
     */
    public function setIsActive(int $isActive): TryOutRegistrant
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param mixed $answers
     * @return TryOutRegistrant
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRewards()
    {
        return $this->rewards;
    }

    /**
     * @param mixed $rewards
     * @return TryOutRegistrant
     */
    public function setRewards($rewards)
    {
        $this->rewards = $rewards;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getConfirmationInfo()
    {
        return $this->confirmationInfo;
    }

    /**
     * @param mixed $confirmationInfo
     * @return TryOutRegistrant
     */
    public function setConfirmationInfo($confirmationInfo)
    {
        $this->confirmationInfo = $confirmationInfo;
        return $this;
    }



    public function getIrtChecked()
    {
        return $this->irtChecked;
    }

    /**
     * @param mixed $irtChecked
     * @return TryOutRegistrant
     */
    public function setIrtChecked($irtChecked)
    {
        $this->irtChecked = $irtChecked;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param mixed $transaction
     * @return TryOutRegistrant
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;
        return $this;
    }











}
<?php

namespace App\Entity;

use App\Repository\AttendancePlanRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AttendancePlanRepository::class)
 */
class AttendancePlan
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subscription", inversedBy="attendancePlan")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $subscription;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Schedule", inversedBy="attendancePlan")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $schedule;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"default"=0})
     */
    private $status;

    /**
     * AttendancePlan constructor.
     */
    public function __construct()
    {
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param mixed $subscription
     * @return AttendancePlan
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * @param mixed $schedule
     * @return AttendancePlan
     */
    public function setSchedule($schedule)
    {
        $this->schedule = $schedule;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return AttendancePlan
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

}

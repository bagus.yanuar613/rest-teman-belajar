<?php

namespace App\Entity;

use App\Repository\CityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CityRepository::class)
 */
class City
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Province", inversedBy="city")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $province;

    /**
     *@ORM\Column(type="string")
     *
     */
    private $cityName;

    /**
     *@ORM\Column(type="string")
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Pricing", mappedBy="city")
     */
    private $pricing;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tentor", mappedBy="city")
     */
    private $tentorProfile;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\District", mappedBy="city")
     */
    private $district;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SubCity", mappedBy="city")
     */
    private $subCity;

    /**
     * City constructor.
     */
    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCityName()
    {
        return $this->cityName;
    }

    /**
     * @param mixed $cityName
     * @return City
     */
    public function setCityName($cityName)
    {
        $this->cityName = $cityName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return City
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPricing()
    {
        return $this->pricing;
    }

    /**
     * @param mixed $pricing
     * @return City
     */
    public function setPricing($pricing)
    {
        $this->pricing = $pricing;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getTentorProfile()
    {
        return $this->tentorProfile;
    }

    /**
     * @param mixed $tentorProfile
     * @return City
     */
    public function setTentorProfile($tentorProfile)
    {
        $this->tentorProfile = $tentorProfile;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param mixed $district
     * @return City
     */
    public function setDistrict($district)
    {
        $this->district = $district;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * @param mixed $province
     * @return City
     */
    public function setProvince($province)
    {
        $this->province = $province;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubCity()
    {
        return $this->subCity;
    }

    /**
     * @param mixed $subCity
     * @return City
     */
    public function setSubCity($subCity)
    {
        $this->subCity = $subCity;
        return $this;
    }

}

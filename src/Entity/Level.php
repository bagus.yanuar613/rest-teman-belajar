<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Level
 *
 * @ORM\Entity(repositoryClass="App\Repository\LevelRepository")
 */
class Level
{
    /**
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;

    /**
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $slug;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default"=0})
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Subject", mappedBy="level")
     */
    private $subject;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Pricing", mappedBy="level")
     */
    private $pricing;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MemberClass", mappedBy="level")
     */
    private $memberClass;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CategoryToParentToLevel", mappedBy="level")
     */
    private $CategoryTolevelToParent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TentorSubject", mappedBy="level")
     */
    private $tentorSubject;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TryOut", mappedBy="level")
     */
    private $tryOut;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Categories", mappedBy="level")
     */
    private $category;

    /**
     * Level constructor.
     */
    public function __construct()
    {
        $this->category = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Level
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return Level
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     * @return Level
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return Level
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPricing()
    {
        return $this->pricing;
    }

    /**
     * @param mixed $pricing
     * @return Level
     */
    public function setPricing($pricing)
    {
        $this->pricing = $pricing;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTentorSubject()
    {
        return $this->tentorSubject;
    }

    /**
     * @param mixed $tentorSubject
     * @return Level
     */
    public function setTentorSubject($tentorSubject)
    {
        $this->tentorSubject = $tentorSubject;
        return $this;
    }

    /**
     * @param mixed $category
     *
     * @return Level
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Categories $item
     */
    public function addCategory(Categories $item)
    {
        if ($this->category->contains($item)) {
            return;
        }
        $this->category->add($item);
        $item->addLevel($this);
    }

    /**
     * @param Categories $item
     */
    public function removeCategory(Categories $item)
    {
        if (!$this->category->contains($item)) {
            return;
        }
        $this->category->removeElement($item);
        $item->removeLevel($this);
    }

    /**
     * @return mixed
     */
    public function getTryOut()
    {
        return $this->tryOut;
    }

    /**
     * @param mixed $tryOut
     * @return Level
     */
    public function setTryOut($tryOut)
    {
        $this->tryOut = $tryOut;
        return $this;
    }


    /**
     * @param mixed $CategoryTolevelToParent
     *
     * @return Level
     */
    public function setCategoryTolevelToParent($CategoryTolevelToParent)
    {
        $this->CategoryTolevelToParent = $CategoryTolevelToParent;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoryTolevelToParent()
    {
        return $this->CategoryTolevelToParent;
    }

    /**
     * @return mixed
     */
    public function getMemberClass()
    {
        return $this->memberClass;
    }

    /**
     * @param mixed $memberClass
     * @return Level
     */
    public function setMemberClass($memberClass)
    {
        $this->memberClass = $memberClass;
        return $this;
    }

}

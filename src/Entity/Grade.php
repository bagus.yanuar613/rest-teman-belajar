<?php

namespace App\Entity;

use App\Repository\GradeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GradeRepository::class)
 */
class Grade
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *@ORM\Column(type="string")
     */
    private $name;

    /**
     *@ORM\Column(type="string")
     */
    private $slug;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default"=0})
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Pricing", mappedBy="grade")
     */
    private $pricing;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tentor", mappedBy="grade")
     */
    private $tentorProfile;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return Grade
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Grade
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return Grade
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPricing()
    {
        return $this->pricing;
    }

    /**
     * @param mixed $pricing
     * @return Grade
     */
    public function setPricing($pricing)
    {
        $this->pricing = $pricing;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTentorProfile()
    {
        return $this->tentorProfile;
    }

    /**
     * @param mixed $tentorProfile
     * @return Grade
     */
    public function setTentorProfile($tentorProfile)
    {
        $this->tentorProfile = $tentorProfile;
        return $this;
    }
}

<?php

namespace App\Entity;

use App\Repository\AttendanceRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use function GuzzleHttp\Psr7\str;

/**
 * @ORM\Entity(repositoryClass=AttendanceRepository::class)
 */
class Attendance
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cart", inversedBy="attendance")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $cart;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subscription", inversedBy="attendance")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $subscription;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startAt;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $finishAt;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $duration;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $note;


    /**
     * @ORM\Column(type="smallint", nullable=true, options={"default"=0})
     */
    private $isPresence;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"default"=0})
     */
    private $isActive;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $clientId;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default"=0})
     */
    private $point;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\AttendanceReport", mappedBy="attendance")
     */
    private $report;


    /**
     * Attendance constructor.
     */
    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     * @return Attendance
     */
    public function setTime($time)
    {
        $this->time = $time;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsPresence()
    {
        return $this->isPresence;
    }

    /**
     * @param mixed $isPresence
     * @return Attendance
     */
    public function setIsPresence($isPresence)
    {
        $this->isPresence = $isPresence;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Attendance
     */
    public function setCreatedAt(\DateTime $createdAt): Attendance
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return Attendance
     */
    public function setUpdatedAt(\DateTime $updatedAt): Attendance
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @param mixed $report
     * @return Attendance
     */
    public function setReport($report)
    {
        $this->report = $report;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param mixed $cart
     * @return Attendance
     */
    public function setCart($cart)
    {
        $this->cart = $cart;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * @param mixed $startAt
     * @return Attendance
     */
    public function setStartAt($startAt)
    {
        $this->startAt = $startAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFinishAt()
    {
        return $this->finishAt;
    }

    /**
     * @param mixed $finishAt
     * @return Attendance
     */
    public function setFinishAt($finishAt)
    {
        $this->finishAt = $finishAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     * @return Attendance
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     * @return Attendance
     */
    public function setNote($note)
    {
        $this->note = $note;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param mixed $subscription
     * @return Attendance
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;
        return $this;
    }


    public function getGeneratedStart()
    {
        setlocale(LC_ALL, 'IND');
        date_default_timezone_set("Asia/Jakarta");
        $start = $this->getStartAt()->format('Y-m-d H:i:s');
        return strftime('%A, %d %B %Y %H:%I:%S', strtotime($start));
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     * @return Attendance
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    public function getGeneratedFinishAt()
    {
        return  $this->getFinishAt()->format('Y-m-d H:i:s');
    }

    public function getGeneratedFinish()
    {
        setlocale(LC_ALL, 'IND');
        date_default_timezone_set("Asia/Jakarta");
        $finish = $this->getFinishAt()->format('Y-m-d H:i:s');
        return strftime('%A, %d %B %Y %H:%I:%S', strtotime($finish));
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param mixed $clientId
     * @return Attendance
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * @param mixed $point
     * @return Attendance
     */
    public function setPoint($point)
    {
        $this->point = $point;
        return $this;
    }

}

<?php

namespace App\Entity;

use App\Repository\TentorSaldoRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=TentorSaldoRepository::class)
 */
class TentorSaldo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="tentorSaldo")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     * @SerializedName("member")
     */
    private $user;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"default"=0})
     */
    private $type;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default"=0})
     */
    private $amount;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * TentorSaldo constructor.
     */
    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return TentorSaldo
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return TentorSaldo
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     * @return TentorSaldo
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return TentorSaldo
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return TentorSaldo
     */
    public function setCreatedAt(\DateTime $createdAt): TentorSaldo
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return TentorSaldo
     */
    public function setUpdatedAt(\DateTime $updatedAt): TentorSaldo
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function getGeneratedCreatedAt()
    {
        setlocale(LC_ALL, 'IND');
        date_default_timezone_set("Asia/Jakarta");
        $created = $this->getCreatedAt()->format('Y-m-d H:i:s');
        return strftime('%d %B %Y, %H:%I', strtotime($created));
    }
}

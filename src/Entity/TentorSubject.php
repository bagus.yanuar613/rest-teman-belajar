<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use App\Repository\TentorSubjectRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=TentorSubjectRepository::class)
 */
class TentorSubject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="tentorSubject")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categories", inversedBy="tentorSubject")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Level", inversedBy="tentorSubject")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $level;

    /**
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $slug;


    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default"=false})
     */
    private $isAvailable;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Method", inversedBy="tentorSubject")
     * @ORM\JoinTable(name="tentor_subject_to_method", joinColumns={@ORM\JoinColumn(name="tentor_subject_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="method_id", referencedColumnName="id")})
     */
    private $method;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction", mappedBy="tentorSubject")
     */
    private $transaction;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Subscription", mappedBy="tentorSubject")
     */
    private $subscription;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProductCategory", inversedBy="tentorSubject")
     */
    private $product;

    /**
     * TentorSubject constructor.
     */
    public function __construct()
    {
        $this->method = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param mixed $transaction
     * @return TentorSubject
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return TentorSubject
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     * @return TentorSubject
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }


    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt->format('Y-m-d H:i:s');
    }

    /**
     * @param \DateTime $createdAt
     * @return TentorSubject
     */
    public function setCreatedAt(\DateTime $createdAt): TentorSubject
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->updatedAt->format('Y-m-d H:i:s');
    }

    /**
     * @param \DateTime $updatedAt
     * @return TentorSubject
     */
    public function setUpdatedAt(\DateTime $updatedAt): TentorSubject
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsAvailable()
    {
        return $this->isAvailable;
    }

    /**
     * @param mixed $isAvailable
     * @return TentorSubject
     */
    public function setIsAvailable($isAvailable)
    {
        $this->isAvailable = $isAvailable;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return TentorSubject
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     * @return TentorSubject
     */
    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return TentorSubject
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    public function addMethod(Method $item)
    {
        if ($this->method->contains($item)) {
            return;
        }
        $this->method->add($item);
        $item->addTentorSubject($this);
    }

    public function removeMethod(Method $item)
    {
        if (!$this->method->contains($item)) {
            return;
        }
        $this->method->removeElement($item);
        $item->removeTentorSubject($this);
    }

    /**
     * @return mixed
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param mixed $subscription
     * @return TentorSubject
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;
        return $this;
    }

}

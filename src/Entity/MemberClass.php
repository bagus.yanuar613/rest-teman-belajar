<?php

namespace App\Entity;

use App\Repository\MemberClassRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MemberClassRepository::class)
 */
class MemberClass
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Member", mappedBy="class")
     */
    private $memberProfile;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MemberClassParent", inversedBy="memberClass")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $memberClassParent;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Level", inversedBy="memberClass")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $level;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Categories", inversedBy="memberClass")
     * @ORM\JoinTable(name="class_to_categories", joinColumns={@ORM\JoinColumn(name="memberClass_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")})
     */
    private $category;

    /**
     * MemberClass constructor.
     */
    public function __construct()
    {
        $this->category = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return MemberClass
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return MemberClass
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    public function addCategory(Categories $param)
    {
        if ($this->category->contains($param)) {
            return;
        }
        $this->category->add($param);
        $param->addMemberClass($this);
    }


    public function removeCategory(Categories $param)
    {
        if (!$this->category->contains($param)) {
            return;
        }
        $this->category->removeElement($param);
        $param->removeMemberClass($this);
    }

    /**
     * @return mixed
     */
    public function getMemberProfile()
    {
        return $this->memberProfile;
    }

    /**
     * @param mixed $memberProfile
     * @return MemberClass
     */
    public function setMemberProfile($memberProfile)
    {
        $this->memberProfile = $memberProfile;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMemberClassParent()
    {
        return $this->memberClassParent;
    }

    /**
     * @param mixed $memberClassParent
     * @return MemberClass
     */
    public function setMemberClassParent($memberClassParent)
    {
        $this->memberClassParent = $memberClassParent;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     * @return MemberClass
     */
    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

}

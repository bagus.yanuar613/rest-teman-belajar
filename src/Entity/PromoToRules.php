<?php

namespace App\Entity;

use App\Repository\PromoToRulesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PromoToRulesRepository::class)
 */
class PromoToRules
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Promo", inversedBy="promoToRules")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $promo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PromoRules", inversedBy="promoToRules")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $promoRules;

    /**
     *@ORM\Column(type="integer", nullable=true, options={"default"=0})
     */
    private $qty;



    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPromo()
    {
        return $this->promo;
    }

    /**
     * @param mixed $promo
     * @return PromoToRules
     */
    public function setPromo($promo)
    {
        $this->promo = $promo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPromoRules()
    {
        return $this->promoRules;
    }

    /**
     * @param mixed $promoRules
     * @return PromoToRules
     */
    public function setPromoRules($promoRules)
    {
        $this->promoRules = $promoRules;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param mixed $qty
     * @return PromoToRules
     */
    public function setQty($qty)
    {
        $this->qty = $qty;
        return $this;
    }
}

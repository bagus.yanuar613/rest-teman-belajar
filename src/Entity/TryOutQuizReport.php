<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TryOutQuizReportRepository")
 */
class TryOutQuizReport
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isSolved;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $solvedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TryOutQuiz")
     */
    private $quiz;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TryOutRegistrant")
     */
    private $registration;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $reporter;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->isSolved = 0;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return TryOutQuizReport
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return TryOutQuizReport
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return TryOutQuizReport
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsSolved(): int
    {
        return $this->isSolved;
    }

    /**
     * @param int $isSolved
     * @return TryOutQuizReport
     */
    public function setIsSolved(int $isSolved): TryOutQuizReport
    {
        $this->isSolved = $isSolved;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSolvedAt()
    {
        return $this->solvedAt;
    }

    /**
     * @param mixed $solvedAt
     * @return TryOutQuizReport
     */
    public function setSolvedAt($solvedAt)
    {
        $this->solvedAt = $solvedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuiz()
    {
        return $this->quiz;
    }

    /**
     * @param mixed $quiz
     * @return TryOutQuizReport
     */
    public function setQuiz($quiz)
    {
        $this->quiz = $quiz;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegistration()
    {
        return $this->registration;
    }

    /**
     * @param mixed $registration
     * @return TryOutQuizReport
     */
    public function setRegistration($registration)
    {
        $this->registration = $registration;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReporter()
    {
        return $this->reporter;
    }

    /**
     * @param mixed $reporter
     * @return TryOutQuizReport
     */
    public function setReporter($reporter)
    {
        $this->reporter = $reporter;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return TryOutQuizReport
     */
    public function setCreatedAt(\DateTime $createdAt): TryOutQuizReport
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return TryOutQuizReport
     */
    public function setUpdatedAt(\DateTime $updatedAt): TryOutQuizReport
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }




}
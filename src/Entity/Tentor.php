<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * Tentor
 *
 * @ORM\Entity(repositoryClass="App\Repository\TentorRepository")
 */
class Tentor
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="tentorProfile")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City", inversedBy="tentorProfile")
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Grade", inversedBy="tentorProfile")
     */
    private $grade;

    /**
     * @SerializedName("name")
     * @ORM\Column(type="string", nullable=true)
     */
    private $fullName;

    /**
     *
     * @ORM\Column(type="boolean", nullable=true, options={"default"=true})
     */
    private $gender;

    /**
     *
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $phone;

    /**
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $address;

    /**
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $avatar;

    /**
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $about;

    /**
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $experience;

    /**
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $education;

    /**
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $achievement;

    /**
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $company;

    /**
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $slugName;

    /**
     *
     * @ORM\Column(type="boolean", nullable=true, options={"default"=false})
     */
    private $isActive;

    /**
     *
     * @ORM\Column(type="boolean", nullable=true, options={"default"=false})
     */
    private $isVerified;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default"=0})
     */
    private $balance;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default"=0})
     */
    private $point;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Bank", inversedBy="tentor")
     */
    private $bank;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $rekening;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $holderName;

    /**
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $deviceID;

    /**
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $deviceModel;


    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @return mixed
     */
    public function getSlugName()
    {
        return $this->slugName;
    }

    /**
     * @param mixed $slugName
     * @return Tentor
     */
    public function setSlugName($slugName)
    {
        $this->slugName = $slugName;
        return $this;
    }


    /**
     * Tentor constructor.
     */
    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     * @return Tentor
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * @param mixed $grade
     * @return Tentor
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     * @return Tentor
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return Tentor
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return Tentor
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * @param mixed $about
     * @return Tentor
     */
    public function setAbout($about)
    {
        $this->about = $about;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * @param mixed $experience
     * @return Tentor
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEducation()
    {
        return $this->education;
    }

    /**
     * @param mixed $education
     * @return Tentor
     */
    public function setEducation($education)
    {
        $this->education = $education;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAchievement()
    {
        return $this->achievement;
    }

    /**
     * @param mixed $achievement
     * @return Tentor
     */
    public function setAchievement($achievement)
    {
        $this->achievement = $achievement;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     * @return Tentor
     */
    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     * @return Tentor
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsVerified()
    {
        return $this->isVerified;
    }

    /**
     * @param mixed $isVerified
     * @return Tentor
     */
    public function setIsVerified($isVerified)
    {
        $this->isVerified = $isVerified;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt->format('Y-m-d H:i:s');
    }

    /**
     * @param \DateTime $createdAt
     * @return Tentor
     */
    public function setCreatedAt(\DateTime $createdAt): Tentor
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->updatedAt->format('Y-m-d H:i:s');
    }

    /**
     * @param \DateTime $updatedAt
     * @return Tentor
     */
    public function setUpdatedAt(\DateTime $updatedAt): Tentor
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param mixed $avatar
     * @return Tentor
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Tentor
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return Tentor
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeviceID()
    {
        return $this->deviceID;
    }

    /**
     * @param mixed $deviceID
     * @return Tentor
     */
    public function setDeviceID($deviceID)
    {
        $this->deviceID = $deviceID;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeviceModel()
    {
        return $this->deviceModel;
    }

    /**
     * @param mixed $deviceModel
     * @return Tentor
     */
    public function setDeviceModel($deviceModel)
    {
        $this->deviceModel = $deviceModel;
        return $this;
    }

    public function getGeneratedAvatar()
    {
        if ($this->avatar !== null) {
            if (str_contains($this->avatar, 'https://')) {
                return $this->avatar;
            }
            $server = 'http';
            if (isset($_SERVER['HTTPS'])) {
                $server = $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http';
            }
            $sName = $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'];
//            $sName = 'a143a6967751.ngrok.io';
            return $server . '://' . $sName . '/images/img-account/' . $this->avatar;
        }
        return $this->avatar;
    }

    /**
     * @return mixed
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param mixed $balance
     * @return Tentor
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * @param mixed $point
     * @return Tentor
     */
    public function setPoint($point)
    {
        $this->point = $point;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @param mixed $bank
     * @return Tentor
     */
    public function setBank($bank)
    {
        $this->bank = $bank;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRekening()
    {
        return $this->rekening;
    }

    /**
     * @param mixed $rekening
     * @return Tentor
     */
    public function setRekening($rekening)
    {
        $this->rekening = $rekening;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHolderName()
    {
        return $this->holderName;
    }

    /**
     * @param mixed $holderName
     * @return Tentor
     */
    public function setHolderName($holderName)
    {
        $this->holderName = $holderName;
        return $this;
    }
}

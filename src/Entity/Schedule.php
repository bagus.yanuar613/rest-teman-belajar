<?php

namespace App\Entity;

use App\Repository\ScheduleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ScheduleRepository::class)
 */
class Schedule
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $day;

    /**
     *
     * @ORM\Column(type="time")
     */
    private $time;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="schedule")
     * @ORM\JoinTable(name="schedule_to_tentor", joinColumns={@ORM\JoinColumn(name="schedule_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")})
     */
    private $user;

//    /**
//     * @ORM\ManyToMany(targetEntity="App\Entity\Subscription", mappedBy="schedule")
//     */
//    private $subscription;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AttendancePlan", mappedBy="schedule")
     */
    private $attendancePan;

    /**
     * Schedule constructor.
     */
    public function __construct()
    {
        $this->user = new ArrayCollection();
//        $this->subscription = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param mixed $day
     * @return Schedule
     */
    public function setDay($day)
    {
        $this->day = $day;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     * @return Schedule
     */
    public function setTime($time)
    {
        $this->time = $time;
        return $this;
    }

    public function getGeneratedTime()
    {
        return $this->time->format('H:i');
    }
    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Schedule
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function addUser(User $item)
    {
        if ($this->user->contains($item)) {
            return;
        }
        $this->user->add($item);
        $item->addSchedule($this);
    }

    public function removeUser(User $item)
    {
        if (!$this->user->contains($item)) {
            return;
        }
        $this->user->removeElement($item);
        $item->removeSchedule($this);
    }

//    public function addSubscription(Subscription $item)
//    {
//        if ($this->subscription->contains($item)) {
//            return;
//        }
//        $this->subscription->add($item);
//        $item->addSchedule($this);
//    }
//
//    public function removeSubscription(Subscription $item)
//    {
//        if (!$this->subscription->contains($item)) {
//            return;
//        }
//        $this->subscription->removeElement($item);
//        $item->removeSchedule($this);
//    }

    /**
     * @return mixed
     */
    public function getAttendancePan()
    {
        return $this->attendancePan;
    }

    /**
     * @param mixed $attendancePan
     * @return Schedule
     */
    public function setAttendancePan($attendancePan)
    {
        $this->attendancePan = $attendancePan;
        return $this;
    }
}

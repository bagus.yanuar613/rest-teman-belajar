<?php


namespace App\Security;


use App\Entity\Member;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Guard\PasswordAuthenticatedInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator implements PasswordAuthenticatedInterface
{

    use TargetPathTrait;

    public const LOGIN_ROUTE = 'login';

    private $entityManager;
    private $urlGenerator;
    private $csrfTokenManager;
    private $passwordEncoder;
    private $session;
    private $flashMessage;

    public function __construct(EntityManagerInterface $entityManager, UrlGeneratorInterface $urlGenerator, CsrfTokenManagerInterface $csrfTokenManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->entityManager = $entityManager;
        $this->urlGenerator = $urlGenerator;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->session = new Session();
    }
    /**
     * @inheritDoc
     */
    protected function getLoginUrl()
    {
        // TODO: Implement getLoginUrl() method.
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }

    /**
     * @inheritDoc
     */
    public function supports(Request $request)
    {
        // TODO: Implement supports() method.
        return self::LOGIN_ROUTE === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    /**
     * @inheritDoc
     */
    public function getCredentials(Request $request)
    {
        // TODO: Implement getCredentials() method.
        $credentials = [
            'email' => $request->request->get('email'),
            'password' => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['email']
        );

        return $credentials;
    }

    /**
     * @inheritDoc
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        // TODO: Implement getUser() method.
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            return null;
        }
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $credentials['email'] ]);
        $role = ['ROLE_MEMBER'];
        if (!$user) {
            $user = new User();
            $user->setEmail($credentials['email'])
                ->setPassword($this->passwordEncoder->encodePassword($user, $credentials['password']))
                ->setProvider('teman-belajar')
                ->setIsEmailVerified(false)->setRoles($role);
            $memberProfile = new Member();
            $memberProfile->setIsActive(true);
            $memberProfile->setUser($user);
            $user->setMemberProfile($memberProfile);
            $this->entityManager->persist($user);
            $this->entityManager->persist($memberProfile);
            $this->entityManager->flush();
        }else{
            $provider = 'teman-belajar';
            if ($provider !== $user->getProvider()) {
                $this->session->getFlashBag()->add('failed-email', 'Email Sudah Di Gunakan');
                return null;
            }

            if (in_array('ROLE_MEMBER', $user->getRoles(), true)) {
                $this->session->set('redirectTo', '/');
                $this->session->set('role', 'member');
            }
        }
        return $user;
    }

    /**
     * @inheritDoc
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        // TODO: Implement checkCredentials() method.
        $this->session->getFlashBag()->add('failed-password', 'Password Salah');
        return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
        // TODO: Implement onAuthenticationSuccess() method.
//        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
//
//        }
        return new RedirectResponse('/');
    }

    /**
     * @inheritDoc
     */
    public function getPassword($credentials): ?string
    {
        // TODO: Implement getPassword() method.
        return $credentials['password'];
    }

    /**
     * @return mixed
     */
    public function getFlashMessage()
    {
        return $this->flashMessage;
    }

    /**
     * @param mixed $flashMessage
     * @return LoginFormAuthenticator
     */
    public function setFlashMessage($flashMessage)
    {
        $this->flashMessage = $flashMessage;
        return $this;
    }
}
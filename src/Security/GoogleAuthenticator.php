<?php

namespace App\Security;

use App\Entity\Member;
use App\Entity\Tentor;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Security\Authenticator\SocialAuthenticator;
use League\OAuth2\Client\Provider\FacebookUser;
use League\OAuth2\Client\Provider\GoogleUser;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class GoogleAuthenticator extends SocialAuthenticator
{

    private $clientRegistry;
    private $em;
    private $router;
    private $redirect_to;
    private $session;
    private $flashMessage;

    public function __construct(ClientRegistry $clientRegistry, EntityManagerInterface $em, RouterInterface $router)
    {
        $this->clientRegistry = $clientRegistry;
        $this->em = $em;
        $this->router = $router;
        $this->session = new Session();
    }

    /**
     * @inheritDoc
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        // TODO: Implement start() method.
        if ($this->session->get('provider') === 'facebook') {
            return new RedirectResponse('/connect/facebook');
        }
        return new RedirectResponse('/connect/google');
    }

    /**
     * @inheritDoc
     */
    public function supports(Request $request)
    {
        // TODO: Implement supports() method.
        if ($this->session->get('provider') === 'facebook') {
            return $request->getPathInfo() === '/connect/facebook/check' && $request->isMethod('GET');
        }
        return $request->getPathInfo() === '/connect/google/check' && $request->isMethod('GET');
    }

    /**
     * @inheritDoc
     */
    public function getCredentials(Request $request)
    {
        // TODO: Implement getCredentials() method.
        if ($this->session->get('provider') === 'facebook') {
            return $this->fetchAccessToken($this->getFacebookClient());
        }
        return $this->fetchAccessToken($this->getGoogleClient());
    }

    /**
     * @inheritDoc
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        // TODO: Implement getUser() method.
        $email = null;
        $name = null;
        $avatar = null;
        if ($this->session->get('provider') === 'facebook') {
            /** @var FacebookUser $facebookUser */
            $facebookUser = $this->getFacebookClient()
                ->fetchUserFromToken($credentials);
            $email = $facebookUser->getEmail();
            $name = $facebookUser->getName();
            $avatar = $facebookUser->getPictureUrl();
        } elseif ($this->session->get('provider') === 'google') {

            /** @var GoogleUser $googleUser */
            $googleUser = $this->getGoogleClient()
                ->fetchUserFromToken($credentials);
            $email = $googleUser->getEmail();
            $name = $googleUser->getName();
            $avatar = $googleUser->getAvatar();
        }
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);
        $role = $this->session->get('role');
        $fcm = $this->session->get('fcm');
        switch ($role) {
            case 'member':
                $role = ['ROLE_MEMBER'];
                break;
            case 'tentor':
                $role = ['ROLE_TENTOR'];
                break;
            case 'admin':
                $role = ['ROLE_ADMIN'];
                break;
            default:
                $role = null;
                break;
        }
        if ($fcm === 'not_granted') {
            $fcm = null;
        }
        if (!$user) {
            $user = new User();
            $user->setEmail($email)
                ->setProvider($this->session->get('provider'))
                ->setWebFcmToken($fcm)
                ->setIsEmailVerified(true);
            $user->setRoles($role);
            if (in_array('ROLE_MEMBER', $role, true)) {
                $memberProfile = new Member();
                $memberProfile->setFullName($name);
                $memberProfile->setAvatar($avatar);
                $memberProfile->setIsActive(true);
                $memberProfile->setUser($user);
                $user->setMemberProfile($memberProfile);
                $this->em->persist($user);
                $this->em->persist($memberProfile);
            } else if (in_array('ROLE_TENTOR', $role, true)) {
                $tentorProfile = new Tentor();
                $tentorProfile->setFullName($name);
                $tentorProfile->setAvatar($avatar);
                $tentorProfile->setIsActive(true);
                $tentorProfile->setUser($user);
                $user->setMemberProfile($tentorProfile);
                $this->em->persist($user);
                $this->em->persist($tentorProfile);
            } else {
                return null;
            }
            $this->em->flush();
        } else {
            $provider = $this->session->get('provider');
            if ($provider !== $user->getProvider()) {
                $this->setFlashMessage('Email Sudah Di Gunakan');
                return null;
            }
            $user->setWebFcmToken($fcm);
            $this->em->flush();
            if (in_array('ROLE_ADMIN', $user->getRoles(), true)) {
                $this->session->set('redirectTo', '/admin');
                $this->session->set('role', 'admin');
            } elseif (in_array('ROLE_TENTOR', $user->getRoles(), true)) {
                $this->session->set('role', 'tentor');
                return null;
            } else {
                if (!$this->session->has('redirectTo')) {
                    $this->session->set('redirectTo', '/');
                }
                $this->session->set('role', 'member');
            }
        }

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        // TODO: Implement onAuthenticationFailure() method.
        if($this->session->get('role') === 'tentor'){
            return new RedirectResponse('/login-tentor');
        }
        $this->session->getFlashBag()->add('failed', $this->getFlashMessage());
        return new RedirectResponse('/login');
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
        // TODO: Implement onAuthenticationSuccess() method.
        return new RedirectResponse($this->session->get('redirectTo'));
    }

    /**
     * @return \KnpU\OAuth2ClientBundle\Client\OAuth2ClientInterface
     */
    private function getGoogleClient()
    {
        return $this->clientRegistry
            ->getClient('google');
    }

    /**
     * @return \KnpU\OAuth2ClientBundle\Client\OAuth2ClientInterface
     */
    private function getFacebookClient()
    {
        return $this->clientRegistry->getClient('facebook_main');
    }

    /**
     * @return mixed
     */
    public function getFlashMessage()
    {
        return $this->flashMessage;
    }

    /**
     * @param mixed $flashMessage
     * @return GoogleAuthenticator
     */
    public function setFlashMessage($flashMessage)
    {
        $this->flashMessage = $flashMessage;
        return $this;
    }
}
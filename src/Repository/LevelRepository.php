<?php
declare(strict_types=1);

namespace App\Repository;


use App\Entity\Level;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class LevelRepository
 * @package App\Repository
 */
class LevelRepository extends BaseRepo
{

    /**
     * MemberRepository constructor.
     *
     * @param ManagerRegistry $registry
     * @param $class
     */
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = Level::class;
        parent::__construct($registry);
    }


}
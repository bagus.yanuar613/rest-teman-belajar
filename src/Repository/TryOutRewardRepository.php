<?php


namespace App\Repository;

use App\Entity\TryOutReward;
use Doctrine\Persistence\ManagerRegistry;

class TryOutRewardRepository extends BaseRepo
{
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = TryOutReward::class;
        parent::__construct($registry);
        $this->isDTjoin = true;
    }

    public function DTJoin()
    {
        $this->select = ['a.id', 'a.freeAccess', 'a.minRegistrant', 't.id', 't.title'];
        $this->query
            ->leftJoin('a.tryOut', 't');
    }

}
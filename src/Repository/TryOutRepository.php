<?php


namespace App\Repository;


use App\Entity\TryOut;
use Doctrine\Persistence\ManagerRegistry;

class TryOutRepository extends BaseRepo
{
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = TryOut::class;
        parent::__construct($registry);
        $this->isDTjoin =  true;

    }

    public function DTJoin()
    {
        $this->select = ['c.id as c_id', 'l.id as l_id', 'a.isFree', 'a.direction', 'a.rules','a.codeAccess', 'a.isIRT', "a.irtChecked","a.startDay", "a.finalDay"];
        $this->query
            ->leftJoin('a.category', 'c')
            ->leftJoin('a.level', 'l');
    }

}
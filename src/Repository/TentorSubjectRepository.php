<?php

namespace App\Repository;

use App\Entity\Subject;
use App\Entity\TentorSubject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TentorSubject|null find($id, $lockMode = null, $lockVersion = null)
 * @method TentorSubject|null findOneBy(array $criteria, array $orderBy = null)
 * @method TentorSubject[]    findAll()
 * @method TentorSubject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TentorSubjectRepository extends BaseRepo
{
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = TentorSubject::class;
        parent::__construct($registry);
    }

    // /**
    //  * @return TentorSubject[] Returns an array of TentorSubject objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TentorSubject
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function dataTable($select, $start, $length, $orderBy, $orderDir, $search, $requiredWhere = null)
    {
        try {
            $query = $this->createQueryBuilder('a')
                          ->innerJoin('a.user', 'u')
                          ->leftJoin(Subject::class, 'sj', 'WITH', 'sj = a.subject');
            $count = $query->select('count(a.id)');

            if ($requiredWhere !== null) {
                $count->where($requiredWhere);
            }
            $count     = $count->getQuery()
                               ->getSingleScalarResult();
            $select[]  = 'u.id';
            $dataQuery = $query
                ->select($select);

            if ($requiredWhere !== null) {
                $dataQuery->where($requiredWhere);
            }

            if ( ! empty($search)) {
                $searchQuery = '';
                foreach ($select as $key => $field) {
                    if (strpos($field, ' as ')) {
                        $field = substr($field, 0, strpos($field, ' as '));
                    }
                    if ($key === 0) {
                        $searchQuery .= "$field LIKE '$search%'";
                    } else {
                        $searchQuery .= " OR $field LIKE '$search%'";
                    }
                }
                $searchQuery = "( " . $searchQuery . " )";

                if ($requiredWhere != NULL) {
                    $dataQuery->andWhere($searchQuery);
                } else {
                    $dataQuery->where($searchQuery);
                }
            }

            return [
                'count' => $count,
                'data'  => $dataQuery
                    ->setFirstResult($start)
                    ->setMaxResults($length)
                    ->orderBy($orderBy, strtoupper($orderDir))
                    ->getQuery()
                    ->getArrayResult(),
            ];
        } catch (\Exception $e) {
            dump($e);
            die();
        }
    }
}

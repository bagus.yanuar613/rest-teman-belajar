<?php

namespace App\Repository;

use App\Entity\AttendanceReport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AttendanceReport|null find($id, $lockMode = null, $lockVersion = null)
 * @method AttendanceReport|null findOneBy(array $criteria, array $orderBy = null)
 * @method AttendanceReport[]    findAll()
 * @method AttendanceReport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AttendanceReportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AttendanceReport::class);
    }

    // /**
    //  * @return AttendanceReport[] Returns an array of AttendanceReport objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AttendanceReport
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

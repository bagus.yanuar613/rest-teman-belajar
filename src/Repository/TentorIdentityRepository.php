<?php

namespace App\Repository;

use App\Entity\TentorIdentity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TentorIdentity|null find($id, $lockMode = null, $lockVersion = null)
 * @method TentorIdentity|null findOneBy(array $criteria, array $orderBy = null)
 * @method TentorIdentity[]    findAll()
 * @method TentorIdentity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TentorIdentityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TentorIdentity::class);
    }

    // /**
    //  * @return TentorIdentity[] Returns an array of TentorIdentity objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TentorIdentity
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

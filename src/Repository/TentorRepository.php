<?php

namespace App\Repository;

use App\Entity\Schedule;
use App\Entity\Subject;
use App\Entity\Tentor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class TentorRepository
 * @package App\Repository
 */
class TentorRepository extends BaseRepo
{

    /**
     * MemberRepository constructor.
     *
     * @param ManagerRegistry $registry
     * @param $class
     */
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = Tentor::class;
        parent::__construct($registry);
    }

    /**
     * @param $select
     * @param $start
     * @param $length
     * @param $orderBy
     * @param $orderDir
     * @param $search
     * @param null $requiredWhere
     *
     * @return array
     */
    public function dataTable($select, $start, $length, $orderBy, $orderDir, $search, $requiredWhere = null, $join = null, $groupBy = null, $addSelect = null)
    {
        try {
            $query = $this->createQueryBuilder('a')
                          ->innerJoin('a.user', 'u');
//                          ->leftJoin('u.tentorIdentity', 'i');
//                          ->leftJoin('u.schedule', 's')
//                          ->leftJoin('u.tentorSubject', 'ts')
//                          ->leftJoin(Subject::class, 'sj', 'WITH', 'sj = ts.subject');

            if ($join !== null) {
                foreach ($join as $j) {
                    if ($j['tipe'] == 'leftjoin') {
                        $query->leftJoin($j['join'], $j['alias']);
                    } elseif ($j['tipe'] == 'join') {
                        $query->join($j['join'], $j['alias']);
                    } else {
                        $query->innerJoin($j['join'], $j['alias']);
                    }
                }
            }

            if ($groupBy !== "") {
                $query->groupBy($groupBy);
            }
//            $count = $query->select('count(a.id)');
            $count = $query;

            if ($requiredWhere !== null) {
                $count->where($requiredWhere);
            }
//            $count     = $count->getQuery()
//                               ->getSingleScalarResult();

            $count = count(
                $query->getQuery()
                      ->getScalarResult()
            );

            $select[]  = 'u.id';
            $dataQuery = $query
                ->select($select);

            if ($addSelect !== null) {
                $dataQuery->addSelect($addSelect);
            }

            if ($requiredWhere !== null) {
                $dataQuery->where($requiredWhere);
            }

            if ( ! empty($search)) {
                $searchQuery = '';
                foreach ($select as $key => $field) {
                    if (strpos($field, ' as ')) {
                        $field = substr($field, 0, strpos($field, ' as '));
                    }
                    if ($key === 0) {
                        $searchQuery .= "$field LIKE '$search%'";
                    } else {
                        $searchQuery .= " OR $field LIKE '$search%'";
                    }
                }
                $searchQuery = "( " . $searchQuery . " )";

                if ($requiredWhere != NULL) {
                    $dataQuery->andWhere($searchQuery);
                } else {
                    $dataQuery->where($searchQuery);
                }
            }
//            dump($dataQuery);die();
            return [
                'count' => $count,
                'data'  => $dataQuery
                    ->setFirstResult($start)
                    ->setMaxResults($length)
                    ->orderBy($orderBy, strtoupper($orderDir))
                    ->getQuery()
                    ->getArrayResult(),
            ];
        } catch (\Exception $e) {
            dump($e);
            die();
        }
    }

    /**
     * @param $select
     * @param null $requiredWhere
     *
     * @return array
     */
    public function card($select, $requiredWhere = null)
    {
        try {
            $query = $this->createQueryBuilder('a');

            $count = $query->select('count(a.id)')
                           ->innerJoin('a.user', 'u')
                           ->leftJoin('u.schedule', 's')
                           ->leftJoin('a.grade', 'g')
                           ->leftJoin('u.tentorIdentity', 'i');

            if ($requiredWhere !== null) {
                $count->where($requiredWhere);
            }
            $count     = $count->getQuery()
                               ->getSingleScalarResult();
            $select[]  = ' a.id';
            $dataQuery = $query
                ->select($select);
            if ($requiredWhere !== null) {
                $dataQuery->where($requiredWhere);
            }

            if ( ! empty($search)) {
                foreach ($select as $key => $field) {
                    if ($key === 0) {
                        $dataQuery->where("$field LIKE '$search%' ");
                    } else {
                        $dataQuery->orWhere("$field LIKE '$search%' ");
                    }
                }
            }

            return [
                'count' => $count,
                'data'  => $dataQuery
                    ->getQuery()
                    ->getArrayResult(),
            ];
        } catch (\Exception $e) {
            dump($e);
            die();
        }
    }

}
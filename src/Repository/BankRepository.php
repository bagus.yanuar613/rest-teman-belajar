<?php

namespace App\Repository;

use App\Entity\Bank;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Bank|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bank|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bank[]    findAll()
 * @method Bank[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BankRepository extends BaseRepo
{
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = Bank::class;
        parent::__construct($registry);
    }

    // /**
    //  * @return Bank[] Returns an array of Bank objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Bank
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @param $select
     * @param $start
     * @param $length
     * @param $orderBy
     * @param $orderDir
     * @param $search
     * @param null $requiredWhere
     *
     * @return array
     */
    public function dataTable($select, $start, $length, $orderBy, $orderDir, $search, $requiredWhere = null, $join = null, $groupBy = null, $addSelect = null)
    {
        try {
            $query = $this->createQueryBuilder('a');

            if ($join !== null) {
                foreach ($join as $j) {
                    if ($j['tipe'] == 'leftjoin') {
                        $query->leftJoin($j['join'], $j['alias']);
                    } elseif ($j['tipe'] == 'join') {
                        $query->join($j['join'], $j['alias']);
                    } else {
                        $query->innerJoin($j['join'], $j['alias']);
                    }
                }
            }
            if ($groupBy !== "") {
                $query->groupBy($groupBy);
            }
            $count = $query;

            if ($requiredWhere !== null) {
                $count->where($requiredWhere);
            }

            $count = count(
                $query->getQuery()
                      ->getScalarResult()
            );

//            $count     = $count->getQuery()
//                               ->getSingleScalarResult();

            $select[] = ' a.id';
//            $select[] = ' cat.name';

//            $add = ['a.id, a.createdAt, a.transactionId, t.fullName as tentor, m.fullName as member,m.class, a.amount, a.isActive, s.name as subject'];
            $dataQuery = $query
                ->select($select);
            if ($requiredWhere !== null) {
                $dataQuery->where($requiredWhere);
            }

            if ($addSelect !== null) {
                $dataQuery->addSelect($addSelect);
            }

            if (strpos($orderBy, ' as ')) {
                $orderBy = substr($orderBy, 0, strpos($orderBy, ' as '));
            }

            if ( ! empty($search)) {
                $searchQuery = '';
                foreach ($select as $key => $field) {
                    if (strpos($field, ' as ')) {
                        $field = substr($field, 0, strpos($field, ' as '));
                    }
                    if ($key === 0) {
                        $searchQuery .= "$field LIKE '$search%'";
                    } else {
                        $searchQuery .= " OR $field LIKE '$search%'";
                    }
                }
                $searchQuery = "( " . $searchQuery . " )";

                if ($requiredWhere != NULL) {
                    $dataQuery->andWhere($searchQuery);
                } else {
                    $dataQuery->where($searchQuery);
                }
            }

            return [
                'count' => $count,
                'data'  => $dataQuery
                    ->setFirstResult($start)
                    ->setMaxResults($length)
                    ->orderBy($orderBy, strtoupper($orderDir))
                    ->getQuery()
                    ->getArrayResult(),
            ];
        } catch (\Exception $e) {
            dump($e);
            die();
        }
    }
}

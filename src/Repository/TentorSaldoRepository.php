<?php

namespace App\Repository;

use App\Entity\Tentor;
use App\Entity\TentorSaldo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TentorSaldo|null find($id, $lockMode = null, $lockVersion = null)
 * @method TentorSaldo|null findOneBy(array $criteria, array $orderBy = null)
 * @method TentorSaldo[]    findAll()
 * @method TentorSaldo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TentorSaldoRepository extends BaseRepo
{
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = TentorSaldo::class;
        parent::__construct($registry);
    }

    // /**
    //  * @return TentorSaldo[] Returns an array of TentorSaldo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TentorSaldo
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @param $select
     * @param $start
     * @param $length
     * @param $orderBy
     * @param $orderDir
     * @param $search
     * @param null $requiredWhere
     * @param null $join
     * @param null $groupBy
     * @param null $addSelect
     *
     * @return array
     */
    public function dataTable($select, $start, $length, $orderBy, $orderDir, $search, $requiredWhere = null, $join = null, $groupBy = null, $addSelect = null)
    {
        try {
            $query = $this->createQueryBuilder('a')
                          ->leftJoin('a.user', 'u');

            if ($join !== null) {
                foreach ($join as $j) {
                    if ($j['tipe'] == 'leftjoin') {
                        $query->leftJoin($j['join'], $j['alias']);
                    } elseif ($j['tipe'] == 'join') {
                        $query->join($j['join'], $j['alias']);
                    } else {
                        $query->innerJoin($j['join'], $j['alias']);
                    }
                }
            }
            if ($groupBy !== "") {
                $query->groupBy($groupBy);
            }
            $count = $query;

            if ($requiredWhere !== null) {
                $count->where($requiredWhere);
            }

            $count = count(
                $query->getQuery()
                      ->getScalarResult()
            );

//            $count     = $count->getQuery()
//                               ->getSingleScalarResult();

            $select[] = ' a.id';
//            $select[] = ' cat.name';

//            $add = ['a.id, a.createdAt, a.transactionId, t.fullName as tentor, m.fullName as member,m.class, a.amount, a.isActive, s.name as subject'];
            $dataQuery = $query
                ->select($select);
            if ($requiredWhere !== null) {
                $dataQuery->where($requiredWhere);
            }

            if ($addSelect !== null) {
                $dataQuery->addSelect($addSelect);
            }

            if ( ! empty($search)) {
                foreach ($select as $key => $field) {
                    if (strpos($field, ' as ')) {
                        $field = substr($field, 0, strpos($field, ' as '));
                    }
                    if ($key === 0) {
                        $dataQuery->where("$field LIKE '$search%' ");
                    } else {
                        $dataQuery->orWhere("$field LIKE '$search%' ");
                    }
                }
            }


            return [
                'count' => $count,
                'data'  => $dataQuery
                    ->setFirstResult($start)
                    ->setMaxResults($length)
                    ->orderBy($orderBy, strtoupper($orderDir))
                    ->getQuery()
                    ->getArrayResult(),
            ];
        } catch (\Exception $e) {
            dump($e);
            die();
        }
    }


    /**
     * @param $select
     * @param null $requiredWhere
     *
     * @return array
     */
    public function card($select, $requiredWhere = null)
    {
        try {
            $query = $this->createQueryBuilder('a');

            $count = $query->select('count(a.id)')
                           ->innerJoin('a.user', 'u')
                           ->leftJoin('u.tentorProfile', 't');

            if ($requiredWhere !== null) {
                $count->where($requiredWhere);
            }
            $count     = $count->getQuery()
                               ->getSingleScalarResult();
            $select[]  = ' a.id';
            $dataQuery = $query
                ->select($select);
            if ($requiredWhere !== null) {
                $dataQuery->where($requiredWhere);
            }

            if ( ! empty($search)) {
                foreach ($select as $key => $field) {
                    if ($key === 0) {
                        $dataQuery->where("$field LIKE '$search%' ");
                    } else {
                        $dataQuery->orWhere("$field LIKE '$search%' ");
                    }
                }
            }

            return [
                'count' => $count,
                'data'  => $dataQuery
                    ->getQuery()
                    ->getArrayResult(),
            ];
        } catch (\Exception $e) {
            dump($e);
            die();
        }
    }
}

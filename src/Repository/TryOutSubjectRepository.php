<?php


namespace App\Repository;


use App\Entity\TryOutSubject;
use Doctrine\Common\Persistence\ManagerRegistry;

class TryOutSubjectRepository extends BaseRepo
{
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = TryOutSubject::class;
        parent::__construct($registry);
        $this->isDTjoin =  true;
    }

    public function DTJoin()
    {
        $this->select = ['s.id as subject', 'ms.id as mainsubject'];
        $this->query
            ->leftJoin('a.subject', 's')
            ->leftJoin('a.mainSubject', 'ms')
            ->leftJoin('a.tryout', 'to');
    }

}
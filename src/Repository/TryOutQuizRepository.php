<?php


namespace App\Repository;


use App\Entity\TryOutQuiz;
use Doctrine\Persistence\ManagerRegistry;

class TryOutQuizRepository extends BaseRepo
{
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = TryOutQuiz::class;
        parent::__construct($registry);
        $this->isDTjoin =  true;
    }

    public function DTJoin()
    {
//        $this->select = ['s.id as subject', 'ms.id as mainsubject'];
        $this->query
            ->leftJoin('a.tryOut', 'ts');
//            ->leftJoin('a.mainSubject', 'ms');
    }

}
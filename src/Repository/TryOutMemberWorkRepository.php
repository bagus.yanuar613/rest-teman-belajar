<?php


namespace App\Repository;


use App\Entity\TryOutMemberWork;
use Doctrine\Persistence\ManagerRegistry;

class TryOutMemberWorkRepository extends BaseRepo
{
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = TryOutMemberWork::class;
        parent::__construct($registry);
    }

}
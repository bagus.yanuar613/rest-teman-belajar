<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class BaseRepo
 * @package App\Repository
 */
class BaseRepo extends ServiceEntityRepository
{
    protected $class;

    protected $isDTjoin = false;
    protected $select;
    /** @var QueryBuilder */
    protected $query;

    /**
     * MemberRepository constructor.
     *
     * @param ManagerRegistry $registry
     * @param $class
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, $this->class);
    }

    public function getMyOptionForComboBox(string $where = null)
    {
        $query = $this->createQueryBuilder('a')
            ->select(['a.id as value', 'a.name as label']);
        if (!empty($where)) {
            $query->where($where);
        }
        return $query->getQuery()
            ->getScalarResult();
    }

    /**
     * @param $clauses
     * @param $fields
     * @return bool
     */
    public function insertIfNoExists($clauses, $fields)
    {
        try {
            $query = $this->createQueryBuilder("a")
                ->select("a");
            $where = null;
            foreach ($clauses as $key => $value) {
                $where .= $where ? " AND a.$key = :$key " : " a.$key = :$key ";
            }
            $query = $query->where($where)
                ->setParameters($clauses)
                ->getQuery()
                ->getSingleResult();

            return false;

        } catch (\Exception $e) {
            $entry = new $this->class();
            foreach ($fields as $field => $fValue) {
                $method = "set" . ucfirst($field);
                $entry->$method($fValue);
            }
            $this->_em->persist($entry);

            $this->_em->flush();
            return true;
        }


    }


    /**
     * @param $clauses
     * @param $fields
     * @return bool
     */
    public function insertOrUpdate($clauses, $fields)
    {
        try {
            $query = $this->createQueryBuilder("a")
                ->select("a");
            $where = null;
            foreach ($clauses as $key => $value) {
                $where .= $where ? " AND a.$key = :$key " : " a.$key = :$key ";
            }
            $query = $query->where($where)
                ->setParameters($clauses)
                ->getQuery()
                ->getSingleResult();

            $entry = $query;
            foreach ($fields as $field => $fValue) {
                $method = "set" . ucfirst($field);
                $entry->$method($fValue);
            }

        } catch (\Exception $e) {
            $entry = new $this->class();
            foreach ($fields as $field => $fValue) {
                $method = "set" . ucfirst($field);
                $entry->$method($fValue);
            }
        }

        try {
            $this->_em->persist($entry);

            $this->_em->flush();
            return true;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function DTJoin()
    {

    }

    /**
     * @param $where
     *
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneIdArray($id)
    {
        return $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getScalarResult();

    }

    /**
     * @return array|int|string
     */
    public function findAllScallar()
    {
        return $this->createQueryBuilder('a')
            ->select('a')
            ->getQuery()->getScalarResult();
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function selectBuilder()
    {
        return $this->createQueryBuilder('a')
            ->select('a');
    }

    /**
     * @param $select
     * @param $start
     * @param $length
     * @param $orderBy
     * @param $orderDir
     * @param $search
     * @param null $requiredWhere
     *
     * @return array
     */
    public function dataTable($select, $start, $length, $orderBy, $orderDir, $search, $requiredWhere = null)
    {
        try {
            $this->query = $this->createQueryBuilder('a');
            $count = $this->query->select('count(a.id)');
            if ($this->isDTjoin) {
                $this->DTJoin();
            }
            if ($requiredWhere !== null) {
                $count->where($requiredWhere);
            }

            $count = $count->getQuery()
                ->getSingleScalarResult();

            $select[] = ' a.id';
            if (is_array($this->select)) {
                $select = array_merge($select, $this->select);
            }
            $this->query
                ->select($select);

            if ($requiredWhere !== null) {
                $this->query->where($requiredWhere);
            }

            if (!empty($search)) {
                $searchQuery = '';
                foreach ($select as $key => $field) {
                    if ($key === 0) {
                        $searchQuery .= "$field LIKE '$search%'";
                    } else {
                        $searchQuery .= " OR $field LIKE '$search%'";
                    }
                }
                $searchQuery = "( " . $searchQuery . " )";

                if ($requiredWhere != NULL) {
                    $this->query->andWhere($searchQuery);
                } else {
                    $this->query->where($searchQuery);
                }
            }

            return [
                'count' => $count,
                'data' => $this->query
                    ->setFirstResult($start)
                    ->setMaxResults($length)
                    ->orderBy($orderBy, strtoupper($orderDir))
                    ->getQuery()
                    ->getArrayResult(),
            ];
        } catch (\Exception $e) {
            dump($e);
            die();
        }
    }

    /**
     * @param $select
     * @param null $requiredWhere
     *
     * @return array
     */
    public function card($select, $requiredWhere = null)
    {
        try {
            $this->query = $this->createQueryBuilder('a');
            $count = $this->query->select('count(a.id)');

            if ($requiredWhere !== null) {
                $count->where($requiredWhere);
            }
            $count = $count->getQuery()
                ->getSingleScalarResult();
            $select[] = ' a.id';
            $dataQuery = $this->query
                ->select($select);
            if ($requiredWhere !== null) {
                $dataQuery->where($requiredWhere);
            }

            if (!empty($search)) {
                foreach ($select as $key => $field) {
                    if ($key === 0) {
                        $dataQuery->where("$field LIKE '$search%' ");
                    } else {
                        $dataQuery->orWhere("$field LIKE '$search%' ");
                    }
                }
            }

            return [
                'count' => $count,
                'data' => $dataQuery
                    ->getQuery()
                    ->getArrayResult(),
            ];
        } catch (\Exception $e) {
            dump($e);
            die();
        }
    }
}
<?php


namespace App\Repository;


use App\Entity\ProductCheckout;
use Doctrine\Persistence\ManagerRegistry;

class ProductCheckoutRepository  extends BaseRepo
{
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = ProductCheckout::class;
        parent::__construct($registry);
    }

}
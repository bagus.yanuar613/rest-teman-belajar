<?php

namespace App\Repository;

use App\Entity\PromoToRules;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PromoToRules|null find($id, $lockMode = null, $lockVersion = null)
 * @method PromoToRules|null findOneBy(array $criteria, array $orderBy = null)
 * @method PromoToRules[]    findAll()
 * @method PromoToRules[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PromoToRulesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PromoToRules::class);
    }

    // /**
    //  * @return PromoToRules[] Returns an array of PromoToRules objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PromoToRules
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

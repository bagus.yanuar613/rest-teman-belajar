<?php


namespace App\Repository;


use App\Entity\TryOutRegistrantAnswer;
use Doctrine\Persistence\ManagerRegistry;

class TryOutRegistrantAnswerRepository extends BaseRepo
{
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = TryOutRegistrantAnswer::class;
        parent::__construct($registry);
    }

}
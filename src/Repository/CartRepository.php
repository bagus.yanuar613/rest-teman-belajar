<?php

namespace App\Repository;

use App\Entity\Cart;
use App\Entity\Tentor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Cart|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cart|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cart[]    findAll()
 * @method Cart[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CartRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cart::class);
    }

    // /**
    //  * @return Cart[] Returns an array of Cart objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Cart
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @param $select
     * @param $start
     * @param $length
     * @param $orderBy
     * @param $orderDir
     * @param $search
     * @param null $requiredWhere
     * @param null $join
     * @param null $groupBy
     * @param null $addSelect
     *
     * @return array
     */
    public function dataTable($select, $start, $length, $orderBy, $orderDir, $search, $requiredWhere = null, $join = null, $groupBy = null, $addSelect = null)
    {
        try {
            $query = $this->createQueryBuilder('a')
                          ->leftJoin('a.subscription', 's')
                          ->leftJoin('s.user', 'u')
                          ->leftJoin('u.memberProfile', 'm')
                          ->leftJoin('s.tentorSubject', 'ts')
                          ->leftJoin(Tentor::class, 'ten', 'WITH', 'ten.user = ts.user')
                          ->leftJoin('ts.category', 'c')
                          ->leftJoin('a.transaction', 't')
                          ->leftJoin('a.method', 'mt')
                          ->leftJoin('a.district', 'd');

            if ($join !== null) {
                foreach ($join as $j) {
                    if ($j['tipe'] == 'leftjoin') {
                        $query->leftJoin($j['join'], $j['alias']);
                    } elseif ($j['tipe'] == 'join') {
                        $query->join($j['join'], $j['alias']);
                    } else {
                        $query->innerJoin($j['join'], $j['alias']);
                    }
                }
            }
            if ($groupBy !== "") {
                $query->groupBy($groupBy);
            }
            $count = $query;

            if ($requiredWhere !== null) {
                $count->where($requiredWhere);
            }

            $count = count(
                $query->getQuery()
                      ->getScalarResult()
            );

//            $count     = $count->getQuery()
//                               ->getSingleScalarResult();

            $select[] = ' a.id';
//            $select[] = ' cat.name';

//            $add = ['a.id, a.createdAt, a.transactionId, t.fullName as tentor, m.fullName as member,m.class, a.amount, a.isActive, s.name as subject'];
            $dataQuery = $query
                ->select($select);
            if ($requiredWhere !== null) {
                $dataQuery->where($requiredWhere);
            }

            if ($addSelect !== null) {
                $dataQuery->addSelect($addSelect);
            }

            if (strpos($orderBy, ' as ')) {
                $orderBy = substr($orderBy, 0, strpos($orderBy, ' as '));
            }

            if ( ! empty($search)) {
                $searchQuery = '';
                foreach ($select as $key => $field) {
                    if (strpos($field, ' as ')) {
                        $field = substr($field, 0, strpos($field, ' as '));
                    }
                    if ($key === 0) {
                        $searchQuery .= "$field LIKE '$search%'";
                    } else {
                        $searchQuery .= " OR $field LIKE '$search%'";
                    }
                }
                $searchQuery = "( ".$searchQuery." )";

                if ($requiredWhere != null) {
                    $dataQuery->andWhere($searchQuery);
                } else {
                    $dataQuery->where($searchQuery);
                }
            }

            return [
                'count' => $count,
                'data'  => $dataQuery
                    ->setFirstResult($start)
                    ->setMaxResults($length)
                    ->orderBy($orderBy, strtoupper($orderDir))
                    ->getQuery()
                    ->getArrayResult(),
            ];
        } catch (\Exception $e) {
            dump($e);
            die();
        }
    }

    /**
     * @param $select
     * @param null $requiredWhere
     * @param null $join
     * @param null $addSelect
     *
     * @return array
     */
    public function card($select, $requiredWhere = null, $join = null, $addSelect = null)
    {
        try {
            $query = $this->createQueryBuilder('a')
                          ->leftJoin('a.subscription', 's')
                          ->leftJoin('s.user', 'u')
                          ->leftJoin('u.memberProfile', 'm')
                          ->leftJoin('s.tentorSubject', 'ts')
                          ->leftJoin('ts.user', 'tu')
                          ->leftJoin(Tentor::class, 'ten', 'WITH', 'ten.user = ts.user')
                          ->leftJoin('ts.category', 'c')
                          ->leftJoin('a.transaction', 't')
                          ->leftJoin('a.method', 'mt')
                          ->leftJoin('a.district', 'd');


            $count = $query->select('count(a.id)');

            if ($join !== null) {
                foreach ($join as $j) {
                    if ($j['tipe'] == 'leftjoin') {
                        $query->leftJoin($j['join'], $j['alias']);
                    } elseif ($j['tipe'] == 'join') {
                        $query->join($j['join'], $j['alias']);
                    } else {
                        $query->innerJoin($j['join'], $j['alias']);
                    }
                }
            }

            if ($requiredWhere !== null) {
                $count->where($requiredWhere);
            }
            $count     = $count->getQuery()
                               ->getSingleScalarResult();
            $select[]  = ' a.id';
            $dataQuery = $query
                ->select($select);

            if ($requiredWhere !== null) {
                $dataQuery->where($requiredWhere);
            }

            if ($addSelect !== null) {
                $dataQuery->addSelect($addSelect);
            }
//            dump($dataQuery->getQuery()->getArrayResult());die();

            if ( ! empty($search)) {
                $searchQuery = '';
                foreach ($select as $key => $field) {
                    if (strpos($field, ' as ')) {
                        $field = substr($field, 0, strpos($field, ' as '));
                    }
                    if ($key === 0) {
                        $searchQuery .= "$field LIKE '$search%'";
                    } else {
                        $searchQuery .= " OR $field LIKE '$search%'";
                    }
                }
                $searchQuery = "( ".$searchQuery." )";

                if ($requiredWhere != null) {
                    $dataQuery->andWhere($searchQuery);
                } else {
                    $dataQuery->where($searchQuery);
                }
            }

//dump($dataQuery);
            return [
                'count' => $count,
                'data'  => $dataQuery
                    ->getQuery()
                    ->getArrayResult(),
            ];
        } catch (\Exception $e) {
            dump($e);
            die();
        }
    }
}

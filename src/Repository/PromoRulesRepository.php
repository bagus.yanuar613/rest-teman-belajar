<?php

namespace App\Repository;

use App\Entity\PromoRules;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PromoRules|null find($id, $lockMode = null, $lockVersion = null)
 * @method PromoRules|null findOneBy(array $criteria, array $orderBy = null)
 * @method PromoRules[]    findAll()
 * @method PromoRules[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PromoRulesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PromoRules::class);
    }

    // /**
    //  * @return PromoRules[] Returns an array of PromoRules objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PromoRules
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

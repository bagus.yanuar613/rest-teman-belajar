<?php

namespace App\Repository;

use App\Entity\Unsubscription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Unsubscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method Unsubscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method Unsubscription[]    findAll()
 * @method Unsubscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UnsubscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Unsubscription::class);
    }

    // /**
    //  * @return Unsubscription[] Returns an array of Unsubscription objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Unsubscription
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

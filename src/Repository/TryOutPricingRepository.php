<?php


namespace App\Repository;


use App\Entity\TryOutPricing;
use Doctrine\Persistence\ManagerRegistry;

class TryOutPricingRepository extends BaseRepo
{
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = TryOutPricing::class;
        parent::__construct($registry);
        $this->isDTjoin = true;
    }

    public function DTJoin()
    {
        $this->select = ['a.id', 'a.price',  'a.min', 'a.max', 't.title'];
        $this->query
            ->leftJoin('a.tryOut', 't');
    }

}
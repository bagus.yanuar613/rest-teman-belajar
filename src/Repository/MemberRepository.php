<?php

namespace App\Repository;

use App\Controller\BaseController;
use App\Entity\Member;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class MemberRepository
 * @package App\Repository
 */
class MemberRepository extends BaseRepo
{

    /**
     * MemberRepository constructor.
     *
     * @param ManagerRegistry $registry
     * @param $class
     */
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = Member::class;
        parent::__construct($registry);
    }

    /**
     * @param $select
     * @param $start
     * @param $length
     * @param $orderBy
     * @param $orderDir
     * @param $search
     * @param null $requiredWhere
     * @param null $join
     * @param null $groupBy
     * @param null $addSelect
     *
     * @return array
     */
    public function dataTable($select, $start, $length, $orderBy, $orderDir, $search, $requiredWhere = null, $join = null, $groupBy = null, $addSelect = null)
    {
        try {
            $query = $this->createQueryBuilder('a')
                          ->innerJoin('a.user', 'u')
                          ->leftJoin('a.class', 'mc')
                          ->leftJoin('mc.memberClassParent', 'cp');

            $count = $query->select('count(a.id)');

            if ($requiredWhere !== null) {
                $count->where($requiredWhere);
            }
            if ($join !== null) {
                foreach ($join as $j) {
                    if ($j['tipe'] == 'leftjoin') {
                        $query->leftJoin($j['join'], $j['alias']);
                    } elseif ($j['tipe'] == 'join') {
                        $query->join($j['join'], $j['alias']);
                    } else {
                        $query->innerJoin($j['join'], $j['alias']);
                    }
                }
            }

            if ($groupBy !== "") {
                $count->groupBy($groupBy);
            }
            $count = $count->getQuery()
                           ->getSingleScalarResult();

            $select[] = 'u.id';

            $dataQuery = $query
                ->select($select);

            if ($requiredWhere !== null) {
                $dataQuery->where($requiredWhere);
            }

            if ($addSelect !== null) {
                $dataQuery->addSelect($addSelect);
            }

//            dump($dataQuery);die();

            if ( ! empty($search)) {
                $searchQuery = '';
                foreach ($select as $key => $field) {
                    if (strpos($field, ' as ')) {
                        $field = substr($field, 0, strpos($field, ' as '));
                    }
                    if ($key === 0) {
                        $searchQuery .= "$field LIKE '$search%'";
                    } else {
                        $searchQuery .= " OR $field LIKE '$search%'";
                    }
                }
                $searchQuery = "( ".$searchQuery." )";

                if ($requiredWhere != null) {
                    $dataQuery->andWhere($searchQuery);
                } else {
                    $dataQuery->where($searchQuery);
                }
            }
            return [
                'count' => $count,
                'data'  => $dataQuery
                    ->setFirstResult($start)
                    ->setMaxResults($length)
                    ->orderBy($orderBy, strtoupper($orderDir))
                    ->getQuery()
                    ->getArrayResult(),
            ];
        } catch (\Exception $e) {
            dump($e);
            die();
        }
    }

    /**
     * @param $select
     * @param null $requiredWhere
     *
     * @return array
     */
    public function card($select, $requiredWhere = null)
    {
        try {
            $query = $this->createQueryBuilder('a')
                          ->innerJoin('a.user', 'u')
                          ->leftJoin('a.class', 'mc')
                          ->leftJoin('mc.memberClassParent', 'cp');
            $count = $query->select('count(a.id)');

            if ($requiredWhere !== null) {
                $count->where($requiredWhere);
            }
            $count     = $count->getQuery()
                               ->getSingleScalarResult();
            $select[]  = ' a.id';
            $dataQuery = $query
                ->select($select);
            if ($requiredWhere !== null) {
                $dataQuery->where($requiredWhere);
            }

            if ( ! empty($search)) {
                foreach ($select as $key => $field) {
                    if ($key === 0) {
                        $dataQuery->where("$field LIKE '$search%' ");
                    } else {
                        $dataQuery->orWhere("$field LIKE '$search%' ");
                    }
                }
            }
//            $dataQuery->addSelect(['cp.name as class','mc.name parentClass']);

            return [
                'count' => $count,
                'data'  => $dataQuery
                    ->getQuery()
                    ->getArrayResult(),
            ];
        } catch (\Exception $e) {
            dump($e);
            die();
        }
    }
}
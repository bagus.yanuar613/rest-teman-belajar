<?php

namespace App\Repository;

use App\Entity\MemberClass;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MemberClass|null find($id, $lockMode = null, $lockVersion = null)
 * @method MemberClass|null findOneBy(array $criteria, array $orderBy = null)
 * @method MemberClass[]    findAll()
 * @method MemberClass[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MemberClassRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MemberClass::class);
    }

    // /**
    //  * @return MemberClass[] Returns an array of MemberClass objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MemberClass
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Repository;

use App\Entity\Admin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Admin|null find($id, $lockMode = null, $lockVersion = null)
 * @method Admin|null findOneBy(array $criteria, array $orderBy = null)
 * @method Admin[]    findAll()
 * @method Admin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdminRepository extends BaseRepo
{
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = Admin::class;
        parent::__construct($registry);
    }

    // /**
    //  * @return Admin[] Returns an array of Admin objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Admin
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @param $select
     * @param $start
     * @param $length
     * @param $orderBy
     * @param $orderDir
     * @param $search
     * @param null $requiredWhere
     *
     * @return array
     */
    public function dataTable($select, $start, $length, $orderBy, $orderDir, $search, $requiredWhere = null)
    {
        try {
            $query = $this->createQueryBuilder('a');
            $count = $query->select('count(a.id)')
                           ->join('a.user', 'u');
            if ($requiredWhere !== null) {
                $count->where($requiredWhere);
            }
            $count    = $count->getQuery()
                              ->getSingleScalarResult();
            $select[] = 'u.id';
            $select[] = 'a.fullName';
            $select[] = 'u.email';
            $select[] = 'a.privilege';

            $dataQuery = $query
                ->select($select);
            if ($requiredWhere !== null) {
                $dataQuery->where($requiredWhere);
            }

            if ( ! empty($search)) {
                $searchQuery = '';
                foreach ($select as $key => $field) {
                    if ($key === 0) {
                        $searchQuery .= "$field LIKE '$search%'";
                    } else {
                        $searchQuery .= " OR $field LIKE '$search%'";
                    }
                }
                $searchQuery = "( " . $searchQuery . " )";

                if ($requiredWhere != NULL) {
                    $dataQuery->andWhere($searchQuery);
                } else {
                    $dataQuery->where($searchQuery);
                }
            }

            return [
                'count' => $count,
                'data'  => $dataQuery
                    ->setFirstResult($start)
                    ->setMaxResults($length)
                    ->orderBy($orderBy, strtoupper($orderDir))
                    ->getQuery()
                    ->getScalarResult(),
            ];
        } catch (\Exception $e) {
            dump($e);
            die();
        }
    }
}

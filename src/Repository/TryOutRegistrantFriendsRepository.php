<?php


namespace App\Repository;


use App\Entity\TryOutRegistrantFriends;
use Doctrine\Persistence\ManagerRegistry;

class TryOutRegistrantFriendsRepository extends BaseRepo
{
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = TryOutRegistrantFriends::class;
        parent::__construct($registry);
    }

}
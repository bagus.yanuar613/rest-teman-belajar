<?php

namespace App\Repository;

use App\Entity\TryOutFreeAccess;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TryOutFreeAccess|null find($id, $lockMode = null, $lockVersion = null)
 * @method TryOutFreeAccess|null findOneBy(array $criteria, array $orderBy = null)
 * @method TryOutFreeAccess[]    findAll()
 * @method TryOutFreeAccess[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TryOutFreeAccessRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TryOutFreeAccess::class);
    }

    // /**
    //  * @return TryOutFreeAccess[] Returns an array of TryOutFreeAccess objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TryOutFreeAccess
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

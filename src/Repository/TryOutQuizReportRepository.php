<?php


namespace App\Repository;


use App\Entity\TryOutQuizReport;
use Doctrine\Persistence\ManagerRegistry;

class TryOutQuizReportRepository extends BaseRepo
{
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = TryOutQuizReport::class;
        parent::__construct($registry);
        $this->isDTjoin =  true;
    }
}
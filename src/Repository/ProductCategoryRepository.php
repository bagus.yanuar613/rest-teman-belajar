<?php


namespace App\Repository;


use App\Entity\ProductCategory;
use Doctrine\Persistence\ManagerRegistry;

class ProductCategoryRepository extends BaseRepo
{
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = ProductCategory::class;
        parent::__construct($registry);
    }

}
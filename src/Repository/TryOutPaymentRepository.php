<?php


namespace App\Repository;


use App\Entity\TryOutFreeAccess;
use App\Entity\TryOutPayment;
use App\Entity\TryOutReward;
use Doctrine\Persistence\ManagerRegistry;

class TryOutPaymentRepository extends BaseRepo
{
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = TryOutPayment::class;
        parent::__construct($registry);
    }

    /**
     * @param $select
     * @param $start
     * @param $length
     * @param $orderBy
     * @param $orderDir
     * @param $search
     * @param null $requiredWhere
     *
     * @return array
     */
    public function dataTable($select, $start, $length, $orderBy, $orderDir, $search, $requiredWhere = null, $join = null, $groupBy = null, $addSelect = null)
    {
        try {
            $query = $this->createQueryBuilder('a')
                          ->leftJoin('a.user', 'u')
                          ->join('u.memberProfile', 'm')
                          ->leftJoin('a.tryOut', 'tr')
                          ->leftJoin(TryOutReward::class, 're', 'WITH', 'tr.id = re.tryOut')
                          ->leftJoin('tr.category', 'c')
                          ->leftJoin('tr.level', 'l')
                          ->leftJoin('a.paymentMethod', 'p')
                          ->leftJoin('p.bank', 'b')
                          ->leftJoin(TryOutFreeAccess::class, 'fr', 'WITH', 'fr.registration = a.id');

            $count = $query->select('count(a.id)');

            if ($join !== null) {
                foreach ($join as $j) {
                    if ($j['tipe'] == 'leftjoin') {
                        $query->leftJoin($j['join'], $j['alias']);
                    } elseif ($j['tipe'] == 'join') {
                        $query->join($j['join'], $j['alias']);
                    } else {
                        $query->innerJoin($j['join'], $j['alias']);
                    }
                }
            }

            if ($groupBy !== "") {
                $query->groupBy($groupBy);
            }

            if ($requiredWhere !== null) {
                $count->where($requiredWhere);
            }
            $count     = $count->getQuery()
                               ->getSingleScalarResult();
            $select[]  = 'a.id';
            $dataQuery = $query
                ->select($select);

            if ($requiredWhere !== null) {
                $dataQuery->where($requiredWhere);
            }

            if ($addSelect !== null) {
                $dataQuery->addSelect($addSelect);
            }

            if ( ! empty($search)) {
                $searchQuery = '';
                foreach ($select as $key => $field) {
                    if (strpos($field, ' as ')) {
                        $field = substr($field, 0, strpos($field, ' as '));
                    }
                    if ($key === 0) {
                        $searchQuery .= "$field LIKE '$search%'";
                    } else {
                        $searchQuery .= " OR $field LIKE '$search%'";
                    }
                }
                $searchQuery = "( ".$searchQuery." )";

                if ($requiredWhere != null) {
                    $dataQuery->andWhere($searchQuery);
                } else {
                    $dataQuery->where($searchQuery);
                }
            }
            if (strpos($orderBy, ' as ')) {
                $orderBy = substr($orderBy, 0, strpos($orderBy, ' as '));
            }

//            dump($orderBy);die();
            return [
                'count' => $count,
                'data'  => $dataQuery
                    ->setFirstResult($start)
                    ->setMaxResults($length)
                    ->orderBy($orderBy, strtoupper($orderDir))
                    ->getQuery()
                    ->getResult(),
            ];
        } catch (\Exception $e) {
            dump($e);
            die();
        }
    }

}
<?php
declare(strict_types=1);

namespace App\Repository;


use App\Entity\Subject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class SubjectRepository
 * @package App\Repository
 */
class SubjectRepository extends BaseRepo
{
    /**
     * SubjectRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = Subject::class;
        parent::__construct($registry);
    }

    /**
     * @param $select
     * @param $start
     * @param $length
     * @param $orderBy
     * @param $orderDir
     * @param $search
     * @param null $requiredWhere
     *
     * @return array
     */
    public function dataTable($select, $start, $length, $orderBy, $orderDir, $search, $requiredWhere = null, $join = null, $groupBy = null, $addSelect = null)
    {
        try {
            $query = $this->createQueryBuilder('a')
                          ->leftJoin('a.category', 'c')
                          ->leftJoin('a.parentCategory', 'p')
                          ->leftJoin('a.level', 'l');
            $count = $query->select('count(a.id)');

            if ($join !== null) {
                foreach ($join as $j) {
                    if ($j['tipe'] == 'leftjoin') {
                        $query->leftJoin($j['join'], $j['alias']);
                    } elseif ($j['tipe'] == 'join') {
                        $query->join($j['join'], $j['alias']);
                    } else {
                        $query->innerJoin($j['join'], $j['alias']);
                    }
                }
            }

            if ($groupBy !== "") {
                $query->groupBy($groupBy);
            }

            if ($requiredWhere !== null) {
                $count->where($requiredWhere);
            }
            $count     = $count->getQuery()
                               ->getSingleScalarResult();
            $select[]  = 'a.id';
            $dataQuery = $query
                ->select($select);

            if ($requiredWhere !== null) {
                $dataQuery->where($requiredWhere);
            }

            if ($addSelect !== null) {
                $dataQuery->addSelect($addSelect);
            }

            if ( ! empty($search)) {
                $searchQuery = '';
                foreach ($select as $key => $field) {
                    if (strpos($field, ' as ')) {
                        $field = substr($field, 0, strpos($field, ' as '));
                    }
                    if ($key === 0) {
                        $searchQuery .= "$field LIKE '$search%'";
                    } else {
                        $searchQuery .= " OR $field LIKE '$search%'";
                    }
                }
                $searchQuery = "( " . $searchQuery . " )";

                if ($requiredWhere != NULL) {
                    $dataQuery->andWhere($searchQuery);
                } else {
                    $dataQuery->where($searchQuery);
                }
            }
            if (strpos($orderBy, ' as ')) {
                $orderBy = substr($orderBy, 0, strpos($orderBy, ' as '));
            }

//            dump($orderBy);die();
            return [
                'count' => $count,
                'data'  => $dataQuery
                    ->setFirstResult($start)
                    ->setMaxResults($length)
                    ->orderBy($orderBy, strtoupper($orderDir))
                    ->getQuery()
                    ->getArrayResult(),
            ];
        } catch (\Exception $e) {
            dump($e);
            die();
        }
    }
}
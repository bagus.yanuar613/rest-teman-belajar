<?php


namespace App\Repository;


use App\Entity\TryOutSubscription;
use Doctrine\Persistence\ManagerRegistry;

class TryOutSubscriptionRepository extends BaseRepo
{
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = TryOutSubscription::class;
        parent::__construct($registry);
        $this->isDTjoin = true;
    }

    public function DTJoin()
    {
        $this->query
            ->leftJoin('a.tryOut', 't');
    }

}
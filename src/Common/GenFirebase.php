<?php


namespace App\Common;

use App\Common\GenBasic;
use Symfony\Component\HttpFoundation\JsonResponse;

class GenFirebase
{

    /**
     * @param $fcmToken
     * @param string $title
     * @param string $body
     * @param array $data
     * @return array
     */
    public static function SendNotification($fcmToken, $title = 'Teman Belajar', $body = 'Default Message', $data = [])
    {
        try {
            $notification = array(
                'title' => $title,
                'body' => $body
            );
            $data['click_action'] = "FLUTTER_NOTIFICATION_CLICK";
            $data['msg'] = $body;
            $apikey = 'AAAAKs_Nlr0:APA91bE3HjPmcvI3ZIkwdXMxPjt_sGlMctLSwU1Pmw1qJiqgZ5_d8ht81e6SVTjOx2vStg7FQOmwxHbBcbPui25JQtMVqlbU7u-Nak7ZLC1kwwcysoWX3YZjb1T36gilADR0yKi8Bdj8';
            $fields = array('to' => $fcmToken, 'notification' => $notification, 'data' => $data);
            $header = array('Authorization: key=' . $apikey, 'Content-Type: application/json');
            $url = 'https://fcm.googleapis.com/fcm/send';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            $code = 200;
            $test = json_decode(curl_exec($ch), true);
            $response = $test;
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data' . $err->getMessage(),
            ];
        }
//        return GenBasic::send($code, $response);
        return ['data' => $test];
    }
}
<?php
declare(strict_types=1);

namespace App\Common;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class GenBasic
 * @package App\Common
 */
class GenBasic
{
    /**
     * @param $code
     * @param $payload
     *
     * @return JsonResponse
     */
    public static function send($code, $payload)
    {
        return new JsonResponse(
            [
                'code' => $code,
                'payload' => $payload,
            ]
        );
    }

    /**
     * @param null $key
     *
     * @return mixed|Request
     */
    public static function getRequest($key = null)
    {
        $req = Request::createFromGlobals();
        $req = json_decode($req->getContent(), true);

        return $key === null ? $req : $req[$key];
    }

    /**
     * @param Request $req
     *
     * @return mixed|Request
     */
    public static function request(Request $req)
    {
        $req = json_decode($req->getContent(), true);

        return $req;
    }

    /**
     * @param $obj
     *
     * @return mixed
     */
    public static function serializeToJson($obj)
    {

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new DateTimeNormalizer(), new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $result = json_decode(
            $serializer->serialize(
                $obj,
                'json',
                [
                    'circular_reference_handler' => function ($object) {
                        return $object->getId();
                    },
                    AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true
                ]
            ),
            true
        );


        return $result;

    }

    /**
     * @param $obj
     * @param array $attr
     * @return array|\ArrayObject|bool|float|int|mixed|string|null
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public static function CustomNormalize($obj, $attr = [])
    {
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
        ];

        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);
        $normalizer = new ObjectNormalizer($classMetadataFactory, $metadataAwareNameConverter, null, null, null, null, $defaultContext);
        $serializer = new Serializer([new DateTimeNormalizer(), $normalizer]);
        return $serializer->normalize($obj, 'json', [
                AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::ATTRIBUTES => $attr]
        );
    }
}
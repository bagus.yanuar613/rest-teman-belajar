<?php

namespace App\DataFixtures;

use App\Entity\Level;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class LevelFixtures extends Fixture
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        // TODO: Implement load() method.

        $data = [
            [
                "name" => 'PAUD',
                "slug" => 'PAUD'
            ],
            [
                "name" => 'SD',
                "slug" => 'SD'
            ],
            [
                "name" => 'SMP',
                "slug" => 'SMP'
            ],
            [
                "name" => 'SMA',
                "slug" => 'SMA'
            ],
            [
                "name" => 'SMK',
                "slug" => 'SMK'
            ],
            [
                "name" => 'SBMPTN',
                "slug" => 'SBMPTN'
            ],
            [
                "name" => 'UMUM',
                "slug" => 'UMUM'
            ],
        ];

        foreach ($data as $dat){
            $level = new Level();
            $level->setName($dat['name'])->setSlug($dat['slug']);
            $manager->persist($level);
        }
        $manager->flush();
    }
}
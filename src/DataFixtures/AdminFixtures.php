<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class AdminFixtures
 * @package App\DataFixtures
 */
class AdminFixtures extends Fixture
{


    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        // TODO: Implement load() method.
        $user = new User();
        $user->setEmail('genossys2019@gmail.com');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setPassword('123123');
        $manager->persist($user);
        $manager->flush();
    }
}
<?php

namespace App\DataFixtures;

use App\Entity\Schedule;
use DateTime;
use DateTimeZone;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ScheduleFixtures extends Fixture
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        // TODO: Implement load() method.
        $data = [
            [
                "day"  => 0,
                "time" => '09:00:00',
            ],
            [
                "day"  => 0,
                "time" => '10:00:00',
            ],
            [
                "day"  => 0,
                "time" => '11:00:00',
            ],
            [
                "day"  => 0,
                "time" => '12:00:00',
            ],
            [
                "day"  => 0,
                "time" => '13:00:00',
            ],
            [
                "day"  => 0,
                "time" => '14:00:00',
            ],
            [
                "day"  => 0,
                "time" => '15:00:00',
            ],
            [
                "day"  => 0,
                "time" => '16:00:00',
            ],
            [
                "day"  => 0,
                "time" => '17:00:00',
            ],
            [
                "day"  => 0,
                "time" => '18:00:00',
            ],
            [
                "day"  => 0,
                "time" => '19:00:00',
            ],
            [
                "day"  => 0,
                "time" => '20:00:00',
            ],

            [
                "day"  => 1,
                "time" => '09:00:00',
            ],
            [
                "day"  => 1,
                "time" => '10:00:00',
            ],
            [
                "day"  => 1,
                "time" => '11:00:00',
            ],
            [
                "day"  => 1,
                "time" => '12:00:00',
            ],
            [
                "day"  => 1,
                "time" => '13:00:00',
            ],
            [
                "day"  => 1,
                "time" => '14:00:00',
            ],
            [
                "day"  => 1,
                "time" => '15:00:00',
            ],
            [
                "day"  => 1,
                "time" => '16:00:00',
            ],
            [
                "day"  => 1,
                "time" => '17:00:00',
            ],
            [
                "day"  => 1,
                "time" => '18:00:00',
            ],
            [
                "day"  => 1,
                "time" => '19:00:00',
            ],
            [
                "day"  => 1,
                "time" => '20:00:00',
            ],

            [
                "day"  => 2,
                "time" => '09:00:00',
            ],
            [
                "day"  => 2,
                "time" => '10:00:00',
            ],
            [
                "day"  => 2,
                "time" => '11:00:00',
            ],
            [
                "day"  => 2,
                "time" => '12:00:00',
            ],
            [
                "day"  => 2,
                "time" => '13:00:00',
            ],
            [
                "day"  => 2,
                "time" => '14:00:00',
            ],
            [
                "day"  => 2,
                "time" => '15:00:00',
            ],
            [
                "day"  => 2,
                "time" => '16:00:00',
            ],
            [
                "day"  => 2,
                "time" => '17:00:00',
            ],
            [
                "day"  => 2,
                "time" => '18:00:00',
            ],
            [
                "day"  => 2,
                "time" => '19:00:00',
            ],
            [
                "day"  => 2,
                "time" => '20:00:00',
            ],

            [
                "day"  => 3,
                "time" => '09:00:00',
            ],
            [
                "day"  => 3,
                "time" => '10:00:00',
            ],
            [
                "day"  => 3,
                "time" => '11:00:00',
            ],
            [
                "day"  => 3,
                "time" => '12:00:00',
            ],
            [
                "day"  => 3,
                "time" => '13:00:00',
            ],
            [
                "day"  => 3,
                "time" => '14:00:00',
            ],
            [
                "day"  => 3,
                "time" => '15:00:00',
            ],
            [
                "day"  => 3,
                "time" => '16:00:00',
            ],
            [
                "day"  => 3,
                "time" => '17:00:00',
            ],
            [
                "day"  => 3,
                "time" => '18:00:00',
            ],
            [
                "day"  => 3,
                "time" => '19:00:00',
            ],
            [
                "day"  => 3,
                "time" => '20:00:00',
            ],

            [
                "day"  => 4,
                "time" => '09:00:00',
            ],
            [
                "day"  => 4,
                "time" => '10:00:00',
            ],
            [
                "day"  => 4,
                "time" => '11:00:00',
            ],
            [
                "day"  => 4,
                "time" => '12:00:00',
            ],
            [
                "day"  => 4,
                "time" => '13:00:00',
            ],
            [
                "day"  => 4,
                "time" => '14:00:00',
            ],
            [
                "day"  => 4,
                "time" => '15:00:00',
            ],
            [
                "day"  => 4,
                "time" => '16:00:00',
            ],
            [
                "day"  => 4,
                "time" => '17:00:00',
            ],
            [
                "day"  => 4,
                "time" => '18:00:00',
            ],
            [
                "day"  => 4,
                "time" => '19:00:00',
            ],
            [
                "day"  => 4,
                "time" => '20:00:00',
            ],

            [
                "day"  => 5,
                "time" => '09:00:00',
            ],
            [
                "day"  => 5,
                "time" => '10:00:00',
            ],
            [
                "day"  => 5,
                "time" => '11:00:00',
            ],
            [
                "day"  => 5,
                "time" => '12:00:00',
            ],
            [
                "day"  => 5,
                "time" => '13:00:00',
            ],
            [
                "day"  => 5,
                "time" => '14:00:00',
            ],
            [
                "day"  => 5,
                "time" => '15:00:00',
            ],
            [
                "day"  => 5,
                "time" => '16:00:00',
            ],
            [
                "day"  => 5,
                "time" => '17:00:00',
            ],
            [
                "day"  => 5,
                "time" => '18:00:00',
            ],
            [
                "day"  => 5,
                "time" => '19:00:00',
            ],
            [
                "day"  => 5,
                "time" => '20:00:00',
            ],

            [
                "day"  => 6,
                "time" => '09:00:00',
            ],
            [
                "day"  => 6,
                "time" => '10:00:00',
            ],
            [
                "day"  => 6,
                "time" => '11:00:00',
            ],
            [
                "day"  => 6,
                "time" => '12:00:00',
            ],
            [
                "day"  => 6,
                "time" => '13:00:00',
            ],
            [
                "day"  => 6,
                "time" => '14:00:00',
            ],
            [
                "day"  => 6,
                "time" => '15:00:00',
            ],
            [
                "day"  => 6,
                "time" => '16:00:00',
            ],
            [
                "day"  => 6,
                "time" => '17:00:00',
            ],
            [
                "day"  => 6,
                "time" => '18:00:00',
            ],
            [
                "day"  => 6,
                "time" => '19:00:00',
            ],
            [
                "day"  => 6,
                "time" => '20:00:00',
            ],
        ];

        foreach ($data as $dat) {
            $schedule = new Schedule();
            $schedule->setDay($dat['day'])->setTime(new DateTime($dat['time'],new DateTimeZone('Asia/Bangkok')));
            $manager->persist($schedule);
        }
        $manager->flush();
    }
}
<?php

namespace App\DataFixtures;

use App\Entity\Grade;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Class GradeFixtures
 * @package App\DataFixtures
 */
class GradeFixtures extends Fixture
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        // TODO: Implement load() method.
        $data = [
            [
                "name" => "Profesional",
                "slug" => "PRO"
            ],
            [
                "name" => "Basic",
                "slug" => "BASIC"
            ],
        ];

        foreach ($data as $dat){
            $grade = new Grade();
            $grade->setName($dat['name'])->setSlug($dat['slug']);
            $manager->persist($grade);
        }
        $manager->flush();
    }
}
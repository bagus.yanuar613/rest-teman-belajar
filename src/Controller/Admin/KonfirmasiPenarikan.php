<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Payment;
use App\Entity\TentorSaldo;
use App\Entity\Withdraw;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class KonfirmasiPenarikan
 * @package App\Controller\Admin
 */
class KonfirmasiPenarikan extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->formatDTResult = true;
        $this->tableFields    = [
            [
                'label'     => 'Tanggal Pengajuan',
                'field'     => 'createdAt',
                'sql'       => 'a.createdAt',
                'formatter' => 'getCreatedAt-'.TransactionController::class,
            ],
            [
                'label' => 'Nama Pengajar',
                'field' => 'fullName',
                'sql'   => 't.fullName',
            ],
            [
                'label'     => 'Nominal',
                'field'     => 'amount',
                'sql'       => 'a.amount',
                'formatter' => 'getDTAmount-'.KonfirmasiPembayaran::class,
            ],
            [
                'label' => 'Bank',
                'field' => 'name',
                'sql'   => 'b.name',
            ],
            [
                'label' => 'Atas Nama',
                'field' => 'holderName',
                'sql'   => 't.holderName',
            ],
            [
                'label' => 'Status',
                'field' => 'status',
                'sql'   => 'a.status',
                'formatter' => 'getDTStatus'
            ],
        ];

        $this->tableActions = [
            'detailPenarikan',
        ];

        $this->addSelect = 'u.id as userid';
        $this->singleNamespace    = 'Admin';
        $this->controllerName     = 'KonfirmasiPenarikan';
        $this->req                = Request::createFromGlobals();
        $this->class              = Withdraw::class;
        $this->data['class']      = $this->class;
        $this->requiredWhereParam = ':param = :id';
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/konfirmasi-penarikan", name="konfirmasi-penarikan")
     */
    public function index()
    {
        return $this->renderTable('admin/konfirmasi-penarikan.html.twig');
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTStatus($data) {
        $string = '';
        switch ($data['status']) {
            case 0 :
                $string = '<i class="fa fa-question-circle colorOrange"> Menunggu Konfirmasi Admin</i>';
                break;
            case 1 :
                $string = '<i class="fa fa-check-circle text-blue"> Penarikan Diterima</i>';
                break;
            default:
                $string = '<i class="fa fa-times-circle colorRed"> Penarikan Ditolak</i>';
                break;
        }

        return $string;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function status($data)
    {
        $string = '';
        switch ($data['status']) {
            case 0 :
                $string = '<i class="fa fa-question-circle colorOrange"> Menunggu Konfirmasi Admin</i>';
                break;
            case 1 :
                $string = '<i class="fa fa-check-circle text-blue"> Penarikan Diterima</i>';
                break;
            default:
                $string = '<i class="fa fa-times-circle colorRed"> Penarikan Ditolak</i>';
                break;
        }

        return $string;
    }


    /**
     * @param $data
     *
     * @return string
     */
    public function amount($data)
    {
        $hasil_rupiah = 'Rp. '.number_format($data['amount'], 0, ',', '.');

        return $hasil_rupiah;
    }

}
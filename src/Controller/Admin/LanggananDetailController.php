<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Cart;
use App\Entity\Level;
use App\Entity\Subscription;
use App\Entity\Transaction;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LanggananController
 * @package App\Controller\Admin
 */
class LanggananDetailController extends BaseController
{

    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->formatDTResult = true;

        $this->tableFields = [
            [
                'label'     => 'Tanggal',
                'field'     => 'createdAt',
                'sql'       => 'a.createdAt',
                'formatter' => 'getCreatedAt-'.LanggananController::class,
            ],

//            [
//                'label' => 'Nomor Referensi',
//                'field' => 'referenceId',
//                'sql'   => 'c.referenceId',
//            ],
            [
                'label'     => 'Nominal Transaksi',
                'field'     => 'total',
                'sql'       => 'c.total',
                'formatter' => 'getDTTotal',
            ],
//            [
//                'label' => 'Status',
//                'field' => 'status',
//                'sql'   => 'c.status',
//            ],
        ];

        $this->data['tableFields1'] = [
            [
                'label' => 'ID Siswa',
                'field' => 'id',
                'sql'   => 'a.id',
            ],
            [
                'label' => 'Nama',
                'field' => 'fullName',
                'sql'   => 'a.fullName',
            ],
            [
                'label' => 'Alamat',
                'field' => 'address',
                'sql'   => 'a.address',
            ],
            [
                'label' => 'Kelas',
                'field' => 'class',
                'sql'   => 'a.class',
            ],
        ];

//        $this->tableActions = [
//            'detail'
//        ];

        $this->joint = [
            [
                'tipe'  => 'leftjoin',
                'join'  => 's.cart',
                'alias' => 'a',
            ],
            [
                'tipe'  => 'leftjoin',
                'join'  => 'm.class',
                'alias' => 'mc',
            ],
            [
                'tipe'  => 'leftjoin',
                'join'  => 'mc.memberClassParent',
                'alias' => 'cp',
            ],
        ];

        $this->addSelectCard      = 'u.id as userId, tu.id as tuser';
        $this->singleNamespace    = 'Admin';
        $this->controllerName     = 'LanggananDetailController';
        $this->req                = Request::createFromGlobals();
        $this->class              = Subscription::class;
        $this->data['class']      = $this->class;
        $this->requiredWhereParam = "s.id = ':id'";

    }

    public function getDTTest()
    {
        return '<a class="btn btn-primary">Test</a>';
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/langganan/detail", name="langganan-detail")
     */
    public function index()
    {
        $this->data['classAttendance'] = 'AttendanceController';
        $this->data['classCart']       = 'CartController';
//
        $this->data['whereLangganan']['id']         = $this->req->get('id');
        $this->data['whereLangganan']['attendance'] = 's.id';
        $this->data['whereLangganan']               = json_encode($this->data['whereLangganan']);

        $this->data['whereCart']['id']    = $this->req->get('id');
        $this->data['whereCart']['param'] = 's.id';
        $this->data['whereCart']          = json_encode($this->data['whereCart']);

        $this->data['cartField'] = [
            [
                'label' => 'Nomor Referensi',
                'field' => 'referenceId',
            ],
            [
                'label' => 'Method',
                'field' => 'method',
            ],
            [
                'label'     => 'Durasi',
                'field'     => 'duration',
            ],
            [
                'label'     => 'Jumlah Pertemuan',
                'field'     => 'encounter',
                'sql'       => 'a.encounter',
                'formatter' => 'getDTEncounter-'.KonfirmasiPembayaranDetail::class,

            ],
            [
                'label'     => 'Jumlah Siswa',
                'field'     => 'attendees',
            ],
            [
                'label'     => 'Nominal',
                'field'     => 'amount',
            ],
            [
                'label'     => 'Total',
                'field'     => 'total',

            ],
            [
                'label'     => 'Diskon',
                'field'     => 'discount',

            ],
            [
                'label'     => 'Total Transaksi',
                'field'     => 'total',
            ],
        ];

        $this->data['detailLangganan'] = [
            [
                'label' => 'Kode Langganan',
                'field' => 'subscriptionCode',
                'sql'   => 's.subscriptionCode',
            ],
            [
                'label'     => 'Status',
                'field'     => 'isActive',
                'sql'       => 'a.isActive',
                'formatter' => 'isActive',
            ],
            [
                'label' => 'Kategori',
                'field' => 'category',
                'sql'   => 'c.name as category',
            ],

        ];

        $this->data['pelanggan'] = [
//            [
//                'label' => 'ID',
//                'field' => 'id',
//                'sql'   => 'u.id',
//            ],
            [
                'label' => 'Nama',
                'field' => 'fullName',
                'sql'   => 'm.fullName',
            ],
//            [
//                'label' => 'Jenis Kelamin',
//                'field' => 'gender',
//                'sql'   => 'm.gender',
//                'formatter' => 'gender'
//            ],
            [
                'label' => 'Sekolah',
                'field' => 'school',
                'sql'   => 'm.school',
            ],
            [
                'label' => 'Jenjang',
                'field' => 'parent',
                'sql'   => 'cp.name as parent',
            ],
            [
                'label' => 'Kelas',
                'field' => 'class',
                'sql'   => 'mc.name as class',
            ],
            [
                'label' => 'Email',
                'field' => 'email',
                'sql'   => 'u.email',
            ],
            [
                'label' => 'Nomor Hp',
                'field' => 'phone',
                'sql'   => 'm.phone',
            ],
            [
                'label' => 'Alamat',
                'field' => 'address',
                'sql'   => 'm.address',
            ],

        ];

        $this->data['pengajar'] = [
//            [
//                'label' => 'ID',
//                'field' => 'tuser',
//                'sql'   => 'tu.id',
//            ],
            [
                'label' => 'Nama',
                'field' => 'fullName',
                'sql'   => 'ten.fullName',
            ],
            [
                'label'     => 'Jenis Kelamin',
                'field'     => 'gender',
                'sql'       => 'ten.gender',
                'formatter' => 'gender',
            ],
            [
                'label' => 'Email',
                'field' => 'email',
                'sql'   => 'tu.email',
            ],
            [
                'label' => 'Nomor Hp',
                'field' => 'phone',
                'sql'   => 'ten.phone',
            ],
            [
                'label' => 'Keahlian Mengajar',
                'field' => 'experience',
                'sql'   => 'ten.experience',
            ],

        ];

        $this->data['tableFieldsPresensi'] = [
            [
                'label' => 'Hari Presensi',
                'field' => 'hari',
            ],
            [
                'label' => 'Tanggal Presensi',
                'field' => 'createdAt',
            ],
            [
                'label' => 'Status',
                'field' => 'isPresence',
            ],
        ];

        $this->data['actionLangganan'] = [
            [

                'label' => 'Ganti Status',
            ],
            [
                'label' => 'Ganti Pengajar',
            ],
            [
                'label' => 'Ganti Paket',
            ],
            [
                'label' => 'Buat Absen',
            ],
        ];

        return $this->renderTable('admin/langgananDetail.html.twig');
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function status($data)
    {
        if ($data['status']) {
            return '<span class="fa fa-circle text-blue"></span> Aktif';

        } else {
            return '<span class="fa fa-circle colorRed"></span> Belum Aktif';
        }

    }

    /**
     * @param $data
     *
     * @return string
     */
    public function isActive($data)
    {
        if ($data['isActive']) {
            return '<span class="fa fa-circle text-blue"></span> Aktif';

        } else {
            return '<span class="fa fa-circle colorRed"></span> Belum Aktif';
        }

    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTTotal($data)
    {
        $hasil_rupiah = 'Rp. '.number_format($data['total'], 0, ',', '.');

        return $hasil_rupiah;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function amount($data)
    {
//        dump($data);die();
        $per          = 'Rp. '.number_format($data['amount'] / $data['encounter'], 0, ',', '.');
        $hasil_rupiah = 'Rp. '.number_format($data['amount'], 0, ',', '.').' ( '.$data['encounter'].' x '.$per.' )';

//        dump($hasil_rupiah);die();

        return $hasil_rupiah;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function gender($data)
    {
        if ($data['gender'] == 0) {
            $gender = 'Perempuan';
        } else {
            $gender = 'Laki - Laki';
        }

        return $gender;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function duration($data)
    {
        return $data['duration'].' Menit';
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function encounter($data)
    {
        return $data['encounter'].' x Pertemuan';
    }

}
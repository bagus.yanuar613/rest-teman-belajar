<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Schedule;
use App\Entity\Tentor;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class JadwalTentorController
 * @package App\Controller\Admin
 */
class JadwalTentorController extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->formatDTResult = true;

        $this->tableFields = [
            [
                'label' => 'Hari',
                'field' => 'day',
                'sql'   => 'a.day',
                'formatter' => 'getDay',
            ],
            [
                'label' => 'Jam',
                'field' => 'time',
                'sql'   => 'a.time',
                'formatter' => 'getTime',
            ],
        ];

        $this->singleNamespace = 'Admin';
        $this->controllerName  = 'JadwalTentorController';
        $this->req             = Request::createFromGlobals();
        $this->class           = Schedule::class;
        $this->data['class']   = $this->class;
        $this->requiredWhereParam = 'u.id = :id';


    }

    /**
     * @param $val
     *
     * @return string
     */
    public function getDay($val)
    {
        switch ($val['day']) {
            case 0:
                $day = 'Senin';
                break;
            case 1:
                $day = 'Selasa';
                break;
            case 2:
                $day = 'Rabu';
                break;
            case 3:
                $day = 'Kamis';
                break;
            case 4:
                $day = 'Jumat';
                break;
            case 5:
                $day = 'Sabtu';
                break;
            default:
                $day = 'Minggu';
                break;
        }
        return $day;
    }

    /**
     * @param $val
     *
     * @return string
     */
    public function getTime($val)
    {
        Carbon::setLocale('id');
        $date = Carbon::parse($val['time'], 'Asia/Jakarta')->isoFormat('HH:mm');
        return $date;
    }

}
<?php

namespace App\Controller\Admin;

use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Member;
use App\Repository\MemberRepository;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SiswaController
 * @package App\Controller\Admin\Pengguna
 */
class SiswaController extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->formatDTResult = true;

        $this->tableFields = [
            [
                'label' => 'ID Siswa',
                'field' => 'id',
                'sql'   => 'a.id',
            ],
            [
                'label' => 'Nama',
                'field' => 'fullName',
                'sql'   => 'a.fullName',
            ],
            [
                'label' => 'Alamat',
                'field' => 'address',
                'sql'   => 'a.address',
            ],
            [
                'label' => 'Kelas',
                'field' => 'class',
                'sql'   => 'cp.name as class',
            ],
            [
                'label' => 'Terakhir Aktif',
                'field' => 'updatedAt',
                'sql'   => 'a.updatedAt',
                'formatter' => 'getDTUpdate'
            ],
        ];

        $this->tableActions = [
            'detail',
        ];

        $this->singleNamespace = 'Admin';
        $this->controllerName  = "SiswaController";
        $this->req             = Request::createFromGlobals();
        $this->class           = Member::class;
        $this->data['class']   = $this->class;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/siswa", name="siswa")
     */
    public function index()
    {
        return $this->renderTable('admin/siswa.html.twig');
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTUpdate($data){
        Carbon::setLocale('id');
        $date = Carbon::parse($data['updatedAt'], 'Asia/Jakarta')->isoFormat('LL, HH:mm');

        return $date;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/admin/get-total-member", name="get-total-member")
     */
    public function getTotalSiswa()
    {
        try {
            /** @var MemberRepository $res */
            $res     = count($this->em->getRepository(Member::class)->findAll());
            $code     = 200;
            $data = [
              'data' => $res
            ];
            $response = GenBasic::serializeToJson($data);
        } catch (\Exception $e) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data',
            ];
        }
        return GenBasic::send($code, $response);
    }


    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/admin/get-count-new-member-month", name="get-count-new-member-month")
     */
    public function getCountNewMemberMonth()
    {
        $date1 = date_create($this->req->get('start'));
        $date2 = date_create($this->req->get('end'));
        $range = date_diff($date1,$date2 );
        $selisih = $range->format("%a")*2;


        $old = "Select count(id) as jum from member where created_at BETWEEN DATE_ADD('".$this->req->get('start')."', INTERVAL -".$selisih." DAY) AND DATE_ADD('".$this->req->get('start')."', INTERVAL -1 DAY) ";
        $new = "Select count(id) as jum from member where created_at BETWEEN '".$this->req->get('start')."' AND '".$this->req->get('end')."' ";


        return $this->getCountPerncen($old, $new);

    }

}
<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Cart;
use App\Entity\Payment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CartController
 * @package App\Controller\Admin
 */
class CartController extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->formatDTResult = true;
        $this->tableFields = [
            [
                'label' => 'Nomor Langganan',
                'field' => 'subscriptionCode',
                'sql'   => 's.subscriptionCode',
            ],
            [
                'label' => 'Nomor Referensi',
                'field' => 'referenceId',
                'sql'   => 'a.referenceId',
            ],
            [
                'label' => 'Pengajar',
                'field' => 'tentor',
                'sql'   => 'ten.fullName as tentor',
            ],
            [
                'label' => 'Kategori',
                'field' => 'category',
                'sql'   => 'c.name as category',
            ],
            [
                'label' => 'Method',
                'field' => 'method',
                'sql'   => 'mt.name as method',
            ],
            [
                'label'     => 'Durasi',
                'field'     => 'duration',
                'sql'       => 'a.duration',
                'formatter' => 'getDTDuration-'.KonfirmasiPembayaranDetail::class,

            ],
            [
                'label'     => 'Jumlah Pertemuan',
                'field'     => 'encounter',
                'sql'       => 'a.encounter',
                'formatter' => 'getDTEncounter-'.KonfirmasiPembayaranDetail::class,

            ],
            [
                'label'     => 'Jumlah Siswa',
                'field'     => 'attendees',
                'sql'       => 'a.attendees',
                'formatter' => 'getDTAttendees-'.KonfirmasiPembayaranDetail::class,

            ],
            [
                'label'     => 'Nominal',
                'field'     => 'amount',
                'sql'       => 'a.amount',
                'formatter' => 'getDTAmount-'.TransactionController::class,

            ],
            [
                'label'     => 'Total',
                'field'     => 'total',
                'sql'       => 'a.total',
                'formatter' => 'getDTTotal-'.KonfirmasiPembayaranDetail::class,

            ],
            [
                'label'     => 'Diskon',
                'field'     => 'discount',
                'sql'       => 'a.discount',
                'formatter' => 'getDTDiskon-'.KonfirmasiPembayaranDetail::class,

            ],
            [
                'label'     => 'Total Transaksi',
                'field'     => 'total',
                'sql'       => 'a.total',
                'formatter' => 'getDTTotalTrans-'.KonfirmasiPembayaranDetail::class,
            ],
        ];

        $this->singleNamespace    = 'Admin';
        $this->controllerName     = 'CartController';
        $this->req                = Request::createFromGlobals();
        $this->class              = Cart::class;
        $this->data['class']      = $this->class;
        $this->requiredWhereParam = ':param = :id';

    }

}
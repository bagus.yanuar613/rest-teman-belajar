<?php

namespace App\Controller\Admin;

use App\Common\GenBasic;
use App\Common\GenFirebase;
use App\Controller\BaseController;
use App\Entity\Cart;
use App\Entity\Payment;
use App\Entity\Subscription;
use App\Entity\Tentor;
use App\Entity\Transaction;
use App\Entity\TryOutFreeAccess;
use App\Entity\TryOutRegistrant;
use App\Entity\TryOutReward;
use App\Repository\PaymentRepository;
use App\Repository\TransactionRepository;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class KonfirmasiPembayaranDetail
 * @package App\Controller\Admin
 */
class KonfirmasiPembayaranDetail extends BaseController
{

    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->formatDTResult = true;

        $this->tableFields = [
            [
                'label' => 'Nomor Langganan',
                'field' => 'subscriptionCode',
                'sql'   => 'tr.subscriptionCode',
            ],
            [
                'label' => 'Nomor Referensi',
                'field' => 'referenceId',
                'sql'   => 'c.referenceId',
            ],
            [
                'label' => 'Pengajar',
                'field' => 'tentor',
                'sql'   => 't.fullName as tentor',
            ],
            [
                'label' => 'Method',
                'field' => 'method',
                'sql'   => 'mt.name as method',
            ],
            [
                'label'     => 'Durasi',
                'field'     => 'duration',
                'sql'       => 'c.duration',
                'formatter' => 'getDTDuration',
            ],
            [
                'label'     => 'Jumlah Pertemuan',
                'field'     => 'encounter',
                'sql'       => 'c.encounter',
                'formatter' => 'getDTEncounter',

            ],
            [
                'label'     => 'Jumlah Siswa',
                'field'     => 'attendees',
                'sql'       => 'c.attendees',
                'formatter' => 'getDTAttendees',

            ],
            [
                'label'     => 'Nominal',
                'field'     => 'amount',
                'sql'       => 'c.amount',
                'formatter' => 'getDTAmount-'.TransactionController::class,
            ],
            [
                'label'     => 'Total',
                'field'     => 'total',
                'sql'       => 'c.total',
                'formatter' => 'getDTTotal',
            ],
            [
                'label'     => 'Diskon',
                'field'     => 'discount',
                'sql'       => 'c.discount',
                'formatter' => 'getDTDiskon',
            ],
            [
                'label'     => 'Total Transaksi',
                'field'     => 'total',
                'sql'       => 'c.total',
                'formatter' => 'getDTTotalTrans',
            ],
        ];

        $this->joint = [
            [
                'tipe'  => 'leftjoin',
                'join'  => 'a.bank',
                'alias' => 'ba',
            ],
            [
                'tipe'  => 'leftjoin',
                'join'  => 'ba.bank',
                'alias' => 'b',
            ],
        ];

        $this->tableActions       = [];
        $this->singleNamespace    = 'Admin';
        $this->controllerName     = 'KonfirmasiPembayaranDetail';
        $this->req                = Request::createFromGlobals();
        $this->class              = Payment::class;
        $this->data['class']      = $this->class;
        $this->requiredWhereParam = ':param = :id';

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/konfirmasi-pembayaran/detail", name="payment-detail")
     */
    public function detail()
    {
        if ($this->req->isMethod('POST')) {
//            dump($this->req);die();
            try {
                //status Accept = true, Reject = False
                $id     = $this->req->get('id');
                $status = $this->req->get('status');
                /** @var TransactionRepository $transRepo */
                $transRepo = $this->em->getRepository(Transaction::class);
                /** @var Transaction $transaction */
                $transaction = $transRepo->createQueryBuilder('t')
                                         ->where('t.isPaid is null')
//                                         ->andWhere('t.confirmationInfo is not null')
                                         ->andWhere('t.id = :id')
                                         ->setParameter('id', $id)
                                         ->getQuery()->getOneOrNullResult();
                if ( ! $transaction) {
                    return GenBasic::send(202, ['Transaction Not Found!']);
                }
                if ($status === "1") {
                    $transaction->setIsPaid(true);
                } else {
                    $transaction->setIsPaid(false);
                }
                $resTentor = '';
                $alasan    = $this->req->get('text');
                if ($transaction->getSlug() === 'cart') {
                    /** @var Cart $cart */
                    foreach ($transaction->getCart() as $cart) {
                        if ($status == "1") {
                            /** @var Subscription $subscription */
                            $subscription = $cart->getSubscription();
                            $cart->setIsActive(true);
                            $encounter      = $cart->getEncounter();
                            $currentRemains = $cart->getSubscription()->getRemains();
                            $current        = $encounter + $currentRemains;
                            $cart->getSubscription()->setStatus(1);
                            $cart->getSubscription()->setRemains($current);
                            if ($subscription->getStatus() === 9) {
                                foreach ($subscription->getAttendancePlan() as $v) {
                                    $v->setStatus(1);
                                }
                            }
                            $method     = $cart->getMethod()->getName();
                            $category   = $cart->getSubscription()->getTentorSubject()->getCategory()->getName();
                            $meet       = Carbon::parse($cart->getFirstMeet(), 'Asia/Jakarta')->isoFormat('LL, HH:mm');
                            $memberName = $transaction->getUser()->getMemberProfile()->getFullName();
                            $fcmTentor  = $cart->getSubscription()->getTentorSubject()->getUser()->getAppFcmToken();
                            $tentorName = $cart->getSubscription()->getTentorSubject()->getUser()->getTentorProfile()->getFullName();
//                            $fcmTentor           = "cAf4IBvOSJKjnhJj4eupI7:APA91bFl5yU1cV05K8mmmazwlcICOe2d7gdmuIJS5-Ws5poZyO7Dr9NaB_BNJ1zSA-gWVg052o-c_zQAf2v9aUufNbi8z0Po1A5gvjhWjn8Gac13uLFLMvbK4VybTBrcBee4QZbplfFl";
                            $messageTentor       = "Halo $tentorName, Pembayaran dari $memberName untuk $method $category sudah diterima, pertemuan pertama anda akan dimulai pada $meet";
                            $type                = 'les_diterima';
                            $responseNotifTentor = [
                                'id'   => $cart->getId(),
                                'type' => 'Pembayaran_'.$type,
                            ];
                            $resTentor           = $this->sendNotification($fcmTentor, "Admin : Info Mengajar (TEMAN BELAJAR)", $messageTentor, $responseNotifTentor);

                            $message = "Selamat , Pembayaran kamu sudah diterima, silahkan buka halaman guruku untuk melihat jadwal pertama kamu";

                        } else {
                            $cart->setIsActive(false);
                            if ($cart->getSubscription()->getStatus() === 0) {
                                $cart->getSubscription()->setStatus(6);
                            }

                            $message = "Maaf, Pembayaran les kamu kami tolak. Alasan : $alasan";
                            $type    = 'les_ditolak';
                        }
                    }
                } else {
                    $date = new DateTime('', new DateTimeZone('Asia/Jakarta'));

                    /** @var TryOutRegistrant $tryOutRegistrant */
                    foreach ($transaction->getTryOutRegistrant() as $tryOutRegistrant) {
                        /** @var TryOutReward $reward */
                        $reward     = $tryOutRegistrant->getTryOut()->getReward();
                        $registrant = $tryOutRegistrant->getRegistrantNumber() - 1;
                        $freeAccess = $this->em->getRepository(TryOutFreeAccess::class)->findOneBy(['registration' => $tryOutRegistrant->getId()]);
                        if ($status == "1") {
                            $tryOutRegistrant->setIsPaid(1);
                            $message = 'Selamat, pembelian tryout kamu sudah diterima & sudah bisa di akses';
                            if ($reward) {
                                $minRegistrant = $reward->getMinRegistrant();
                                $statusAktiv   = $reward->getIsActive();
                                $free          = $reward->getFreeAccess();
                                if ($registrant >= $minRegistrant && $statusAktiv == true) {
                                    if ($freeAccess == null) {
                                        $freeAccess = new TryOutFreeAccess;
                                        $freeAccess->setCreatedAt($date)
                                                   ->setUpdatedAt($date)
                                                   ->setRegistration($tryOutRegistrant)
                                                   ->setReferalCode($this->setReferralCode());
                                        $this->em->persist($freeAccess);
                                        $message = "$message. Kamu juga dapat $free akses tryout gratis";
                                    }
                                }
                            }
                            $type = 'tryout_diterima';

                        } else {
                            $tryOutRegistrant->setIsPaid(0);

                            $type    = 'tryout_ditolak';
                            $message = "Maaf, Pembayaran tryout kamu kami tolak. Alasan : $alasan";

                        }
                    }
                }
                $fcmMember = $transaction->getUser()->getAppFcmToken();
//                $fcmMember = "cAf4IBvOSJKjnhJj4eupI7:APA91bFl5yU1cV05K8mmmazwlcICOe2d7gdmuIJS5-Ws5poZyO7Dr9NaB_BNJ1zSA-gWVg052o-c_zQAf2v9aUufNbi8z0Po1A5gvjhWjn8Gac13uLFLMvbK4VybTBrcBee4QZbplfFl";

                $responseNotif = [
                    'id'   => $transaction->getId(),
                    'type' => 'Pembayaran_'.$type,
                ];
                $this->em->flush();
                $code     = 200;
                $dataRes  = $this->sendNotification($fcmMember, "Admin : Konfirmasi Pembayaran (TEMAN BELAJAR)", $message, $responseNotif);
                $response = [
                    'status'         => 'Success',
                    'response'       => $dataRes,
                    'responseTentor' => $resTentor,
                ];
            } catch (\Exception $err) {
                $code     = 500;
                $response = [
                    'msg' => 'Failed send message '.$err->getMessage(),
                ];
            }

            return GenBasic::send($code, $response);

        }

        /** @var PaymentRepository $dat */
        $dat = $this->em->getRepository(Transaction::class);
        /** @var Transaction $repo */
        $repo = $dat->createQueryBuilder('a')
                    ->leftJoin('a.bank', 'ba')
                    ->leftJoin('ba.bank', 'b')
                    ->leftJoin('a.user', 'u')
                    ->leftJoin('u.memberProfile', 'm')
                    ->leftJoin('m.class', 'c')
                    ->leftJoin('c.memberClassParent', 'mc')
                    ->where('a.id = '.$this->req->get('id'))
                    ->getQuery()
                    ->getResult();
//        dump($repo[0]->getStatus());die();
        $this->data['payment'] = $repo;
        /** @var Cart $car */
//        $car = $repo[0]->getCart();
//        dump($car);die();
        $this->data['isPaid']    = $repo[0]->getIsPaid();
        $this->data['getStatus'] = $this->getIsPaid($this->data);

        if ($this->req->get('type') == 'cart') {


            $this->data['classPaymentDetail']          = 'CartController';
            $this->data['whereDetailPayment']['id']    = $this->req->get('id');
            $this->data['whereDetailPayment']['param'] = 't.id';
            $this->data['whereDetailPayment']          = json_encode($this->data['whereDetailPayment']);
            $this->data['tableFieldsDetail']           = [
                [
                    'label' => 'Nomor Langganan',
                    'field' => 'subscriptionCode',
                ],
                [
                    'label' => 'Nomor Referensi',
                    'field' => 'referenceId',
                ],
                [
                    'label' => 'Pengajar',
                    'field' => 'tentor',
                    'sql'   => 't.fullName as tentor',
                ],
                [
                    'label' => 'Kategori',
                    'field' => 'category',
                ],
                [
                    'label' => 'Method',
                    'field' => 'method',
                ],
                [
                    'label' => 'Durasi',
                    'field' => 'duration',
                ],
                [
                    'label' => 'Jumlah Pertemuan',
                    'field' => 'encounter',
                ],
                [
                    'label' => 'Jumlah Siswa',
                    'field' => 'attendees',
                ],
                [
                    'label' => 'Nominal',
                    'field' => 'amount',
                ],
                [
                    'label' => 'Total',
                    'field' => 'total',
                ],
                [
                    'label' => 'Diskon',
                    'field' => 'discount',
                ],
                [
                    'label' => 'Total Transaksi',
                    'field' => 'total',
                ],

            ];
        } elseif ($this->req->get('type') == 'tryOutRegistrant') {
            $this->data['classPaymentDetail']          = 'KonfirmasiPembayaranTryOut';
            $this->data['whereDetailPayment']['id']    = $this->req->get('id');
            $this->data['whereDetailPayment']['param'] = 't.id';
            $this->data['whereDetailPayment']          = json_encode($this->data['whereDetailPayment']);
            $this->data['tableFieldsDetail']           = [
                [
                    'label' => 'Tanggal',
                    'field' => 'createdAt',
                ],
                [
                    'label' => 'Nama Try Out',
                    'field' => 'title',
                ],
                [
                    'label' => 'level',
                    'field' => 'name',
                    'sql'   => 'l.name',
                ],
                [
                    'label' => 'Jumlah Pendaftar',
                    'field' => 'registrantNumber',
                ],
                [
                    'label' => 'Total Harga',
                    'field' => 'totalPrice',
                ],

            ];

        }

        return $this->renderTable('admin/konfirmasiPembayaranDetail.html.twig');
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getIsPaid($data)
    {
        if ($data['isPaid'] === null) {
            $string = 'Menunggu Konfirmasi';
        } elseif ($data['isPaid'] == 1) {
            $string = 'Lunas';
        } else {
            $string = 'Ditolak';
        }

        return $string;
    }

    /**
     * @param $data
     *
     * @return bool|string
     */
    public
    function getDTStatus(
        $data
    ) {
        $string = '';
        switch ($data['status']) {
            case 2 :
                $string = 'Lunas';
                break;
            case 6 :
                $string = 'Ditolak';
                break;
            default:
                $string = 'Menunggu Konfirmasi';
                break;
        }

        return $string;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTDuration($data)
    {
        return $data['duration'].' Menit';
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTAttendees($data)
    {
        return $data['attendees'].' Siswa';
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTEncounter($data)
    {
        return $data['encounter'].' Pertemuan';
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTMethod($data)
    {
        return $data['method'] = 1 ? 'Tatap Muka' : 'Online';
    }

    /**
     * @param $data
     *
     * @return string
     */
    public
    function getDTTotal(
        $data
    ) {
        $hasil_rupiah = 'Rp. '.number_format($data['total'], 0, ',', '.');

        return $hasil_rupiah;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public
    function getDTDiskon(
        $data
    ) {
        $hasil_rupiah = 'Rp. '.number_format($data['discount'], 0, ',', '.');

        return $hasil_rupiah;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTTotalTrans($data)
    {
        $har          = (int)$data['total'];
        $dis          = (int)$data['discount'];
        $total        = $har - $dis;
        $hasil_rupiah = 'Rp. '.number_format($total, 0, ',', '.');

        return $hasil_rupiah;
    }

    /**
     * @return string
     * @throws \Exception
     */
    protected function setReferralCode()
    {
        $characters       = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString     = '';
        for ($i = 0; $i < 6; $i++) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }

        $current = $this->em->getRepository(TryOutFreeAccess::class)->findOneBy(['referalCode' => $randomString]);
        if ($current !== null) {
            return $this->setReferralCode();
        }

        return 'TT'.$randomString;
    }

}
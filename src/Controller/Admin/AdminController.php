<?php

namespace App\Controller\Admin;

use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Admin;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

/**
 * Class AdminController
 * @package App\Controller\Admin
 */
class AdminController extends BaseController
{

    /** @var PasswordEncoderInterface */
    private $encoder;

    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->formatDTResult = true;

        $this->fields = [
            'email',
            'roles:method:setRoles:',
            'isEmailVerified:method:verify:',
            'provider:method:provider:',
        ];

        $this->secondFieldstoAdd = [
            'fullName',
            'phone',
            'position',
            'privilege',
            'avatar:method:uploadAvatar:entity',
            'license:method:uploadLicense:entity',
            'user:relationPrev',
            'isActive:method:setAdminActive:',
            'adminProfile:PrevToMe',
        ];

        $this->tableFields = [
            [
                'label' => 'Nama Admin',
                'field' => 'fullName',
                'sql'   => 'a.fullName',
            ],
            [
                'label' => 'Nomor Ponsel',
                'field' => 'phone',
                'sql'   => 'a.phone',
            ],
            [
                'label' => 'Email',
                'field' => 'email',
                'sql'   => 'u.email',
            ],
            [
                'label' => 'Jabatan',
                'field' => 'position',
                'sql'   => 'a.position',
            ],
            [
                'label' => 'Privilege',
                'field' => 'privilege',
                'sql'   => 'a.privilege',
            ],
        ];

        $this->tableActions = [
            'detail',
        ];

        $this->fieldReferenceOnUpdate = 'adminProfile';
        $this->singleNamespace        = 'Admin';
        $this->controllerName         = 'AdminController';

        $this->req           = Request::createFromGlobals();
        $this->class         = Admin::class;
        $this->data['class'] = $this->class;

    }

    /**
     * @param $val
     *
     * @return string
     */
    public function provider($val)
    {

        return $this->req->get('tipeEmail');
    }

    /**
     * @param $val
     *
     * @return int
     */
    public function verify($val)
    {
        return 1;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/admin-user", name="admin-user")
     */
    public function index()
    {
        return $this->renderTable('admin/adminUser.html.twig');
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getRoles($data)
    {
        $privilege = $data['roles'];
        switch ($privilege) {
            case '["ROLE_SUPER"]' :
                $role = 'Super Admin';
                break;
            case '["ROLE_ADMIN"]' :
                $role = 'Admin';
                break;
            case '["ROLE_AKUNTAN"]' :
                $role = 'Akuntan';
                break;
            case '["ROLE_OPERATOR"]' :
                $role = 'Operasional';
                break;
            default :
                $role = 'Verifikator';
                break;
        }

        return $role;

    }

    /**
     * @param $val
     *
     * @return bool
     */
    protected function setAdminActive($val)
    {
        return true;
    }

    /**
     * @param $val
     *
     * @return string
     */
    public function setRoles($val)
    {
        $privilege = $this->req->get('privilege');
        switch ($privilege) {
            case 'Super Admin' :
                $role = ['ROLE_ADMIN', 'ROLE_SUPER'];
                break;
            case 'Admin' :
                $role = ['ROLE_ADMIN', 'ROLE_ADMINISTRATOR'];
                break;
            case 'Akuntan' :
                $role = ['ROLE_ADMIN', 'ROLE_AKUNTAN'];
                break;
            case 'Operasional' :
                $role = ['ROLE_ADMIN', 'ROLE_OPERATOR'];
                break;
            default :
                $role = ['ROLE_ADMIN', 'ROLE_VERIFIKATOR'];
                break;
        }

        return $role;
    }

    /**
     * @param $val
     *
     * @return string
     */
    protected function encodePassword($val)
    {
        $user = new User();

        return $this->encoder->encodePassword($user, $this->req->request->get('password'));
    }

    /**
     * @param $entity
     *
     * @return mixed|string
     */
    public function uploadAvatar($entity)
    {

        $img = $this->req->files->get('avatar');
        /** @var Admin $entity */
        if ($img) {
            if (file_exists('../public'.$img)) {
                if ($entity) {
                    if ($entity->getAvatar()) {
                        unlink('../public'.$entity->getAvatar());
                    }
                }
            }

            $targetDir        = '../public/images/admin';
            $originalFilename = pathinfo($img->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename     = str_replace(' ', '-', $originalFilename);
            $fileName         = $safeFilename.'-'.uniqid().'.'.$img->guessExtension();
            $img->move($targetDir, $fileName);

            return '/images/admin/'.$fileName;
        }

        return $entity->getAvatar();
    }

    /**
     * @param $entity
     *
     * @return mixed|string
     */
    public function uploadLicense($entity)
    {
        $img = $this->req->files->get('license');
        /** @var Admin $entity */

        if ($img) {
            if ($entity) {
                if ($entity->getLicense()) {
                    unlink('../public/'.$entity->getLicense());
                }
            }

            $targetDir        = '../public/images/admin';
            $originalFilename = pathinfo($img->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename     = str_replace(' ', '-', $originalFilename);
            $fileName         = $safeFilename.'-'.uniqid().'.'.$img->guessExtension();
            $img->move($targetDir, $fileName);

            return '/images/admin/'.$fileName;
        }

        return $entity->getLicense();
    }

    /**
     * @param $entity
     *
     * @return string
     */
    public function EditLicense($entity)
    {
        $img = $this->req->files->get('foto');
        /** @var Admin $entity */
        if (file_exists('../public'.$img)) {
            if ($entity->getLicense()) {
                unlink('../public'.$entity->getLicense());
            }
        }

        $targetDir        = '../public/images/admin';
        $originalFilename = pathinfo($img->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename     = str_replace(' ', '-', $originalFilename);
        $fileName         = $safeFilename.'-'.uniqid().'.'.$img->guessExtension();
        $img->move($targetDir, $fileName);

        return '/images/admin/'.$fileName;

    }

    /**
     * @param $entity
     *
     * @return string
     */
    public function EditAvatar($entity)
    {
        $img = $this->req->files->get('foto');
        /** @var Admin $entity */

        if (file_exists('../public'.$img)) {
            if ($entity->getAvatar()) {
                unlink('../public'.$entity->getAvatar());
            }
        }

        $targetDir        = '../public/images/admin';
        $originalFilename = pathinfo($img->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename     = str_replace(' ', '-', $originalFilename);
        $fileName         = $safeFilename.'-'.uniqid().'.'.$img->guessExtension();
        $img->move($targetDir, $fileName);

        return '/images/admin/'.$fileName;

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/admin-user/add-admin", name="add-admin")
     */
    public function pageAdd()
    {
        if ($this->req->isMethod('POST')) {
            try {
                $this->class            = User::class;
                $this->secondClassToAdd = Admin::class;
                $this->data['class']    = $this->class;
                $this->redirectAction   = 'add-admin';
                $this->post();
                $code     = 200;
                $response = [
                    'status' => 'Berhasil menyimpan data',
                    'label'  => 'success',
                ];
            } catch (\Exception $e) {
                $code     = 500;
                $response = [
                    'status' => 'Terjadi kesalahan saat menghapus data '.$e->getMessage(),
                    'label'  => 'Warning',
                ];
            }

            return GenBasic::send($code, $response);
        }

        return $this->renderTable('admin/adminUserAdd.html.twig');
    }

    /**
     * @param $id
     *
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/admin/admin-user/edit-admin/{id}", name="edit-admin")
     */
    public function pageEdit($id)
    {

        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findOneBy(['id' => $id]);
        /** @var Admin $admin */
        $admin = $this->em->getRepository(Admin::class)->findOneBy(['user' => $user]);

        if ($this->req->isMethod('POST')) {
            if ($this->req->get('action') == 'avatar') {
                $admin->setAvatar($this->EditAvatar($admin));
            } elseif ($this->req->get('action') == 'license') {
                $admin->setLicense($this->EditLicense($admin));

            } else {
                $user->setEmail($this->req->get('email'))
                     ->setRoles($this->setRoles(''))
                     ->setProvider($this->req->get('tipeEmail'));

                $admin->setFullName($this->req->get('fullName'))
                      ->setPhone($this->req->get('phone'))
                      ->setPrivilege($this->req->get('privilege'))
                      ->setPosition($this->req->get('position'));
            }
            $this->addFlash('success', 'Berhasil dirubah');

            $this->em->flush();

            return $this->redirectToRoute('edit-admin', ['id' => $id]);
        }
        $admin               = $this->em->getRepository(Admin::class)->findOneBy(['user' => $id]);
        $this->data['admin'] = $admin;

        return $this->renderTable('admin/adminUserEdit.html.twig');
    }

}
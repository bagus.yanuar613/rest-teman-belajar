<?php

namespace App\Controller\Admin\Keuangan;

use App\Controller\Admin\KonfirmasiPembayaran;
use App\Controller\Admin\TransactionController;
use App\Controller\BaseController;
use App\Entity\TentorSaldo;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PengeluaranController
 * @package App\Controller\Admin\Keuangan
 */
class PengeluaranController extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        parent::__construct($em);
        $this->formatDTResult = true;
        $this->tableFields    = [
            [
                'label'     => 'Tanggal Penarikan',
                'field'     => 'createdAt',
                'sql'       => 'a.createdAt',
                'formatter' => 'getCreatedAt-'.TransactionController::class,
            ],
            [
                'label'     => 'Nominal',
                'field'     => 'amount',
                'sql'       => 'a.amount',
                'formatter' => 'getDTAmount-'.KonfirmasiPembayaran::class,
            ],

        ];


        $this->singleNamespace = 'Admin\\\Keuangan';
        $this->controllerName  = 'PengeluaranController';
        $this->req             = Request::createFromGlobals();
        $this->class           = TentorSaldo::class;
        $this->data['class']   = $this->class;
        $this->requiredWhere   = 'a.type = 1';
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/keuangan/pengeluaran", name="pengeluaran")
     */
    public function index(){
        return $this->renderTable('admin/keuangan/pengeluaran.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("admin/transaksi/get-total-withdraw", name="get-total-withdraw")
     */
    public function getAktivStudent()
    {
        $date1 = date_create($this->req->get('start'));
        $date2 = date_create($this->req->get('end'));
        $range = date_diff($date1, $date2);
        $selisih = $range->format("%a") * 2;


        $old = "Select SUM(`amount`) as jum from tentor_saldo where `type` = 1 AND created_at BETWEEN DATE_ADD('" . $this->req->get('start') . "', INTERVAL -" . $selisih . " DAY) AND DATE_ADD('" . $this->req->get('start') . "', INTERVAL -1 DAY) ";
        $new = "Select SUM(`amount`) as jum from tentor_saldo where `type` = 1 AND created_at BETWEEN '" . $this->req->get('start') . "' AND '" . $this->req->get('end') . "' ";



        return $this->getCountPerncen($old, $new, 'decimal');
    }

}
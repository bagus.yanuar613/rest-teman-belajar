<?php
declare(strict_types=1);

namespace App\Controller\Admin\Keuangan;

use App\Controller\Admin\KonfirmasiPembayaran;
use App\Controller\Admin\TransactionController;
use App\Controller\BaseController;
use App\Entity\Transaction;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PendapatanController
 * @package App\Controller\Admin\Keuangan
 */
class PendapatanController extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->formatDTResult = true;

        $this->tableFields = [

            [
                'label'     => 'Tanggal Transaksi',
                'field'     => 'createdAt',
                'sql'       => 'a.createdAt',
                'formatter' => 'getCreatedAt-'.TransactionController::class,
            ],
            [
                'label' => 'Jenis Pendapatan',
                'field' => 'slug',
                'sql'   => 'a.slug',
                'formatter' => 'getDTSlug'
            ],

            [
                'label'     => 'Total Pembayaran',
                'field'     => 'amount',
                'sql'       => 'a.amount',
                'formatter' => 'getDTAmount-'.KonfirmasiPembayaran::class,
            ],
            [
                'label' => 'Kota',
                'field' => 'cityName',
                'sql'   => 'ct.cityName',
            ],
        ];

        $this->joint = [
            [
                'tipe'  => 'leftjoin',
                'join'  => 'm.district',
                'alias' => 'dis',
            ],
            [
                'tipe'  => 'leftjoin',
                'join'  => 'dis.city',
                'alias' => 'ct',
            ],
        ];
//        $this->data['order'] = ['column' => 1, 'type' => 'desc' ];
        $this->addSelect = "ct.cityName";
        $this->singleNamespace        = 'Admin\\\Keuangan';
        $this->controllerName         = 'PendapatanController';
        $this->req                    = Request::createFromGlobals();
        $this->class                  = Transaction::class;
        $this->data['class']          = $this->class;
        $this->requiredWhere          = "a.isPaid = true";

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/keuangan/pendapatan", name="pendapatan")
     */
    public function index(){
        return $this->renderTable('admin/keuangan/pendapatan.html.twig');
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTSlug($data){
        if($data['slug'] == 'cart'){
            $string = 'Les';
        }else{
            $string = 'Try Out';
        }

        return $string;
    }

}
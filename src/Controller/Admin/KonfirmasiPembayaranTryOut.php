<?php

namespace App\Controller\Admin;

use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Bank;
use App\Entity\BankAccount;
use App\Entity\Cart;
use App\Entity\Payment;
use App\Entity\Transaction;
use App\Entity\TryOut;
use App\Entity\TryOutFreeAccess;
use App\Entity\TryOutPayment;
use App\Entity\TryOutRegistrant;
use App\Entity\TryOutReward;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class KonfirmasiPembayaran
 * @package App\Controller\Admin
 */
class KonfirmasiPembayaranTryOut extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->formatDTResult = true;

        $this->tableFields = [
            [
                'label'     => 'Tanggal',
                'field'     => 'createdAt',
                'sql'       => 'a.createdAt',
                'formatter' => 'getCreatedAt-'.TransactionController::class,
            ],
            [
                'label' => 'Nama Try Out',
                'field' => 'title',
                'sql'   => 'tr.title',
            ],
            [
                'label' => 'level',
                'field' => 'name',
                'sql'   => 'l.name',
            ],
            [
                'label' => 'Jumlah Pendaftar',
                'field' => 'registrantNumber',
                'sql'   => 'a.registrantNumber',
            ],
            [
                'label'     => 'Total Harga',
                'field'     => 'totalPrice',
                'sql'       => 'a.totalPrice',
                'formatter' => 'getDTAmount',
            ],

        ];

        $this->data['order'] = ['column' => 1, 'type' => 'desc' ];

        $this->joint = [
            [
                'tipe'  => 'leftjoin',
                'join'  => 'a.transaction',
                'alias' => 't',
            ],
        ];
//        $this->joint = [
//            [
//                'tipe'  => 'leftJoin',
//                'join'  => 'a.user',
//                'alias' => 'u',
//            ],
//            [
//                'tipe'  => 'leftJoin',
//                'join'  => 'u.memberProfile',
//                'alias' => 'm',
//            ],
//            [
//                'tipe'  => 'leftJoin',
//                'join'  => 'a.tryOut',
//                'alias' => 'tr',
//            ],
//            [
//                'tipe'  => 'leftJoin',
//                'join'  => 'tr.category',
//                'alias' => 'c',
//            ],
//            [
//                'tipe'  => 'leftJoin',
//                'join'  => 'tr.level',
//                'alias' => 'l',
//            ],
//            [
//                'tipe'  => 'leftJoin',
//                'join'  => 'a.paymentMethod',
//                'alias' => 'p',
//            ],
////            [
////                'tipe'  => 'leftJoin',
////                'join'  => 'p.bank',
////                'alias' => 'b',
////            ],
//        ];
        $this->tableActions           = [
            'edit',
        ];
        $this->addSelect              = 'c.name as category, re.freeAccess, l.name as level, fr.referalCode, re.minRegistrant, p.id as bankid, re.isActive';
        $this->fieldReferenceOnUpdate = 'payment';
        $this->singleNamespace        = 'Admin';
        $this->controllerName         = 'KonfirmasiPembayaranTryOut';
        $this->req                    = Request::createFromGlobals();
        $this->class                  = TryOutRegistrant::class;
        $this->data['class']          = $this->class;
        $this->requiredWhereParam     = ':param = :id';

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/konfirmasi-pembayaran/try-out", name="try-out-payment")
     */
    public function index()
    {
        if ($this->req->isMethod('POST')) {


            try {
//                $this->redirectAction = 'konfirmasi-pembayaran';
//                $this->edit();

//                $payMethod = $this->em->getRepository(BankAccount::class)->findOneBy(['id' => $this->req->get('bank')]);
                /** @var TryOutRegistrant $tryOutRegistrant */
                $tryOutRegistrant = $this->em->getRepository(TryOutRegistrant::class)->findOneBy(['id' => $this->req->get('id')]);
                /** @var TryOutPayment $tryOutPayment */
                $tryOutPayment = $this->em->getRepository(TryOutPayment::class)->findOneBy((['registration' => $tryOutRegistrant->getId()]));
                $date          = new DateTime('', new DateTimeZone('Asia/Jakarta'));
                $freeAccess    = $this->em->getRepository(TryOutFreeAccess::class)->findOneBy(['registration' => $tryOutRegistrant->getId()]);
                $registrant    = ((int)$this->req->get('registrant')) - 1;

                /** @var TryOut $tryOut */
                $tryOut = $tryOutRegistrant->getTryOut();
                /** @var TryOutReward $reward */
                $reward = $tryOut->getReward();

                $fcmMember = $tryOutRegistrant->getUser()->getAppFcmToken();
//                $fcmMember = "c0ANMHoDTx6n292oQOzDGE:APA91bGR0k2_6lW-RJmXP-j74Fmn8vd68u3_xpeQxZJ_Y9RuvCQGZ2R8CKFNlAEdhg5Eb-St_Jz3pw4oTyJuxUoHNvIA6hSjF7tsqZKJAPoi6P7NyRoX5ABMvsg6Xy_j4PrhA3Nt8r8G";
                $message = '';
                if ($this->req->get('status') === '1') {

                    $tryOutRegistrant->setIsPaid(1);
                    if ($tryOutPayment) {
                        $tryOutPayment->setPaymentMethod('Transfer')
                                      ->setRegistration($tryOutRegistrant)
                                      ->setPaymentReference('')
                                      ->setUpdatedAt($date);
                    } else {
                        $tryOutPayment = new  TryOutPayment;
                        $tryOutPayment->setPaymentMethod('Transfer')
                                      ->setRegistration($tryOutRegistrant)
                                      ->setPaymentReference('')
                                      ->setCreatedAt($date)
                                      ->setUpdatedAt($date);
                        $this->em->persist($tryOutPayment);
                    }
                    $message = 'Selamat, pembelian tryout kamu sudah diterima & sudah bisa di akses';
                    if ($reward) {
                        $minRegistrant = $reward->getMinRegistrant();
                        $status        = $reward->getIsActive();
                        $free          = $reward->getFreeAccess();
                        if ($registrant >= $minRegistrant && $status == true) {
                            if ($freeAccess == null) {
                                $freeAccess = new TryOutFreeAccess;
                                $freeAccess->setCreatedAt($date)
                                           ->setUpdatedAt($date)
                                           ->setRegistration($tryOutRegistrant)
                                           ->setReferalCode($this->setReferralCode());
                                $this->em->persist($freeAccess);
                                $message = "$message. Kamu juga dapat $free akses tryout gratis";
                            }
                        }
                    }
                } else {
                    $message = "Maaf, Pembayaran tryout kamu kami tolak";
                }

                $responseNotif = [
                    'type' => 'Tryout Tolak',
                ];

                $dataRes = $this->sendNotification($fcmMember, "Admin : Konfirmasi Pembayaran Tryout (TEMAN BELAJAR)", $message, $responseNotif);

                $this->em->flush();
                $code     = 200;
                $response = [
                    'status' => 'Success',
                    'member' => $dataRes,
                ];
            } catch (\Exception $e) {
                $code     = 500;
                $response = [
                    'msg' => 'Terjadi kesalahan dalam pengambilan data : '.$e->getMessage(),
                ];
            }

            return GenBasic::send($code, $response);
        }

        return $this->renderTable('admin/konfirmasiPembayaranTryOut.html.twig');
    }

    /**
     * @param $val
     *
     * @return string
     * @throws \Exception
     */
    protected function setReferralCode()
    {
        $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString     = '';
        for ($i = 0; $i < 6; $i++) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }

        $current = $this->em->getRepository(TryOutFreeAccess::class)->findOneBy(['referalCode' => $randomString]);
        if ($current !== null) {
            return $this->setReferralCode();
        }

        return 'TT'.$randomString;
    }

    /**
     * @param $data
     *
     * @return bool|string
     */
    public function getDTStatus($data)
    {
        $a = '';
        if ($data['isPaid']) {
            $a = '<i class="fa fa-check-circle colorGreen"> Lunas';
        } else {
            $a = '<i class="fa fa-times-circle colorRed"> Belum ada pembayaran';
        }

        return $a;
    }

    /**
     * @param $data
     *
     * @return bool|string
     */
    public function getDTBank($data)
    {
        return $data['bank'] ?? '<i class="fa fa-times-circle colorRed"> Belum ada pembayaran';
    }

    /**
     * @param $data
     *
     * @return bool|string
     */
    public function getDTBankName($data)
    {
        return $data['holderName'] ?? '<i class="fa fa-times-circle colorRed"> Belum ada pembayaran';
    }

    /**
     * @param $data
     *
     * @return int
     */
    public function setStatus($data)
    {
        return 1;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getExpiredPay($data)
    {
        Carbon::setLocale('id');
        $date = Carbon::parse($data['expiredPay'], 'Asia/Jakarta')->isoFormat('LL, HH:mm');

        return $date;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTAmount($data)
    {
        $hasil_rupiah = 'Rp. '.number_format($data['totalPrice'], 0, ',', '.');

        return $hasil_rupiah;
    }

    /**
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/admin/konfirmasi-pembayaran/try-out/get-account/{id}", name="get-account")
     */
    public function getKonfirmation($id)
    {
        try {
            /** @var TryOutRegistrant $dat */
            $dat      = $this->em->getRepository(TryOutRegistrant::class)->findOneBy(['id' => $id]);
            $response = $dat->getConfirmationInfo();
            $code     = 200;
        } catch (\Exception $e) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data : '.$e->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }
}
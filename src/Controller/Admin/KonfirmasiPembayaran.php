<?php

namespace App\Controller\Admin;

use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Bank;
use App\Entity\BankAccount;
use App\Entity\Cart;
use App\Entity\Payment;
use App\Entity\Transaction;
use App\Repository\PaymentRepository;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use function MongoDB\BSON\toJSON;

/**
 * Class KonfirmasiPembayaran
 * @package App\Controller\Admin
 */
class KonfirmasiPembayaran extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->formatDTResult = true;

        $this->secondFieldstoAdd = [
            'bank:relation:'.BankAccount::class,
            'status:method:setStatus:',
        ];

        $this->tableFields = [
//            [
//                'label' => 'Nomor Pembayaran',
//                'field' => 'paymentReference',
//                'sql'   => 'a.paymentReference',
//            ],
            [
                'label' => 'Nama Siswa',
                'field' => 'member',
                'sql'   => 'm.fullName as member',
            ],
            [
                'label'     => 'Tanggal Transaksi',
                'field'     => 'createdAt',
                'sql'       => 'a.createdAt',
                'formatter' => 'getCreatedAt-'.TransactionController::class,
            ],
            [
                'label'     => 'Tanggal Expired',
                'field'     => 'paymentExpiration',
                'sql'       => 'a.paymentExpiration',
                'formatter' => 'getExpiredPay',
            ],
            [
                'label' => 'Tipe Transaksi',
                'field' => 'slug',
                'sql'   => 'a.slug',
                'formatter' => 'getDTSlug'
            ],
//            [
//                'label'     => 'Bank',
//                'field'     => 'bank',
//                'sql'       => 'b.name as bank',
//                'formatter' => 'getDTBank',
//            ],
            [
                'label'     => 'Total Pembayaran',
                'field'     => 'amount',
                'sql'       => 'a.amount',
                'formatter' => 'getDTAmount',
            ],
            [
                'label'     => 'Status Pembayaran',
                'field'     => 'isPaid',
                'sql'       => 'a.isPaid',
                'formatter' => 'getIsPaid-'.KonfirmasiPembayaranDetail::class,
            ],

        ];

        $this->data['tableFieldsPayment'] = [
            [
                'label' => 'Nomor Pembayaran',
                'field' => 'paymentReference',
            ],
            [
                'label' => 'Nama Siswa',
                'field' => 'member',
            ],
            [
                'label' => 'Tanggal Transaksi',
                'field' => 'createdAt',
            ],
            [
                'label' => 'Bank',
                'field' => 'bank',
                'sql'   => 'b.name as bank',
            ],
            [
                'label' => 'Total Pembayaran',
                'field' => 'amount',
            ],
            [
                'label' => 'Status',
                'field' => 'status',
            ],
        ];
//
//        $this->tableFields = [
//            [
//                'label'     => 'Total Pembayaran',
//                'field'     => 'amount',
//                'sql'       => 'a.amount',
//                'formatter' => 'getDTAmount',
//            ],
//        ];

        $this->joint = [
            [
                'tipe'  => 'leftjoin',
                'join'  => 'a.bank',
                'alias' => 'ba',
            ],
            [
                'tipe'  => 'leftjoin',
                'join'  => 'ba.bank',
                'alias' => 'b',
            ],
        ];

        $this->tableActions = [
            'edit',
        ];

        $this->data['order'] = ['column' => 3, 'type' => 'desc'];

        $this->addSelect = 'ba.id as bankid, a.id as paymentid';

        $this->fieldReferenceOnUpdate = 'payment';
        $this->singleNamespace        = 'Admin';
        $this->controllerName         = 'KonfirmasiPembayaran';
        $this->req                    = Request::createFromGlobals();
        $this->class                  = Transaction::class;
        $this->data['class']          = $this->class;
        $this->requiredWhere          = "a.slug is not null";
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/konfirmasi-pembayaran", name="payment")
     */
    public function index()
    {

        return $this->renderTable('admin/konfirmasiPembayaran.html.twig');
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTAmount($data)
    {
        $hasil_rupiah = 'Rp. '.number_format($data['amount'], 0, ',', '.');

        return $hasil_rupiah;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTSlug($data){
        if ($data['slug'] == 'cart'){
            $string = 'Les';
        }else{
            $string = 'Try Out';

        }
        return $string;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTisPaid($data)
    {
        return $data['isPaid'] == true ? 'Lunas' : 'Ditolak';
    }

    /**
     * @param $data
     *
     * @return bool|string
     */
    public
    function getDTStatus(
        $data
    ) {
        $string = '';
        switch ($data['status']) {
            case 2 :
                $string = 'Lunas';
                break;
            case 6 :
                $string = 'Ditolak';
                break;
            default:
                $string = 'Menunggu Konfirmasi';
                break;
        }

        return $string;
    }

    /**
     * @param $data
     *
     * @return bool|string
     */
    public
    function getDTBank(
        $data
    ) {
        return $data['bank'] ?? '<i class="fa fa-times-circle colorRed"> Belum ada pembayaran';
    }

    /**
     * @param $data
     *
     * @return bool|string
     */
    public
    function getDTBankName(
        $data
    ) {
        return $data['holderName'] ?? '<i class="fa fa-times-circle colorRed"> Belum ada pembayaran';
    }

    /**
     * @param $data
     *
     * @return int
     */
    public
    function setStatus(
        $data
    ) {
        return 1;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public
    function getExpiredPay(
        $data
    ) {
        Carbon::setLocale('id');
        $date = Carbon::parse($data['paymentExpiration'])->isoFormat('LL, HH:mm');

        return $date;
    }

}
<?php

namespace App\Controller\Admin;

use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Level;
use App\Repository\LevelRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use function MongoDB\BSON\toPHP;

/**
 * Class DashboardController
 * @package App\Controller\Admin
 */
class DashboardController extends BaseController
{

    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->tableFields = [
            [
                'label' => 'Nama',
                'field'   => 'name',
                'sql'   => 'a.name',
            ],
            [
                'label' => 'Alamat',
                'field' => 'slug',
                'sql'   => 'a.slug',
            ],
        ];
        $this->singleNamespace = 'Admin';
        $this->class         = Level::class;
        $this->data['class'] = $this->class;
        $this->data['class1'] = 'AdminController';
    }

    /**
     * @return mixed|\Symfony\Component\HttpFoundation\Response
     * @Route("/admin", name="admin")
     */
    public function berandaAdmin()
    {
//        $level     = $this->em->getRepository(Level::class);
//        $datalevel = $level->findAll();
//        $datale    = $level->findBy(['id' => 1]);

//        $data = [
//            [
//                'field' => 'name',
//            ],
//            [
//                'field' => 'slug',
//            ],
//        ];

//        dump($datalevel);
//        dump($datale);die();
//        if ($datale) {
//            $dat = [];
//            foreach ($datale as $key => $item) {
//                $dat[$key]['title'] = $item->getName();
//                $dat[$key]['isi'] = $item->getSlug();
//            }
//           $this->data['detail'] = $dat;
//        }

//dump($this->data);die();

        $this->data['action'] = [
            [

                'name'   => 'Edit',
                'target' => '/edit',
            ],
            [
                'name'   => 'Delete',
                'target' => '/delete',
            ],
        ];
        $this->data['where'] = 'a.id=1';
        return $this->renderTable('admin/beranda.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/getdata", name="getdata")
     */
    public function getData()
    {
//        try {
        /** @var LevelRepository $level */
        $level     = $this->em->getRepository(Level::class);
        $datalevel = $level->findAll();
        $datale    = $level->findBy(['id' => 1]);
        $code      = 200;
        $label = [
            [
                'label' => 'Nama',
                'field' => 'Name'
            ],
            [
                'label' => 'Slug',
                'field' => 'Slug'
            ]
        ];
        if ($datale) {
            $dat = [];
            foreach ($label as $key => $lab){
                $dat[$key]['label'] = $lab['label'];
                $getter = 'get'.$lab['field'];
                $entiti = $datale;
//                    dump($entiti);die();
                foreach ($datale as $item) {
                    $dat[$key]['title'] = $getter;
                }
            }

//                array_push($this->data['detail'],$dat);
            $this->data['detail'] = $dat;
        }
        $response = GenBasic::serializeToJson($this->data['detail']);
//        } catch (\Exception $e) {
//            $code     = 500;
//            $response = [
//                'msg' => 'Terjadi kesalahan dalam pengambilan data',
//            ];
//        }
//
//        return GenBasic::send($code, $response);

    }


}
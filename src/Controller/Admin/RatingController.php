<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Member;
use App\Entity\Rating;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RatingController
 * @package App\Controller\Admin
 */
class RatingController extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->formatDTResult = true;

        $this->tableFields = [
            [
                'label' => 'User',
                'field' => 'fullName',
                'sql' => 'm.fullName'
            ],
            [
                'label' => 'Rating',
                'field' => 'rating',
                'sql' => 'a.rating',
                'formatter' => 'getDTRating'
            ],
            [
                'label' => 'Ulasan',
                'field' => 'review',
                'sql' => 'a.review'
            ]
        ];

        $this->singleNamespace = 'Admin';
        $this->controllerName  = "RatingController";
        $this->req             = Request::createFromGlobals();
        $this->class           = Rating::class;
        $this->data['class']   = $this->class;
        $this->requiredWhereParam = ':rating = :id';

    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function getDTRating($data){
        $rating = (int)$data['rating'];

        $set = '';
        for ($i = 1; $i <= $rating; $i++){
            $st = '<i class="fa fa-star colorOrange" aria-hidden="true"></i>';
            $set = $set.''.$st;
        }
        return $set;


    }

}
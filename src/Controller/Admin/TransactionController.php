<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Attendance;
use App\Entity\Cart;
use App\Entity\Member;
use App\Entity\Transaction;
use Carbon\Carbon;
use DateInterval;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TransactionController
 * @package App\Controller\Admin
 */
class TransactionController extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->formatDTResult = true;

        $this->tableFields = [
            [
                'label' => 'Tanggal',
                'field' => 'createdAt',
                'sql' => 'a.createdAt',
                'formatter' => 'getCreatedAt',
            ],
            [
                'label' => 'Nomor Langganan',
                'field' => 'subscriptionCode',
                'sql' => 'a.subscriptionCode',
            ],
            [
                'label' => 'Nominal Transaksi',
                'field' => 'amount',
                'sql' => 'c.amount',
                'formatter' => 'getDTAmount'
            ],
            [
                'label' => 'Mata Pelajaran',
                'field' => 'subject',
                'sql' => 's.name as subject',
            ],
            [
                'label' => 'Nama Pengajar',
                'field' => 'tentor',
                'sql' => 't.fullName as tentor',
            ],
            [
                'label' => 'Nama Siswa',
                'field' => 'member',
                'sql' => 'm.fullName as member',
            ],
            [
                'label' => 'Nomor Referensi ',
                'field' => 'referenceId',
                'sql' => 'c.referenceId',
            ],
            [
                'label' => 'Total Transaksi',
                'field' => 'total',
                'sql' => 'c.total',
                'formatter' => 'getDTTotal-' . LanggananDetailController::class
            ],
            [
                'label' => 'Sisa Pertemuan',
                'field' => 'encounter',
                'sql' => 'c.encounter',
            ],
            [
                'label' => 'Jenis Transaksi',
                'field' => 'paymentMethod',
                'sql' => 'p.paymentMethod',
            ],
            [
                'label' => 'Sisa',
                'field' => 'sisaPertemuan',
                'sql' => 'a.id',
            ],
        ];

        $this->addSelect = '(SELECT (c.encounter - COUNT(at.id)) from ' . Attendance::class . ' at where at.cart = c.id) as sisaPertemuan';

        $this->joint = [
            [
                'tipe' => 'join',
                'join' => 'a.cart',
                'alias' => 'c',
            ],
            [
                'tipe' => 'join',
                'join' => 'c.payment',
                'alias' => 'p',
            ],

        ];

        $this->singleNamespace = 'Admin';
        $this->controllerName = 'TransactionController';
        $this->req = Request::createFromGlobals();

        $this->class = Transaction::class;
        $this->requiredWhereParam = ':param = :id';

    }

    public function newTransaction($object, $amount, $slug, $bank)
    {
        try {
            $expired =  new DateTime('', new DateTimeZone('Asia/Jakarta'));
            $expired->add(new DateInterval('PT24H'));
            $transaction = new Transaction();
            $transaction->setUser($this->getMyUser())
                ->setSlug($slug)
                ->setAmount($amount)
                ->setBank($bank)
                ->setPaymentExpiration($expired);
            $this->em->persist($transaction);
            $this->em->flush();
            if(is_array($object)) {
                foreach ($object as $obj){
                    $obj->setTransaction($transaction);
                }
            }else {
                $object->setTransaction($transaction);
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }

    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTAmount($data)
    {
        $hasil_rupiah = 'Rp. ' . number_format($data['amount'], 0, ',', '.');

        return $hasil_rupiah;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getCreatedAt($data)
    {
        Carbon::setLocale('id');
        $date = Carbon::parse($data['createdAt'])->isoFormat('LL, HH:mm');

        return $date;
    }


    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("admin/transaksi/get-total-transaksi", name="get-total-transaksi")
     */
    public function getAktivStudent()
    {
        $date1 = date_create($this->req->get('start'));
        $date2 = date_create($this->req->get('end'));
        $range = date_diff($date1, $date2);
        $selisih = $range->format("%a") * 2;


        $old = "Select count(id) as jum from transaction where created_at BETWEEN DATE_ADD('" . $this->req->get('start') . "', INTERVAL -" . $selisih . " DAY) AND DATE_ADD('" . $this->req->get('start') . "', INTERVAL -1 DAY) ";
        $new = "Select count(id) as jum from transaction where created_at BETWEEN '" . $this->req->get('start') . "' AND '" . $this->req->get('end') . "' ";


        return $this->getCountPerncen($old, $new);
    }

}
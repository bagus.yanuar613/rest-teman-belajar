<?php

namespace App\Controller\Admin\Laporan;

use App\Controller\BaseController;
use App\Entity\Member;
use App\Entity\Subscription;
use App\Entity\Transaction;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SiswaController
 * @package App\Controller\Admin\Laporan
 */
class SiswaController extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->tableFields = [
            [
                'label' => 'Kota',
                'field' => 'cityName',
                'sql'   => 'ct.cityName',
            ],
            [
                'label' => 'Mata Pelajaran',
                'field' => 'subject',
                'sql'   => 'c.name as subject',
            ],
            [
                'label' => 'Kelas',
                'field' => 'class',
                'sql'   => 'cp.name as class',
            ],
            [
                'label' => 'Jumlah',
                'field' => 'jum',
                'sql'   => 's.id',
            ],

        ];

        $this->joint     = [
            [
                'tipe'  => 'leftjoin',
                'join'  => 'm.district',
                'alias' => 'd',
            ],
            [
                'tipe'  => 'leftjoin',
                'join'  => 'd.city',
                'alias' => 'ct',
            ],
            [
                'tipe'  => 'leftjoin',
                'join'  => 'm.class',
                'alias' => 'mc',
            ],
            [
                'tipe'  => 'leftjoin',
                'join'  => 'mc.memberClassParent',
                'alias' => 'cp',
            ],
        ];
        $this->addSelect = 'count(s.id) as jum, ct.cityName';

        $this->singleNamespace    = 'Admin\\\Laporan';
        $this->controllerName     = 'SiswaController';
        $this->req                = Request::createFromGlobals();
        $this->class              = Subscription::class;
        $this->data['class']      = $this->class;
        $this->requiredWhereParam = "s.createdAt between ':dateStart' and ':dateEnd' ";
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/pengguna/siswa", name="laporan-siswa")
     */
    public function index()
    {
        $now = new \DateTime();

        $this->data['dateStart'] = $this->req->get('dateStart') ?? $now->format('d-m-Y');
        $this->data['dateEnd']   = $this->req->get('dateEnd') ?? $now->format('d-m-Y');
        $start                   = new \DateTime($this->data['dateStart'].' 00:00:00');
        $end                     = new \DateTime($this->data['dateEnd'].' 23:59:59');

        if ($this->req->get('dateStart') === '' || $this->req->get('dateStart') == null) {
            $start->modify('-29 day');
        }
        $this->data['where']['dateStart'] = $start->format('Y-m-d H:i:s');
        $this->data['where']['dateEnd']   = $end->format('Y-m-d H:i:s');

//        $this->groupBy = 'm.class';
        $this->data['groupBy']        = 'ct.id';
        $this->data['groupByKelas']   = 'cp.id';
        $this->data['groupBySubject'] = 'c.id';

        $this->data['tableFieldsKelas'] = [
            [
                'label' => 'Kelas',
                'field' => 'class',
            ],
            [
                'label' => 'Jumlah',
                'field' => 'jum',
            ],
        ];

        $this->data['tableFieldsKota'] = [
            [
                'label' => 'Kota',
                'field' => 'cityName',
            ],
            [
                'label' => 'Jumlah',
                'field' => 'jum',
            ],
        ];

        $this->data['tableFieldsSubject'] = [
            [
                'label' => 'Mata Pelajaran',
                'field' => 'subject',
            ],
            [
                'label' => 'Jumlah',
                'field' => 'jum',
            ],
        ];

        return $this->renderTable('admin/Laporan/laporanSiswa.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("admin/pengguna/siswa/get-aktif-siswa", name="get-aktif-siswa")
     */
    public function getAktivStudent()
    {
        $date1   = date_create($this->req->get('start'));
        $date2   = date_create($this->req->get('end'));
        $range   = date_diff($date1, $date2);
        $selisih = $range->format("%a") * 2;

        $old = "Select count(id) as jum from member where created_at BETWEEN DATE_ADD('".$this->req->get('start')."', INTERVAL -".$selisih." DAY) AND DATE_ADD('".$this->req->get(
                'start'
            )."', INTERVAL -1 DAY) ";
        $new = "Select count(id) as jum from member where created_at BETWEEN '".$this->req->get('start')."' AND '".$this->req->get('end')."' ";

        return $this->getCountPerncen($old, $new);
    }

}
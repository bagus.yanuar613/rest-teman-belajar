<?php

namespace App\Controller\Admin\Laporan;

use App\Controller\BaseController;
use App\Entity\Subscription;
use App\Entity\Tentor;
use App\Entity\Transaction;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PengajarController
 * @package App\Controller\Admin\Laporan
 */
class PengajarController extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->tableFields = [
            [
                'label' => 'Kota',
                'field' => 'cityName',
                'sql'   => 'ct.cityName',
            ],
            [
                'label' => 'Mata Pelajaran',
                'field' => 'subject',
                'sql'   => 'c.name as subject',
            ],
            [
                'label' => 'Jumlah',
                'field' => 'jum',
                'sql'   => 's.id',
            ],
        ];

        $this->joint = [
//            [
//                'tipe'  => 'leftjoin',
//                'join'  => 's.category',
//                'alias' => 'cat',
//            ],
            [
                'tipe'  => 'leftjoin',
                'join'  => 'ts.user',
                'alias' => 'ut',
            ],
            [
                'tipe'  => 'leftjoin',
                'join'  => 'ut.district',
                'alias' => 'd',
            ],
            [
                'tipe'  => 'leftjoin',
                'join'  => 'd.city',
                'alias' => 'ct',
            ],
        ];

        $this->addSelect = 'count(s.id) as jum';

        $this->singleNamespace    = 'Admin\\\Laporan';
        $this->controllerName     = 'PengajarController';
        $this->req                = Request::createFromGlobals();
        $this->class              = Subscription::class;
        $this->data['class']      = $this->class;
        $this->requiredWhereParam = "s.createdAt between ':dateStart' and ':dateEnd' ";

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/pengguna/pengajar", name="laporan-pengajar")
     */
    public function index()
    {
        $now = new \DateTime();

        $this->data['dateStart'] = $this->req->get('dateStart') ?? $now->format('d-m-Y');
        $this->data['dateEnd']   = $this->req->get('dateEnd') ?? $now->format('d-m-Y');
        $start                   = new \DateTime($this->data['dateStart'].' 00:00:00');
        $end                     = new \DateTime($this->data['dateEnd'].' 23:59:59');

        if ($this->req->get('dateStart') === '' || $this->req->get('dateStart') == null) {
            $start->modify('-29 day');
        }
        $this->data['where']['dateStart'] = $start->format('Y-m-d H:i:s');
        $this->data['where']['dateEnd']   = $end->format('Y-m-d H:i:s');

//        $this->groupBy = 'a.id';
        $this->data['groupByKota']    = 'ct.id';
        $this->data['groupBySubject'] = 'c.id';

        $this->data['tableFieldsKota'] = [
            [
                'label' => 'Kota',
                'field' => 'cityName',
            ],
            [
                'label' => 'Jumlah',
                'field' => 'jum',
            ],
        ];

        $this->data['tableFieldsSubject'] = [
            [
                'label' => 'Mata Pelajaran',
                'field' => 'subject',
            ],
            [
                'label' => 'Jumlah',
                'field' => 'jum',
            ],
        ];

        return $this->renderTable('admin/Laporan/laporanPengajar.html.twig');
    }

}
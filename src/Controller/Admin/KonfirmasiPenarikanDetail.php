<?php

namespace App\Controller\Admin;

use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Payment;
use App\Entity\TentorSaldo;
use App\Entity\Withdraw;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class KonfirmasiPenarikanDetail
 * @package App\Controller\Admin
 */
class KonfirmasiPenarikanDetail extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->formatDTResult = true;
        $this->tableFields    = [
            [
                'label'     => 'Tanggal Pengajuan',
                'field'     => 'createdAt',
                'sql'       => 'a.createdAt',
                'formatter' => 'getCreatedAt-'.TransactionController::class,
            ],
            [
                'label'     => 'Nominal',
                'field'     => 'amount',
                'sql'       => 'a.amount',
                'formatter' => 'getDTAmount-'.KonfirmasiPembayaran::class,
            ],
        ];

        $this->singleNamespace    = 'Admin';
        $this->controllerName     = 'KonfirmasiPenarikanDetail';
        $this->req                = Request::createFromGlobals();
        $this->class              = TentorSaldo::class;
        $this->data['class']      = $this->class;
        $this->requiredWhereParam = 'u.id = :user AND a.type = :type';
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/konfirmasi-penarikan/detail", name="konfirmasi-penarikan-detail")
     */
    public function index()
    {
        $this->data['wherePemasukan']['user'] = $this->req->get('id');
        $this->data['wherePemasukan']['type'] = 0;
        $this->data['wherePemasukan']         = json_encode($this->data['wherePemasukan']);

        $this->data['wherePenarikan']['user'] = $this->req->get('id');
        $this->data['wherePenarikan']['type'] = 1;
        $this->data['wherePenarikan']         = json_encode($this->data['wherePenarikan']);

        $this->data['classPenarikan']              = 'KonfirmasiPenarikan';
        $this->data['whereCardPenarikan']['param'] = 'a.id';
        $this->data['whereCardPenarikan']['id']    = $this->req->get('p');
        $this->data['whereCardPenarikan']          = json_encode($this->data['whereCardPenarikan']);

        /** @var Withdraw $wid */
        $wid                  = $this->em->getRepository(Withdraw::class)->findOneBy(['id' => $this->req->get('p')]);
        $this->data['status'] = $wid->getStatus();

        $this->data['pengajar'] = [
            [
                'label' => 'Nama',
                'field' => 'fullName',
                'sql'   => 't.fullName',
            ],
            [
                'label'     => 'Jenis Kelamin',
                'field'     => 'gender',
                'sql'       => 't.gender',
                'formatter' => 'gender',
            ],
            [
                'label' => 'Email',
                'field' => 'email',
                'sql'   => 'u.email',
            ],
            [
                'label' => 'Nomor Hp',
                'field' => 'phone',
                'sql'   => 't.phone',
            ],
            [
                'label'     => 'Saldo',
                'field'     => 'balance',
                'sql'       => 't.balance',
                'formatter' => 'balance',
            ],
        ];

        $this->data['penarikan'] = [
            [
                'label'     => 'Status',
                'field'     => 'status',
                'sql'       => 'a.status',
                'formatter' => 'status',
            ],
            [
                'label'     => 'Nominal',
                'field'     => 'amount',
                'sql'       => 'a.amount',
                'formatter' => 'amount',
            ],
            [
                'label' => 'Bank',
                'field' => 'name',
                'sql'   => 'b.name',
            ],
            [
                'label' => 'Nomor Rekening',
                'field' => 'rekening',
                'sql'   => 't.rekening',
            ],
            [
                'label' => 'Atas Nama',
                'field' => 'holderName',
                'sql'   => 't.holderName',
            ],
        ];

        return $this->renderTable('admin/konfirmasi-penarikan-detail.html.twig');
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTStatus($data)
    {
        $string = '';
        switch ($data['status']) {
            case 0 :
                $string = '<i class="fa fa-question-circle colorOrange"> Menunggu Konfirmasi Admin</i>';
                break;
            case 1 :
                $string = '<i class="fa fa-check-circle text-blue"> Pengajuan Diterima</i>';
                break;
            default:
                $string = '<i class="fa fa-times-circle colorRed"> Pengajuan Ditolak</i>';
                break;
        }

        return $string;
    }

    /**
     * @param $type
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\DBAL\Driver\Exception
     * @Route("/admin/saldo-tentor", name="saldo-tentor")
     */
    public function TotalAmount(){
        try {
            $id = $this->req->get('id');
            $type = $this->req->get('type');
            $sql = "SELECT SUM(`amount`) as total FROM `tentor_saldo` WHERE `type` = $type AND user_id = $id";
            $saldo = $this->em->getConnection()->prepare($sql);
            $saldo->execute();
            $total = $saldo->fetchColumn();
            $data = [
                'total' => number_format($total, 0, ',', '.'),
            ];
            $code = 200;
            $response = GenBasic::serializeToJson($data);
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data',
            ];
        }

        return GenBasic::send($code, $response);
    }
    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("admin/konfirmasi-penarikan/detail/konfirmasi", methods="POST")
     */
    public function konfirmasi()
    {
        try {
            /** @var Withdraw $wid */
            $wid    = $this->em->getRepository(Withdraw::class)->findOneBy(['id' => $this->req->get('id')]);
            $status = $this->req->get('status');
            $date   = new DateTime('', new DateTimeZone('Asia/Jakarta'));

            $wid->setStatus($status);

            $tarik = 'Rp. '.number_format($wid->getAmount(), 0, ',', '.');
            $alasan = $this->req->get('text');
            $message = "Maaf, Permintaan penarikan anda sebesar $tarik, kami tolak dengan alasan : $alasan";

            if ($status == 1) {
                $saldo = new TentorSaldo;
                $saldo->setUser($wid->getUser())
                      ->setType(1)
                      ->setAmount($wid->getAmount())
                      ->setDescription('Penarikan')
                      ->setCreatedAt($date)
                      ->setUpdatedAt($date);

                $this->em->persist($saldo);
                $message = "Selamat, Permintaan penarikan anda sebesar $tarik, kami terima";

            }
            $responseNotif = [
                'type' => 'Penarikan',
            ];
            $fcm     = $wid->getUser()->getAppFcmToken();
//                $fcmMember = "c0ANMHoDTx6n292oQOzDGE:APA91bGR0k2_6lW-RJmXP-j74Fmn8vd68u3_xpeQxZJ_Y9RuvCQGZ2R8CKFNlAEdhg5Eb-St_Jz3pw4oTyJuxUoHNvIA6hSjF7tsqZKJAPoi6P7NyRoX5ABMvsg6Xy_j4PrhA3Nt8r8G";
            $dataRes = $this->sendNotification($fcm, "Admin : Konfirmasi Penarikan (TEMAN BELAJAR)", $message, $responseNotif);
            $this->em->flush();

            $code     = 200;
            $response = [
                'status' => 'Success',
                'response' => $dataRes
            ];
        } catch (\Exception $e) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data : '.$e->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);

    }

    /**
     * @param $data
     *
     * @return string
     */
    public function gender($data)
    {
        if ($data['gender'] == 0) {
            $gender = 'Perempuan';
        } else {
            $gender = 'Laki - Laki';
        }

        return $gender;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function balance($data)
    {
        $hasil_rupiah = 'Rp. '.number_format($data['balance'], 0, ',', '.');

        return $hasil_rupiah;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTAmount($data)
    {
        $hasil_rupiah = 'Rp. '.number_format($data['amount'], 0, ',', '.');

        return $hasil_rupiah;
    }

}
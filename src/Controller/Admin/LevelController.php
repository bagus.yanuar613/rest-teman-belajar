<?php

namespace App\Controller\Admin;

use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Level;
use App\Repository\LevelRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;

class LevelController extends BaseController
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/admin/get-level", name="get-level")
     */
    public function getLevel(){
        try {
            /** @var LevelRepository $level */
            $level = $this->em->getRepository(Level::class);
            $repo = $level->createQueryBuilder('a')
                ->select(['a.id,a.name'])
                ->getQuery()
                ->getArrayResult();
            $code     = 200;
            $response = GenBasic::serializeToJson($repo);
        }catch (\Exception $e){
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data',
            ];
        }
        return GenBasic::send($code, $response);
    }

}
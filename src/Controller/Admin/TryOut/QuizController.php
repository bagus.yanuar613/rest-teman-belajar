<?php


namespace App\Controller\Admin\TryOut;


use App\Controller\Admin\LanggananDetailController;
use App\Controller\Admin\Pengaturan\SubjectController;
use App\Controller\BaseController;
use App\Entity\Categories;
use App\Entity\CategoriesParent;
use App\Entity\Level;
use App\Entity\TryOutQuiz;
use App\Entity\TryOutSubject;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class QuizController extends BaseController
{

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->singleNamespace = 'Admin\\\TryOut';
        $this->controllerName = 'QuizController';

        $this->req = Request::createFromGlobals();
        $this->class = TryOutQuiz::class;
        $this->data['class'] = $this->class;
        $this->requiredWhereParam = " ts.id = :subject ";
        $this->tableFields = [
            [
                'label' => 'Naskah Soal',
                'field' => 'question',
                'sql' => 'a.question',
            ]

        ];

        $this->tableActions = [
            'quiz',
//            'delete',
        ];

        $this->formatDTResult = true;

        $this->fields = [
            'tryOut:method:getTryOut:',
            'topic',
            'question',
            'explanation',
            'options:method:getOptions:'
        ];
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/tryout/quiz/{subject}", name="try-out-quiz-data")
     */
    public function getQuiz($subject)
    {
        $this->data['where']['subject'] = $subject;
        $this->data['subject'] = $subject;
        $this->data['add_url'] = $this->generateUrl("try-out-quiz-new", ['subject' => $subject]);
        return $this->renderTable('admin/TryOut/quiz.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/tryout/quiz-new/{subject}", name="try-out-quiz-new")
     */
    public function addQuiz($subject)
    {
        if ($this->req->isMethod('POST')) {
            $this->redirectAction = 'try-out-quiz-data';
            $this->parameterRedirect = ['subject' => $subject];
//            if ($this->req->get('action') == 'add') {
            return $this->post();
//            } else {
//                return $this->edit();
//            }
        }
        $this->data['subject'] = (int)$subject;
        return $this->renderTable('admin/TryOut/quiz_new.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/tryout/image", name="tryout_images")
     */
    public function imageUploadHandle()
    {
        $img = $this->req->files->get('file');
        $imgUrl =  "/images/no-image.png";
        /** @var Categories $entity */
        if ($img) {

            $targetDir = '../public/images/mapel';
            $originalFilename = pathinfo($img->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = str_replace(' ', '-', $originalFilename);
            $fileName = $safeFilename . '-' . uniqid() .(new \DateTime())->format("YmdHmis").'.'. $img->guessExtension();
            $upload = $img->move($targetDir, $fileName);
            $imgUrl =  '/images/mapel/' . $fileName;
        }

        return new JsonResponse(["link" => $imgUrl]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/tryout/quiz-edit/{subject}/{idQuiz}", name="try-out-quiz-edit")
     */
    public function editQuiz($subject, $idQuiz)
    {
        if ($this->req->isMethod('POST')) {
            $this->redirectAction = 'try-out-quiz-data';
            $this->parameterRedirect = ['subject' => $subject];
            return $this->edit();
        }
        $this->data['subject'] = (int)$subject;
        $this->data['quiz'] = $this->em->getRepository(TryOutQuiz::class)->findOneBy(['id' => $idQuiz]);
        return $this->renderTable('admin/TryOut/quiz_edit.html.twig');
    }

    public function getTryOut()
    {
        $subjectId = $this->req->get('subjectId');
        return $this->em->getRepository(TryOutSubject::class)->findOneBy(['id' => $subjectId]);
    }

    public function getOptions()
    {
        $data = [
            [
                "option" => $this->req->get('answerA_editor'),
                "score" => $this->req->get('answerAScore')
            ],
            [
                "option" => $this->req->get('answerB_editor'),
                "score" => $this->req->get('answerBScore')
            ],
            [
                "option" => $this->req->get('answerC_editor'),
                "score" => $this->req->get('answerCScore')
            ],
            [
                "option" => $this->req->get('answerD_editor'),
                "score" => $this->req->get('answerDScore')
            ],
            [
                "option" => $this->req->get('answerE_editor'),
                "score" => $this->req->get('answerEScore')
            ]
        ];

        return $data;

    }

    public function quizButton($data)
    {
        return '<a name="action" data-cred="' . $data['id'] . '" id="editData"  class="btn btn-sm btn-warning btn-orange"><i class="fa fa-edit"></i></a>
        <a name="action" data-cred="' . $data['id'] . '" id="deleteData" class="btn btn-sm btn-danger btn-rose" ><i class="fa fa-trash"></i></a>';

    }
}
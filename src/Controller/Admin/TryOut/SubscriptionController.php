<?php


namespace App\Controller\Admin\TryOut;


use App\Controller\BaseController;
use App\Entity\Categories;
use App\Entity\TryOut;
use App\Entity\TryOutPricing;
use App\Entity\TryOutSubscription;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SubscriptionController extends BaseController
{
    private $to;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->singleNamespace = 'Admin\\\TryOut';
        $this->controllerName = 'SubscriptionController';
        $this->formatDTResult = true;

        $this->req = Request::createFromGlobals();
        $this->class = TryOutSubscription::class;
        $this->data['class'] = $this->class;
        $this->fields = [
            'tryOut:method:getTryOutRelation:',
            'price',
            'name',
            'description',
            'duration'
        ];

        $this->tableActions = [
            'delete',
            'edit'
        ];

        $this->tableFields = [
            [
                'label' => 'Name',
                'field' => 'name',
                'sql' => 'a.name',
            ],
            [
                'label' => 'Description',
                'field' => 'description',
                'sql' => 'a.description',
            ],
            [
                'label' => 'Harga',
                'field' => 'price',
                'sql' => 'a.price',
                'formatter' => 'getDTPrice-' . PackageController::class
            ],
            [
                'label' => 'Duration',
                'field' => 'duration',
                'sql' => 'a.duration',
            ],
        ];

        $this->requiredWhereParam = "t.id = :tid";

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/tryout-subscription/{id}", name="try-out-subscription")
     */
    public function index($id)
    {
        if ($this->req->isMethod('POST')) {
            $this->to = $id;
            $this->redirectAction = 'try-out-subscription';
            $this->parameterRedirect = ["id" => $id];
            $action = $this->req->get('action');
            return $this->$action();
        }

        $this->data['data'] = $this->em->getRepository(Categories::class)->findOneBy(["id" => $id]);
        $this->data['where']['tid'] = $id;

        return $this->renderTable('admin/TryOut/subs.html.twig');
    }

    public function getTryOutRelation()
    {
        return $this->em->getRepository(Categories::class)->findOneBy(['id' => $this->to]);
    }
}
<?php


namespace App\Controller\Admin\TryOut;


use App\Controller\Admin\Pengaturan\SubjectController;
use App\Controller\BaseController;
use App\Entity\Categories;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MapelController extends BaseController
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->fields = [
            'name',
            'isTryOut:method-'.CategoryController::class.':getIsTryOut:',
            'slug:method-'.SubjectController::class.':setSlug:',
            'icon:method-'.SubjectController::class.':setIcon:entity',

        ];

        $this->formatDTResult = true;
        $this->requiredWhere = "a.isTryOut = true AND a.isProduct = false";
        $this->tableFields = [
            [
                'label' => 'Icon',
                'field' => 'icon',
                'sql'   => 'a.icon',
                'formatter' => 'getDTIcon-'.SubjectController::class
            ],
            [
                'label' => 'Nama',
                'field' => 'name',
                'sql'   => 'a.name',
            ],

        ];

        $this->tableActions = [
            'edit',
        ];

        $this->singleNamespace = 'Admin\\\TryOut';
        $this->controllerName  = 'MapelController';

        $this->req           = Request::createFromGlobals();
        $this->class         = Categories::class;
        $this->data['class'] = $this->class;

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/tryout/mapel-new", name="try-out-mapel-new")
     */
    public function newCategory()
    {
        if($this->req->isMethod('POST')){
            $this->redirectAction = 'try-out-mapel-new';
            if($this->req->get('action') == 'add'){
                return $this->post();
            }else{
                return $this->edit();
            }
        }
        return $this->renderTable('admin/TryOut/MaPel.html.twig');
    }
}
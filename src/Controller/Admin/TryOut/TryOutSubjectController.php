<?php


namespace App\Controller\Admin\TryOut;


use App\Controller\Admin\LanggananDetailController;
use App\Controller\Admin\Pengaturan\SubjectController;
use App\Controller\BaseController;
use App\Entity\Categories;
use App\Entity\CategoriesParent;
use App\Entity\Subject;
use App\Entity\TryOut;
use App\Entity\TryOutSubject;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TryOutSubjectController extends BaseController
{

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->singleNamespace = 'Admin\\\TryOut';
        $this->controllerName = 'TryOutSubjectController';

        $this->req = Request::createFromGlobals();
        $this->class = TryOutSubject::class;
        $this->data['class'] = $this->class;

        $this->tableFields = [
            [
                'label' => 'Mata Pelajaran',
                'field' => 'name',
                'sql' => 's.name',
            ],
            [
                'label' => 'Kategori',
                'field' => 'cat_name',
                'sql' => 'ms.name as cat_name',
            ],
            [
                'label' => 'Jumlah Soal',
                'field' => 'quizNumber',
                'sql' => 'a.quizNumber',
            ],
            [
                'label' => 'Durasi',
                'field' => 'duration',
                'sql' => 'a.duration',
            ],
            [
                'label' => 'Icon',
                'field' => 'icon',
                'sql' => 'a.icon',
                'formatter' => 'getDTIcon-' . SubjectController::class
            ],
            [
                'label' => 'No Urut',
                'field' => 'sorting',
                'sql' => 'a.sorting'
            ],
            [
                'label' => 'Aktif',
                'field' => 'isActive',
                'sql' => 'a.isActive',
                'formatter' => 'isActive-' . LanggananDetailController::class
            ],

        ];

        $this->tableActions = [
            'manage',
            'edit',
            'delete',
        ];

        $this->formatDTResult = true;

        $this->fields = [
            'duration',
            'quizNumber',
            'isActive',
            'sorting',
            'subject:method:getSubject:',
            'mainSubject:method:getMainSubject:',
            'tryout:method:getTryOut:',
            'icon:method-' . SubjectController::class . ':setIcon:entity',
        ];
        $this->requiredWhereParam = "to.id = :to";
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/tryout/package/manage/{id}", name="try-out-package-manage")
     */
    public function tryOutSubject($id)
    {
        if ($this->req->isMethod('POST')) {
            $this->parameterRedirect = ['id' => $id];
            $this->redirectAction = 'try-out-package-manage';
            if ($this->req->get('action') == 'add') {
                return $this->post();
            } else {
                return $this->edit();
            }
        }

        $this->data['form'] = [

            [
                'name' => 'subject',
                'label' => 'Mata Pelajaran',
                'type' => 'combobox',
                'data' => $this->em->getRepository(Categories::class)->getMyOptionForComboBox()
            ],
            [
                'name' => 'mainSubject',
                'label' => 'Kategori',
                'type' => 'combobox',
                'data' => $this->em->getRepository(CategoriesParent::class)->getMyOptionForComboBox('a.isTryOut = true')
            ],
            [
                'name' => 'duration',
                'label' => 'Durasi'
            ],
            [
                'name' => 'tryout',
                'type' => 'hidden',
                'data' => $id
            ],
            [
                'name' => 'icon',
                'label' => 'Icon',
                'type' => 'image'
            ],
            [
                'name' => 'quizNumber',
                'label' => 'Jumlah Quiz'
            ],
            [
                'name' => 'isActive',
                'label' => 'Status',
                'type' => 'combobox'
            ],
            [
                'name' => 'sorting',
                'label' => 'Urutan No.'
            ],

        ];
        $this->data['to'] = $id;
        $this->data['where']['to'] = $id;
        return $this->renderTable('admin/TryOut/package_subject.html.twig');
    }


    public function getTryOut($data)
    {
        $id = $this->req->get('tryout');
        return $this->em->getRepository(TryOut::class)->findOneBy(['id' => $id]);
    }

    public function getSubject($data)
    {
        $id = $this->req->get('subject');
        return $this->em->getRepository(Categories::class)->findOneBy(['id' => $id]);
    }

    public function getMainSubject($data)
    {
        $id = $this->req->get('mainSubject');
        return $this->em->getRepository(CategoriesParent::class)->findOneBy(['id' => $id]);
    }

    public function manageButton($data)
    {
        $dataAttr = '';
        foreach ($data as $key => $val) {
            $dataAttr .= ' data-' . $key . '="' . $val . '" ';
        }

        $url = "/admin/tryout/quiz/" . $data['id'];
        return "<a href='$url' data-cred='" . $data['id'] . "' " . $dataAttr . " class='btn btn-sm btn-secondary' style='background-color:#4CAF50;'>Kelola</a>";
    }
}
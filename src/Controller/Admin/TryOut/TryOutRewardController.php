<?php


namespace App\Controller\Admin\TryOut;


use App\Controller\BaseController;
use App\Entity\TryOut;
use App\Entity\TryOutReward;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TryOutRewardController extends BaseController
{
    private $to;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->singleNamespace = 'Admin\\\TryOut';
        $this->controllerName = 'TryOutRewardController';

        $this->req = Request::createFromGlobals();
        $this->class = TryOutReward::class;
        $this->data['class'] = $this->class;
        $this->fields = [
            'tryOut:method:getTryOutRelation:',
            'freeAccess',
            'minRegistrant',
            'isActive'
        ];

        $this->tableFields = [
            [
                'label' => 'Try Out',
                'field' => 'title',
                'sql' => 't.title',
            ],
            [
                'label' => 'Jumlah akses',
                'field' => 'freeAccess',
                'sql' => 'a.freeAccess',
            ],
            [
                'label' => 'Jumlah pendaftar',
                'field' => 'minRegistrant',
                'sql' => 'a.minRegistrant',
            ],
            [
                'label' => 'Active',
                'field' => 'isActive',
                'sql' => 'a.isActive',
            ],
        ];

        $this->requiredWhereParam = "t.id = :tid";

    }



    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/tryout-reward/{id}", name="try-out-reward")
     */
    public function index($id)
    {
        if ($this->req->isMethod('POST')) {
            $this->to = $id;
            $this->redirectAction = 'try-out-package-manage';
            $this->parameterRedirect = ["id" => $id];
            return $this->post();
        }

        $this->data['where']['tid'] = $id;
        return $this->renderTable('admin/TryOut/reward.html.twig');
    }

    public function getTryOutRelation()
    {
        return $this->em->getRepository(TryOut::class)->findOneBy(['id' => $this->to]);
    }

}
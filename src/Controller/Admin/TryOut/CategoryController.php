<?php


namespace App\Controller\Admin\TryOut;


use App\Controller\Admin\Pengaturan\SubjectController;
use App\Controller\BaseController;
use App\Entity\Categories;
use App\Entity\CategoriesParent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->fields = [
            'name',
            'slug:method-'.SubjectController::class.':setSlug:',
            'isTryOut:method:getIsTryOut:'
        ];

        $this->formatDTResult = true;

        $this->tableFields = [
            [
                'label' => 'Nama',
                'field' => 'name',
                'sql'   => 'a.name',
            ],
            [
                'label' => 'Slug',
                'field' => 'slug',
                'sql'   => 'a.slug',
            ],

        ];
        $this->requiredWhere = "a.isTryOut = true";
        $this->tableActions = [
            'edit',
        ];

        $this->singleNamespace = 'Admin\\\TryOut';
        $this->controllerName  = 'CategoryController';

        $this->req           = Request::createFromGlobals();
        $this->class         = CategoriesParent::class;
        $this->data['class'] = $this->class;
    }
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/tryout/category-new", name="try-out-category-new")
     */
    public function newCategory()
    {
        if($this->req->isMethod('POST')){
            $this->redirectAction = 'try-out-category-new';
            if($this->req->get('action') == 'add'){
                return $this->post();
            }else{
                return $this->edit();
            }
        }
        return $this->renderTable('admin/TryOut/category.html.twig');
    }

    public function getIsTryOut()
    {
        return true;
    }
}
<?php


namespace App\Controller\Admin\TryOut;


use App\Controller\Admin\LanggananController;
use App\Controller\BaseController;
use App\Entity\TryOut;
use App\Entity\TryOutPricing;
use App\Entity\TryOutReward;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TryOutPricingController extends BaseController
{
    private $to;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->singleNamespace = 'Admin\\\TryOut';
        $this->controllerName = 'TryOutPricingController';
        $this->formatDTResult = true;

        $this->req = Request::createFromGlobals();
        $this->class = TryOutPricing::class;
        $this->data['class'] = $this->class;
        $this->fields = [
            'tryOut:method:getTryOutRelation:',
            'price',
            'min',
            'max'
        ];

        $this->tableActions = [
            'delete',
            'edit'
        ];

        $this->tableFields = [
            [
                'label' => 'Try Out',
                'field' => 'title',
                'sql' => 't.title',
            ],
            [
                'label' => 'Harga',
                'field' => 'price',
                'sql' => 'a.price',
                'formatter' => 'getDTPrice-'.PackageController::class
            ],
            [
                'label' => 'Min Pendaftar',
                'field' => 'min',
                'sql' => 'a.min',
            ],
            [
                'label' => 'Max Pendaftar',
                'field' => 'max',
                'sql' => 'a.max',
            ],
        ];

        $this->requiredWhereParam = "t.id = :tid";

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/tryout-pricing/{id}", name="try-out-pricing")
     */
    public function index($id)
    {
        if ($this->req->isMethod('POST')) {
            $this->to = $id;
            $this->redirectAction = 'try-out-pricing';
            $this->parameterRedirect = ["id" => $id];
            $action = $this->req->get('action');
            return $this->$action();
        }

        $this->data['data'] = $this->em->getRepository(TryOut::class)->findOneBy(["id" => $id]);

        $this->data['where']['tid'] = $id;
        return $this->renderTable('admin/TryOut/pricing.html.twig');
    }

    public function getTryOutRelation()
    {
        return $this->em->getRepository(TryOut::class)->findOneBy(['id' => $this->to]);
    }
}
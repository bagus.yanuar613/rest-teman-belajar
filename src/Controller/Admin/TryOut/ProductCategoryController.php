<?php


namespace App\Controller\Admin\TryOut;


use App\Controller\Admin\Pengaturan\SubjectController;
use App\Controller\BaseController;
use App\Entity\Categories;
use App\Entity\CategoriesParent;
use App\Entity\Level;
use App\Entity\ProductCategory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductCategoryController extends BaseController
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->fields = [
            'name',
            'price:method:priceInputFormat:',
            'isTryOut:method-' . CategoryController::class . ':getIsTryOut:',
            'product:method:getProduct:',
            'isProduct:method-' . CategoryController::class . ':getIsTryOut:',
            'slug:method-' . SubjectController::class . ':setSlug:',
            'icon:method-' . SubjectController::class . ':setIcon:entity',

        ];

        $this->formatDTResult = true;
        $this->requiredWhere = "a.isTryOut = true AND a.isProduct = true";
        $this->tableFields = [

            [
                'label' => 'Nama',
                'field' => 'name',
                'sql' => 'a.name',
            ],
            [
                'label' => 'Icon',
                'field' => 'icon',
                'sql' => 'a.icon',
                'formatter' => 'getDTIcon-' . SubjectController::class
            ],
            [
                'label' => 'Harga Langganan',
                'field' => 'price',
                'sql' => 'a.price',
                'formatter' => "getDTPrice-" . PackageController::class,
            ],
        ];

        $this->tableActions = [
            'edit',
            'subscription'
        ];

        $this->singleNamespace = 'Admin\\\TryOut';
        $this->controllerName = 'ProductCategoryController';

        $this->req = Request::createFromGlobals();
        $this->class = Categories::class;
        $this->data['class'] = $this->class;

    }

    public function priceInputFormat()
    {
        return str_replace(',', '', $this->req->get('price'));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/tryout/product-cat-new", name="try-out-product-cat-new")
     */
    public function newCategory()
    {
        if ($this->req->isMethod('POST')) {
            $this->redirectAction = 'try-out-product-cat-new';
            if ($this->req->get('action') == 'add') {
                return $this->post();
            } else {
                return $this->edit();
            }
        }

        $this->data['products'] = $this->em->getRepository(ProductCategory::class)->findAll();

        return $this->renderTable('admin/TryOut/product_cat.html.twig');
    }

    public function getProduct($param)
    {
        return $this->em->getRepository(ProductCategory::class)->findOneBy(["id" => $this->req->get("product")]);
    }
    public function subscriptionButton($data)
    {
        return "<a href='/admin/tryout-subscription/" . $data['id'] . "' class='btn btn-sm btn-secondary' style='background-color:#4CAF50;'>Subscription</a>";
    }
}
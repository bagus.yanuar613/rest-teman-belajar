<?php


namespace App\Controller\Admin\TryOut;


use App\Common\GenBasic;
use App\Controller\Admin\LanggananController;
use App\Controller\Admin\LanggananDetailController;
use App\Controller\Admin\Pengaturan\SubjectController;
use App\Controller\BaseController;
use App\Entity\Categories;
use App\Entity\Level;
use App\Entity\TryOut;
use App\Entity\TryOutQuiz;
use App\Entity\TryOutRegistrant;
use App\Entity\TryOutRegistrantAnswer;
use App\Entity\TryOutSubject;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PackageController extends BaseController
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->singleNamespace = 'Admin\\\TryOut';
        $this->controllerName = 'PackageController';

        $this->req = Request::createFromGlobals();
        $this->class = TryOut::class;
        $this->data['class'] = $this->class;


        $this->fields = [
            'title',
            'description',
            'isFree',
            'isActive',
            'codeAccess',
            'direction',
            'rules',
            'isStrict',
            'isIRT',
            'restTime',
            'estimatedTime',
            'startDay',
            'finalDay',
            'rules',
            'price:method-' . ProductCategoryController::class . ':priceInputFormat:',
            'banner:method:setBanner:entity',
            'level:method:getLevelRelation:',
            'category:method:getCategoryRelation:entity'
        ];

        $this->tableFields = [
            [
                'label' => 'Nama Paket',
                'field' => 'title',
                'sql' => 'a.title',
            ],
            [
                'label' => 'Nama Jenis',
                'field' => 'name',
                'sql' => 'c.name',
            ],
            [
                'label' => 'Harga',
                'field' => 'price',
                'sql' => 'a.price',
                'formatter' => 'getDTPrice'
            ],
            [
                'label' => 'Periode',
                'field' => 'finalDay',
                'sql' => 'a.finalDay',
                'formatter' => 'periodeTIme'
            ],
            [
                'label' => 'Aktif',
                'field' => 'isActive',
                'sql' => 'a.isActive',
                'formatter' => 'isActive-' . LanggananDetailController::class
            ],
            [
                'label' => 'Pengerjaan Urut',
                'field' => 'isStrict',
                'sql' => 'a.isStrict',
                'formatter' => 'isStrict'
            ],
            [
                'label' => 'IRT',
                'field' => 'isIRT',
                'sql' => 'a.isIRT',
                'formatter' => 'isIRT'
            ],
            [
                'label' => 'Waktu Istirahat',
                'field' => 'restTime',
                'sql' => 'a.restTime',
            ],

            [
                'label' => 'Waktu Pengerjaan',
                'field' => 'estimatedTime',
                'sql' => 'a.estimatedTime',
            ],
            [
                'label' => 'Banner',
                'field' => 'banner',
                'sql' => 'a.banner',
                'formatter' => 'getDTBanner'
            ],

        ];

        $this->tableActions = [
            'TryOutPack',
        ];

        $this->formatDTResult = true;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/tryout/package-new", name="try-out-package-new")
     */
    public function getPackage()
    {
        if ($this->req->isMethod('POST')) {
            $this->redirectAction = 'try-out-product-cat-new';
            if ($this->req->get('action') == 'add') {
                return $this->post();
            } else {
                return $this->edit();
            }
        }

        $this->data['levels'] = $this->em->getRepository(Level::class)->findAll();
        $this->data['cats'] = $this->em->getRepository(Categories::class)->findBy(["isProduct" => true, "isTryOut" => true]);
        return $this->renderTable('admin/TryOut/package.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/tryout/irt/{id}", name="try-out-irt-calculation")
     */
    public function calculateIRT($id)
    {
        $code = 200;
        try {
            $data = $this->em->createQueryBuilder()
                ->from(TryOutQuiz::class, "q")
                ->select(["q", "s", "t"])
                ->addSelect("(SELECT COUNT(q1.id) FROM " . TryOutQuiz::class . " q1 WHERE q1.tryOut = q.tryOut) as nSoal")
                ->addSelect("(SELECT SUM(a.score) FROM " . TryOutRegistrantAnswer::class . " a WHERE a.quiz = q AND a.irtChecked = false) as score")
                ->addSelect("(SELECT COUNT(a1.id) FROM " . TryOutRegistrantAnswer::class . " a1 WHERE a1.quiz = q AND a1.irtChecked = false) as nAnswer")
                ->addSelect("(SELECT SUM(r.registrantNumber) FROM " . TryOutRegistrant::class . " r WHERE r.tryOut = t AND r.irtChecked = false ) as nRegistrant")
                ->innerJoin("q.tryOut", "s")
                ->innerJoin("s.tryout", "t")
                ->innerJoin("t.registrant", "reg")
                ->where("t.id = :tid  AND t.irtChecked = false")
                ->setParameter("tid", $id)
                ->getQuery()
                ->getResult();
            if (count($data) > 0) {
                $points = [];
                foreach ($data as $key => $score) {
                    /** @var TryOutQuiz $quiz */
                    $quiz = $score[0];
                    $maxScore = $score['nRegistrant'] * 100;
                    $n = ($score['score'] / 100);
                    $b = ((1 / $n) / $score['nSoal']);
                    $sid = $quiz->getTryOut()->getId();
                    $points[$sid][] = [
                        "data" => $quiz,
                        "score" => round($maxScore * $b),
                        "actualScore" => $score['score'],
                        "maxScore" => ($score['nSoal'] * 100)
                    ];

                }

                foreach ($points as $key => $point) {
                    $rest = ($point[0]['maxScore'] - array_sum(array_column($point, "score"))) / count($point);
                    foreach ($point as $k => $p) {
                        $points[$key][$k]['irtScore'] = $p['score'] + $rest;
                    }
                }

                foreach ($points as $key => $d) {
                    dump(array_sum(array_column($d, "irtScore")));
                    foreach ($d as $k => $qz) {
                        /** @var TryOutQuiz $quiz */
                        $quiz = $qz['data'];
                        /** @var TryOutSubject $subject */
                        $subject = $quiz->getTryOut();
                        /** @var TryOut $tryOut */
                        $tryOut = $subject->getTryout();
                        $tryOut->setIrtChecked(true);
                        $quiz->setIRTScore($qz['irtScore']);
                        $answers = $quiz->getAnswers()->toArray();
                        /** @var TryOutRegistrantAnswer $answer */
                        foreach ($answers as $answer) {
                            $answer->setIrtChecked(true);
                        }
                    }

                }

                $registrants = $this->em->getRepository(TryOutRegistrant::class)->findBy(["tryOut" => $id, "irtChecked" => false]);
                /** @var TryOutRegistrant $registrant */
                foreach ($registrants as $registrant) {
                    $registrant->setIrtChecked(true);
                }

                $this->em->flush();
                $response = [
                    "code" => $code,
                    "msg" => "IRT Berhasil dihitung"];

            } else {
                $code = 404;
                $response = [
                    "code" => $code, "msg" => "Data tidak ditemukan "];
            }

        } catch (\Exception $e) {
            $code = 501;
            $response = ["code" => $code, "msg" => "Terjadi masalah " . $e->getMessage()];
        }
        return GenBasic::send($code, ['data' => $response]);

    }

    public function TryOutPackButton($data)
    {
        $attributes = " data-irt='".(int)$data['isIRT']."' data-banner='$data[banner]' data-estimated='$data[estimatedTime]' data-cred='$data[id]' data-strict='".(int)$data['isStrict']."' data-rest='$data[restTime]' data-title='$data[title]' data-level='$data[l_id]' data-cat='$data[c_id]' data-status='$data[isActive]' data-free='$data[isFree]' data-code='$data[codeAccess]' data-price='$data[price]' data-direction='$data[direction]' data-rules='$data[rules]' data-start='".($data['startDay'] ? $data['startDay']->format("Y-m-d") : "")."' data-final ='".($data['finalDay'] ? $data['finalDay']->format("Y-m-d") : "")."'";
        $url = '/admin/tryout/package/manage/' . $data['id'];


        return "<a href='$url' class='btn btn-sm btn-secondary' style='background-color:#4CAF50;'>Kelola</a>
<a href='/admin/tryout-reward/" . $data['id'] . "' class='btn btn-sm btn-secondary' style='background-color:#4CAF50;'>Rewards</a><br>
<a href='/admin/tryout-pricing/" . $data['id'] . "' class='btn btn-sm btn-secondary' style='background-color:#4CAF50;'>Pricing</a>" .
            ($data['isIRT'] ? "<a data-cred='" . $data['id'] . "' id='IRT' class='btn btn-sm " . ($data["irtChecked"] ? "disabled" : "") . " btn-outline-danger'>Hitung IRT</a>" : "")
            . "<a class='btn btn-sm btn-secondary' id='editData' $attributes  style='background-color:#FF6F00;'>Edit</a><br>
<a name='action' id='deleteData' class='btn btn-sm btn-secondary ' data-cred=' " . $data['id'] . "' style='background-color:#FF0C3E;'>Delete</a>";
    }

    public function getDTPrice($data)
    {

        $hasil_rupiah = 'Rp. ' . number_format($data['price'], 0, ',', '.');

        return $hasil_rupiah;
    }

    public function getLevelRelation($data)
    {
        $levelId = $this->req->get('level');
        return $this->em->getRepository(Level::class)->findOneBy(["id" => $levelId]);
    }

    public function getCategoryRelation($data)
    {
        $levelId = $this->req->get('category');
        return $this->em->getRepository(Categories::class)->findOneBy(["id" => $levelId]);
    }

    public function isStrict($data)
    {
        if ($data['isStrict']) {
            return '<span class="fa fa-circle text-blue"></span> Aktif';

        } else {
            return '<span class="fa fa-circle colorRed"></span> Belum Aktif';
        }
    }

    public function isIRT($data)
    {
        if ($data['isIRT']) {
            return '<span class="fa fa-circle text-blue"></span> Aktif';

        } else {
            return '<span class="fa fa-circle colorRed"></span> Belum Aktif';
        }
    }

    public function setBanner($entity)
    {
        /** @var UploadedFile $img */
        $img = $this->req->files->get('banner');

        /** @var TryOut $entity */
        if ($img) {
            if ($entity) {
                if ($entity->getBanner() && file_exists('../public/images/mapel/' . $entity->getBanner())) {
                    unlink('../public/images/mapel/' . $entity->getBanner());
                }
            }

            $targetDir = '../public/images/mapel';
            $originalFilename = pathinfo($img->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = str_replace(' ', '-', $originalFilename);
            $fileName = $safeFilename . '-' . uniqid() . '.' . $img->guessExtension();
            $upload = $img->move($targetDir, $fileName);
            return '/images/mapel/' . $fileName;
        }


        return $entity->getBanner();
    }

    public function getDTBanner($val)
    {
        if ($val['banner']) {
            return '<img src="' . $val['banner'] . '" height="50" />';
        }
        return '';
    }

    public function periodeTime($data)
    {

        return $data['startDay'] ? $data['startDay']->format("d M Y").' / '.$data['finalDay']->format("d M Y") : "-";

    }

}
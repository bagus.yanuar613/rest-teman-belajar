<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Attendance;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AttendanceController
 * @package App\Controller\Admin
 */
class AttendanceController extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->formatDTResult = true;

        $this->tableFields = [
            [
                'label'     => 'Durasi',
                'field'     => 'duration',
                'sql'       => 'a.duration',
//                'formatter' => 'getDTDuration',
            ],
            [
                'label'     => 'Hari Presensi',
                'field'     => 'hari',
                'sql'       => 'a.startAt as hari',
                'formatter' => 'getDTDay',
            ],
            [
                'label'     => 'Tanggal Presensi',
                'field'     => 'createdAt',
                'sql'       => 'a.createdAt',
                'formatter' => 'getDTDate',
            ],
            [
                'label'     => 'Status',
                'field'     => 'isPresence',
                'sql'       => 'a.isPresence',
                'formatter' => 'getDTPresence',
            ],
//            [
//                'label'     => 'Nominal Transaksi',
//                'field'     => 'amount',
//                'sql'       => 'c.amount',
//                'formatter' => 'getDTAmount',
//            ],
            [
                'label'     => 'Mulai',
                'field'     => 'startAt',
                'sql'       => 'a.startAt',
//                'formatter' => 'getDTStart',
            ],
            [
                'label'     => 'Selesai',
                'field'     => 'finishAt',
                'sql'       => 'a.finishAt',
//                'formatter' => 'getDTEnd',
            ],
            [
                'label'     => 'Selesai',
                'field'     => 'finishAt',
                'sql'       => 'a.finishAt',
//                'formatter' => 'getDTEnd',
            ],
        ];

        $this->joint = [
            [
                'tipe'  => 'join',
                'join'  => 's.tentorSubject',
                'alias' => 'ts',
            ],
//            [
//                'tipe'  => 'join',
//                'join'  => 'a.cart',
//                'alias' => 'c',
//            ],
        ];


        $this->singleNamespace    = 'Admin';
        $this->controllerName     = 'AttendanceController';
        $this->req                = Request::createFromGlobals();
        $this->class              = Attendance::class;
        $this->data['class']      = $this->class;
        $this->requiredWhereParam = ":attendance = ':id'";
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTAmount($data)
    {
        $hasil_rupiah = 'Rp. '.number_format($data['amount'], 0, ',', '.');

        return $hasil_rupiah;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTDay($data)
    {
        Carbon::setLocale('id');
        $date = Carbon::parse($data['hari'], 'Asia/Jakarta')->isoFormat('dddd');

        return $date;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTDate($data)
    {
        Carbon::setLocale('id');
        $date = Carbon::parse($data['createdAt'], 'Asia/Jakarta')->isoFormat('LL, HH:mm');

        return $date;
    }

    /**
     * @param $data
     */
    public function getDTPresence($data)
    {
        if ($data['isPresence'] == 0) {
            $aktiv = '<span class="fa fa-circle colorOrange"></span> Sedang Berlangsung';
        } else {
            $aktiv = '<span class="fa fa-circle text-blue"></span> Selesai';
        }

        return $aktiv;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/admin/transaksi/get-total-presence", name="get-presence")
     */
    public function getTotalPresence(){
        $date1 = date_create($this->req->get('start'));
        $date2 = date_create($this->req->get('end'));
        $range = date_diff($date1,$date2 );
        $selisih = $range->format("%a")*2;


        $old = "Select count(id) as jum from attendance where created_at BETWEEN DATE_ADD('".$this->req->get('start')."', INTERVAL -".$selisih." DAY) AND DATE_ADD('".$this->req->get('start')."', INTERVAL -1 DAY) ";
        $new = "Select count(id) as jum from attendance where created_at BETWEEN '".$this->req->get('start')."' AND '".$this->req->get('end')."' ";


        return $this->getCountPerncen($old, $new);
    }

}
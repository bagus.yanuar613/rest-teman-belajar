<?php

namespace App\Controller\Admin\Pengaturan;

use App\Controller\BaseController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;

class PotonganPendapatanController extends BaseController
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);


    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/pengaturan/potongan-pendapatan", name="potongan-pendapatan")
     */
    public function index(){
        return $this->renderTable('admin/pengaturan/potonganPendapatan.html.twig');
    }

}
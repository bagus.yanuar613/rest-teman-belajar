<?php

namespace App\Controller\Admin\Pengaturan;

use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Bank;
use App\Entity\BankAccount;
use App\Entity\CategoriesParent;
use App\Repository\BankRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BankController
 * @package App\Controller\Admin\Pengaturan
 */
class BankController extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->fields = [
            'bank:relation:'.Bank::class,
            'holderName',
            'number',
        ];

        $this->tableFields = [
            [
                'label' => 'Bank',
                'field' => 'name',
                'sql'   => 'b.name',
            ],
            [
                'label' => 'Atas Nama Rekening',
                'field' => 'holderName',
                'sql'   => 'a.holderName',
            ],
            [
                'label' => 'Nomor Rekening',
                'field' => 'number',
                'sql'   => 'a.number',
            ],
        ];

        $this->addSelect = "b.id as bankid";

        $this->tableActions = [
            'edit',
            'deleteAccount',
        ];

        $this->data['order'] = ['column' => 1, 'type' => 'asc'];
        $this->data['orderMaster'] = ['column' => 2, 'type' => 'asc'];
//        $this->data['orderMaster']['type'] = 'asc';

        $aksi               = [
            'editBank',
            'delete',
        ];
        $this->data['aksi'] = implode(',', $aksi);

        $this->singleNamespace = 'Admin\\\Pengaturan';
        $this->controllerName  = 'BankController';
        $this->req             = Request::createFromGlobals();
        $this->class           = BankAccount::class;
        $this->data['class']   = $this->class;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/pengaturan/bank", name="bank")
     */
    public function index()
    {
        if ($this->req->isMethod('POST')) {
            try {
                $this->redirectAction = 'bank';
                if ($this->req->get('action') == 'add') {
                     $this->post();
                } else {
                     $this->edit();
                }
                $code     = 200;
                $response = [
                    'msg' => 'Success',
                ];
            } catch (\Exception $err) {
                $code     = 500;
                $response = [
                    'msg' => 'Terjadi kesalahan dalam pengambilan data',
                ];
            }

            return GenBasic::send($code, $response);
        }

        $this->data['classBank'] = 'BankMasterController';

        $this->data['tableFieldsBank'] = [
            [
                'label' => 'Icon',
                'field' => 'icon',
            ],
            [
                'label' => 'Nama Bank',
                'field' => 'name',
            ],
            [
                'label' => 'Kode Bank',
                'field' => 'code',
            ],
        ];

        return $this->renderTable('admin/pengaturan/bank.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/admin/get-bank", name="get-bank")
     */
    public function getBank()
    {
        try {
            /** @var BankRepository $bank */
            $bank = $this->em->getRepository(Bank::class);
            $repo = $bank->createQueryBuilder('a')
                         ->select(['a.id', 'a.name', 'a.code'])
                         ->orderBy('a.name', 'asc')
                         ->getQuery()
                         ->getScalarResult();
            $code = 200;

            $response = GenBasic::serializeToJson($repo);
        } catch (\Exception $err) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data',
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/admin/get-bank-account", name="get-bank-account")
     */
    public function getBankAccount()
    {
        try {
            /** @var BankRepository $bank */
            $bank = $this->em->getRepository(BankAccount::class);
            $repo = $bank->createQueryBuilder('a')
                         ->select(['a.id', 'b.name', 'a.holderName', 'b.code'])
                         ->join('a.bank', 'b')
                         ->getQuery()
                         ->getScalarResult();
            $code = 200;

            $response = GenBasic::serializeToJson($repo);
        } catch (\Exception $err) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data',
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @return JsonResponse
     * @Route("/admin/pengaturan/bank/detele", name="delete-bank")
     */
    public function deleteAcount(){
        try {
            $bank = $this->em->getRepository(BankAccount::class)->findOneBy(['id' => $this->req->get('id')]);
            $this->em->remove($bank);
            $this->em->flush();
            $response = [
                'code' => 200,
                'status' => 'Berhasil dihapus',
                'label' => 'success',
            ];
        } catch (\Exception $e) {
            $response = [
                'code' => 403,
                'status' => 'Terjadi kesalahan saat menghapus data ' . $e->getMessage(),
                'label' => 'danger',
            ];
        }

        return new JsonResponse(
            $response,
            200
        );
    }

}
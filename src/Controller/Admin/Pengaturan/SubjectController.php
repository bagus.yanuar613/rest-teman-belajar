<?php

namespace App\Controller\Admin\Pengaturan;

use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Categories;
use App\Entity\CategoriesParent;
use App\Entity\Subject;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SubjectController extends BaseController
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->fields = [
            'name',
            'slug:method:setSlug:',
            'icon:method:setIcon:entity',
            'isReference',
            'isDefault',
            'isPopular'
        ];

        $this->formatDTResult = true;

        $this->tableFields = [
            [
                'label' => 'Icon',
                'field' => 'icon',
                'sql' => 'a.icon',
                'formatter' => 'getDTIcon'
            ],
            [
                'label' => 'Nama',
                'field' => 'name',
                'sql' => 'a.name',
            ],
            [
                'label' => 'Referensi',
                'field' => 'isReference',
                'sql'   => 'a.isReference',
                'formatter' => 'getDTReferensi'
            ],
            [
                'label' => 'Default',
                'field' => 'isDefault',
                'sql'   => 'a.isDefault',
                'formatter'   => 'getDTDefault',
            ],
            [
                'label' => 'Populer',
                'field' => 'isPopular',
                'sql'   => 'a.isPopular',
                'formatter'   => 'getDTPopular',
            ],
        ];

        $this->tableActions = [
            'edit',
        ];

        $this->singleNamespace = 'Admin\\\Pengaturan';
        $this->controllerName = 'SubjectController';

        $this->req = Request::createFromGlobals();
        $this->class = Categories::class;
        $this->data['class'] = $this->class;
        $this->requiredWhere = "a.isTryOut = false AND a.isProduct = false";

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/pengaturan/mata-pelajaran", name="mapel")
     */
    public function index()
    {
//        $this->data['where']               = json_encode($this->data['where']);

        if ($this->req->isMethod('POST')) {
            $this->redirectAction = 'mapel';
            if ($this->req->get('action') == 'add') {
                return $this->post();
            } else {
                return $this->edit();
            }
        }
        return $this->renderTable('admin/pengaturan/subject.html.twig');
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTReferensi($data){
        $result = '<i class="fa fa-times-circle colorRed"></i>';
        if($data['isReference'] == 1){
            $result = '<i class="fa fa-check-circle colorGreen"></i>';
        }
        return $result;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTPopular($data){
        $result = '<i class="fa fa-times-circle colorRed"></i>';
        if($data['isPopular'] == 1){
            $result = '<i class="fa fa-check-circle colorGreen"></i>';
        }
        return $result;
    }


    /**
     * @param $data
     *
     * @return string
     */
    public function getDTDefault($data){
        $result = '<i class="fa fa-times-circle colorRed"></i>';
        if($data['isDefault'] == 1){
            $result = '<i class="fa fa-check-circle colorGreen"></i>';
        }
        return $result;
    }


    /**
     * @param $v
     *
     * @return string|string[]
     */
    public function setSlug($v)
    {
        return str_replace(' ', '-', strtolower($this->req->get('name')));
    }

    /**
     * @param $entity
     *
     * @return string
     */
    public function setIcon($entity)
    {
        /** @var UploadedFile $img */
        $img = $this->req->files->get('icon');

        /** @var Categories $entity */
        if ($img) {
            if ($entity) {
                if ($entity->getIcon() && file_exists('../public' . $entity->getIcon())) {
                    unlink('../public' . $entity->getIcon());
                }
            }

            $targetDir = '../public/images/mapel';
            $originalFilename = pathinfo($img->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = str_replace(' ', '-', $originalFilename);
            $fileName = $safeFilename . '-' . uniqid() . '.' . $img->guessExtension();
            $upload = $img->move($targetDir, $fileName);
            return '/images/mapel/' . $fileName;
        }


        return $entity->getIcon();
    }

    /**
     * @return JsonResponse
     * @Route("/admin/pengaturan/mata-pelataran/add", name="add-mapel")
     */
    public function addData()
    {
        try {
            $name = $this->req->get('name');
            $parent = new Categories();
            $parent->setName($name)
                ->setSlug(str_replace(' ', '-', strtolower($name)))
                ->setIsDefault($this->req->get('isDefault'))
                ->setIsTryOut(false)
                ->setIsProduct(false)
                ->setIsPopular($this->req->get('isPopular'))
                ->setIsReference($this->req->get('isReference'));
            $this->em->persist($parent);
            $this->em->flush();
            $code = 200;
            $data = [
                'data' => $parent,
            ];
            $response = $data;
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data : ' . $e->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    public function getDTIcon($val)
    {
        if ($val['icon']) {
            return '<img src="' . $val['icon'] . '" height="50" />';
        }
        return '';
    }
}


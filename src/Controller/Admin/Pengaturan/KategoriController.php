<?php
declare(strict_types=1);

namespace App\Controller\Admin\Pengaturan;

use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Categories;
use App\Entity\CategoriesParent;
use App\Entity\CategoryToParentToLevel;
use App\Entity\CategoryToParentToMethod;
use App\Entity\City;
use App\Entity\Grade;
use App\Entity\Level;
use App\Entity\Method;
use App\Entity\Pricing;
use App\Entity\Subject;
use App\Repository\CategoriesParentRepository;
use App\Repository\CategoriesRepository;
use App\Repository\SubjectRepository;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use function GuzzleHttp\Psr7\str;

/**
 * Class KategoriController
 * @package App\Controller\Admin
 */
class KategoriController extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->formatDTResult = true;

        $this->tableFields = [
            [
                'label' => 'Subjek',
                'field' => 'category',
                'sql'   => 'c.name',
            ],
            [
                'label'     => 'Jenjang',
                'field'     => 'level',
                'sql'       => 'c.name',
                'formatter' => 'getDTLevel',
            ],
//            [
//                'label' => 'Grade',
//                'field' => 'grade',
//                'sql'   => 'g.name as grade',
//            ],
            [
                'label'     => 'Method',
                'field'     => 'method',
                'sql'       => 'c.name',
                'formatter' => 'getDTMethod',
            ],
        ];

        $this->tableActions = [
            'edit',
            'deleteSubject',
        ];

        $this->joint           = [
            [
                'tipe'  => 'join',
                'join'  => 'a.category',
                'alias' => 'c',
            ],
//            [
//                'tipe'  => 'leftjoin',
//                'join'  => 'a.CategoryTolevelToParent',
//                'alias' => 'l',
//            ],
//            [
//                'tipe'  => 'leftjoin',
//                'join'  => 'a.CategoryToMethodToParent',
//                'alias' => 'm',
//            ],
//            [
//                'tipe'  => 'leftjoin',
//                'join'  => 'm.method',
//                'alias' => 'mt',
//            ],
//
        ];
        $this->addSelect       = 'c.name as category, a.name as parentName,c.id as categoryId';
        $this->singleNamespace = 'Admin\\\Pengaturan';
        $this->controllerName  = 'KategoriController';
        $this->req             = Request::createFromGlobals();
        $this->class           = CategoriesParent::class;
        $this->data['class']   = $this->class;

        $this->requiredWhereParam = 'a.id = :id';

    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTMethod($data)
    {
        /** @var SubjectRepository $method */
        $method = $this->em->getRepository(CategoryToParentToMethod::class);
        $qb     = $method->createQueryBuilder('a')
                         ->select(['m.name'])
                         ->join('a.method', 'm')
                         ->where('a.category = '.$data['categoryId'])
                         ->andWhere('a.parentCategory = '.$data['id'])
                         ->getQuery()
                         ->getArrayResult();

        $dataMethod = '';
        /** @var Method $dat */
        if (count($qb) > 0) {
            foreach ($qb as $dat) {
                $dataMethod .= ', '.$dat['name'];
            }
        }
        $str = substr($dataMethod, 0, 1);
        if ($str == ',') {
            $dataMethod = substr($dataMethod, 2);
        }

        return $dataMethod;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTLevel($data)
    {
        /** @var SubjectRepository $method */
        $method = $this->em->getRepository(CategoryToParentToLevel::class);
        $qb     = $method->createQueryBuilder('a')
                         ->select(['l.name'])
                         ->join('a.level', 'l')
                         ->where('a.category = '.$data['categoryId'])
                         ->andWhere('a.parentCategory = '.$data['id'])
                         ->getQuery()
                         ->getArrayResult();
//dump($qb);die();
        $dataMethod = '';
        /** @var Method $dat */
        if (count($qb) > 0) {
            foreach ($qb as $dat) {
                $dataMethod .= ', '.$dat['name'];
            }
        }
        $str = substr($dataMethod, 0, 1);
        if ($str == ',') {
            $dataMethod = substr($dataMethod, 2);
        }

        return $dataMethod;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTPrice($data)
    {
        return number_format($data['price'], 0, ',', '.');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/pengaturan/subject", name="subject")
     */
    public function index()
    {
        if ($this->req->isMethod('POST')) {
            $city = $this->em->getRepository(City::class)->findOneBy(['id' => $this->req->get('city')]);
            /** @var Categories $category */
            $category = $this->em->getRepository(Categories::class)->findOneBy(['id' => $this->req->get('category')]);
            /** @var Level $level */
            /** @var CategoriesParent $parentCate */
            $parentCate = $this->em->getRepository(CategoriesParent::class)->findOneBy(['id' => $this->req->get('idParent')]);

            $idjenjangAr  = $this->req->get('idjenjang');
            $idmethodAr   = $this->req->get('idmethod');
            $jenjangArray = $this->req->get('jenjang');
            $methodArray  = $this->req->get('method');
            try {
                if ($this->req->get('action') == 'add') {

                    $category->addCategoryParent($parentCate);
                    foreach ($jenjangArray as $key => $v) {
                        $level               = $this->em->getRepository(Level::class)->findOneBy(['id' => $v]);
                        $categoryParentLevel = new CategoryToParentToLevel;
                        $levelId             = $level ?? null;
                        $categoryParentLevel->setLevel($levelId)
                                            ->setParentCategory($parentCate)
                                            ->setCategory($category);
                        $this->em->persist($categoryParentLevel);
                    }

                    foreach ($methodArray as $key => $m) {
                        $method = $this->em->getRepository(Method::class)->findOneBy(['id' => $m]);

                        $categoryParentMethod = new CategoryToParentToMethod();
                        $methodId             = $method ?? null;
                        $categoryParentMethod->setMethod($methodId)
                                             ->setParentCategory($parentCate)
                                             ->setCategory($category);
                        $this->em->persist($categoryParentMethod);

                    }
                } else {
                    foreach ($idjenjangAr as $key => $v) {
                        $level               = $this->em->getRepository(Level::class)->findOneBy(['id' => $jenjangArray[$key]]);
                        $categoryParentLevel = $this->em->getRepository(CategoryToParentToLevel::class)->findOneBy(['id' => $v]);

                        if ($categoryParentLevel == null) {
                            $categoryParentLevel = new CategoryToParentToLevel;
                            $categoryParentLevel->setLevel($level)
                                                ->setParentCategory($parentCate)
                                                ->setCategory($category);
                            $this->em->persist($categoryParentLevel);
                        } else {
                            $categoryParentLevel->setLevel($level);

                        }

                    }
                    foreach ($idmethodAr as $key => $m) {
                        $method               = $this->em->getRepository(Method::class)->findOneBy(['id' => $methodArray[$key]]);
                        $categoryParentMethod = $this->em->getRepository(CategoryToParentToMethod::class)->findOneBy(['id' => $m]);

                        /** @var CategoryToParentToMethod $categoryParentMethod */
                        if ($categoryParentMethod == null) {
                            $categoryParentMethod = new CategoryToParentToMethod();
                            $categoryParentMethod->setMethod($method)
                                                 ->setParentCategory($parentCate)
                                                 ->setCategory($category);
                            $this->em->persist($categoryParentMethod);
                        } else {
                            $categoryParentMethod->setMethod($method);

                        }
                    }

                }

                $this->em->flush();
                $code     = 200;
                $response = [
                    'tbId' => str_replace(' ', '', $parentCate->getName()).''.$parentCate->getId(),
                ];
            } catch (\Exception $e) {
                $code     = 500;
                $response = [
                    'msg' => 'Terjadi kesalahan dalam pengambilan data : '.$e->getMessage(),
                ];
            }

            return GenBasic::send($code, $response);

        }
        $this->data['parrent'] = [];
        /** @var CategoriesParentRepository $parent */
        $parent = $this->em->getRepository(CategoriesParent::class);
        $repo   = $parent->createQueryBuilder('a')
                         ->select(['a'])
                         ->where('a.isTryOut = false')
                         ->getQuery()
                         ->getResult();
        /** @var CategoriesParent $r */
        foreach ($repo as $key => $r) {
            $this->data['parrent'][$key]['name']   = $r->getName();
            $this->data['parrent'][$key]['id']     = $r->getId();
            $this->data['parrent'][$key]['idtb']   = $r->getName().' '.$r->getId();
            $this->data['where'.$r->getId()]['id'] = $r->getId();
            $this->data['where'.$r->getId()]       = json_encode($this->data['where'.$r->getId()]);
            $this->data['parrent'][$key]['where']  = $this->data['where'.$r->getId()];

        }

        return $this->renderTable('admin/pengaturan/kategori.html.twig');
    }

    /**
     * @return JsonResponse
     * @Route("/admin/pengaturan/subject/delete-subject", name="delete-subject")
     */
    public function deletePricing()
    {
        try {
            /** @var CategoriesParent $parent */
            $parent = $this->em->getRepository(CategoriesParent::class)->findOneBy(['id' => $this->req->get('parent')]);
            /** @var Categories $category */
            $category = $this->em->getRepository(Categories::class)->findOneBy(['id' => $this->req->get('category')]);

            $cateLevel  = $this->em->getRepository(CategoryToParentToLevel::class)->findBy(['category' => $category, 'parentCategory' => $parent]);
            $cateMethod = $this->em->getRepository(CategoryToParentToMethod::class)->findBy(['category' => $category, 'parentCategory' => $parent]);

            $parent->removeCategory($category);
            foreach ($cateLevel as $l) {
                $this->em->remove($l);
            }

            foreach ($cateMethod as $m) {
                $this->em->remove($m);
            }

            $this->em->flush();
            $code     = 200;
            $data     = [
                'status' => "Data berhasil dihapus",
                'label'  => "success",
            ];
            $response = $data;
        } catch (\Exception $e) {
            $code     = 500;
            $response = [
                'status' => 'Terjadi kesalahan saat menghapus data '.$e->getMessage(),
                'label'  => 'danger',
            ];
        }

        return new JsonResponse(
            $response,
            $code
        );

    }

    /**
     * @return JsonResponse
     * @Route("/admin/pengaturan/add-parent-category", name="add-parent-category")
     */
    public function addParentCategory()
    {

        try {
            $name   = $this->req->get('name');
            $parent = new CategoriesParent();
            $parent->setName($name)
                   ->setSlug(str_replace(' ', '-', strtolower($name)));
            $this->em->persist($parent);
            $this->em->flush();
            $code     = 200;
            $data     = [
                'data' => $parent,
            ];
            $response = $data;
        } catch (\Exception $e) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data',
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @return JsonResponse
     * @Route("/admin/pengaturan/edit-parent-category", name="edit-parent-category")
     */
    public function editParentCategory()
    {
        try {
            $name = $this->req->get('name');
            /** @var CategoriesParent $parent */
            $parent = $this->em->getRepository(CategoriesParent::class)->findOneBy(['id' => $this->req->get('id')]);
            $parent->setName($name)
                   ->setSlug(str_replace(' ', '-', strtolower($name)))
                   ->setIsTryOut(false);
            $this->em->flush();
            $code     = 200;
            $data     = [
                'data' => $parent,
            ];
            $response = $data;
        } catch (\Exception $e) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data',
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @return JsonResponse
     * @Route("/admin/pengaturan/delete-parent-category", name="delete-parent-category")
     */
    public function deleteParentCategory()
    {
        try {
            $parent  = $this->em->getRepository(CategoriesParent::class)->findOneBy(['id' => $this->req->get('id')]);
            $subject = $this->em->getRepository(Subject::class)->findBy(['parentCategory' => $parent]);
//            dump($this->req);
//            dump($subject);
//            dump($parent);
//            die();
            if (isset($subject)) {
                foreach ($subject as $sub) {
                    $this->em->remove($sub);
                }
            }
            $this->em->remove($parent);

            $this->em->flush();
            $code     = 200;
            $data     = [
                'data' => 'Success',
            ];
            $response = $data;
        } catch (\Exception $e) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data '.$e->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @return JsonResponse
     * @Route("/admin/pengaturan/get-category", name="getCategory")
     */
    public function getCategory()
    {

        try {
            /** @var CategoriesRepository $dat */
            $dat      = $this->em->getRepository(Categories::class);
            $repo     = $dat->createQueryBuilder('a')
                            ->select(['a.name', 'a.id'])
                            ->getQuery()
                            ->getScalarResult();
            $code     = 200;
            $response = GenBasic::serializeToJson($repo);
        } catch (\Exception $e) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data ',
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @return JsonResponse
     * @Route("/admin/pengaturan/get-level", name="getLevel")
     */
    public function getLevel()
    {
        try {
            /** @var CategoriesRepository $dat */
            $dat      = $this->em->getRepository(Level::class);
            $repo     = $dat->createQueryBuilder('a')
                            ->select(['a.name', 'a.id'])
                            ->getQuery()
                            ->getScalarResult();
            $code     = 200;
            $response = GenBasic::serializeToJson($repo);
        } catch (\Exception $e) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data ',
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @return JsonResponse
     * @Route("/admin/pengaturan/get-grade", name="getGrade")
     */
    public function getGrade()
    {
        try {
            /** @var CategoriesRepository $dat */
            $dat      = $this->em->getRepository(Grade::class);
            $repo     = $dat->createQueryBuilder('a')
                            ->select(['a.name', 'a.id'])
                            ->getQuery()
                            ->getScalarResult();
            $code     = 200;
            $response = GenBasic::serializeToJson($repo);
        } catch (\Exception $e) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data ',
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @return JsonResponse
     * @Route("/admin/pengaturan/get-city", name="getCity")
     */
    public function getCity()
    {
        try {
            /** @var City $d */
            $dat  = $this->em->getRepository(City::class)->findAll();
            $repo = [];
            foreach ($dat as $key => $d) {
                $repo[$key]['name'] = $d->getCityName();
                $repo[$key]['id']   = $d->getId();
            }
            $code     = 200;
            $response = GenBasic::serializeToJson($repo);
        } catch (\Exception $e) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data ',
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @return JsonResponse
     * @Route("/admin/pengaturan/get-method", name="get-method")
     */
    public function getMethod()
    {
        try {
            /** @var CategoriesRepository $dat */
            $dat      = $this->em->getRepository(Method::class);
            $repo     = $dat->createQueryBuilder('a')
                            ->select(['a.name', 'a.id'])
                            ->getQuery()
                            ->getScalarResult();
            $code     = 200;
            $response = GenBasic::serializeToJson($repo);
        } catch (\Exception $e) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data ',
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @param $id
     *
     * @return JsonResponse
     * @Route("/admin/pengaturan/get-data-subject", name="get-data-subject")
     */
    public function getDataSubject()
    {

//        dump($data);
//        die();
        try {
            /** @var Categories $cate */
//            $cate     = $this->em->getRepository(Categories::class)->findOneBy(['id' => $this->req->get('categoryId')]);
            $levelCate  = $this->em->getRepository(CategoryToParentToLevel::class)->findBy(['parentCategory' => $this->req->get('parentId'), 'category' => $this->req->get('categoryId')]);
            $methodCate = $this->em->getRepository(CategoryToParentToMethod::class)->findBy(['parentCategory' => $this->req->get('parentId'), 'category' => $this->req->get('categoryId')]);
            /** @var CategoryToParentToLevel $l */
            $level  = [];
            $method = [];
            foreach ($levelCate as $key => $l) {
                /** @var Level $level */
                $levelD                = $this->em->getRepository(Level::class)->findOneBy(['id' => $l->getLevel()]);
                $level[$key]['idFild'] = $l->getId();
                if ($levelD) {
                    $level[$key]['id']   = $levelD->getId();
                    $level[$key]['name'] = $levelD->getName();
                }
            }
            /**
             * @var  $key
             * @var CategoryToParentToMethod $m
             */
            foreach ($methodCate as $key => $m) {
                /** @var Method $method */
                $methodD                = $this->em->getRepository(Method::class)->findOneBy(['id' => $m->getMethod()]);
                $method[$key]['idFild'] = $m->getId();
                if ($methodD) {
                    $method[$key]['id']   = $methodD->getId();
                    $method[$key]['name'] = $methodD->getName();
                }
            }
            $code = 200;
//            $level    = GenBasic::CustomNormalize($cate->getLevel(), ['id', 'name']);
//            $method   = GenBasic::CustomNormalize($cate->getMethod(), ['id', 'name']);
            $response = [
                'level'  => $level,
                'method' => $method,
            ];
        } catch (\Exception $e) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data '.$e->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @return JsonResponse
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     * @Route("/admin/getdatatable/{id}", name="getdatatable")
     */
    public function getDatatable($id, Request $request)
    {

        try {
            /** @var CategoriesParent $cate */
            $cate          = $this->em->getRepository(CategoriesParent::class)->findOneBy(['id' => $id]);
            $data['count'] = count($cate->getCategory());
            /**
             * @var  $ketP
             * @var Categories $cat
             */
            foreach ($cate->getCategory() as $ketP => $cat) {
                $data['data'][$ketP]['idParent']   = $cate->getId();
                $data['data'][$ketP]['idCategory'] = $cat->getId();
                $data['data'][$ketP]['name']       = $cat->getName();
                $data['data'][$ketP]['level']      = [];
                $data['data'][$ketP]['method']     = [];
                $levelCate                         = $this->em->getRepository(CategoryToParentToLevel::class)->findBy(['parentCategory' => $cate, 'category' => $cat]);
                $methodCate                        = $this->em->getRepository(CategoryToParentToMethod::class)->findBy(['parentCategory' => $cate, 'category' => $cat]);
                /** @var CategoryToParentToLevel $l */
                foreach ($levelCate as $key => $l) {
                    /** @var Level $level */
                    $level                                           = $this->em->getRepository(Level::class)->findOneBy(['id' => $l->getLevel()]);
                    $data['data'][$ketP]['level'][$key]['levelName'] = $level->getName();
                }
                /**
                 * @var  $key
                 * @var CategoryToParentToMethod $m
                 */
                foreach ($methodCate as $key => $m) {
                    /** @var Method $method */
                    $method                                            = $this->em->getRepository(Method::class)->findOneBy(['id' => $m->getMethod()]);
                    $data['data'][$ketP]['method'][$key]['methodName'] = $method->getName();
                }
            }

            return new JsonResponse(
                [
                    'data'            => $data['data'],
                    'draw'            => $request->get('draw'),
                    'recordsTotal'    => $data['count'],
                    'recordsFiltered' => $data['count'],
                    'all'             => $data,
                ], 200
            );
        } catch (\Exception $e) {
            dump($e);
            die();
        }
    }

}
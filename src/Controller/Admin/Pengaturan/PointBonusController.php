<?php

namespace App\Controller\Admin\Pengaturan;

use App\Controller\BaseController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PointBonusController
 * @package App\Controller\Admin\Pengaturan
 */
class PointBonusController extends BaseController
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/pengaturan/point-bonus", name="point-bonus")
     */
    public function index(){
        return $this->renderTable('admin/pengaturan/poinBonus.html.twig');
    }

}
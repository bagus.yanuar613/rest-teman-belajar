<?php

namespace App\Controller\Admin\Pengaturan;

use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Bank;
use App\Entity\Categories;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BankMasterController
 * @package App\Controller\Admin\Pengaturan
 */
class BankMasterController extends BaseController
{

    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->formatDTResult = true;

        $this->fields = [
            'name',
            'code',
            'icon:method:setIcon:entity',
        ];

        $this->tableFields = [
            [
                'label' => 'Icon',
                'field' => 'icon',
                'sql'   => 'a.icon',
                'formatter' => 'getDTicon'
            ],
            [
                'label' => 'Nama Bank',
                'field' => 'name',
                'sql'   => 'a.name',
            ],
            [
                'label' => 'Kode Bank',
                'field' => 'code',
                'sql'   => 'a.code',
            ],
        ];


        $this->singleNamespace = 'Admin\\\Pengaturan';
        $this->controllerName  = 'BankMasterController';
        $this->req             = Request::createFromGlobals();
        $this->class           = Bank::class;
        $this->data['class']   = $this->class;

    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTicon($data){
        return '<img src="'.$data['icon'].'" class="" height="30">';

    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/admin/pengaturan/bank/master/", name="master-bank")
     */
    public function addBank(){

        try {
            $this->redirectAction = 'bank';
            if ($this->req->get('action') == 'add') {
                $this->post();
            }else{
                $this->edit();
            }
            $code     = 200;
            $response = [
                'msg' => 'Success',
            ];
        } catch (\Exception $err) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data',
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @param $entity
     *
     * @return mixed|string
     */
    public function setIcon($entity)
    {
        /** @var UploadedFile $img */
        $img = $this->req->files->get('icon');

        /** @var Categories $entity */
        if ($img) {
            if ($entity) {
                if ($entity->getIcon() && file_exists('../public' . $entity->getIcon())) {
                    unlink('../public' . $entity->getIcon());
                }
            }

            $targetDir = '../public/images/common/bank';
            $originalFilename = pathinfo($img->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = str_replace(' ', '-', $originalFilename);
            $fileName = $safeFilename . '-' . uniqid() . '.' . $img->guessExtension();
            $upload = $img->move($targetDir, $fileName);
            return '/images/common/bank/' . $fileName;
        }


        return $entity->getIcon();
    }
}
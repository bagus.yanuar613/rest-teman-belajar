<?php

namespace App\Controller\Admin\Pengaturan;

use App\Controller\BaseController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class KuponController
 * @package App\Controller\Admin\Pengaturan
 */
class KuponController extends BaseController
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/pengaturan/kupon", name="kupon")
     */
    public function index(){
        return $this->renderTable('admin/pengaturan/kupon.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/pengaturan/kupon/add", name="kupon-add")
     */
    public function pageAdd(){
        return $this->renderTable('admin/pengaturan/kuponadd.html.twig');
    }

}
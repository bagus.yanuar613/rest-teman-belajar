<?php

namespace App\Controller\Admin\Pengaturan;

use App\Controller\BaseController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DiamondController
 * @package App\Controller\Admin\Pengaturan
 */
class DiamondController extends BaseController
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/pengaturan/kuota-pertanyaan", name="diamond")
     */
    public function index(){
        return $this->renderTable('admin/pengaturan/diamond.html.twig');
    }

}
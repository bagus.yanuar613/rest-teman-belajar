<?php

namespace App\Controller\Admin\Pengaturan;

use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\City;
use App\Entity\District;
use App\Entity\Grade;
use App\Entity\Level;
use App\Entity\Method;
use App\Entity\Pricing;
use App\Entity\Province;
use App\Entity\Subject;
use App\Repository\PricingRepository;
use App\Repository\ProvinceRepository;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use http\Message;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class WilayahLayananController extends BaseController
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->tableFields = [
            [
                'label' => 'Kota / Kabupaten',
                'field' => 'cityName',
                'sql'   => 'c.cityName',
            ],
            [
                'label' => 'Provinsi',
                'field' => 'provinceName',
                'sql'   => 'p.provinceName',
            ],

        ];

        $this->joint = [
            [
                'tipe'  => 'leftjoin',
                'join'  => 'a.city',
                'alias' => 'c',
            ],
            [
                'tipe'  => 'leftjoin',
                'join'  => 'c.province',
                'alias' => 'p',
            ],
//            [
//                'tipe'  => 'join',
//                'join'  => 'a.level',
//                'alias' => 'l',
//            ],
//            [
//                'tipe'  => 'leftjoin',
//                'join'  => 'a.grade',
//                'alias' => 'g',
//            ],
        ];

        $this->data['groupByKota'] = 'c.id';
        $this->requiredWhere       = 'a.city is not null';

        $this->tableActions = [
            'edit',
            'deletePricingWilayah',
        ];

        $this->addSelect = ' p.id as provinceid, c.id as cityid';

        $this->singleNamespace = 'Admin\\\Pengaturan';
        $this->controllerName  = 'WilayahLayananController';
        $this->req             = Request::createFromGlobals();
        $this->class           = Pricing::class;
        $this->data['class']   = $this->class;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/pricing/tatap-muka", name="pricing-tatap-muka")
     */
    public function index()
    {
        if ($this->req->isMethod('POST')) {
            $city         = $this->em->getRepository(City::class)->findOneBy(['id' => $this->req->get('city')]);
            $offline      = $this->em->getRepository(Method::class)->findOneBy(['name' => 'Tatap Muka']);
            $basic        = $this->em->getRepository(Grade::class)->findOneBy(['name' => 'BASIC']);
            $pro          = $this->em->getRepository(Grade::class)->findOneBy(['name' => 'PRO']);
            $pricingID    = $this->req->get('idPricing');
            $basicLevel[] = $this->req->get('basiclevel');
            $basicPrice[] = $this->req->get('basicprice');
            $proLevel[]   = $this->req->get('profesionallevel');
            $proprice[]   = $this->req->get('profesionalprice');
            $date         = new DateTime('', new DateTimeZone('Asia/Jakarta'));

            if ($this->req->get('action') == 'add') {
                if (isset($basicLevel[0])) {
                    for ($i = 0; $i < count($basicLevel[0]); ++$i) {
                        $pricingB = new Pricing;
                        $level    = $this->em->getRepository(Level::class)->findOneBy(['id' => $basicLevel[0][$i]]);
                        $pricingB->setCity($city)
                                 ->setLevel($level)
                                 ->setPrice(str_replace(',', '', $basicPrice[0][$i]) ?? 0)
                                 ->setMethod($offline)
                                 ->setGrade($basic)
                                 ->setCreatedAt($date)
                                 ->setUpdatedAt($date);
                        $this->em->persist($pricingB);
                    }
                }

                if (isset($proLevel[0])) {
                    for ($i = 0; $i < count($proLevel[0]); ++$i) {
                        $pricingP = new Pricing;
                        $pricingP->setCity($city)
                                 ->setLevel($this->em->getRepository(Level::class)->findOneBy(['id' => $proLevel[0][$i]]))
                                 ->setPrice(str_replace(',', '', $proprice[0][$i]) ?? 0)
                                 ->setMethod($offline)
                                 ->setGrade($pro)
                                 ->setCreatedAt($date)
                                 ->setUpdatedAt($date);
                        $this->em->persist($pricingP);

                    }
                }
                $this->em->flush();
            } else {
                if (isset($basicLevel[0])) {
                    for ($i = 0; $i < count($basicLevel[0]); ++$i) {
                        $pricingB = $this->em->getRepository(Pricing::class)->findOneBy(['id' => $pricingID[$i]]);
                        if ($pricingB == null) {
                            $pricingB = new Pricing;
                        }
//                        dump($pricingB);
                        $level = $this->em->getRepository(Level::class)->findOneBy(['id' => $basicLevel[0][$i]]);
                        $pricingB->setCity($city)
                                 ->setLevel($level)
                                 ->setPrice(str_replace(',', '', $basicPrice[0][$i]) ?? 0)
                                 ->setMethod($offline)
                                 ->setGrade($basic)
                                 ->setCreatedAt($date)
                                 ->setUpdatedAt($date);
                        if ($pricingB == null) {
                            $this->em->persist($pricingB);
                        }

                    }
                }

                if (isset($proLevel[0])) {
                    for ($i = 0; $i < count($proLevel[0]); ++$i) {
                        $pricingP = $this->em->getRepository(Pricing::class)->findOneBy(['id' => $pricingID[$i + 5]]);
                        if ($pricingP == null) {
                            $pricingP = new Pricing;
                        }
//                        dump($pricingP);

                        $pricingP->setCity($city)
                                 ->setLevel($this->em->getRepository(Level::class)->findOneBy(['id' => $proLevel[0][$i]]))
                                 ->setPrice(str_replace(',', '', $proprice[0][$i]) ?? 0)
                                 ->setMethod($offline)
                                 ->setGrade($pro)
                                 ->setCreatedAt($date)
                                 ->setUpdatedAt($date);

                        if ($pricingP == null) {
                            $this->em->persist($pricingP);
                        }
                    }
                }
                $this->em->flush();

            }

            return $this->redirectToRoute('pricing-tatap-muka');

        }

//        $this->data['where']['id'] = 1;

        return $this->renderTable('admin/pengaturan/wilayahLayanan.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/admin/get-province", name="get-province")
     */
    public function getProvince()
    {
        try {
            /** @var ProvinceRepository $prov */
            $prov = $this->em->getRepository(Province::class)->findAll();
            /**
             * @var Province $p
             */
            foreach ($prov as $key => $p) {
                $data[$key]['id']   = $p->getId();
                $data[$key]['nama'] = $p->getProvinceName();
            }
            $code     = 200;
            $response = $data;
        } catch (\Exception $e) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data : '.$e,
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/admin/get-city", name="get-city")
     */
    public function getCity()
    {
        try {
            /** @var ProvinceRepository $prov */
            $prov = $this->em->getRepository(City::class)->findBy(['province' => $this->req->get('province')]);
            /**
             * @var City $p
             */
            $data = [];
            foreach ($prov as $key => $p) {
                $data[$key]['id']   = $p->getId();
                $data[$key]['nama'] = $p->getCityName();
            }
            $code     = 200;
            $response = $data;
        } catch (\Exception $e) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data : '.$e,
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/admin/get-price", name="get-price")
     */
    public function getPrice()
    {

        try {
            /** @var PricingRepository $price */
            $idCity = $this->req->get('city');
            $city   = 'a.city is null';
            if ($idCity !== null) {
                $city = "a.city = '$idCity'";
            }
            $price    = $this->em->getRepository(Pricing::class);
            $repo     = $price->createQueryBuilder('a')
                              ->select(['a.id', 'a.price', 'l.id level_id, l.name as level_name', 'g.name as grade'])
                              ->join('a.level', 'l')
                              ->join('a.grade', 'g')
                              ->where($city)
                              ->getQuery()
                              ->getScalarResult();
            $code     = 200;
            $response = GenBasic::serializeToJson($repo);

        } catch (\Exception $e) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data : '.$e->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @return JsonResponse
     * @Route("/admin/pricing/delete-pricing", name="delete-pricing-wilayah")
     */
    public function deletePricing()
    {
//        $pricing = $this->em->getRepository(Pricing::class)->findOneBy(['city' => $this->req->get('cityid')]);
//        dump($pricing);
//        die();
        try {
            $pricing = $this->em->getRepository(Pricing::class)->findBy(['city' => $this->req->get('cityid')]);
            foreach ($pricing as $p) {
                $this->em->remove($p);
            }
            $this->em->flush();
            $code     = 200;
            $data     = [
                'status' => "Data berhasil dihapus",
                'label'  => "success",
            ];
            $response = $data;
        } catch (\Exception $e) {
            $code     = 500;
            $response = [
                'status' => 'Terjadi kesalahan saat menghapus data '.$e->getMessage(),
                'label'  => 'danger',
            ];
        }

        return new JsonResponse(
            $response,
            $code
        );

    }

}
<?php

namespace App\Controller\Admin\Pengaturan;

use App\Controller\BaseController;
use App\Entity\City;
use App\Entity\Grade;
use App\Entity\Level;
use App\Entity\Method;
use App\Entity\Pricing;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PricingOnlineController extends BaseController
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->formatDTResult = true;
        $this->tableFields    = [
            [
                'label' => 'Level',
                'field' => 'level',
                'sql'   => 'l.name',
            ],
            [
                'label'     => 'Grade',
                'field'     => 'grade',
                'sql'       => 'g.name',
                'formatter' => 'getDTGrade',
            ],
            [
                'label' => 'Harga',
                'field' => 'price',
                'sql'   => 'a.price',
                'formatter' => 'getDTPrice'
            ],

        ];

        $this->joint = [
            [
                'tipe'  => 'join',
                'join'  => 'a.level',
                'alias' => 'l',
            ],
            [
                'tipe'  => 'leftjoin',
                'join'  => 'a.grade',
                'alias' => 'g',
            ],
        ];

        $this->requiredWhere = 'a.city is null';

//        $this->tableActions = [
//            'edit',
//        ];

        $this->addSelect = 'l.name as level, g.name as grade';

        $this->singleNamespace = 'Admin\\\Pengaturan';
        $this->controllerName  = 'PricingOnlineController';
        $this->req             = Request::createFromGlobals();
        $this->class           = Pricing::class;
        $this->data['class']   = $this->class;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/pricing/online", name="pricing-online")
     */
    public function index()
    {
        if ($this->req->isMethod('POST')) {
            $offline      = $this->em->getRepository(Method::class)->findOneBy(['name' => 'Online']);
            $basic        = $this->em->getRepository(Grade::class)->findOneBy(['name' => 'BASIC']);
            $pro          = $this->em->getRepository(Grade::class)->findOneBy(['name' => 'PRO']);
            $pricingID    = $this->req->get('idPricing');
            $basicLevel[] = $this->req->get('basiclevel');
            $basicPrice[] = $this->req->get('basicprice');
            $proLevel[]   = $this->req->get('profesionallevel');
            $proprice[]   = $this->req->get('profesionalprice');
            $date         = new DateTime('', new DateTimeZone('Asia/Jakarta'));

            if ($this->req->get('action') == 'add') {
                if (isset($basicLevel[0])) {
                    for ($i = 0; $i < count($basicLevel[0]); ++$i) {
                        $pricingB = new Pricing;
                        $level    = $this->em->getRepository(Level::class)->findOneBy(['id' => $basicLevel[0][$i]]);
                        $pricingB->setLevel($level)
                                 ->setPrice(str_replace(',', '', $basicPrice[0][$i]) ?? 0)
                                 ->setMethod($offline)
                                 ->setGrade($basic)
                                 ->setCreatedAt($date)
                                 ->setUpdatedAt($date);
                        $this->em->persist($pricingB);
                    }
                }

                if (isset($proLevel[0])) {
                    for ($i = 0; $i < count($proLevel[0]); ++$i) {
                        $pricingP = new Pricing;
                        $pricingP->setLevel($this->em->getRepository(Level::class)->findOneBy(['id' => $proLevel[0][$i]]))
                                 ->setPrice(str_replace(',', '', $proprice[0][$i]) ?? 0)
                                 ->setMethod($offline)
                                 ->setGrade($pro)
                                 ->setCreatedAt($date)
                                 ->setUpdatedAt($date);
                        $this->em->persist($pricingP);

                    }
                }
                $this->em->flush();
            } else {
                if (isset($basicLevel[0])) {
                    for ($i = 0; $i < count($basicLevel[0]); ++$i) {
                        $pricingB = $this->em->getRepository(Pricing::class)->findOneBy(['id' => $pricingID[$i]]);
                        if ($pricingB == null) {
                            $pricingB = new Pricing;
                        }
//                        dump($pricingB);
                        $level = $this->em->getRepository(Level::class)->findOneBy(['id' => $basicLevel[0][$i]]);
                        $pricingB->setLevel($level)
                                 ->setPrice(str_replace(',', '', $basicPrice[0][$i]) ?? 0)
                                 ->setMethod($offline)
                                 ->setGrade($basic)
                                 ->setCreatedAt($date)
                                 ->setUpdatedAt($date);
                        if ($pricingB == null) {
                            $this->em->persist($pricingB);
                        }

                    }
                }

                if (isset($proLevel[0])) {
                    for ($i = 0; $i < count($proLevel[0]); ++$i) {
                        $pricingP = $this->em->getRepository(Pricing::class)->findOneBy(['id' => $pricingID[$i + 5]]);
                        if ($pricingP == null) {
                            $pricingP = new Pricing;
                        }
//                        dump($pricingP);

                        $pricingP->setLevel($this->em->getRepository(Level::class)->findOneBy(['id' => $proLevel[0][$i]]))
                                 ->setPrice(str_replace(',', '', $proprice[0][$i]) ?? 0)
                                 ->setMethod($offline)
                                 ->setGrade($pro)
                                 ->setCreatedAt($date)
                                 ->setUpdatedAt($date);

                        if ($pricingP == null) {
                            $this->em->persist($pricingP);
                        }
                    }
                }
                $this->em->flush();

            }

            return $this->redirectToRoute('pricing-online');

        }

//        $this->data['where']['id'] = 1;

        return $this->renderTable('admin/pengaturan/pricing-online.html.twig');
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTGrade($data)
    {
        return $data['grade'] == 'PRO' ? 'PROFESIONAL' : $data['grade'];
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTPrice($data){
        return number_format($data['price'],0);
    }
}
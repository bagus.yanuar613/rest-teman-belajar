<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;

class PerpanjanganController extends BaseController
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/perpanjangan", name="perpanjangan")
     */
    public function index(){
        return $this->renderTable('admin/perpanjangan.html.twig');
    }

}
<?php

namespace App\Controller\Admin;

use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Grade;
use App\Entity\Member;
use App\Entity\Rating;
use App\Entity\Subject;
use App\Entity\Tentor;
use App\Entity\User;
use App\Repository\RatingRepository;
use App\Repository\TentorRepository;
use App\Repository\UserRepository;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PengajarDetailController
 * @package App\Controller\Admin
 */
class PengajarDetailController extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->formatDTResult = true;

        $this->singleNamespace = 'Admin';
        $this->controllerName  = 'PengajarDetailController';
        $this->req             = Request::createFromGlobals();

        $this->class              = Tentor::class;
        $this->data['class']      = $this->class;
        $this->requiredWhereParam = 'u.id = :id';
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("admin/pengajar/detail/{id}", name="pengajar-detail")
     */
    public function detail($id)
    {
        $this->data['classJadwal']                  = 'JadwalTentorController';
        $this->data['spaceJadwal']                  = 'Admin';
        $this->data['classWilayah']                 = 'PengajarWilayahController';
        $this->data['classTransaksi']               = 'TransactionController';
        $this->data['classPemasukan']               = 'KonfirmasiPenarikanDetail';
        $this->data['classRating']                  = 'RatingController';

        $this->data['wherePemasukan']['user'] = $id;
        $this->data['wherePemasukan']['type'] = 0;
        $this->data['wherePemasukan']         = json_encode($this->data['wherePemasukan']);

        $this->data['wherePenarikan']['user'] = $id;
        $this->data['wherePenarikan']['type'] = 1;
        $this->data['wherePenarikan']         = json_encode($this->data['wherePenarikan']);


        $this->data['spaceWilayah']           = 'Admin';
        $this->data['whereProfile']['id']     = $id;
        $this->data['whereProfile']['param']  = 'ts.user';
        $this->data['whereProfile']['rating'] = 'a.tentor';
        $this->data['whereProfile']           = json_encode($this->data['whereProfile']);
        $this->data['id']                     = $id;

        /** @var Tentor $nama */
        $nama                            = $this->em->getRepository(Tentor::class)->findOneBy(['user' => $id]);
        $this->data['profileName']       = $nama;
        $this->data['tablefieldsJadwal'] = [
            [
                'label' => 'Hari',
                'field' => 'day',
            ],
            [
                'label' => 'Jam',
                'field' => 'time',
            ],
        ];

        $this->data['tablefieldsWilayah'] = [
            [
                'label' => 'Wilayah',
                'field' => 'districtName',
            ],
            [
                'label' => 'Kota / Kabupaten',
                'field' => 'cityName',
            ],
            [
                'label' => 'Provinsi',
                'field' => 'provinceName',
            ],
        ];

        $this->data['profile'] = [
            [
                'label'     => 'Foto',
                'field'     => 'avatar',
                'sql'       => 'a.avatar',
                'formatter' => 'avatar',
            ],
            [
                'label' => 'Nama Lengkap',
                'field' => 'fullName',
                'sql'   => 'a.fullName',
            ],
            [
                'label'     => 'Jenis Kelamin',
                'field'     => 'gender',
                'sql'       => 'a.gender',
                'formatter' => 'gender',
            ],
            [
                'label' => 'Tenyang Saya',
                'field' => 'about',
                'sql'   => 'a.about',
            ],
            [
                'label' => 'Pengalaman Mengajar',
                'field' => 'experience',
                'sql'   => 'a.experience',
            ],
            [
                'label' => 'Pendidikan Terakhir',
                'field' => 'education',
                'sql'   => 'a.education',
            ],
            [
                'label' => 'Prestasi Mengajar',
                'field' => 'achievement',
                'sql'   => 'a.achievement',
            ],
            [
                'label' => 'Pekerjaan',
                'field' => 'company',
                'sql'   => 'a.company',
            ],

        ];

        $this->data['nama'] = [
            [
                'label'     => 'Status Akun',
                'field'     => 'isActive',
                'sql'       => 'a.isActive',
                'formatter' => 'isActive',
            ],
            [
                'label'     => 'Status Verifikasi',
                'field'     => 'isVerified',
                'sql'       => 'a.isVerified',
                'formatter' => 'isVerified',
            ],
            [
                'label'     => 'Tipe Akun',
                'field'     => 'name',
                'sql'       => 'g.name',
                'formatter' => 'name',
            ],
            [
                'label' => 'Email',
                'field' => 'email',
                'sql'   => 'u.email',
            ],
            [
                'label' => 'Nomor Handphone',
                'field' => 'phone',
                'sql'   => 'a.phone',
            ],
            [
                'label'     => 'Tanggal Mendaftar',
                'field'     => 'createdAt',
                'sql'       => 'a.createdAt',
                'formatter' => 'createdAt',
            ],
            [
                'label'     => 'Mendaftar Melalui',
                'field'     => 'deviceModel',
                'sql'       => 'a.deviceModel',
                'formatter' => 'deviceModel',
            ],
        ];

        $this->data['identitas'] = [
            [
                'label' => 'NIK',
                'field' => 'nik',
                'sql'   => 'i.nik',
            ],
            [
                'label'     => 'Foto Ijazah',
                'field'     => 'certificate',
                'sql'       => 'i.certificate',
                'formatter' => 'certificate',
            ],
            [
                'label' => 'Nama Bank',
                'field' => 'bank',
                'sql'   => 'i.bank',
            ],
            [
                'label' => 'Nomor Rekening',
                'field' => 'rekening',
                'sql'   => 'i.rekening',
            ],
            [
                'label' => 'Pemilik Rekening',
                'field' => 'holderName',
                'sql'   => 'i.holderName',
            ],
        ];

        $this->data['tableFieldsTransaksi'] = [
            [
                'label' => 'Tanggal',
                'field' => 'createdAt',
            ],
            [
                'label' => 'Nomor Transaksi',
                'field' => 'transactionId',
            ],
            [
                'label' => 'Nominal Transaksi',
                'field' => 'amount',
            ],
        ];

        $this->data['tableFieldsPemasukan'] = [
            [
                'label'     => 'Tanggal Pemasukan',
                'field'     => 'createdAt',
            ],
            [
                'label'     => 'Nominal',
                'field'     => 'amount',
            ],
        ];

        $this->data['tableFieldsRating'] = [
            [
                'label' => 'User',
                'field' => 'fullName',
            ],
            [
                'label' => 'Rating',
                'field' => 'rating',
            ],
            [
                'label' => 'Ulasan',
                'field' => 'review',
            ],
        ];

        $this->data['actionName'] = [
            [
                'label' => 'Aktifkan / Suspend Akun',
            ],
            [
                'label' => 'Verifikasi Akun',
            ],
            [
                'label' => 'Kirim Pesan',
            ],
            [
                'label' => 'Upgrade Akun ke Professional',
            ],
        ];
//        /** @var RatingRepository $dat */
//        $dat   = $this->em->getRepository(Rating::class);
//        $query = $dat->createQueryBuilder('a')
//                     ->innerJoin('a.tentor', 't')
//                     ->innerJoin('a.user', 'u')
//                     ->leftJoin('u.memberProfile', 'm')
//                    ->where('a.tentor = 4')
//                     ->getQuery()
//                     ->getScalarResult();
//
//        dump($query);die();

        return $this->renderTable('admin/pengajarDetail.html.twig');
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function certificate($data)
    {
        return '<img src="/images/img-certificate/'.$data['certificate'].'" class="" height="100">';
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function createdAt($data)
    {
        Carbon::setLocale('id');
        $date = Carbon::parse($data['createdAt'], 'Asia/Jakarta')->isoFormat('LLLL');

        return $date;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function name($data)
    {
        return "<span class='btn-blue pr-2 pl-2 text-white' style='border-radius: 50px'>".$data['name']."</span>";
//        return $data['name'];
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function deviceModel($data)
    {
        $device = $data['deviceModel'] ?? 'Website';

        return $device;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function isActive($data)
    {
        if ($data['isActive'] == 0) {
            $aktiv = '<span class="fa fa-circle colorRed"></span> Tidak Aktif';
        } else {
            $aktiv = '<span class="fa fa-circle text-blue"></span> Aktif';
        }

        return $aktiv;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function gender($data)
    {
        if ($data['gender'] == 0) {
            $gender = 'Perempuan';
        } else {
            $gender = 'Laki - Laki';
        }

        return $gender;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function isVerified($data)
    {
        if ($data['isVerified'] == 0) {
            $aktiv = 'Belum diverifikasi';
        } else {
            $aktiv = 'Sudah diverifikasi <i class="fa fa-check-circle colorGreen"></i>';
        }

        return $aktiv;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function avatar($data)
    {
        return '<img src="/images/img-account/'.$data['avatar'].'" class="" height="100">';
    }

    /**
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/Admin/pengajar/detail/update-active-pengajar", name="update-active-pengajar")
     */
    public function changeActive()
    {
        try {
            $tentor = $this->em->getRepository(Tentor::class)->findOneBy(['user' => $this->req->get('id')]);
            $tentor->setIsActive($this->req->get('status'));
            $fcm = $tentor->getUser()->getAppFcmToken();

            $message = 'Selamat akun tentor kamu sudah aktif, sekarang kamu sudah bisa menerima siswa';
            $type = 'Actived';
            if($this->req->get('status') == '0'){
                $message = 'Maaf ya, akun tentor kamu sekarang di Suspend';
                $type = 'Suspend';
            }
//            $fcm = "c0ANMHoDTx6n292oQOzDGE:APA91bGR0k2_6lW-RJmXP-j74Fmn8vd68u3_xpeQxZJ_Y9RuvCQGZ2R8CKFNlAEdhg5Eb-St_Jz3pw4oTyJuxUoHNvIA6hSjF7tsqZKJAPoi6P7NyRoX5ABMvsg6Xy_j4PrhA3Nt8r8G";
            $responseNotif = [
                'type' => $type,
            ];
            $dataRes = $this->sendNotification($fcm,"Admin : Konfirmasi Akun (TEMAN BELAJAR)", $message, $responseNotif );

            $this->em->flush();
            $code     = 200;
            $response = [
                'status'   => 'success',
                'response' => $dataRes,
            ];
        } catch (\Exception $err) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data',
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/Admin/pengajar/detail/update-verifikasi-pengajar", name="update-verifikasi-pengajar")
     */
    public function changeVerifikasi()
    {
        try {
            $tentor = $this->em->getRepository(Tentor::class)->findOneBy(['user' => $this->req->get('id')]);
            $tentor->setIsVerified($this->req->get('status'));

            $message = 'Selamat akun tentor kamu sudah diverifikasi, sekarang siswa bisa lebih yakin dengan kemampuan yang kamu miliki.';
            $fcm = $tentor->getUser()->getAppFcmToken();

//            $fcm = "c0ANMHoDTx6n292oQOzDGE:APA91bGR0k2_6lW-RJmXP-j74Fmn8vd68u3_xpeQxZJ_Y9RuvCQGZ2R8CKFNlAEdhg5Eb-St_Jz3pw4oTyJuxUoHNvIA6hSjF7tsqZKJAPoi6P7NyRoX5ABMvsg6Xy_j4PrhA3Nt8r8G";
            $responseNotif = [
                'type' => 'Verified',
            ];
            $dataRes = $this->sendNotification($fcm,"Admin : Konfirmasi Akun (TEMAN BELAJAR)", $message, $responseNotif );

            $this->em->flush();
            $code     = 200;
            $response = [
                'status'   => 'success',
                'response' => $dataRes,
            ];
        } catch (\Exception $err) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data',
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/Admin/pengajar/detail/update-tipe-pengajar", name="update-tipe-pengajar")
     */
    public function changeTipeAkun()
    {
        try {
            $grade = $this->em->getRepository(Grade::class)->findOneBy(['id' => $this->req->get('status')]);

            $tentor = $this->em->getRepository(Tentor::class)->findOneBy(['user' => $this->req->get('id')]);
            $tentor->setGrade($grade);
            $this->em->flush();
            $fcm = $tentor->getUser()->getAppFcmToken();

//            $fcm = "c0ANMHoDTx6n292oQOzDGE:APA91bGR0k2_6lW-RJmXP-j74Fmn8vd68u3_xpeQxZJ_Y9RuvCQGZ2R8CKFNlAEdhg5Eb-St_Jz3pw4oTyJuxUoHNvIA6hSjF7tsqZKJAPoi6P7NyRoX5ABMvsg6Xy_j4PrhA3Nt8r8G";
            $responseNotif = [
                'type' => 'Pro',
            ];
            $dataRes = $this->sendNotification($fcm, "Admin : Status Tentor (TEMAN BELAJAR)", "Status anda dirubah menjadi ".$grade->getSlug(), $responseNotif);

            $code     = 200;
            $response = [
                'status'   => 'success',
                'response' => $dataRes,
            ];
        } catch (\Exception $err) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data',
            ];
        }

        return GenBasic::send($code, $response);
    }

}
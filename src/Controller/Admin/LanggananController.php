<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Cart;
use App\Entity\Subscription;
use App\Entity\Transaction;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LanggananController
 * @package App\Controller\Admin
 */
class LanggananController extends BaseController
{

    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->formatDTResult = true;

        $this->tableFields = [
            [
                'label'     => 'Tanggal Berlangganan',
                'field'     => 'updatedAt',
                'sql'       => 's.updatedAt',
                'formatter' => 'getCreatedAt',
            ],

            [
                'label' => 'Nama Siswa',
                'field' => 'member',
                'sql'   => 'm.fullName as member',
            ],
            [
                'label' => 'Pengajar',
                'field' => 'tentor',
                'sql'   => 'ten.fullName as tentor',
            ],
            [
                'label' => 'Kategori',
                'field' => 'category',
                'sql'   => 'c.name as category',
            ],
//            [
//                'label' => 'Method',
//                'field' => 'method',
//                'sql'   => 'mt.name as method',
//            ],
//            [
//                'label'     => 'Jumlah Siswa',
//                'field'     => 'attendees',
//                'sql'       => 'a.attendees',
//                'formatter' => 'getDTAttendees-'.KonfirmasiPembayaranDetail::class,
//
//            ],

        ];

        $this->tableActions = [
            'detailTrans',
        ];

        $this->data['order'] = ['column' => 1, 'type' => 'desc' ];

//        $this->addSelect = "t.id as trans";

        $this->singleNamespace = 'Admin';
        $this->controllerName  = 'LanggananController';
        $this->req             = Request::createFromGlobals();
        $this->class           = Subscription::class;
        $this->data['class']   = $this->class;
        $this->requiredWhere   = "s.status = 1";

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/langganan", name="langganan")
     */
    public function index()
    {

        return $this->renderTable('admin/langganan.html.twig');
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTAmount($data)
    {
        $hasil_rupiah = 'Rp. '.number_format($data['amount'], 0, ',', '.');

        return $hasil_rupiah;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getCreatedAt($data)
    {
        Carbon::setLocale('id');
        $date = Carbon::parse($data['updatedAt'], 'Asia/Jakarta')->isoFormat('LL, HH:mm');

        return $date;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/admin/get-count-new-subscribe-month", name="get-count-new-subscribe")
     */
    public function getCountNewSubsMonth()
    {
        $date1   = date_create($this->req->get('start'));
        $date2   = date_create($this->req->get('end'));
        $range   = date_diff($date1, $date2);
        $selisih = $range->format("%a") * 2;

        $old = "Select count(id) as jum from `subscription` where `status` = 1 AND created_at BETWEEN DATE_ADD('".$this->req->get('start')."', INTERVAL -".$selisih." DAY) AND DATE_ADD('".$this->req->get(
                'start'
            )."', INTERVAL -1 DAY) ";
        $new = "Select count(id) as jum from `subscription` where `status` = 1  AND created_at BETWEEN '".$this->req->get('start')."' AND '".$this->req->get('end')."' ";

        return $this->getCountPerncen($old, $new);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/admin/get-count-end-subscribe-month", name="get-count-end-subscribe")
     */
    public function getCountEndSubsMonth()
    {
        $date1   = date_create($this->req->get('start'));
        $date2   = date_create($this->req->get('end'));
        $range   = date_diff($date1, $date2);
        $selisih = $range->format("%a") * 2;

        $old = "Select count(id) as jum from `subscription` where `status` = 9 AND created_at BETWEEN DATE_ADD('".$this->req->get('start')."', INTERVAL -".$selisih." DAY) AND DATE_ADD('".$this->req->get(
                'start'
            )."', INTERVAL -1 DAY)";
        $new = "Select count(id) as jum from `subscription` where `status` = 9 AND created_at BETWEEN '".$this->req->get('start')."' AND '".$this->req->get('end')."'";

        return $this->getCountPerncen($old, $new);
    }

}
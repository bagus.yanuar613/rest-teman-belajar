<?php

namespace App\Controller\Admin;

use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Member;
use App\Entity\Tentor;
use App\Entity\Transaction;
use App\Entity\TryOutRegistrant;
use App\Repository\TransactionRepository;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SiswaDetailController
 * @package App\Controller\Admin
 */
class SiswaDetailController extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->formatDTResult = true;

        $this->singleNamespace = 'Admin';
        $this->data['space1']  = 'Admin';
        $this->controllerName  = 'SiswaDetailController';
        $this->req             = Request::createFromGlobals();

        $this->class              = Member::class;
        $this->data['class']      = $this->class;
        $this->requiredWhereParam = 'u.id = :id';
    }

    /**
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("admin/siswa/detail/{id}", name="siswa-detail")
     */
    public function index($id)
    {
        $this->data['tableFieldsTransaksi'] = [
            [
                'label' => 'Tanggal',
                'field' => 'createdAt',
            ],
            [
                'label' => 'Nomor Transaksi',
                'field' => 'subscriptionCode',
            ],
            [
                'label' => 'Jenis Transaksi',
                'field' => 'paymentMethod',
            ],
            [
                'label' => 'Total Transaksi',
                'field' => 'total',
            ],

        ];

        $this->data['tableFieldsBerlangganan'] = [
            [
                'label' => 'Tanggal',
                'field' => 'createdAt',
            ],
            [
                'label' => 'Nomor Langganan',
                'field' => 'subscriptionCode',
            ],
            [
                'label' => 'Nomor Referensi',
                'field' => 'referenceId',
            ],
            [
                'label' => 'Mata Pelajaran',
                'field' => 'subject',
            ],
            [
                'label' => 'Sisa Pertemuan',
                'field' => 'sisaPertemuan',
            ],
            [
                'label' => 'Nama Pengajar',
                'field' => 'tentor',
            ],
        ];

        $this->data['classTransaksi']        = 'TransactionController';
        $this->data['whereProfile']['id']    = $id;
        $this->data['whereProfile']['param'] = 'u.id';
        $this->data['whereProfile']          = json_encode($this->data['whereProfile']);
        $this->data['id']    = $id;


        /** @var Member $nama */
        $nama                      = $this->em->getRepository(Member::class)->findOneBy(['user' => $id]);
        $this->data['profileName'] = $nama;
        $this->data['profile']     = [
            [
                'label'     => 'Foto',
                'field'     => 'avatar',
                'sql'       => 'a.avatar',
                'formatter' => 'avatar',
            ],
            [
                'label' => 'Nama Lengkap',
                'field' => 'fullName',
                'sql'   => 'a.fullName',
            ],
//            [
//                'label'     => 'Jenis Kelamin',
//                'field'     => 'gender',
//                'sql'       => 'a.gender',
//                'formatter' => 'gender',
//            ],
            [
                'label' => 'Alamat Lengkap',
                'field' => 'address',
                'sql'   => 'a.address',
            ],
            [
                'label' => 'Asal Sekolah',
                'field' => 'school',
                'sql'   => 'a.school',
            ],
            [
                'label' => 'Jenjang',
                'field' => 'parent',
                'sql'   => 'cp.name as parent',
            ],
            [
                'label' => 'Kelas',
                'field' => 'name',
                'sql'   => 'mc.name',
            ],
        ];

        $this->data['nama'] = [
            [
                'label'     => 'Status Akun',
                'field'     => 'isActive',
                'sql'       => 'a.isActive',
                'formatter' => 'isActive',
            ],
            [
                'label' => 'Akun ID',
                'field' => 'id',
                'sql'   => 'a.id',
            ],
            [
                'label' => 'Email',
                'field' => 'email',
                'sql'   => 'u.email',
            ],
            [
                'label' => 'Nomor Handphone',
                'field' => 'phone',
                'sql'   => 'a.phone',
            ],
            [
                'label'     => 'Tanggal Mendaftar',
                'field'     => 'createdAt',
                'sql'       => 'a.createdAt',
                'formatter' => 'createdAt',
            ],
            [
                'label'     => 'Mendaftar Melalui',
                'field'     => 'deviceModel',
                'sql'       => 'a.deviceModel',
                'formatter' => 'deviceModel',
            ],
            [
                'label' => 'Nama Orang Tua',
                'field' => 'parentName',
                'sql'   => 'a.parentName',
            ],
            [
                'label' => 'Nomor Telepon Ortu',
                'field' => 'parentPhone',
                'sql'   => 'a.parentPhone',
            ],
        ];

        $this->data['actionName'] = [
            [
                'label' => 'Suspend Akun',
            ],
            [
                'label' => 'Verifikasi Akun',
            ],
            [
                'label' => 'Kirim Pesan',
            ],
        ];

        return $this->renderTable('admin/siswaDetail.html.twig');
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function createdAt($data)
    {
        Carbon::setLocale('id');
        $date = Carbon::parse($data['createdAt'], 'Asia/Jakarta')->isoFormat('LL, HH:mm');

        return $date;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function deviceModel($data)
    {
        $device = $data['deviceModel'] ?? 'Website';

        return $device;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function isActive($data)
    {
        if ($data['isActive'] == 0) {
            $aktiv = '<span class="fa fa-circle colorRed"></span> Belum Aktif';
        } else {
            $aktiv = '<span class="fa fa-circle text-blue"></span> Aktif';
        }

        return $aktiv;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function gender($data)
    {
        if ($data['gender'] == 0) {
            $gender = 'Perempuan';
        } else {
            $gender = 'Laki - Laki';
        }

        return $gender;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function isVerified($data)
    {
        if ($data['isVerified'] == 0) {
            $aktiv = 'Belum diverifikasi';
        } else {
            $aktiv = 'Sudah diverifikasi';
        }

        return $aktiv;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function avatar($data)
    {
        return '<img src="'.$data['avatar'].'" class="" height="100">';
    }

    /**
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/Admin/siswa/detail/update-active-siswa", name="update-active-siswa")
     */
    public function changeActive()
    {
//        dump($this->req);
//        die();
        try {
            $member = $this->em->getRepository(Member::class)->findOneBy(['user' => $this->req->get('id')]);
            $member->setIsActive($this->req->get('status'));
            $this->em->flush();
            $code     = 200;
            $response = 'Berhasil di rubah';
        } catch (\Exception $err) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data',
            ];
        }

        return GenBasic::send($code, $response);

    }

    /**
     * @param $id
     * @Route("/admin/siswa/detail/get-info/{id}", name="get-info-siswa")
     */
    public function getInfoPengguna($id)
    {
        try {
            $jumBerlangganan = count($this->em->getRepository(Transaction::class)->findBy(['user' => $id]));
            $JumKodeTryOut   = count($this->em->getRepository(TryOutRegistrant::class)->findBy(['user' => $id]));
            $info            = [
                [
                    'label' => 'Jumlah Transaksi',
                    'data'  => $jumBerlangganan,
                ],
                [
                    'label' => 'Jumlah Kode Tryout',
                    'data'  => $JumKodeTryOut,
                ],
            ];
            $code            = 200;
            $response        = $info;
        } catch (\Exception $err) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data '.$err->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);

    }

}
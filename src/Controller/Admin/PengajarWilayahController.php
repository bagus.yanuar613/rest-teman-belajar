<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\District;
use App\Repository\DistrictRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class PengajarWilayahController extends BaseController
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->tableFields = [
            [
                'label' => 'Wilayah',
                'field' => 'districtName',
                'sql' => 'a.districtName'
            ],
            [
                'label' => 'Kota / Kabupaten',
                'field' => 'cityName',
                'sql' => 'c.cityName'
            ],
            [
                'label' => 'Provinsi',
                'field' => 'provinceName',
                'sql' => 'p.provinceName'
            ],

        ];

        $this->tableActions = [
            'edit',
            'delete'
        ];

        $this->singleNamespace = 'Admin';
        $this->controllerName = 'PengajarWilayahController';
        $this->req             = Request::createFromGlobals();
        $this->class = District::class;
        $this->data['class'] = $this->class;
        $this->joint           = [
            [
                'tipe' => 'join',
                'join' => 'a.user',
                'alias' => 'u'
            ],
        ];
        $this->requiredWhereParam = 'u.id = :id';

    }



}
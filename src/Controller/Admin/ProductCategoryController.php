<?php


namespace App\Controller\Admin;


use App\Controller\Admin\Pengaturan\SubjectController;
use App\Controller\Admin\TryOut\CategoryController;
use App\Controller\BaseController;
use App\Entity\ProductCategory;
use App\Entity\Rating;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductCategoryController extends BaseController
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

//        $this->formatDTResult = true;

        $this->fields = [
            'name',
            'isActive',
            'childSlug',
        ];

        $this->tableFields = [
            [
                'label' => 'Name',
                'field' => 'name',
                'sql' => 'a.name'
            ],
            [
                'label' => 'Data',
                'field' => 'childSlug',
                'sql' => 'a.childSlug'
            ],
            [
                'label' => 'Aktif',
                'field' => 'isActive',
                'sql' => 'a.isActive',
            ]
        ];

        $this->tableActions = [
            'edit',
            'delete',
        ];

        $this->singleNamespace = 'Admin';
        $this->controllerName = "ProductCategoryController";
        $this->req = Request::createFromGlobals();
        $this->class = ProductCategory::class;
        $this->data['class'] = $this->class;

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/products", name="products")
     */
    public function index()
    {
        if ($this->req->isMethod("POST")) {
            $this->redirectAction = "products";
            if($this->req->get('action') == "edit") {
                return $this->edit();
            }else{
                return $this->post();
            }
        }

        return $this->renderTable('admin/product.html.twig');
    }


}
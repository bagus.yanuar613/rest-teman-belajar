<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Categories;
use App\Entity\Schedule;
use App\Entity\Subject;
use App\Entity\Tentor;
use App\Entity\TentorSubject;
use App\Entity\User;
use App\Repository\ScheduleRepository;
use App\Repository\TentorRepository;
use App\Repository\TentorSubjectRepository;
use App\Repository\UserRepository;
use Carbon\Carbon;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PengajarController
 * @package App\Controller\Admin
 */
class PengajarController extends BaseController
{
    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->formatDTResult = true;

        $this->tableFields = [
            [
                'label' => 'Nama Pengajar',
                'field' => 'fullName',
                'sql'   => 'a.fullName',
            ],
            [
                'label' => 'Email',
                'field' => 'email',
                'sql'   => 'u.email',
            ],
            [
                'label' => 'Identitas',
                'field' => 'nik',
                'sql'   => 'i.nik, i.certificate',
                'formatter' => 'getDTIdentitas'
            ],
            [
                'label' => 'Keahlian',
                'field' => 'experience',
                'sql'   => 'a.experience',
                'formatter' => 'getDTExperience'
            ],
            [
                'label' => 'Status',
                'field' => 'isVerified',
                'sql'   => 'a.isVerified',
                'formatter' => 'getDTVerified'
            ],
        ];

        $this->joint = [
            [
                'tipe'  => 'leftjoin',
                'join'  => 'u.tentorIdentity',
                'alias' => 'i',
            ],
        ];

        $this->tableActions = [
            'detail',
        ];


        $this->singleNamespace = 'Admin';
        $this->data['space1']  = 'Admin';
        $this->controllerName  = 'PengajarController';
        $this->req             = Request::createFromGlobals();

        $this->class              = Tentor::class;
        $this->data['class']      = $this->class;
        $this->requiredWhereParam = 'u.id = :id';

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/pengajar", name="pengajar")
     */
    public function index()
    {


        /** @var ScheduleRepository $dat */
        $dat   = $this->em->getRepository(Schedule::class);
        $query = $dat->createQueryBuilder('a')
                     ->select(['a', 'u'])
                     ->innerJoin('a.user', 'u')
                     ->getQuery()
                     ->getScalarResult();

//        dump($query);die();
        return $this->renderTable('admin/pengajar.html.twig');
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTIdentitas($data){
        $result = '<i class="fa fa-times-circle colorRed"></i>';
        if(isset($data['nik']) && isset($data['certificate'])){
            if($data['nik'] !== '' && $data['certificate'] !== ''){
                $result = '<i class="fa fa-check-circle colorGreen"></i>';
            }
        }
        return $result;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTExperience($data){
        $result = '<i class="fa fa-times-circle colorRed"></i>';
        if(isset($data['experience'])){
            $result = '<i class="fa fa-check-circle colorGreen"></i>';
        }
        return $result;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function getDTVerified($data){
        $aktiv = '<i class="fa fa-check-circle colorGreen"> Sudah diverifikasi';

        if ($data['isVerified'] == 0) {
            $aktiv = '<i class="fa fa-times-circle colorRed"> Belum diverifikasi';
        }

        return $aktiv;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/admin/pengajar/detail/get-keahlian/{id}", name="get-keahlian")
     */
    public function getKeahlian($id)
    {
        try {
            /** @var UserRepository $dat */
            $dat      = $this->em->getRepository(TentorSubject::class);
            $query    = $dat->createQueryBuilder('a')
                            ->select('c.name as card', 'l.name as level')
                            ->leftJoin('a.category', 'c')
                            ->leftJoin('a.level', 'l')
                            ->where('a.user = '.$id)
                            ->getQuery()
                            ->getScalarResult();
            $data     = [
                'data' => $query,
            ];
            $code     = 200;
            $response = GenBasic::serializeToJson($query);
        } catch (\Exception $err) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data',
            ];
        }

        return GenBasic::send($code, $response);

    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/admin/get-total-pengajar", name="get-total-pengajar")
     */
    public function getTotalTentor()
    {
        try {
            $res      = count($this->em->getRepository(Tentor::class)->findAll());
            $code     = 200;
            $data     = [
                'data' => $res,
            ];
            $response = GenBasic::serializeToJson($data);
        } catch (\Exception $e) {
            $code     = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data',
            ];
        }

        return GenBasic::send($code, $response);

    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/admin/get-total-pengajar-verified", name="get-total-pengajar-verified")
     */
    public function getTotalTentorVerified()
    {
//        try {
//            $res      = count($this->em->getRepository(Tentor::class)->findBy(['isVerified' => '1']));
//            $code     = 200;
//            $data     = [
//                'data' => $res,
//            ];
//            $response = GenBasic::serializeToJson($data);
//        } catch (\Exception $e) {
//            $code     = 500;
//            $response = [
//                'msg' => 'Terjadi kesalahan dalam pengambilan data',
//            ];
//        }
//
//        return GenBasic::send($code, $response);

        $date1 = date_create($this->req->get('start'));
        $date2 = date_create($this->req->get('end'));
        $range = date_diff($date1,$date2 );
        $selisih = $range->format("%a")*2;


//            dump($selisih);die();
        $old = "Select count(id) as jum from tentor where is_verified = '1' AND created_at BETWEEN DATE_ADD('".$this->req->get('start')."', INTERVAL -".$selisih." DAY) AND DATE_ADD('".$this->req->get('start')."', INTERVAL -1 DAY) ";
        $new = "Select count(id) as jum from tentor where is_verified = '1' AND created_at BETWEEN '".$this->req->get('start')."' AND '".$this->req->get('end')."' ";
        return $this->getCountPerncen($old, $new);

    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/admin/get-total-pengajar-profesional", name="get-total-pengajar-profesional")
     */
    public function getTotalTentorPro()
    {

//        try {
//            /** @var TentorRepository $res */
//            $res      = $this->em->getRepository(Tentor::class);
//            $rep      = count(
//                $res->createQueryBuilder('a')
//                    ->select(['a'])
//                    ->leftJoin('a.grade', 'g', 'WITH', 'a.grade = g')
//                    ->where('g.name = :grade')
//                    ->setParameter(':grade', 'Profesional')
//                    ->getQuery()
//                    ->getResult()
//            );
//            $code     = 200;
//            $data     = [
//                'data' => $rep,
//            ];
//            $response = GenBasic::serializeToJson($data);
//        } catch (\Exception $e) {
//            $code     = 500;
//            $response = [
//                'msg' => 'Terjadi kesalahan dalam pengambilan data',
//            ];
//        }
//
//        return GenBasic::send($code, $response);
        $date1 = date_create($this->req->get('start'));
        $date2 = date_create($this->req->get('end'));
        $range = date_diff($date1,$date2 );
        $selisih = $range->format("%a")*2;


        $old = "Select count(tentor.id) as jum from tentor LEFT JOIN grade ON tentor.grade_id = grade.id where grade.name = 'PROFESIONAL' AND created_at BETWEEN DATE_ADD('".$this->req->get('start')."', INTERVAL -".$selisih." DAY) AND DATE_ADD('".$this->req->get('start')."', INTERVAL -1 DAY)";
        $new = "Select count(tentor.id) as jum from tentor LEFT JOIN grade ON tentor.grade_id = grade.id where grade.name = 'PROFESIONAL' AND created_at BETWEEN '".$this->req->get('start')."' AND '".$this->req->get('end')."'";
        return $this->getCountPerncen($old, $new);

    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/admin/get-count-new-tentor-month", name="get-count-new-tentor")
     */
    public function getCountNewTentorMonth()
    {

//        if($this->req->get('parameter') == 'Last 30 Days'){
//            $old = "Select count(id) as jum from tentor where created_at BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -60 DAY) AND DATE_ADD(CURRENT_DATE, INTERVAL -31 DAY)";
//            $new = "Select count(id) as jum from tentor where created_at BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -30 DAY) AND CURRENT_DATE ";
//        }elseif ($this->req->get('parameter') == 'Today'){
//            $old = "Select count(id) as jum from tentor where created_at = DATE_ADD(CURRENT_DATE, INTERVAL -1 DAY)";
//            $new = "Select count(id) as jum from tentor where created_at = CURRENT_DATE ";
//        }elseif ($this->req->get('parameter') == 'Last 7 Days'){
//            $old = "Select count(id) as jum from tentor where created_at BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -15 DAY) AND DATE_ADD(CURRENT_DATE, INTERVAL -8 DAY)";
//            $new = "Select count(id) as jum from tentor where created_at BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -7 DAY) AND CURRENT_DATE ";
//        }else{
            $date1 = date_create($this->req->get('start'));
            $date2 = date_create($this->req->get('end'));
            $range = date_diff($date1,$date2 );
            $selisih = $range->format("%a")*2;


            $old = "Select count(id) as jum from tentor where created_at BETWEEN DATE_ADD('".$this->req->get('start')."', INTERVAL -".$selisih." DAY) AND DATE_ADD('".$this->req->get('start')."', INTERVAL -1 DAY) ";
            $new = "Select count(id) as jum from tentor where created_at BETWEEN '".$this->req->get('start')."' AND '".$this->req->get('end')."' ";


        return $this->getCountPerncen($old, $new);
    }



}
<?php


namespace App\Controller;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class QueryController extends BaseController
{
    /** @var ServiceEntityRepository $repository */
    private $repository;

    /**
     * UtilityController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    public function setRepository($class)
    {
        $this->repository = $this->em->getRepository($class);
        return $this;
    }
}
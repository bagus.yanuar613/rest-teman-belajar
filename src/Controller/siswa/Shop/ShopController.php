<?php


namespace App\Controller\siswa\Shop;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\ProductCategory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShopController extends BaseController
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->singleNamespace = 'siswa\\\Shop';
        $this->controllerName = 'ShopController';
        $this->req = Request::createFromGlobals();
        $this->class = ProductCategory::class;
        $this->data['class'] = $this->class;

    }


    /**
     * @return Response
     * @Route("/shop", name="belanja")
     */
    public function shop()
    {
        return $this->render('siswa/belanja/belanja.html.twig');
    }

    /**
     *
     * @Route("/shop/api", name="belanja_api")
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function shopApi()
    {
        $code = 200;
        $response = [];
        try {
            $data = $this->em->getRepository(ProductCategory::class)->findBy(["isActive" => true]);
            $response = [
                "msg" => "success",
                "data" => GenBasic::CustomNormalize($data, [
                    "id",
                    "name",
                    "childSlug",
                    "tryOut" => [
                        "name",
                        "price",
                        "icon",
                        "tryOutPricing" => [
                            "id",
                            "name",
                            "description",
                            "price",
                            "duration"
                        ]
                    ]
                ])
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan ' . $e->getMessage()
            ];
        }

        return GenBasic::send($code, $response);

    }


}
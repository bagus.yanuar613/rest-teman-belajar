<?php


namespace App\Controller\siswa\TryOut;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Member;
use App\Entity\Pricing;
use App\Entity\TentorSubject;
use App\Entity\TryOut;
use App\Entity\TryOutRegistrant;
use App\Entity\TryOutRegistrantAnswer;
use App\Entity\TryOutSubject;
use App\Entity\User;
use App\Repository\TentorSubjectRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class RaporController extends BaseController
{

    /**
     * TransactionController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return JsonResponse
     * @Route("/tryout-result/api", name="result-tryout-api")
     */
    public function resultTryoutApi()
    {
        $code = 200;
        $response = [];
        try {
            $data = $this->em
                ->createQueryBuilder()
                ->select(["a", "reg", "to", "q"])
                ->addSelect("( CASE WHEN to.isIRT = true and to.irtChecked = true THEN SUM(q.IRTScore) ELSE SUM(a.score) END ) AS finalScore")
                ->addSelect("SUM(q.IRTScore) AS finalIRTScore")
                ->addSelect("count(a.score) AS numberOfDone")
                ->addSelect("MIN(a.startedAt) AS startedAt")
                ->addSelect("time_diff(MAX(a.endedAt), MIN(a.startedAt)) AS durationWork")
                ->addSelect("(SELECT count(crt.id) FROM " . TryOutRegistrantAnswer::class . " crt WHERE crt.registration = a.registration AND crt.score > 0 AND crt.quiz IS NOT NULL) AS numberOfCorrect")
                ->addSelect("(SELECT count(wrg.id) FROM " . TryOutRegistrantAnswer::class . " wrg WHERE wrg.registration = a.registration AND wrg.score = 0 AND wrg.quiz IS NOT NULL) AS numberOfWrong")
                ->addSelect("(SELECT SUM(sbj.quizNumber) FROM " . TryOutSubject::class . " sbj WHERE sbj.tryout = to  ) AS finalQuizNumber")
                ->from(TryOutRegistrantAnswer::class, "a")
                ->join("a.quiz", "q", "WITH", "a.score > 0")
                ->join("a.registration", "reg")
                ->join("reg.tryOut", "to")
                ->where("a.quiz IS NOT NULL AND reg.user = :user")
                ->setParameter("user", $this->getMyUser())
                ->groupBy("a.registration")
                ->getQuery()
                ->getResult();

            $response = [
                "msg" => "success",
                "data" => GenBasic::CustomNormalize($data, [
                    'id',
                    'registrantNumber',
                    'finalPrice',
                    'totalPrice',
                    'isPaid',
                    "quiz" => [
                        "id"
                    ],
                    "registration" => [
                        "id",
                        'tryOut' => [
                            'id',
                            'title',
                            "isIRT",
                            "irtChecked"
                        ],
                    ]

                ])
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan ' . $e->getMessage()
            ];
        }
        return GenBasic::send($code, $response);
    }


    /**
     * @return JsonResponse
     * @Route("/tryout-chart/api", name="chart-tryout-api")
     */
    public function growthTryoutApi()
    {
        $code = 200;
        $response = [];
        try {
            $data = $this->em
                ->createQueryBuilder()
                ->select(["a", "to", "reg"])
                ->addSelect("( CASE WHEN to.isIRT = true and to.irtChecked = true THEN SUM(q.IRTScore) ELSE SUM(a.score) END ) AS finalScore")
                ->addSelect("MIN(a.startedAt) AS startedAt")
                ->addSelect("(SELECT SUM(sbj.quizNumber) FROM " . TryOutSubject::class . " sbj WHERE sbj.tryout = to  ) AS finalQuizNumber")
                ->from(TryOutRegistrantAnswer::class, "a")
                ->join("a.quiz", "q", "WITH", "a.score > 0")
                ->join("a.registration", "reg")
                ->join("reg.tryOut", "to")
                ->where("a.user = :user AND ((to.isIRT = true AND to.irtChecked = true) OR to.isIRT = false)")
                ->setParameter('user', $this->getMyUser())
                ->groupBy("a.registration")
                ->orderBy('startedAt', 'ASC')
                ->getQuery()
                ->getResult();


            $response = [
                "msg" => "success",
                "data" => GenBasic::CustomNormalize($data, [
                    'id',
                ])
            ];
            $data = $response['data'];
            foreach ($data as $key => $single) {
                $date = new DateTime($single['startedAt']);
                $data[$key]['finalScore'] = number_format($single['finalScore'] / $single['finalQuizNumber'], 2);
                $data[$key]['startedAt'] = $date->format("d M H:i");
            }
            $response['data'] = $data;
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan ' . $e->getMessage()
            ];
        }
        return GenBasic::send($code, $response);

    }


    /**
     * @return JsonResponse
     * @Route("/tryout-result-detail/api/{to}", name="result-tryout-detail-api")
     */
    public function resultDetailTryoutApi($to)
    {
        $code = 200;
        $response = [];
        try {

            $data = $this->em
                ->createQueryBuilder()
                ->select(["a", "reg", "to"])
                ->addSelect("( CASE WHEN to.isIRT = true and to.irtChecked = true THEN SUM(q.IRTScore) ELSE SUM(a.score) END ) AS finalScore")
                ->addSelect("count(a.score) AS numberOfDone")
                ->addSelect("MIN(a.startedAt) AS startedAt")
                ->addSelect("time_diff(MAX(a.endedAt), MIN(a.startedAt)) AS durationWork")
                ->addSelect("(SELECT count(crt.id) FROM " . TryOutRegistrantAnswer::class . " crt WHERE crt.registration = a.registration AND crt.score > 0 AND crt.quiz IS NOT NULL) AS numberOfCorrect")
                ->addSelect("(SELECT count(wrg.id) FROM " . TryOutRegistrantAnswer::class . " wrg WHERE wrg.registration = a.registration AND wrg.score = 0 AND wrg.quiz IS NOT NULL) AS numberOfWrong")
                ->addSelect("(SELECT SUM(sbj.quizNumber) FROM " . TryOutSubject::class . " sbj WHERE sbj.tryout = to  ) AS finalQuizNumber")
                ->from(TryOutRegistrantAnswer::class, "a")
                ->join("a.quiz", "q", "WITH", "a.score > 0")
                ->join("a.registration", "reg")
                ->join("reg.tryOut", "to")
                ->where("a.quiz IS NOT NULL AND reg.user = :user AND reg.id = :id AND ((to.isIRT = true AND to.irtChecked = true) OR to.isIRT = false)")
                ->setParameters([
                    "user" => $this->getMyUser(),
                    "id" => $to
                ])
                ->groupBy("a.registration")
                ->getQuery()
                ->getResult();


            $subs = $this->em
                ->createQueryBuilder()
                ->select(["a", "reg", "to"])
//                ->addSelect("SUM(a.score) AS finalScore")
                ->addSelect("( CASE WHEN to.isIRT = true and to.irtChecked = true THEN SUM(q.IRTScore) ELSE SUM(a.score) END ) AS finalScore")

                ->addSelect("count(a.score) AS numberOfDone")
                ->addSelect("MIN(a.startedAt) AS startedAt")
                ->addSelect("time_diff(MAX(a.endedAt), MIN(a.startedAt)) AS durationWork")
                ->addSelect("(SELECT count(crt.id) FROM " . TryOutRegistrantAnswer::class . " crt WHERE  crt.subject = a.subject AND  crt.registration = a.registration AND crt.score > 0 AND crt.quiz IS NOT NULL) AS numberOfCorrect")
                ->addSelect("(SELECT count(wrg.id) FROM " . TryOutRegistrantAnswer::class . " wrg WHERE  wrg.subject = a.subject AND wrg.registration = a.registration AND wrg.score = 0 AND wrg.quiz IS NOT NULL) AS numberOfWrong")
                ->addSelect("(SELECT SUM(sbj.quizNumber) FROM " . TryOutSubject::class . " sbj WHERE sbj.tryout = to AND sbj = a.subject  ) AS finalQuizNumber")
                ->from(TryOutRegistrantAnswer::class, "a")
                ->join("a.quiz", "q", "WITH", "a.score > 0")

                ->join("a.registration", "reg")
                ->join("reg.tryOut", "to")
                ->where("a.quiz IS NOT NULL AND reg.user = :user AND reg.id = :id")
                ->setParameters([
                    "user" => $this->getMyUser(),
                    "id" => $to
                ])
                ->groupBy("a.subject")
                ->getQuery()
                ->getResult();

            $response = [
                "msg" => "success",
                "data" => [
                    "main" => GenBasic::CustomNormalize($data, [
                        'id',
                        'registrantNumber',
                        'finalPrice',
                        'totalPrice',
                        'isPaid',
                        "registration" => [
                            'tryOut' => [
                                'id',
                                'title',
                            ],
                        ]

                    ]),
                    "subjects" => GenBasic::CustomNormalize($subs, [
                        'id',
                        'registrantNumber',
                        'finalPrice',
                        'totalPrice',
                        'isPaid',
                        "subject" => [
                            "subject" => [
                                "name",
                                "id",
                                "icon"
                            ]
                        ],
                        "registration" => [
                            'tryOut' => [
                                'id',
                                'title',
                                "level" => [
                                    "id"
                                ]
                            ],
                        ]

                    ])
                ]
            ];
            /** @var User $user */
            $user = $this->getMyUser();
            /** @var Member $member */
            $member = $user->getMemberProfile();
            $district = $member->getDistrict();
            if ($district) {

                $dSubs = $response['data']['subjects'];
                $toFindTentor = [];
                $subsName = [];
                $level = null;
                foreach ($dSubs as $subject) {
                    if (($subject['finalScore'] / $subject['finalQuizNumber']) < 80) {
                        $toFindTentor[] = $subject[0]['subject']['subject']['id'];
                        $subsName[] = $subject[0]['subject']['subject']['name'];
                        $level = $subject[0]['registration']['tryOut']['level']['id'];
                    }
                }

                $response['data']['recommendation'] = $this->findTentor($toFindTentor, $district, $level);
                $response['data']['subjectToLearn'] = $subsName;

            } else {
                $response['data']['recommendation'] = null;
                $response['data']['subjectToLearn'] = null;
            }
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan ' . $e->getMessage()
            ];
        }
        return GenBasic::send($code, $response);
    }


    /**
     * @return JsonResponse
     * @Route("/tryout-result-rank/api/{to}", name="result-tryout-rank-api")
     */
    public function resultRankTryoutApi($to)
    {
        $code = 200;
        try {
            $endScore = "(SELECT ( CASE WHEN to.isIRT = true and to.irtChecked = true THEN SUM(q.IRTScore) ELSE SUM(ans.score) END ) / SUM(ts.quizNumber) FROM " . TryOutSubject::class . " ts WHERE ts.tryout = to)";
            $endScore1 = "(SELECT ( CASE WHEN to.isIRT = true and to.irtChecked = true THEN SUM(q.IRTScore) ELSE SUM(ans.score) END ) / SUM(t1s.quizNumber) FROM " . TryOutSubject::class . " t1s WHERE t1s.tryout = to AND ans.user != :user)";

            $me = $this->em->createQueryBuilder()
                ->select(["a", "ans", "to"])
                ->addSelect("SUM(ans.score) AS finalScore")
                ->addSelect(" $endScore AS endScore")
                ->from(TryOutRegistrant::class, "a")
                ->join("a.tryOut", "to", "WITH", "a.tryOut = to AND to.id = :rid")
                ->join("a.answers", "ans", "WITH", "ans.registration = a AND a.user = ans.user")
                ->join("ans.quiz", "q", "WITH", "ans.score > 0")
                ->where("a.isEnded = true")
                ->andWhere("ans.quiz IS NOT NULL")
                ->setParameter("rid", $to)
                ->groupBy("a.id")
                ->orderBy('finalScore', 'DESC')
                ->setMaxResults(1)
                ->andWhere("a.user = :user")
                ->setParameter("user", $this->getMyUser())
                ->getQuery()
                ->getSingleResult();

            $data = $this->em->createQueryBuilder()
                ->select(["a", "ans", "to"])
                ->addSelect("SUM(ans.score) AS finalScore")
                ->addSelect(" $endScore AS endScore")
                ->from(TryOutRegistrant::class, "a")
                ->join("a.tryOut", "to", "WITH", "a.tryOut = to AND to.id = :rid")
                ->join("a.answers", "ans", "WITH", "ans.registration = a AND a.user = ans.user")
                ->join("ans.quiz", "q", "WITH", "ans.score > 0")
                ->where("a.isEnded = true")
                ->andWhere("ans.quiz IS NOT NULL")
                ->having("$endScore1 > :myscore")
                ->setParameter("rid", $to)
                ->setParameter("myscore", $me['endScore'])
                ->setParameter("user", $this->getMyUser())
                ->groupBy("a.id")
                ->orderBy('finalScore', 'DESC')
                ->getQuery()
                ->getResult();

            $biggerNumber = count($data);
            $data = array_splice($data, 0, 10);

            if (count($data) <= 10) {
                $data[count($data)] = $me;
                $lower = $this->em->createQueryBuilder()
                    ->select(["a", "ans", "to"])
                    ->addSelect("SUM(ans.score) AS finalScore")
                    ->addSelect(" $endScore AS endScore")
                    ->from(TryOutRegistrant::class, "a")
                    ->join("a.tryOut", "to", "WITH", "a.tryOut = to AND to.id = :rid")
                    ->join("a.answers", "ans", "WITH", "ans.registration = a AND a.user = ans.user")
                    ->join("ans.quiz", "q", "WITH", "ans.score > 0")
                    ->where("a.isEnded = true")
                    ->andWhere("ans.quiz IS NOT NULL")
                    ->having("$endScore1 <= :myscore")
                    ->setParameter("rid", $to)
                    ->setParameter("myscore", $me['endScore'])
                    ->setParameter("user", $this->getMyUser())
                    ->groupBy("a.id")
                    ->orderBy('finalScore', 'DESC')
                    ->setMaxResults(10)
                    ->getQuery()
                    ->getResult();

                $data = array_merge($data, $lower);

            }
            $serializer = [
                'id',
                'registrantNumber',
                'finalPrice',
                'totalPrice',
                'isPaid',
                "user" => [
                    "memberProfile" => [
                        "fullName",
                        'avatar'
                    ]
                ],
                'tryOut' => [
                    'id',
                    'title',
                ],
            ];
            $first = $data[0];
            $third = null;
            $second = null;
            if (isset($data[2])) {
                $third = $data[2];
            }
            if (isset($data[1])) {
                $second = $data[1];
            }

            $response = [
                "msg" => "success",
                "data" => [
                    "biggerNumber" => $biggerNumber,
                    "mine" => GenBasic::CustomNormalize($me, $serializer),
                    "first" => GenBasic::CustomNormalize($first, $serializer),
                    "second" => GenBasic::CustomNormalize($second, $serializer),
                    "third" => GenBasic::CustomNormalize($third, $serializer),
                    "list" => GenBasic::CustomNormalize($data, $serializer)
                ]
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan ' . $e->getMessage()
            ];
        }
        return GenBasic::send($code, $response);
    }


    public function findTentor($category, $district, $level, $method = 2, $limit = 5, $offset = 0)
    {
        try {

            if ($category === null || $level === null || ($method === '1' && $district === null)) {
                return GenBasic::send(500, 'Wrong Parameter!');
            }
            /** @var TentorSubjectRepository $ts */
            $ts = $this->em->getRepository(TentorSubject::class);
            $qb = $ts->createQueryBuilder('ts')
                ->addSelect('p.price as price')
                ->innerJoin('ts.user', 'u')
                ->innerJoin('u.tentorProfile', 'tp')
                ->leftJoin('ts.method', 'm')
                ->leftJoin('u.district', 'ds')
                ->leftJoin('u.ratings', 'r')
                ->where('ts.isAvailable = :status')
                ->andWhere('tp.isActive = :active')
                ->setParameter('status', true)
                ->setParameter('active', true)
                ->groupBy('ts.id')
                ->orderBy('AVG(r.rating)', 'DESC');
//
            if ($category !== null) {
                $qb->andWhere('ts.category IN (:category)')->setParameter('category', $category);
            }
            if ($level !== null) {
                $qb->andWhere('ts.level = :level')->setParameter('level', $level);
            }
            if ($method !== null) {
                $vMethod = [1, 2];
                $qb->andWhere('m.id IN (:method)')->setParameter('method', $vMethod);
            }
            if ($district !== null) {
                $v = [$district];
                $qb->andWhere('ds.id IN(:district)')->setParameter('district', $v);
            }

            $city = 'is null';
            if ($method === '1') {
                $city = '= ds.city';
            }
            $qb->innerJoin(Pricing::class, 'p', Join::WITH, 'p.level = ts.level and p.grade = tp.grade and p.city ' . $city . ' and p.method = :mid');
            $qb->setParameter('mid', $method);

            $tempResults = $qb
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult();

            $attributes = [
                'id',
                'slug',
                "level" => [
                    "name"
                ],
                'user' => [
                    'tentorProfile' => [
                        'fullName',
                        'about',
                        'slugName',
                        'generatedAvatar',
                        'grade' => ['id', 'name']
                    ],
                    'email',
                    'district' => ['id', 'districtName'],
                    'avgRatings'
                ],
            ];
            $code = 200;
            $response = GenBasic::CustomNormalize($tempResults, $attributes);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data Level Subject ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Data Level Subject ' . $e->getMessage(),
            ];
        }
        return $response;
    }
}
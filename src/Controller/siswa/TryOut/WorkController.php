<?php


namespace App\Controller\siswa\TryOut;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Categories;
use App\Entity\CategoriesParent;
use App\Entity\Level;
use App\Entity\TryOut;
use App\Entity\TryOutQuiz;
use App\Entity\TryOutRegistrant;
use App\Entity\TryOutRegistrantAnswer;
use App\Entity\TryOutSubject;
use App\Repository\TryOutRegistrantAnswerRepository;
use DateInterval;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class WorkController extends BaseController
{

    /**
     * TransactionController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/kerjain-paket/{cat}/{reg}", name="kerjain_paket")
     */
    public function paketSmbptn($cat, $reg)
    {
        $data = $this->em->createQueryBuilder()
            ->from(TryOut::class, "a")
            ->select(["a", "r"])
            ->join('a.registrant', 'r', " WITH ", "r.tryOut = a AND r.user = :user")
            ->where("a.id = :id AND r.isPaid = true ANd r.id = :rid")
            ->setParameter('id', $cat)
            ->setParameter('rid', $reg)
            ->setParameter('user', $this->getMyUser())
            ->getQuery()
            ->getResult();

        if ($data) {
            $started = $this->em->createQueryBuilder()
                ->from(TryOutRegistrantAnswer::class, "a")
                ->select("a, reg")
                ->join("a.registration", "reg")
                ->where("reg.id = :rid AND a.user = :user AND a.isBegin = true")
                ->setParameters(["rid" => $reg, "user" => $this->getUser()])
                ->orderBy("a.startedAt", "ASC")
                ->setMaxResults(1)
                ->getQuery()
                ->getResult();

            return $this->render('siswa/tryout/paketSmbptn.html.twig', [
                'data' => $data[0],
                "reg" => $reg,
                "startedAt" => count($started) > 0 ?  ($started[0]->getStartedAt())->format("Y-m-d H:i:s") : null,
                "isEnded" => count($started) > 0 ? $started[0]->getRegistration()->getIsEnded() : false,
            ]);
        } else {
            $this->addFlash('Oops..', "Terjadi galat, tryout tidak bisa ditemukan ");
            return $this->redirect($this->generateUrl('history-tryout'));
        }
    }

    /**
     *
     * @Route("/get-tab-menu/api/{cat}/{reg}", name="get_tab_menu_work_tryout_api")
     */
    public function getTabMenu($cat, $reg)
    {
        $code = 200;
        $response = [];
        try {
            $tryout = $this->em->getRepository(TryOut::class)->findBy(["id" => $cat]);
            $data = $this->em->createQueryBuilder()
                ->from(CategoriesParent::class, 'a')
                ->select(["a", "t", "ts", "r", "ans"])
                ->join('a.tryOutSubject', 'ts', " WITH ", "ts.mainSubject = a AND ts.tryout = :tryOut")
                ->join('ts.tryout', 't', ' WITH ', 'ts.tryout = t')
                ->leftJoin('t.registrant', 'r', ' WITH ', 'r.tryOut = t ')
                ->leftJoin('ts.tryoutAnswer', 'ans', 'WITH', 'ans.registration = r AND ans.user = :user ')
                ->where('a.isTryOut = true AND ts.isActive = true AND r.id = :rid ')
                ->andWhere("r.user = :user")
                ->setParameter("tryOut", $tryout)
                ->setParameter("rid", $reg)
                ->setParameter("user", $this->getMyUser())
                ->orderBy("ts.sorting", "ASC")
                ->addOrderBy("ans.id", "ASC")
                ->getQuery()
                ->getResult();

            $response = [
                "msg" => "success",
                "data" => GenBasic::CustomNormalize($data, [
                    "id",
                    "name",
                    "tryOutSubject" => [
                        "id",
                        "duration",
                        "quizNumber",
                        "icon",
                        "tryoutAnswer" => [
                            "id",
                            "isEnded"
                        ],
                        "tryout" => [
                            "isStrict",
                            "registrant" => [
                                "id",
                                "isEnded",
                                "expiredAt",
                                "user" => [
                                    "id"
                                ]
                            ]
                        ],
                        "subject" => [
                            "id",
                            "name"
                        ]
                    ]

                ])
            ];
            if(!empty($response['data'])) {
                if ($response['data'][0]['tryOutSubject'][0]['tryout']['registrant'][0]['expiredAt']) {
                    $expired = new DateTime($response['data'][0]['tryOutSubject'][0]['tryout']['registrant'][0]['expiredAt']);
                    $response['data'][0]['expired'] = ($expired < new DateTime('', new DateTimeZone('Asia/Bangkok')));
                } else {
                    $response['data'][0]['expired'] = false;
                }
            }
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan ' . $e->getMessage()
            ];
        }
        return GenBasic::send($code, $response);

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/mengerjakan-soal/{cat}/{reg}", name="mengerjakanSoal")
     */
    public function mengerjakanSoal($cat, $reg)
    {
        return $this->render('siswa/tryout/mengerjakanSoal.html.twig', ['ref' => $cat, 'reg' => $reg]);
    }

    /**
     *
     * @Route("/end-tryout/api/{reg}", name="end_tryout_session")
     */
    public function endTryOut($reg)
    {
        try {
            $data = $this->em->createQueryBuilder()
                ->from(TryOutRegistrantAnswer::class, 'a')
                ->select(["a"])
                ->join("a.registration", 'r')
                ->where(' r.id = :reg AND a.user = :user AND a.isBegin = true')
                ->setParameters(["reg" => $reg, "user" => $this->getMyUser()])
                ->getQuery()
                ->getResult();

            /** @var TryOutRegistrantAnswer $answer */
            $end = new \DateTime('', new DateTimeZone('Asia/Bangkok'));
            foreach ($data as $answer) {
                $answer->setIsEnded(true)
                    ->setEndedAt($end);
            }
            /** @var TryOutRegistrant $registrant */
            $registrant = $data[0]->getRegistration();
            $registrant->setIsEnded(true)
                ->setEndedAt($end);

            $this->em->flush();
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => [
                    "msg" => "Jawaban berhasil dikirim"
                ]
            ];
        } catch (\Exception $e) {
            $code = 502;
            $response = ['msg' => 'Terjadi kesalahan ' . $e->getMessage()];
        }
        return GenBasic::send($code, $response);
    }

    /**
     *
     * @Route("/end-quiz/api/{cat}/{reg}", name="end_quiz_session")
     */
    public function endQuiz($cat, $reg)
    {
        try {
            $data = $this->em->createQueryBuilder()
                ->from(TryOutRegistrantAnswer::class, 'a')
                ->select(["a"])
                ->join("a.subject", 's')
                ->join("a.registration", 'r')
                ->where('s.id = :subjectId AND r.id = :reg AND a.user = :user')
                ->setParameters(["subjectId" => $cat, "reg" => $reg, "user" => $this->getMyUser()])
                ->getQuery()
                ->getResult();

            /** @var TryOutRegistrantAnswer $answer */
            $end = new \DateTime('', new DateTimeZone('Asia/Bangkok'));
            foreach ($data as $answer) {
                if (!$answer->getIsBegin()) {
                    $answer->setIsEnded(true)
                        ->setEndedAt($end);
                }
            }

            $this->em->flush();
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => [
                    'redirect' => $this->container ? $this->generateUrl("kerjain_paket", ['cat' => $data[0]->getSubject()->getTryout()->getId(), 'reg' => $reg]) : null,
                ]
            ];
        } catch (\Exception $e) {
            $code = 502;
            $response = ['msg' => 'Terjadi kesalahan ' . $e->getMessage()];
        }
        return GenBasic::send($code, $response);
    }

    /**
     *
     * @Route("/save-quiz-answer/api/{cat}/{reg}", name="save_answer_quiz")
     */
    public function saveAnswer($cat, $reg)
    {
        try {
            $answer = $this->req->get('QA');
            $quiz = $this->req->get('Q');
            /** @var TryOutQuiz $quizObj */
            $quizObj = $this->em->getRepository(TryOutQuiz::class)->findOneBy(['id' => $quiz]);

            $options = $quizObj->getOptions();

            $answerRes = $options[$answer];

            $tryout = $this->em->getRepository(TryOutSubject::class)->findOneBy(["id" => $cat]);
            $registrant = $this->em->getRepository(TryOutRegistrant::class)->findOneBy(["id" => $reg]);
            /** @var TryOutRegistrantAnswerRepository $answerNew */
            $answerNew = $this->em->getRepository(TryOutRegistrantAnswer::class);
            $begin = $answerNew->insertOrUpdate([
                "isBegin" => false,
                "user" => $this->getMyUser(),
                "quiz" => $quizObj,
                "subject" => $tryout,
                "registration" => $registrant,
            ], [
                "isBegin" => false,
                "user" => $this->getMyUser(),
                "subject" => $tryout,
                "quiz" => $quizObj,
                'answer' => $answer,
                'score' => $answerRes['score'],
                "registration" => $registrant,
                "startedAt" => new \DateTime('', new DateTimeZone('Asia/Bangkok'))
            ]);
            $code = 200;
            $response = [
                'msg' => 'Success'
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan ' . $e->getMessage()
            ];
        }
        return GenBasic::send($code, $response);

    }


    /**
     *
     * @Route("/get-all-quizes/api/{cat}/{reg}", name="get_all_quizes")
     */
    public function getALlQuizes($cat, $reg)
    {
        $code = 200;
        try {
            /** @var TryOutRegistrantAnswerRepository $answer */

            $tryout = $this->em->getRepository(TryOutSubject::class)->findOneBy(["id" => $cat]);
            /** @var  TryOutRegistrant $registrant */
            $start = new \DateTime('', new DateTimeZone('Asia/Bangkok'));
            $expired = new \DateTime('', new DateTimeZone('Asia/Bangkok'));
            $registrant = $this->em->createQueryBuilder()
                ->select(["tr"])
                ->from(TryOutRegistrant::class, 'tr')
                ->join("tr.tryOut", "t")
                ->join("t.subject", "s", "WITH", " s = :subject")
                ->where("tr.id = :rid")
                ->setParameter("subject", $tryout)
                ->setParameter("rid", $reg)
                ->getQuery()
                ->getSingleResult();
            /** @var TryOut $tryOutOr */
            $tryOutOr = $registrant->getTryOut();
            $tryOutOr->getEstimatedTime();
            $expired->add(new DateInterval('PT' . $tryOutOr->getEstimatedTime() . 'M'));
            $registrant->setExpiredAt($expired);
            $answer = $this->em->getRepository(TryOutRegistrantAnswer::class);
            $begin = $answer->insertIfNoExists([
                "isBegin" => true,
                "user" => $this->getMyUser(),
                "subject" => $tryout,
                "registration" => $registrant,

            ], [
                "isBegin" => true,
                "user" => $this->getMyUser(),
                "subject" => $tryout,
                "registration" => $registrant,
                "startedAt" => $start
            ]);

            $data = $this->em->createQueryBuilder()
                ->from(TryOutQuiz::class, 'a')
                ->select(["a"])
                ->addSelect("( SELECT ans.answer FROM App\Entity\TryOutRegistrantAnswer ans WHERE ans.quiz = a  AND ans.registration = :reg  AND ans.user = :user ) as answer ")
                ->addSelect("( SELECT ans1.startedAt FROM App\Entity\TryOutRegistrantAnswer ans1 WHERE ans1.subject = t AND ans1.user = :user AND ans1.registration = :reg AND ans1.isBegin = true ) as startedAt ")
                ->join("a.tryOut", 't')
                ->innerJoin("t.tryout", 'r', 'WITH', "t.tryout = r")
                ->where('a.tryOut = :tryout')
                ->setParameter("tryout", $tryout)
                ->setParameter("reg", $registrant)
                ->setParameter("user", $this->getMyUser())
                ->orderBy('t.mainSubject', 'ASC')
                ->getQuery()
                ->getResult();


            $response = [
                "msg" => "success " . $begin,
                "data" => GenBasic::CustomNormalize($data, [
                    "id",
                    "question",
                    "options",
                    "topic",
                    "explanation",
                    "answer",
                    "startedAt",
                    'tryOut' => [
                        'duration',
                        'subject' => [
                            "name"
                        ]
                    ]
                ])
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan ' . $e->getMessage()
            ];
        }
        return GenBasic::send($code, $response);
    }

}
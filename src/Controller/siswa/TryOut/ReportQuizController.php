<?php


namespace App\Controller\siswa\TryOut;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\TryOutQuiz;
use App\Entity\TryOutQuizReport;
use App\Entity\TryOutRegistrant;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReportQuizController extends BaseController
{

    /**
     * TransactionController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return Response
     * @Route("/laporkan-soal/{quiz}/{reg}", name="laporkanSoal")
     */
    public function laporkanSoal($quiz, $reg)
    {
        return $this->render('siswa/tryout/laporkan.html.twig', ['data' => $quiz, 'reg' => $reg]);
    }

    /**
     * @return Response
     * @Route("/laporkansoal-detail/{quiz}/{reg}/{title}", name="laporkanSoal-detail")
     */
    public function laporkanSoalDetail($quiz, $reg, $title)
    {
        $data = $this->em->getRepository(TryOutQuiz::class)->findOneBy(["id" => $quiz]);
        return $this->render('siswa/tryout/laporkanDetail.html.twig', [
            'data' => $data,
            'reg' => $reg,
            'title' => $title
        ]);
    }

    /**
     * @return Response
     * @Route("/laporkansoal/api/{quiz}/{reg}", name="laporkanSoal-api")
     */
    public function laporkanSoalDetailApi($quiz, $reg)
    {
        try {
            /** @var TryOutRegistrant $data */
            $data = $this->em->getRepository(TryOutRegistrant::class)->findOneBy(["id" => $reg, "user" => $this->getMyUser()]);
            /** @var TryOutQuiz $quiz */
            $quiz = $this->em->getRepository(TryOutQuiz::class)->findOneBy(["id" => $quiz]);

            $report = new TryOutQuizReport();
            $report->setQuiz($quiz)
                ->setRegistration($data)
                ->setReporter($this->getMyUser())
                ->setTitle($this->req->get('title'))
                ->setDescription($this->req->get('description'));

            $this->em->persist($report);
            $this->em->flush();
            $url = $this->container ? $this->generateUrl('mengerjakanSoal', ['cat' => $quiz->getTryOut()->getId(), 'reg' => $reg]) : null;
            $code = 200;
            $data = ["msg" => "Data berhasil ditambahkan", "data" => [
                'redirect' => $url
            ]];
        } catch (\Exception $e) {
            $code = 501;
            $data = ["msg" => "Terjadi kesalahan"];
        }
        return GenBasic::send($code, $data);
    }

}
<?php


namespace App\Controller\siswa\TryOut;


use App\Common\GenBasic;
use App\Controller\Admin\TransactionController;
use App\Controller\BaseController;
use App\Entity\BankAccount;
use App\Entity\Categories;
use App\Entity\Level;
use App\Entity\Transaction;
use App\Entity\TryOut;
use App\Entity\TryOutFreeClaim;
use App\Entity\TryOutPricing;
use App\Entity\TryOutRegistrant;
use App\Entity\TryOutReward;
use App\Entity\TryOutSubject;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DetailController extends BaseController
{

    /**
     * TransactionController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return Response
     * @Route("/tryout-code-access/api/{cat}/{codeAccess}", name="tryoutCodeAccessApi", methods="POST|HEAD")
     */
    public function tryoutCodeAccessApi($cat, $codeAccess)
    {
        try {
            $data = $this->em->createQueryBuilder()
                ->select([
                    "a",
                    "r",
                    "re"
                ])
                ->from(TryOut::class, "a")
                ->leftJoin("a.registrant", "r")
                ->leftJoin('r.rewards', "re")
                ->where("a.id = :id  AND (a.codeAccess = :code OR (re.referalCode  = :code AND r.user = :user)) ")
                ->setParameters([
                    "id" => $cat,
                    "code" => $codeAccess,
                    "user" => $this->getMyUser()
                ])
                ->getQuery()
                ->getResult();
            $redirect = null;
            $idReg = null;

            if (count($data) > 0) {
                /** @var TryOut $tryOut */
                $tryOut = $data[0];
                $bank = $this->em->getRepository(BankAccount::class)->findOneBy(['id' => $this->req->get('paymentMethod')]);

                $newRegistrant = new TryOutRegistrant();
                $newRegistrant->setUser($this->getMyUser())
                    ->setIsPaid(false)
                    ->setBasePrice($tryOut->getPrice())
                    ->setFinalPrice(0)
                    ->setIsPaid(true)
                    ->setPromoDescription("FREE_" . $codeAccess)
                    ->setTotalPrice(0)
                    ->setRegistrantNumber(1)
                    ->setTryOut($tryOut);
                $this->em->persist($newRegistrant);
                $newClaim = new TryOutFreeClaim();
                $newClaim->setUser($this->getMyUser())
                    ->setTryOut($tryOut)
                    ->setCodeAccess($codeAccess)
                    ->setRegistrant($newRegistrant);

                if (count($tryOut->getRegistrant()->toArray()) > 0 AND count($tryOut->getRegistrant()->toArray()[0]->getRewards()->toArray()) > 0 AND $tryOut->getRegistrant()->toArray()[0]->getRewards()->toArray()[0]->getReferalCode() == $codeAccess) {
                    $newClaim->setFreeAccess($tryOut->getRegistrant()->toArray()[0]->getRewards()->toArray()[0]);
                }

                $this->em->persist($newClaim);

                $this->em->flush();
                $idReg = $newRegistrant->getId();
                $redirect = $this->container ? $this->generateUrl('history-tryout') : null;
            }


            $code = 200;
            $response = [
                "msg" => "success ",
                "data" => GenBasic::CustomNormalize($data, [
                    "id",
                    "title",
                    'registrant' => [
                        'rewards' => [
                            "referalCode"
                        ],
                        'user' => [
                            "id"
                        ]
                    ]
                ])
            ];
            $response['data']["redirect"] = $redirect;
            $response['data']["idReg"] = $idReg;
        } catch (\Exception $e) {
            $code = 404;
            $response = [
                "msg" => "Kode akses tidak ditemukan " . $e->getMessage(). " LINE".$e->getLine()
            ];
        }
        return GenBasic::send($code, $response);

    }

    /**
     * @return Response
     * @Route("/detail-paket-soal/{cat}", name="detailPaketSoal")
     */
    public function detailPaketSoal($cat)
    {
        $data = $this->em->createQueryBuilder()
            ->from(TryOut::class, "a")
            ->select(["a"])
            ->addSelect("(SELECT SUM(s.duration) FROM " . TryOutSubject::class . " s WHERE s.tryout = a) as durationTotal")
            ->addSelect("(SELECT SUM(s1.quizNumber) FROM " . TryOutSubject::class . " s1 WHERE s1.tryout = a) as number")
            ->where("a.id = :id")
            ->setParameter('id', $cat)
            ->getQuery()
            ->getSingleResult();

        return $this->render('siswa/tryout/detailPaketSoal.html.twig', ['data' => $data, 'ref' => $cat]);
    }

    /**
     * @return Response
     * @Route("/pendaftaran-tryout/{cat}", name="pendaftaranTryout")
     */
    public function pendaftaranTryout($cat)
    {
        $data = $this->em->createQueryBuilder()
            ->from(TryOut::class, "a")
            ->select(["a"])
            ->where("a.id = :id")
            ->setParameter('id', $cat)
            ->getQuery()
            ->getSingleResult();
        if($data->getPrice() == 0){
            try{
                $newRegistrant = new TryOutRegistrant();
                $newRegistrant->setUser($this->getMyUser())
                    ->setIsPaid(false)
                    ->setBasePrice($data->getPrice())
                    ->setFinalPrice(0)
                    ->setIsPaid(true)
                    ->setPromoDescription("CLAIM_RP_0")
                    ->setTotalPrice(0)
                    ->setRegistrantNumber(1)
                    ->setTryOut($data);
                $this->em->persist($newRegistrant);
                $this->em->flush();
                return $this->redirectToRoute("history-tryout");
            }catch(\Exception $msg){
                $this->addFlash("danger", "Gagal saat melakukan claim, coba beberapa saat lagi.");

                return $this->redirectToRoute("detailPaketSoal", ["cat" => $cat]);


            }
        }
        return $this->render('siswa/tryout/pendaftaranTryout.html.twig', ['data' => $data]);
    }

    /**
     * @return JsonResponse
     * @Route("/pendaftaran-tryout/api/amount/{cat}/{number}", name="pendaftaranTryoutApiAmount")
     */
    public function getTotalAmountApi($cat, $number, $digits = false)
    {

        $code = 200;
        $id = null;
        try {
            /** @var TryOut $data */
            $data = $this->em->getRepository(TryOut::class)->findOneBy(["id" => $cat]);
            if ($number >= 1) {
                /** @var TryOutPricing $pricing */
                foreach ($data->getPricing() as $pricing) {
                    if ($number >= $pricing->getMin() && ($number <= $pricing->getMax() || $pricing->getMax() == 0)) {
                        $price = $number * $pricing->getPrice();
                        $id = $pricing->getId();
                        break;
                    }
                }
            }

            if (!isset($price)) {
                $price = $number * $data->getPrice();
            }
            $digit = 0;
            if($digits) {
                $digit = $price > 0 ? rand(100, 999) : 0 ;
                $price += $digit;
            }
            $response = [
                "msg" => "success",
                "data" => [
                    'number' => $number,
                    'price' => $price,
                    'ref' => $id,
                    'digits' => $digit
                ]
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan ' . $e->getMessage()
            ];
        }
        return GenBasic::send($code, $response);

    }


    /**
     * @return Response
     * @Route("/pembayaran-tryout", name="pembayaranTryout")
     */
    public function pembayaranTryout()
    {
        if ($this->req->isMethod('POST')) {
            $cat = $this->req->get("tryOutProduct");
            $total = $this->req->get("totalProduct");
            $amount = $this->req->get("amountProduct");
            $data = $this->em->createQueryBuilder()
                ->from(TryOut::class, "a")
                ->select(["a"])
                ->where("a.id = :id")
                ->setParameter('id', $cat)
                ->getQuery()
                ->getSingleResult();
            $bank = $this->em->createQueryBuilder()
                ->from(BankAccount::class, "a")
                ->select(["a", "b"])
                ->join("a.bank", "b")
                ->getQuery()
                ->getScalarResult();
            return $this->render('siswa/tryout/pembayaran.html.twig', [
                'data' => $data,
                'number' => $total,
                'amount' => $amount,
                'digits' => $amount > 0 ? rand(100, 999) : 0,
                'ref' => $cat,
                'banks' => $bank,
                'isView' => false
            ]);
        }
        return $this->redirectToRoute("pilihTryout");
    }

    /**
     * @return Response
     * @Route("/lihat-pembayaran-tryout/{id}", name="pembayaranTryoutView")
     */
    public function pembayaranViewTryout($id)
    {
        /** @var TryOutRegistrant $reg */
        $reg = $this->em->getRepository(TryOutRegistrant::class)->findOneBy(["id" => $id]);
        /** @var Transaction $data */
        $data = $reg->getTransaction();
        if($this->req->isMethod("POST")){
            $account = [
                "accountHolder" => $this->req->get("accountHolder"),
                "accountNumber" => $this->req->get("accountNumber")
            ];
            $data->setConfirmationInfo($account);
            $this->em->flush();
            return $this->redirectToRoute("pembayaranTryoutView", ["id" => $id] );
        }
        $bank = $this->em->createQueryBuilder()
            ->from(BankAccount::class, "a")
            ->select(["a", "b"])
            ->join("a.bank", "b")
            ->getQuery()
            ->getScalarResult();
        return $this->render('siswa/tryout/pembayaran.html.twig', [
            'data' => $reg->getTryOut(),
            'registrant' => $reg,
            'number' => $reg->getRegistrantNumber(),
            'amount' => $reg->getTotalPrice(),
            'ref' => $reg->getTryOut()->getId(),
            'digits' => $reg->getTotalPrice() - ($reg->getRegistrantNumber() * $reg->getFinalPrice()),
            'banks' => $bank,
            'isView' => true
        ]);
    }

    /**
     * @return Response
     * @Route("/checkout-tryout", name="checkOutTryOut")
     */
    public function checkOutTryout()
    {
        $pricing = json_decode($this->getTotalAmountApi($this->req->get('ref'), $this->req->get('total'))->getContent());
        $pricingObj = $pricing->payload->data->ref ? $this->em->getRepository(TryOutPricing::class)->findOneBy(["id" => $pricing->payload->data->ref]) : NULL;
        try {
            /** @var TryOut $tryOut */
            $tryOut = $this->em->getRepository(TryOut::class)->findOneBy(["id" => $this->req->get('ref')]);
            $bank = $this->em->getRepository(BankAccount::class)->findOneBy(['id' => $this->req->get('paymentMethod')]);

            $newRegistrant = new TryOutRegistrant();
            $finalPrice = ($pricing->payload->data->price / $this->req->get('total'));
            $newRegistrant->setUser($this->getUser())
                ->setIsPaid(false)
                ->setBasePrice($tryOut->getPrice())
                ->setFinalPrice($finalPrice)
                ->setTotalPrice(($pricing->payload->data->price + $this->req->get('digits')))
                ->setRegistrantNumber($this->req->get('total'))
                ->setPaymentMethod($bank)
                ->setTryOut($tryOut)
                ->setTryOutPricing($pricingObj);
            $this->em->persist($newRegistrant);

            $transaction = new TransactionController($this->em);
            $transaction->setUser($this->getUser());
            $newTransaction = $transaction->newTransaction($newRegistrant, $finalPrice, "tryOutRegistrant", $bank);

            if(!$newTransaction){
                $this->em->rollback();
                throw new \Exception("Error while creating new transaction");
            }
            $this->em->flush();

//            $this->addFlash('Berhasil', "Transaksi berhasil, silahkan lakukan pembayaran");
            return $this->redirectToRoute('history-tryout');
        } catch (\Exception $e) {
            $this->addFlash('PERHATIAN!!', "Transaksi gagal, silahkan lakukan beberapa saat lagi ");
            return $this->redirect($this->generateUrl('pendaftaranTryout', ['cat' => $this->req->get('ref')]));
        }

    }


    /**
     * @return Response
     * @Route("/history-tryout", name="history-tryout")
     */
    public function historyTryout()
    {
        return $this->render('siswa/tryout/historyTryout.html.twig');
    }

    /**
     * @return JsonResponse
     * @Route("/history-tryout-api", name="history-tryout-api")
     */
    public function historyTryoutApi()
    {
        $code = 200;
        $response = [];
        try {
            $data = $this->em->getRepository(TryOutRegistrant::class)->findBy(
                ["user" => $this->getMyUser()],
                ["id" => "DESC"]
            );

            $response = [
                "msg" => "success",
                "data" => GenBasic::CustomNormalize($data, [
                    'id',
                    'registrantNumber',
                    'finalPrice',
                    'totalPrice',
                    'isPaid',
                    'promoDescription',
                    'invitedBy' => [
                        "user" => [
                            "memberProfile" => [
                                "fullName"
                            ]
                        ]
                    ],
                    'tryOut' => [
                        'id',
                        'title',
                        'quizNumber',
                        'isFree',
                        'price',
                        'rules',
                        'direction'
                    ],

                ])
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan ' . $e->getMessage()
            ];
        }
        return GenBasic::send($code, $response);
    }

//    /**
//     * @return Response
//     * @Route("/detail-tryout", name="detailPaketSoal")
//     */
//    public function detailHistory()
//    {
//        return $this->render('siswa/tryout/detailPaketSoal.html.twig');
//    }


}
<?php


namespace App\Controller\siswa\TryOut;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Categories;
use App\Entity\Level;
use App\Entity\TryOut;
use App\Entity\TryOutQuiz;
use App\Entity\TryOutRegistrant;
use App\Entity\TryOutSubject;
use App\Entity\User;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Google\Cloud\Core\Exception\ServiceException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends BaseController
{

    /**
     * TransactionController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->req = Request::createFromGlobals();
        parent::__construct($em);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/pilih-tryout", name="pilihTryout")
     */
    public function pilihTryout()
    {
        return $this->render('siswa/tryout/pilihTryOut.html.twig');
    }

    /**
     *
     * @Route("/pilih-tryout/api", name="pilihTryoutApi")
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function pilihTryoutApi()
    {
        $code = 200;
        $response = [];
        try {
            $data = $this->em->getRepository(Categories::class)->findBy(["isTryOut" => true, "isProduct" => true]);
            $response = [
                "msg" => "success",
                "data" => GenBasic::serializeToJson($data)
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan ' . $e->getMessage()
            ];
        }
        return GenBasic::send($code, $response);

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/pilih-soal-tryout/{cat}", name="pilihSoalTryout")
     */
    public function pilihSoalTryout($cat)
    {
        $data = $this->em->getRepository(Categories::class)->findOneBy(["id" => $cat]);
        return $this->render('siswa/tryout/pilihSoalTryOut.html.twig', ['ref' => $cat, "data" => $data]);
    }

    /**
     *
     * @Route("/pilih-tryout-soal/api/{cat}", name="pilihTryoutOptionsApi")
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function pilihTryoutSoalApi($cat)
    {
        $code = 200;
        $response = [];
        try {
            $category = $this->em->getRepository(Categories::class)->findOneBy(['id' => $cat]);
            $now =  new DateTime('', new DateTimeZone('Asia/Jakarta'));
            $data = $this->em->createQueryBuilder()
                ->from(Level::class, 'a')
                ->select(["a", "t", "r", "(SELECT SUM(s1.quizNumber) FROM " . TryOutSubject::class . " s1 WHERE s1.tryout = t) as number"])
//                ->addSelect("(SELECT SUM(s1.quizNumber) FROM " . TryOutSubject::class . " s1 WHERE s1.tryout = t) as number")
                ->leftJoin('a.tryOut', 't')
                ->leftJoin('t.registrant', 'r', " WITH ", "r.tryOut = t AND r.user = :user AND r.isEnded = false")
                ->where('t.category = :cat AND (t.startDay is NULL OR t.finalDay > :now) AND t.isActive = true')
                ->setParameter("cat", $cat)
                ->setParameter("now", $now)
                ->setParameter("user", $this->getMyUser())
                ->getQuery()
                ->getResult();
            $normalized = GenBasic::CustomNormalize($data, [
                'id',
                'name',
                'tryOut' => [
                    'id',
                    'title',
                    'quizNumber',
                    'isFree',
                    'price',
                    'rules',
                    'direction',
                    "registrant" => [
                        "id",
                        "isPaid"
                    ]
                ],
            ]);
            $formattedData = [];
            foreach ($normalized as $normal){
                $s = [];
                $s = $normal[0];
                $s['number'] = $normal['number'];
                $formattedData[] = $s;
            }
            $response = [
                "msg" => "success",
                "data" =>$formattedData
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan ' . $e->getMessage()
            ];
        }
        return GenBasic::send($code, $response);

    }

    /**
     * @return Response
     * @Route("/tambah-peserta-tryout/{reg}", name="tambahPesertaTryout")
     */
    public function tambahPesertaTryout($reg)
    {
        $data = $this->em->getRepository(TryOutRegistrant::class)->findOneBy(["id" => $reg, "user" => $this->getMyUser()]);
        return $this->render('siswa/tryout/tambahPesertaTryout.html.twig', ['data' => $data]);
    }

    /**
     * @return JsonResponse
     * @Route("/tambah-peserta-tryout/api/{reg}", name="tambahPesertaTryoutApi", methods="POST|HEAD")
     */
    public function tambahPesertaApi($reg)
    {
        try {
            $email = $this->req->get("email");
            /** @var TryOutRegistrant $data */
            $data = $this->em->getRepository(TryOutRegistrant::class)->findOneBy(["id" => $reg, "user" => $this->getMyUser()]);
            $newUser = $this->em->getRepository(User::class)->findOneBy(["email" => $email]);
            if ($newUser) {
                $code = 200;
                $newRegistrant = new TryOutRegistrant();
                $newRegistrant->setUser($newUser)
                    ->setIsPaid(false)
                    ->setBasePrice($data->getBasePrice())
                    ->setFinalPrice(0)
                    ->setTotalPrice(0)
                    ->setRegistrantNumber(1)
                    ->setTryOut($data->getTryOut())
                    ->setTryOutPricing($data->getTryOutPricing())
                    ->setInvitedBy($data)
                    ->setIsPaid(true)
                    ->setIsActive(false);
                $this->em->persist($newRegistrant);
                $this->em->flush();
                $serialized = GenBasic::CustomNormalize($data, [
                    "id",
                    "invitation" => [
                        'id',
                        'isActive',
                        "user" => [
                            "email"
                        ]
                    ]
                ]);
                $data = ["msg" => "Berhasil ditambahkan", "data" => $serialized['invitation']];
            } else {
                $code = 404;
                $data = ["msg" => "Email tidak ditemukan"];
            }

        } catch (\Exception $e) {
            $code = 501;
            $data = ["msg" => "Terjadi kesalahan"];
        }
        return GenBasic::send($code, $data);
    }

    /**
     * @return JsonResponse
     * @Route("/aktivasi-peserta-tryout/api/{reg}", name="aktivasiPesertaTryoutApi", methods="POST|HEAD")
     */
    public function aktivasiPesertaApi($reg)
    {
        try {
            /** @var TryOutRegistrant $data */
            $data = $this->em->getRepository(TryOutRegistrant::class)->findOneBy(["id" => $reg, "user" => $this->getMyUser()]);
            $invitation = $data->getInvitation();
            /** @var TryOutRegistrant $invite */
            foreach ($invitation as $invite) {
                $invite->setIsActive(true);
            }
            $this->em->flush();
            $code = 200;
            $data = ["msg" => "Data berhasil ditambahkan", "data" => GenBasic::CustomNormalize($data, [
                "id",
                "invitation" => [
                    'id',
                    "user" => [
                        "email"
                    ]
                ]
            ])];
        } catch (\Exception $e) {
            $code = 501;
            $data = ["msg" => "Terjadi kesalahan"];
        }
        return GenBasic::send($code, $data);
    }

    /**
     * @return JsonResponse
     * @Route("/remove-peserta-tryout/api/{reg}", name="removePesertaTryoutApi", methods="POST|HEAD")
     */
    public function removePesertaApi($reg)
    {
        try {
            /** @var TryOutRegistrant $data */
            $data = $this->em->getRepository(TryOutRegistrant::class)->findOneBy(["id" => $reg, "user" => $this->getMyUser()]);
            $invitation = $data->getInvitation();
            $cred = $this->req->get("cred");
            /** @var TryOutRegistrant $invite */
            foreach ($invitation as $invite) {
                if($invite->getId() == $cred) {
                    $this->em->remove($invite);
                }
            }

            $this->em->flush();
            $code = 200;
            $serialized = GenBasic::CustomNormalize($data, [
                "id",
                "invitation" => [
                    'id',
                    'isActive',
                    "user" => [
                        "email"
                    ]
                ]
            ]);
            $data = ["msg" => "Data berhasil ditambahkan", "data" => $serialized['invitation']];
        } catch (\Exception $e) {
            $code = 501;
            $data = ["msg" => "Terjadi kesalahan"];
        }
        return GenBasic::send($code, $data);
    }

}
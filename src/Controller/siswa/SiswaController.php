<?php

namespace App\Controller\siswa;

use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Cart;
use App\Entity\District;
use App\Entity\Member;
use App\Entity\Tentor;
use App\Entity\TentorSubject;
use App\Entity\Transaction;
use App\Repository\TentorSubjectRepository;
use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class AdminController
 * @package App\Controller\admin
 */
class SiswaController extends BaseController
{
    /** @var UserPasswordEncoderInterface */
    private $encoder;

    /**
     * TransactionController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/", name="beranda")
//     */
//    public function berandaSiswa()
//    {
//        return $this->render('siswa/landingpage.html.twig');
//    }






//    /**
//     * @return Response
//     * @Route("/landingpageKota", name="kota")
//     */
//    public function landingpageKota()
//    {
//
//        return $this->render('landingPageKota.html.twig', ['firstMeet' => '', 'vFirstMeet' => '']);
//    }
//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/pencarian-mapel", name="pencarianMapel")
//     */
    public function pencarianPelajaran()
    {

        return $this->render('siswa/pencarian/pencarianmapel.html.twig');
    }

//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/pencarian-lokasi", name="pencarianLokasi")
//     */
    public function pencarianLokasi()
    {
        return $this->render('siswa/pencarianlokasi.html.twig');
    }

    /**
     * @return Response
     * @Route("/pencarian-tanggal", name="pencarianTanggal")
     */
    public function pencarianTanggal()
    {
        return $this->render('siswa/pencarian/pencarianTanggal.html.twig');
    }

    /**
     * @return Response
     * @Route("/pkonfirmasiPembayaran", name="konfirmasiPembayaran")
     */
    public function konfirmasiPembayaran()
    {
        return $this->render('siswa/konfirmasiPembayaran.html.twig');
    }


//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/pencarian-kategori", name="pencarianKategori")
//     */

    public function pencarianKategori()
    {
        return $this->render('siswa/pencarian/pencarianKategori.html.twig');
    }

    /**
     * @return Response
     * @Route("/tambah-penghasilan", name="tambahPenghasilan")
     */
    public function tambahPenghasilan()
    {
        return $this->render('siswa/tambahPenghasilan.html.twig');
    }

    /**
     * @return Response
     * @Route("/hasil-pencarian", name="hasilPencarian")
     */
    public function hasilPencarian()
    {
        return $this->render('siswa/pencarian/hasilPencarian.html.twig');
    }


    /**
     * @return Response
     * @Route("/detail-guru", name="detailGuru")
     */
    public function detailGuru()
    {
        return $this->render('siswa/detailGuru.html.twig');
    }

//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/pesan-step-pertama", name="pesanStepPertama")
//     */
    public function pesanStep1()
    {
        return $this->render('siswa/pesan/pesanstepsatu.html.twig');
    }

//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/pesan-step-kedua", name="pesanStepKedua")
//     */
    public function pesanStep2()
    {
        return $this->render('siswa/pesan/pesanstepdua.html.twig');
    }

//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/pesan-step-ketiga", name="pesanStepKetigaa")
//     */
    public function pesanStep3()
    {
        return $this->render('siswa/pesan/pesansteptiga.html.twig');
    }

//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/selesai-pesan", name="selesaiPesan")
//     */
    public function selesaiPesan()
    {
        return $this->render('siswa/pesan/selesaiPesan.html.twig');
    }









//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/tagihan", name="tagihan")
//     */
    public function tagihan()
    {
        return $this->render('siswa/tagihan/tagihan.html.twig');
    }


//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/tagihan-detail", name="tagihanDetail")
//     */
    public function tagihanDetail()
    {
        return $this->render('siswa/tagihan/tagihanDetail.html.twig');
    }

//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/pembayaran", name="pembayaran")
//     */
    public function pembayaran()
    {
        return $this->render('siswa/tagihan/pembayaran.html.twig');
    }


    /**
     * @return Response
     * @Route("/ganti-paket", name="gantiPaket")
     */
    public function gantiPaket()
    {
        return $this->render('siswa/tagihan/gantiPaket.html.twig');
    }


//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/pesanan", name="pesanan")
//     */
    public function pesanan()
    {
        return $this->render('siswa/pesanan/pesanan.html.twig');
    }

//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/pesanan-detail", name="pesananDetail")
//     */
    public function pesananDetail()
    {
        return $this->render('siswa/pesanan/pesananDetail.html.twig');
    }

//    /**
//     * @return Response
//     * @Route("/pembayaran-detail", name="pembayaranDetail")
//     */
//    public function pembayaranDetail()
//    {
//        return $this->render('siswa/pesanan/pembayaranDetail.html.twig');
//    }

//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/guruku", name="guruku")
//     */
    public function guruku()
    {
        return $this->render('siswa/guruku/guruku.html.twig');
    }


//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/guruku-detail", name="gurukuDetail")
//     */
    public function gurukuDetail()
    {
        return $this->render('siswa/guruku/gurukuDetail.html.twig');
    }

    /**
     * @return Response
     * @Route("/alasan", name="alasan")
     */
    public function alasan()
    {
        return $this->render('siswa/masalah/alasan.html.twig');
    }


    /**
     * @return Response
     * @Route("/refund", name="refund")
     */
    public function refund()
    {
        return $this->render('siswa/masalah/refund.html.twig');
    }

    /**
     * @return Response
     * @Route("/laporkan", name="laporkan")
     */
    public function laporkan()
    {
        return $this->render('siswa/masalah/laporkan.html.twig');
    }

    /**
     * @return Response
     * @Route("/laporkan-detail", name="laporkanDetail")
     */
    public function laporkanDetail()
    {
        return $this->render('siswa/masalah/laporkanDetail.html.twig');
    }



//    TRY OUT

    /**
     * @return Response
     * @Route("/try-out", name="tryOut")
     */
    public function tryOutLandingPage()
    {
        return $this->render('siswa/tryout/landingpageTryout.html.twig');
    }



    //    TEMAN KONSUL

    /**
     * @return Response
     * @Route("/teman-konsul", name="temanKonsul")
     */
    public function temanKonsul()
    {
        return $this->render('siswa/temanKonsul/landingpageTemanKonsul.html.twig');
    }


//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/beranda")
//     */
    public function beranda()
    {
        return $this->render('siswa/beranda/beranda.html.twig');
    }


//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/pencarian-beranda", name="pencarian-beranda")
//     */
    public function pencarian()
    {
        return $this->render('siswa/beranda/pencarian.html.twig');
    }

    /**
     * @return Response
     * @Route("/daftar", name="daftar")
     */
    public function daftar()
    {
        return $this->render('siswa/auth/daftar.html.twig');
    }

    /**
     * @return Response
     * @Route("/lengkapidata1", name="lengkapidata1")
     */
    public function lengkapidata1()
    {
        return $this->render('siswa/auth/lengkapidata1.html.twig');
    }

    /**
     * @return Response
     * @Route("/lengkapidata2", name="lengkapidata2")
     */
    public function lengkapidata2()
    {
        return $this->render('siswa/auth/lengkapidata2.html.twig');
    }

    /**
     * @return Response
     * @Route("/lengkapidata3", name="lengkapidata3")
     */
    public function lengkapidata3()
    {
        $profile = $this->em->getRepository(Member::class)->findOneBy(['user' => $this->getUser()]);
        return $this->render('siswa/auth/lengkapidata3.html.twig', ['profile' => $profile]);
    }

//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/login", name="login")
//     */
//    public function login()
//    {
//        return $this->render('siswa/auth/login.html.twig');
//    }


    /**
     * @return Response
     * @Route("/promo", name="promo")
     */
    public function promo()
    {
        return $this->render('siswa/promo/promo.html.twig');
    }

    /**
     * @return Response
     * @Route("/rapor", name="rapor")
     */
    public function rapor()
    {
        return $this->render('siswa/rapor/rapor.html.twig');
    }

    /**
     * @return Response
     * @Route("/rapor-detail", name="raporDetail")
     */
    public function raporDetail()
    {
        return $this->render('siswa/rapor/raporDetail.html.twig');
    }

    /**
     * @return Response
     * @Route("/nilai-detail/{tryout}", name="nilaiDetail")
     */
    public function nilaiDetail($tryout)
    {
        return $this->render('siswa/rapor/nilaiDetail.html.twig', ['ref' => $tryout]);
    }

    /**
     * @return Response
     * @Route("/peringkat-tryout/{tryout}", name="peringkatTryout")
     */
    public function peringkatTryOut($tryout)
    {
        return $this->render('siswa/rapor/peringkatTryout.html.twig', ['ref' => $tryout]);
    }

    /**
     * @return Response
     * @Route("/pilih-tryout", name="pilihTryout")
     */
    public function pilihTryout()
    {
        return $this->render('siswa/tryout/pilihTryOut.html.twig');
    }

    /**
     * @return Response
     * @Route("/pilih-soal-tryout", name="pilihSoalTryout")
     */
    public function pilihSoalTryout()
    {
        return $this->render('siswa/tryout/pilihSoalTryOut.html.twig');
    }




    /**
     * @return Response
     * @Route("/paket-sbmptn", name="paketsmbptn")
     */
    public function peringkatTryOut1()
    {
        return $this->render('siswa/rapor/peringkatTryout.html.twig');
    }


    /**
     * @return Response
     * @Route("/mengerjakan-soal", name="mengerjakanSoal")
     */
    public function mengerjakanSoal()
    {
        return $this->render('siswa/tryout/mengerjakanSoal.html.twig');
    }


    /**
     * @return Response
     * @Route("/quiz", name="quiz")
     */
    public function quiz()
    {

        $this->denyAccessUnlessGranted('ROLE_MEMBER');

        $profile = $this->em->getRepository(Member::class)->findOneBy(['user' => $this->getUser()]);
        return $this->render('siswa/quiz/quiz.html.twig', ['profile' => $profile]);
    }

//QRCODE


//    AKUN SAYA
//    /**
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/akun-saya", name="akunSaya")
//     */
    public function akunSaya()
    {
        return $this->render('siswa/akunSaya/akunSaya.html.twig');
    }

//    /**
//     * @return Response
//     * @Route("/akun-pengaturan", name="pengaturanAkun")
//     */
//    public function pengaturanAkun()
//    {
//
//    }

    /**
     * @return Response
     * @Route("/akun-profil", name="akunProfil")
     */
    public function akunProfil()
    {

    }



    /**
     * @return Response
     * @Route("/akun-nohp", name="akunNoHp")
     */
    public function akun()
    {
        return $this->render('siswa/akunSaya/akun.html.twig');
    }

    /**
     * @return Response
     * @Route("/akun-ubahnohp", name="akunUbahNoHp")
     */
    public function AkunUbahNoHp()
    {
        return $this->render('siswa/akunSaya/akunUbahNoHp.html.twig');
    }

//    ULASAN

//    /**
//     * @return Response
//     * @Route("/beri-ulasan", name="beriUlasan")
//     */
//    public function beriUlasan()
//    {
//        return $this->render('siswa/ulasan/beriUlasan.html.twig');
//    }
}

<?php
declare(strict_types=1);

namespace App\Controller;

use App\Common\GenBasic;
use App\Common\GenFirebase;
use App\Entity\Admin;
use App\Entity\FacilityCategory;
use App\Entity\Mitra;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

/**
 * Class BaseController
 * @package App\Controller
 */
class BaseController extends AbstractController
{

    protected $em;

    protected $methodInput = [];

    protected $data = [];

    protected $fields = [];

    protected $secondFieldstoAdd = [];

    public $tableFields = [];

    public $tableActions = [];

    public $tableActions2 = [];

    public $requiredWhere;

    public $joint;

    public $groupBy;

    public $addSelect;

    public $addSelectCard;

    public $requiredWhereParam;

    public $controllerName;

    public $singleNamespace;

    public $class;

    public $secondClassToAdd;

    public $fieldReferenceOnUpdate;

    /** @var Request */
    protected $req;

    private $targetEntity;

    protected $redirectAction;

    protected $parameterRedirect;

    public $formatDTResult = false;

    protected $entity;

    public $notifyUpload;

    protected $user;


    /**
     * BaseController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        date_default_timezone_set("Asia/Jakarta");
    }

    public function setUser($user)
    {
        $this->user = $user;

    }

    public function getMyUser()
    {
        return $this->container ? $this->getUser() : $this->user;

    }

    /**
     * @param $template
     *
     * @return Response
     */
    public function renderTable($template)
    {
        switch ($this->singleNamespace) {
            case 'Admin':
                /** @var Admin $query */
                $this->data['adminName'] = $this->getUser()->getAdminProfile()->getFullName();
                $this->data['adminRole'] = $this->getUser()->getAdminProfile()->getPrivilege();
                break;
            default:
                $this->data['adminName'] = 'Administrator';
                $this->data['adminRole'] = 'Admin';
                break;
        }
        $this->data['class'] = $this->controllerName;
        $this->data['space'] = $this->singleNamespace;
        $this->data['tableFields'] = $this->tableFields;
        $this->data['tableAction'] = implode(',', $this->tableActions) ?? null;
        $this->data['where'] = isset($this->data['where']) ? json_encode($this->data['where']) : null;
        $this->data['groupBy'] = isset($this->data['groupBy']) ? $this->data['groupBy'] : null;
        return $this->render(
            $template,
            $this->data
        );

    }

    /**
     * @return RedirectResponse
     */
    public function post()
    {
        try {
            $fields = $this->fields;
            /** @var FacilityCategory $entity */
            $entity = new $this->class();

            foreach ($fields as $field) {
                $finder = explode(':', $field);
                $value = $this->findValue($finder, $entity);
                dump($value);
                if (is_array($value) && (isset($value['resetEntity']))) {
                    $entity = $value['class'];
                } else {
                    $setter = 'set' . ucfirst($finder[0]);

                    $entity->$setter($value);
                }

            }


            if (method_exists($entity, 'setAuthor')) {
                $entity->setAuthor($this->getUser());
            }

            if ($this->secondClassToAdd !== null) {
                $sEntity = new $this->secondClassToAdd();
                foreach ($this->secondFieldstoAdd as $field) {
                    $finder = explode(':', $field);
                    if (isset($finder[1]) && $finder[1] === 'PrevToMe') {
                        $setter = 'set' . ucfirst($finder[0]);
                        $entity->$setter($sEntity);
                    } else {
                        if (isset($finder[1]) && $finder[1] === 'relationPrev') {
                            $value = $entity;
                        } else {
                            $value = $this->findValue($finder);
                        }
                        $setter = 'set' . ucfirst($finder[0]);
                        $sEntity->$setter($value);
                    }
                }
                $this->em->persist($sEntity);
            }
            $this->em->persist($entity);

            $this->em->flush();
            $this->addFlash('success', 'Berhasil ditambahkan');

        } catch (\Exception $e) {
            dump($e);
            die();
            $this->addFlash('danger', 'Periksa data, gagal ditambahkan ' . $e->getMessage());

        }

        if ($this->parameterRedirect !== null) {
            return $this->redirectToRoute($this->redirectAction, $this->parameterRedirect);
        } else {
            return $this->redirectToRoute($this->redirectAction);
        }
    }

    /**
     * @return RedirectResponse
     */
    public function edit()
    {
        try {
            $fields = $this->fields;
            $this->entity = new $this->class();

            /** @var FacilityCategory $entity */
            if (method_exists($this->entity, 'setAuthor')) {
                $this->entity = $this->em->getRepository($this->class)->findOneBy(
                    [
                        'id' => $this->req->get('id'),
                        'author' => $this->getUser(),
                    ]
                );
            } else {
                $this->entity = $this->em->getRepository($this->class)->findOneBy(
                    [
                        'id' => $this->req->get('id'),
                    ]
                );
            }
            if ($this->entity === null) {
                $this->addFlash('danger', 'Akses ditolak');
                if ($this->parameterRedirect !== null) {
                    return $this->redirectToRoute($this->redirectAction, $this->parameterRedirect);
                } else {
                    return $this->redirectToRoute($this->redirectAction);
                }
            }


            foreach ($fields as $field) {
                $finder = explode(':', $field);
                $value = $this->findValue($finder, $this->entity);
                if (is_array($value) && (isset($value['resetEntity']))) {
                    $this->entity = $value['class'];
                } else {
                    $setter = 'set' . ucfirst($finder[0]);

                    $this->entity->$setter($value);
                }
            }

            if (method_exists($this->entity, 'setAuthor')) {
                $this->entity->setAuthor($this->getUser());
            }
            if ($this->secondClassToAdd !== null) {
                $getter = 'get' . $this->fieldReferenceOnUpdate;
                $sEntity = $this->entity->$getter();
                foreach ($this->secondFieldstoAdd as $field) {
                    $finder = explode(':', $field);

                    if (!isset($finder[1])) {
                        $value = $this->findValue($finder);
                        $setter = 'set' . ucfirst($finder[0]);
                        $sEntity->$setter($value);
                    } else {
                        $value = $this->findValue($finder);
                        $setter = 'set' . ucfirst($finder[0]);
                        $sEntity->$setter($value);
                    }
                }

            }
//            dump($sEntity);die();
            $this->em->flush();
            $this->addFlash('success', 'Berhasil dirubah');

        } catch (\Exception $e) {
            dump($e);
            die();
            $this->addFlash('danger', $e->getMessage());

        }
//        dump($this->parameterRedirect);die();
        if ($this->parameterRedirect !== null) {
            return $this->redirectToRoute($this->redirectAction, $this->parameterRedirect);
        } else {
            return $this->redirectToRoute($this->redirectAction);
        }
    }

    /**
     * @param $finder
     *
     * @param null $entity
     * @return mixed|object|string|string[]|null
     */
    private function findValue($finder, $entity = null)
    {
        if (isset($finder[1])) {
            if ($finder[1] === 'auto') {
                return str_replace(' ', '-', $this->req->get($finder[2])) . '-' . strtotime("now");
            }

            if ($finder[1] === 'relation') {
                $val = $this->req->get($finder[0]);
                return $this->em->getRepository($finder[2])->findOneBy(['id' => $val]);
            }

            if ($finder[1] === 'relationMTM') {
                $val = $this->req->get($finder[0]);
                $data = $this->em->getRepository($finder[2])->findBy(['id' => $val]);
                $remover = 'remove' . ucfirst($finder[0]);
                $setter = 'add' . ucfirst($finder[0]);
                $getter = 'get' . ucfirst($finder[0]);
                $existingData = $entity->$getter()->toArray();
                foreach ($existingData as $singleData) {
                    $this->entity->$remover($singleData);
                }
                foreach ($data as $single) {
                    $entity->$setter($single);
                }
                return ['resetEntity' => true, 'class' => $entity];
            }
            $findMethod = explode("-", $finder[1]);
            if ($findMethod[0] === 'method') {
                $method = $finder[2];
                if (!empty($findMethod[1])) {
                    /** @var BaseController $class */
                    $class = new $findMethod[1]($this->em);
//                    $class->setGlobalRequest($this->req);
                    return $class->$method($finder[3] === 'entity' ? $entity : $finder[3]);
                } else {
                    return $this->$method($finder[3] === 'entity' ? $entity : $finder[3]);
                }
            }
        }

        return $this->req->get($finder[0]);

    }

    public function setGlobalRequest($req)
    {
        $this->req = $req;
    }

    /**
     * @param $old
     * @param $new
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getCountPerncen($old, $new, $tipe = 'number')
    {
        try {
            $sqlmin = $old;
            $countmin = $this->em->getConnection()->prepare($sqlmin);
            $countmin->execute();
            $lama = $countmin->fetchColumn();
            $lama = $lama ?? 0;

            $sql = $new;
            $count = $this->em->getConnection()->prepare($sql);
            $count->execute();
            $baru = $count->fetchColumn();
            $baru = $baru ?? 0;
            $siswabaru = (int)$baru == 0 ? 1 : (int)$baru;
            $siswalama = (int)$lama == 0 ? 1 : (int)$lama;

            $persen = ($siswalama / $siswabaru) * 100;
            $status = 'down';
            if ($baru > $lama) {
                $persen = ($siswabaru / $siswalama) * 100;
                $status = 'up';
            } else if ($baru == $lama) {
                $persen = 0;
                $status = 'equal';
            }
            if ($tipe == 'decimal'){
                $baru = 'Rp. '.number_format((int)$baru, 0, ',', '.');
            }
            $data = [
                'data' => $baru ,
                'percen' => $persen,
                'status' => $status,
            ];
            $code = 200;
            $response = GenBasic::serializeToJson($data);
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data '.$e->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    public function ajaxReq()
    {
        return GenBasic::request($this->req);
    }

    /**
     * @param $tentorFcm
     * @param $message
     * @param $title
     * @param $response
     *
     * @return array|string
     */
    public function sendNotification($Fcm, $title, $message, $response){
        $notification = 'No response';
        if ($Fcm !== null) {
            $notification = GenFirebase::SendNotification($Fcm, $title, $message, $response);
        }
        return $notification;
    }

}
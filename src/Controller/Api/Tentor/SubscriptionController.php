<?php


namespace App\Controller\Api\Tentor;


use App\Common\GenBasic;
use App\Common\GenFirebase;
use App\Controller\BaseController;
use App\Entity\Cart;
use App\Entity\Transaction;
use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class SubscriptionController extends BaseController
{

    private $utility;
    /**
     * SubscriptionController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Api\Tentor\UtilityController($this->em);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/subscription/request", methods="GET|HEAD")
     */
    public function getSubscriptionRequestList()
    {
        $offset = $this->req->query->get('offset') ?? 0;
        $limit = $this->req->query->get('limit') ?? 5;
        return $this->utility->getSubscriptionRequestList($this->getUser(), $offset, $limit);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/subscription/request/detail/{id}", methods="GET|HEAD")
     */
    public function getSubscriptionRequestDetail($id)
    {
        return $this->utility->getSubscriptionRequestDetail($this->getUser(), $id);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/subscription/confirm", methods="POST|HEAD")
     */
    public function confirmSubscription()
    {
        //id = Transaction ID
        // status Accept = true, Reject = false
        $id = GenBasic::request($this->req)['id'];
        $status = GenBasic::request($this->req)['status'];
        $data = [
            'id' => $id,
            'status' => $status,
        ];
        return $this->utility->confirmSubscription($this->getUser(), $data);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/subscription/list", methods="GET|HEAD")
     */
    public function getSubscriptionList()
    {
        //status 1 = pure active, 9 = pure finish, 6 = cancel
        $offset = $this->req->query->get('offset') ?? 0;
        $limit = $this->req->query->get('limit') ?? 5;
        $status = $this->req->query->get('status') ?? 'all';
        return $this->utility->getSubscriptionList($this->getUser(), $status, $offset, $limit);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/subscription/detail/{id}", methods="GET|HEAD")
     */
    public function getSubscriptionDetail($id)
    {
        return $this->utility->getSubscriptionDetail($this->getUser(), $id);
    }
}
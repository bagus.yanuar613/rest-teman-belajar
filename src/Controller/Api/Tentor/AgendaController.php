<?php


namespace App\Controller\Api\Tentor;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Attendance;
use App\Entity\Categories;
use App\Entity\Tentor;
use App\Entity\TentorSubject;
use App\Entity\Transaction;
use App\Entity\User;
use App\Repository\AttendanceRepository;
use App\Repository\CategoriesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AgendaController extends BaseController
{

    /**
     * AgendaController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/agenda", methods="GET|HEAD")
     */
    public function getAgenda()
    {
        try {
            /** @var User $user */
            $user = $this->getUser();

            $emailVerified = $user->getIsEmailVerified();
            $data = [
                'email' => $emailVerified
            ];
            /** @var Tentor $profile */
            $profile = $user->getTentorProfile();
            $criteria = ['fullName', 'slugName', 'gender', 'about', 'experience', 'education', 'achievement', 'company'];
            foreach ($criteria as $v) {
                $getter = 'get' . ucfirst($v);
                $data['profile'] = true;
                if ($profile->$getter() === null) {
                    $data['profile'] = false;
                    break;
                }
            }
            $districts = $user->getDistrict();
            if (count($districts) > 0) {
                $data['district'] = true;
            } else {
                $data['district'] = false;
            }

            $schedule = $user->getSchedule();
            if (count($schedule) > 0) {
                $data['schedule'] = true;
            } else {
                $data['schedule'] = false;
            }

            $skill = $user->getTentorSubject();
            if (count($skill) > 0) {
                $data['skill'] = true;
            } else {
                $data['skill'] = false;
            }

            $identity = $user->getTentorIdentity();
            if ($identity) {
                $data['identity'] = true;
            } else {
                $data['identity'] = false;
            }

            /** @var TentorSubject $ts */
            $ts = $this->em->getRepository(TentorSubject::class)->findOneBy(['user' => $this->getUser()]);
            if(!$ts){
                $data['transaction'] = false;
            }else{
                $transactions = $ts->getSubscription();
                if (count($transactions) > 0) {
                    $data['transaction'] = true;
                } else {
                    $data['transaction'] = false;
                }
            }



            /** @var AttendanceRepository $qAttendance */
            $qAttendance = $this->em->getRepository(Attendance::class);
            $attendance = $qAttendance->createQueryBuilder('a')
                ->innerJoin('a.subscription', 's')
                ->innerJoin('s.tentorSubject', 'ts')
                ->innerJoin('ts.user', 'u')
                ->where('ts.user = :user')
                ->setParameter('user', $this->getUser())
                ->getQuery()->getScalarResult();
            if (count($attendance) > 0) {
                $data['attendance'] = true;
            } else {
                $data['attendance'] = false;
            }
            $presence = $qAttendance->createQueryBuilder('a')
                ->innerJoin('a.subscription', 's')
                ->innerJoin('s.tentorSubject', 'ts')
                ->innerJoin('ts.user', 'u')
                ->where('ts.user = :user')
                ->andWhere('a.isPresence = :status')
                ->setParameter('user', $this->getUser())
                ->setParameter('status', true)
                ->getQuery()->getScalarResult();
            if (count($presence) > 0) {
                $data['presence'] = true;
            } else {
                $data['presence'] = false;
            }
            $code = 200;
            $response = [
                'msg' => 'Berhasil Mengunduh Agenda',
                'data' => GenBasic::serializeToJson($data)
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data Categories ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
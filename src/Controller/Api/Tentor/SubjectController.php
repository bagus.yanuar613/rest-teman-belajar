<?php


namespace App\Controller\Api\Tentor;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Categories;
use App\Entity\CategoryToParentToLevel;
use App\Entity\CategoryToParentToMethod;
use App\Entity\Level;
use App\Entity\Method;
use App\Entity\Subject;
use App\Entity\TentorSubject;
use App\Entity\User;
use App\Repository\CategoriesRepository;
use App\Repository\CategoryToParentToLevelRepository;
use App\Repository\CategoryToParentToMethodRepository;
use App\Repository\LevelRepository;
use App\Repository\MethodRepository;
use App\Repository\TentorSubjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class SubjectController extends BaseController
{

    protected $utility;
    /**
     * SubjectController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($em);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/method", methods="GET|HEAD")
     */
    public function getMethod()
    {
        try {
            /** @var MethodRepository $method */
            $method = $this->em->getRepository(Method::class);
            $results = $method->createQueryBuilder('m')
                ->select([
                    'm.id',
                    'm.name',
                    'm.slug'
                ])->getQuery()->getScalarResult();
            $code = 200;
            $response = [
                'msg' => 'Success Load Method!',
                'data' => GenBasic::serializeToJson($results)
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Method ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/subject", methods="GET|HEAD")
     */
    public function getSubject()
    {
        try {
            /** @var User $user */
            $user = $this->getUser();
            $subject = $user->getTentorSubject();
            $code = 200;
            $response =
                [
                    'msg' => 'Success Fetch Schedule!',
                    'data' => GenBasic::CustomNormalize(
                        $subject,
                        [
                            'id',
                            'category' => ['id', 'name'],
                            'level' => ['id', 'name'],
                            'method' => [
                                'id',
                                'name'
                            ],
                            'isAvailable'
                        ])
                ];

        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch District' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize District ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/subject/create", methods="POST|HEAD")
     */
    public function createSubject()
    {
        try {
            $user = $this->getUser();
            $category = GenBasic::request($this->req)['category'];
            $level = GenBasic::request($this->req)['level'];
            $method = GenBasic::request($this->req)['method'] ?? [];
            $vCategory = $this->em->getRepository(Categories::class)->find($category);
            $vLevel = $this->em->getRepository(Level::class)->find($level);
            $isSkillExist = $this->em->getRepository(TentorSubject::class)->findOneBy(['category' => $category, 'level' => $level, 'user' => $user]);
            if (!$vCategory || !$vLevel ) {
                return GenBasic::send(202, ['msg' => 'Mata Pelajaran Atau Jenjang Tidak Di Temukan']);
            }
            if ($isSkillExist) {
                return GenBasic::send(202, ['msg' => 'Kamu Sudah Memiliki Skill Ini!']);
            }
            $slug = strtolower($vCategory->getName()) . '-' . strtolower($vLevel->getName()) . '-' . $user->getTentorProfile()->getSlugName();
            date_default_timezone_set('Asia/Jakarta');
            $skill = new TentorSubject();
            $skill->setUser($user)
                ->setCategory($vCategory)
                ->setLevel($vLevel)
                ->setIsAvailable(true)
                ->setSlug($slug);
            $vMethod = $this->em->getRepository(Method::class)->findBy(['id' => $method]);
            foreach ($vMethod as $v) {
                $skill->addMethod($v);
            }
            $this->em->persist($skill);
            $this->em->flush();
            $code = 200;
            $response = [
                'msg' => 'Berhasil Menambahkan Keahlian',
//                'data' => GenBasic::CustomNormalize($vMethod, ['id'])
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/subject/setAvailable", methods="POST|HEAD")
     */
    public function setAvailable()
    {
        try {
            $tsId = GenBasic::request($this->req)['tsId'];
            /** @var TentorSubject $subject */
            $subject = $this->em->getRepository(TentorSubject::class)->findOneBy(['id' => $tsId, 'user' => $this->getUser()]);
            if (!$subject) {
                return GenBasic::send(202, ['msg' => 'Subject Not Found!']);
            }
            $available = false;
            if (!$subject->getIsAvailable()) {
                $available = true;
            }
            $subject->setIsAvailable($available);
            $this->em->flush();
            $code = 200;
            $response = [
                'msg' => 'Success!'
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/subject/level", methods="GET|HEAD")
     */
    public function getSubjectLevels()
    {
        try {
            $id = $this->req->query->get('id');
            $level = $this->utility->getSubjectLevel($id);
            $code = 200;
            $response = [
                'msg' => 'Success Fetch Level!',
                'data' => $level
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/subject/method", methods="GET|HEAD")
     */
    public function getSubjectMethods()
    {
        try {
            $id = $this->req->query->get('id');
            /** @var CategoryToParentToMethodRepository $methodRepo */
            $methodRepo = $this->em->getRepository(CategoryToParentToMethod::class);
            $method = $methodRepo->createQueryBuilder('p')
                ->select([
                    'm.id',
                    'm.name'
                ])
                ->innerJoin('p.category', 'c')
                ->innerJoin('p.method', 'm')
                ->where('c.id = :id')
                ->setParameter('id', $id)
                ->groupBy('m.id')
                ->getQuery()->getScalarResult();
            $code = 200;
            $response = [
                'msg' => 'Success Fetch Method!',
                'data' => $method
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/subject/detail", methods="GET|HEAD")
     */
    public function getDetailSubject()
    {
        try {
            $id = GenBasic::request($this->req)['id'];
            /** @var TentorSubjectRepository $skill */
            $skill = $this->em->getRepository(TentorSubject::class)->find($id);
            if (!$skill) {
                return GenBasic::send(202, ['Skill Not Found!']);
            }
            $code = 200;
            $response = [
                'msg' => 'Success Fetch Detail Skill!',
                'data' => GenBasic::CustomNormalize($skill,
                    [
                        'id',
                        'category' => ['id', 'name'],
                        'level' => ['id', 'name'],
                        'isAvailable',
                        'method' => ['id', 'name']
                    ]
                )
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/subject/patch", methods="POST|HEAD")
     */
    public function patchMethod()
    {
        try {
            $id = GenBasic::request($this->req)['id'];
            $id_method = GenBasic::request($this->req)['method'] ?? [];
            /** @var TentorSubject $skill */
            $skill = $this->em->getRepository(TentorSubject::class)->find($id);
            $method = $this->em->getRepository(Method::class)->findBy(['id' => $id_method]);
            $oldMethod = $skill->getMethod()->toArray();
            /** @var Method $v */
            foreach ($oldMethod as $v) {
                $skill->removeMethod($v);
            }
            foreach ($method as $v) {
                $skill->addMethod($v);
            }
            $this->em->flush();
            $code = 200;
            $response = [
                'msg' => 'Success Patch Skill!'
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
<?php


namespace App\Controller\Api\Tentor;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Rating;
use App\Repository\RatingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Routing\Annotation\Route;

class ReviewController extends BaseController
{

    /**
     * ReviewController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/reviews", methods="GET|HEAD")
     */
    public function getReviews()
    {
        try {
            $limit = $this->req->query->get('limit') ?? 5;
            $offset = $this->req->query->get('offset') ?? 0;
            /** @var RatingRepository $reviewsRepo */
            $reviewsRepo = $this->em->getRepository(Rating::class);
            $reviews = $reviewsRepo->createQueryBuilder('r')
                ->where('r.tentor = :user')
                ->setParameter('user', $this->getUser())
                ->orderBy('r.id', 'DESC')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult();
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($reviews, [
                    'id',
                    'user' => [
                        'memberProfile' => [
                            'fullName'
                        ]
                    ],
                    'rating',
                    'review',
                    'isAnonymous',
                    'generatedCreatedAt'
                ])
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data Schedule ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data Schedule ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
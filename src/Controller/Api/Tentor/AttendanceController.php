<?php


namespace App\Controller\Api\Tentor;


use App\Common\GenBasic;
use App\Common\GenFirebase;
use App\Controller\BaseController;
use App\Entity\Attendance;
use App\Entity\Cart;
use App\Entity\Member;
use App\Entity\Schedule;
use App\Entity\Subscription;
use App\Entity\Tentor;
use App\Entity\TentorSaldo;
use App\Entity\Transaction;
use App\Entity\User;
use App\Repository\AttendanceRepository;
use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class AttendanceController extends BaseController
{

    private $utility;

    /**
     * AttendanceController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Api\Tentor\UtilityController($this->em);
    }


    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/attendance/create", methods="POST|HEAD")
     */
    public function createAttendance()
    {
        $id = GenBasic::request($this->req)['id'];
        $type = GenBasic::request($this->req)['type'];
        $clientId = GenBasic::request($this->req)['clientId'] ?? '';
        $data = [
            'id' => $id,
            'type' => $type,
            'clientId' => $clientId
        ];
        return $this->utility->createAttendance($this->getUser(), $data);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("api/tentor/attendance/check", methods="GET|HEAD")
     */
    public function checkAttendanceActive()
    {
        try {
            /** @var Attendance $attendance */
            $attendance = $this->utility->checkActiveAttendance($this->getUser());
            $code = 200;
            $status = false;
            $response = [
                'msg' => 'Berhasil Mengunduh Data Absensi',
                'data' => [
                    'status' => $status,
                    'attendance' => [
                        'id' => null
                    ]
                ]
            ];
            if ($attendance) {
                $status = true;
                $response = [
                    'msg' => 'Berhasil Mengunduh Data Absensi',
                    'data' => [
                        'status' => $status,
                        'attendance' => [
                            'id' => $attendance->getId(),
                            'subscriptionId' => $attendance->getSubscription()->getId(),
                            'finish' => $attendance->getGeneratedFinishAt(),
                            'start' => $attendance->getGeneratedStart()
                        ]
                    ]
                ];
            }
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Check Attendance ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/attendance/finish", methods="POST|HEAD")
     */
    public function finishAttendance()
    {
        $id = GenBasic::request($this->req)['id'];
        $type = GenBasic::request($this->req)['type'];
        $note = GenBasic::request($this->req)['note'] ?? '';
        return $this->utility->setFinishAttendance($this->getUser(), $id, $type, $note);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/attendance/scan-qr", methods="POST|HEAD")
     */
    public function validateQRCode()
    {
        $id = GenBasic::request($this->req)['id'];
        $qrCode = hex2bin(GenBasic::request($this->req)['userId']);
        return $this->utility->validateQR($this->getUser(), $id, $qrCode);
    }

//    /**
//     * @return \Symfony\Component\HttpFoundation\JsonResponse
//     * @Route("api/tentor/attendance/finish-button", methods="POST|HEAD")
//     */
//    public function finishButton()
//    {
//        try {
//            $id = GenBasic::request($this->req)['id'];
//            $userId = hex2bin(GenBasic::request($this->req)['userId']);
//            /** @var TransactionRepository $transRepo */
//            $transRepo = $this->em->getRepository(Transaction::class);
//            $transaction = $transRepo->createQueryBuilder('t')
//                ->innerJoin('t.tentorSubject', 'ts')
//                ->innerJoin('t.user', 'u')
//                ->where('ts.user = :user')
//                ->andWhere('t.status = :status')
//                ->andWhere('t.id = :id')
//                ->andWhere('u.id = :member')
//                ->setParameters(['user' => $this->getUser(), 'status' => 1, 'id' => $id, 'member' => $userId])
//                ->getQuery()->getOneOrNullResult();
//            if (!$transaction) {
//                return GenBasic::send(202, ['msg' => 'Transaction Not Found!']);
//            }
//
//            $attendance = new Attendance();
//            $attendance->setTransaction($transaction)
//                ->setIsPresence(false)
//                ->setStartAt(new \DateTime());
//            $this->em->persist($attendance);
//            $this->em->flush();
//            $code = 200;
//            $response = [
//                'msg' => 'Success create Attendance!',
//                'data' => [
//                    'id' => $attendance->getId()
//                ]
//            ];
//        } catch (\Exception $err) {
//            $code = 500;
//            $response = [
//                'msg' => 'Failed To Patch Attendance ' . $err->getMessage(),
//            ];
//        }
//        return GenBasic::send($code, $response);
//    }
//    /**
//     * @return \Symfony\Component\HttpFoundation\JsonResponse
//     * @Route("api/tentor/attendance/recall", methods="POST|HEAD")
//     */
//    public function recallAttendance()
//    {
//        try {
//            $id = GenBasic::request($this->req)['id'];
//            $clientId = GenBasic::request($this->req)['clientId'] ?? '';
//            /** @var AttendanceRepository $attendRepo */
//            $attendRepo = $this->em->getRepository(Attendance::class);
//            /** @var Attendance $attendance */
//            $attendance = $attendRepo->createQueryBuilder('a')
//                ->innerJoin('a.transaction', 't')
//                ->innerJoin('t.tentorSubject', 'ts')
//                ->where('ts.user = :user')
//                ->andWhere('a.isActive = :active')
//                ->andWhere('a.id = :id')
//                ->setParameter('user', $this->getUser())
//                ->setParameter('active', 0)
//                ->setParameter('id', $id)
//                ->getQuery()->getOneOrNullResult();
//            if (!$attendance) {
//                return GenBasic::send(202, ['msg' => 'Attendance Not Found!']);
//            }
//            $attendance->setClientId($clientId);
//            $this->em->flush();
//            $token = $attendance->getTransaction()->getUser()->getAppFcmToken();
//            $notification = null;
//            $data = [
//                'id' => $attendance->getTransaction()->getId(),
//                'type' => 'Konfirmasi',
//                'clientId' => $clientId,
//                'method' => 2
//            ];
//            $message = 'Hai, Guru Mu Menelpon Ulang Silahkan Konfirmasi.';
//            if ($token !== null) {
//                $notification = GenFirebase::SendNotification($token, 'Memulai Pelajaran (TEMAN BELAJAR)', $message, $data);
//            }
//            $code = 200;
//            $response = [
//                'msg' => 'Success Recall!',
//                'data' => [
//                    'id' => $attendance->getId()
//                ]
//            ];
//        } catch (\Exception $err) {
//            $code = 500;
//            $response = [
//                'msg' => 'Failed To Check Attendance ' . $err->getMessage(),
//            ];
//        }
//        return GenBasic::send($code, $response);
//    }
}
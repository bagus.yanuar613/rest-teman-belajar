<?php


namespace App\Controller\Api\Tentor;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Tentor;
use App\Entity\TentorSaldo;
use App\Repository\TentorSaldoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class SaldoController extends BaseController
{

    /**
     * SaldoController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/balance", methods="GET|HEAD")
     */
    public function getBalance()
    {
        try {
            /** @var Tentor $tentor */
            $tentor = $this->em->getRepository(Tentor::class)->findOneBy(['user' => $this->getUser()]);
            $code = 200;
            $response = [
              'msg' => 'Success Fetch Balance!',
              'data' => ['balance' => $tentor->getBalance(), 'point' => $tentor->getPoint()]
            ];
        }catch (\Exception $err){
            $code = 500;
            $response = [
                'msg' => 'Failed To Patch Attendance ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/balance/mutation", methods="GET|HEAD")
     */
    public function getSaldoHistory()
    {
        try {
            $status = $this->req->query->get('status') ?? 'all';
            $limit = $this->req->query->get('limit') ?? 5;
            $offset = $this->req->query->get('offset') ?? 0;
            /** @var TentorSaldoRepository $mutationRepo */
            $mutationRepo = $this->em->getRepository(TentorSaldo::class);
            $qb = $mutationRepo->createQueryBuilder('ts')
                ->where('ts.user = :user')
                ->setParameter('user', $this->getUser());
            if (!$this->req->query->has('status') || $this->req->query->get('status') !== 'all') {
                $qb->andWhere('ts.type = :status')->setParameter('status', $status);
            }
             $mutation = $qb->orderBy('ts.id', 'DESC')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult();
            $code = 200;
            $response = [
                'msg' => 'Success Fetch Balance!',
                'data' => GenBasic::CustomNormalize($mutation, [
                    'id',
                    'type',
                    'amount',
                    'description',
                    'generatedCreatedAt',
                ])
            ];
        }catch (\Exception $err){
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Mutation ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Mutation ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
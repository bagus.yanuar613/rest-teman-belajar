<?php


namespace App\Controller\Api\Tentor;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Bank;
use App\Entity\Tentor;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Intervention\Image\ImageManagerStatic as Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends BaseController
{

    /**
     * ProfileController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/profile", methods="GET|HEAD")
     */
    public function getProfile()
    {
        try {
            /** @var User $user */
            $user = $this->getUser();
            /** @var Tentor $profile */
            $profile = $user->getTentorProfile();
            $code = 200;
            $response = [
                'msg' => 'Berhasil Mengunduh Data Profil',
                'data' => GenBasic::CustomNormalize($profile,
                    [
                        'id',
                        'fullName',
                        'generatedAvatar',
                        'avatar',
                        'gender',
                        'about',
                        'experience',
                        'education',
                        'achievement',
                        'company',
                        'user' => [
                            'id',
                            'avgRatings'
                        ],
                        'grade' => [
                            'id', 'name', 'slug'
                        ]
                    ]
                )
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Profile' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Profile ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/profile/patch", methods="POST|HEAD")
     */
    public function patchProfile()
    {
        try {
            $fullName = GenBasic::request($this->req)['name'];
            $gender = GenBasic::request($this->req)['gender'];
            $about = GenBasic::request($this->req)['about'];
            $experience = GenBasic::request($this->req)['experience'];
            $education = GenBasic::request($this->req)['education'] ?? '';
            $achievement = GenBasic::request($this->req)['achievement'] ?? '';
            $company = GenBasic::request($this->req)['company'] ?? '';
            /** @var Tentor $tentor */
            $tentor = $this->em->getRepository(Tentor::class)->findOneBy(['user' => $this->getUser()]);
            $slugName = $tentor->getSlugName();
            if ($fullName !== null) {
                $slugName = str_replace(' ', '-', $fullName) . '-' . strtotime("now");
            }
            $tentor->setFullName($fullName)
                ->setSlugName($slugName)
                ->setGender($gender)
                ->setAbout($about)
                ->setExperience($experience)
                ->setEducation($education)
                ->setAchievement($achievement)
                ->setCompany($company);
            $this->em->flush();
            $code = 200;
            $response =
                [
                    'msg' => 'Berhasil Memperbarui Profil Pengguna',
                ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Patch Profile' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/profile/patch-picture", methods="POST|HEAD")
     */
    public function patchPictureProfile()
    {
        try {
            $avatar = $this->req->files->get('avatar');
            /** @var Tentor $tentor */
            $tentor = $this->em->getRepository(Tentor::class)->findOneBy(['user' => $this->getUser()]);
            if ($avatar) {
                /** @var UploadedFile $avatar */
                $avatarString = (string)Image::make($avatar->getRealPath())->encode('jpeg', '75');
                $avatarResize = Image::make($avatarString);
                $avatarResize->resize(null, 140, function ($constraint) {
                    $constraint->aspectRatio();
                });
                if (!empty($tentor->getAvatar()) && file_exists('../public/images/img-account/' . $tentor->getAvatar())) {
                    unlink('../public/images/img-account/' . $tentor->getAvatar());
                }

                $avatarName = $this->getUser()->getId() . '-' . date('YmdHis') . '.jpeg';
                $avatarResize->save('../public/images/img-account/' . $avatarName);
                $tentor->setAvatar($avatarName);
            } else {
                return GenBasic::send(202,
                    [
                        'msg' => 'Tidak Ada Gambar Yang Terlampir',
                    ]
                );
            }
            $this->em->flush();
            $code = 200;
            $response =
                [
                    'msg' => 'Berhasil Memperbarui Foto Profil Pengguna',
                ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Patch Profile' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);

    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/profile/bank", methods="GET|HEAD")
     */
    public function getBankProfile()
    {
        try {
            /** @var User $user */
            $user = $this->getUser();
            /** @var Tentor $profile */
            $profile = $user->getTentorProfile();
            $code = 200;
            $response =
                [
                    'msg' => 'Berhasil Mengunduh Data Bank',
                    'data' => GenBasic::CustomNormalize($profile,
                        [
                            'id',
                            'bank' => ['id', 'name'],
                            'rekening',
                            'holderName'
                        ]
                    )
                ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Bank Profile' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Bank Profile ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/profile/bank/patch", methods="POST|HEAD")
     */
    public function patchBankProfile()
    {
        try {
            $bankId = GenBasic::request($this->req)['bankId'];
            $rekening = GenBasic::request($this->req)['rekening'];
            $holderName = GenBasic::request($this->req)['name'];
            /** @var Tentor $tentor */
            $tentor = $this->em->getRepository(Tentor::class)->findOneBy(['user' => $this->getUser()]);
            $bank = $this->em->getRepository(Bank::class)->find($bankId);
            if (!$bank) {
                return GenBasic::send(202, ['msg' => 'Bank ID not Found!']);
            }
            $tentor->setBank($bank)
                ->setHolderName($holderName)
                ->setRekening($rekening);
            $this->em->flush();
            $code = 200;
            $response =
                [
                    'msg' => 'Berhasil Memperbarui Profil',
                ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Patch Bank Profile' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/profile/point", methods="GET|HEAD")
     */
    public function getPoint()
    {
        try {
            /** @var User $user */
            $user = $this->getUser();
            $point = $user->getTentorProfile()->getPoint();
            $code = 200;
            $response =
                [
                    'msg' => 'Berhasil Mengunduh Data Poin',
                    'data' => $point
                ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Bank Profile' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
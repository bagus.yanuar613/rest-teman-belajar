<?php


namespace App\Controller\Api\Tentor;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Attendance;
use App\Entity\Tentor;
use App\Entity\Withdraw;
use App\Repository\AttendanceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class WithdrawController extends BaseController
{

    /**
     * WithdrawController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/withdraw/send", methods="POST|HEAD")
     */
    public function sendWithdraw()
    {
        try {
            $amount = GenBasic::request($this->req)['amount'];
            /** @var Tentor $tentor */
            $tentor = $this->getUser()->getTentorProfile();
            if (!$tentor->getBank()) {
                return GenBasic::send(202, ['msg' => 'You Must Set Bank First!']);
            }
            $balance = $tentor->getBalance();
            if ($balance < $amount || $amount < 50000) {
                return GenBasic::send(202, ['msg' => 'Not Enough Balance Or Amount Must Be More Than Rp.50.000,00']);
            }

            $withdrawPending = $this->em->getRepository(Withdraw::class)->findOneBy(['user' => $this->getUser(), 'status' => 0]);
            if ($withdrawPending) {
                return GenBasic::send(202, ['msg' => 'You Have Pending Withdraw Transaction!']);

            }
            date_default_timezone_set('Asia/Jakarta');
            $withdraw = new Withdraw();
            $withdraw->setUser($this->getUser())
                ->setAmount($amount)
                ->setStatus(0);
            $this->em->persist($withdraw);
            $this->em->flush();
            $code = 200;
            $response = [
                'msg' => 'Success Send Request Withdraw!',
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Send Withdraw!',
            ];
        }
        return GenBasic::send($code, $response);
    }
}
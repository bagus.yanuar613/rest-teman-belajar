<?php


namespace App\Controller\Api\Tentor;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Tentor;
use App\Entity\TentorIdentity;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Intervention\Image\ImageManagerStatic as Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class IdentityController extends BaseController
{
    /**
     * IdentityController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/identity", methods="GET|HEAD")
     */
    public function getIdentity()
    {
        try {
            /** @var User $user */
            $user = $this->getUser();
            /** @var Tentor $profile */
            $profile = $user->getTentorIdentity();
            $code = 200;
            $response =
                [
                    'msg' => 'Berhasil Mengunduh Profil',
                    'data' => GenBasic::CustomNormalize($profile,
                        [
                            'nik',
                            'generatedCertificate',
                            'certificate',
                        ]
                    )
                ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Profile' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Profile ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/identity/patch", methods="POST|HEAD")
     */
    public function setIdentity()
    {
        try {
            $nik = $this->req->request->get('nik');
            $certificate = $this->req->files->get('certificate');

            /** @var TentorIdentity $identity */
            $identity = $this->em->getRepository(TentorIdentity::class)->findOneBy(['user' => $this->getUser()]);
            if(!$identity){
                $idt = new TentorIdentity();
                $idt->setUser($this->getUser())
                    ->setNik($nik);
                if($certificate){
                    /** @var UploadedFile $certificate */
                    $avatarString = (string)Image::make($certificate->getRealPath())->encode('jpeg', '75');
                    $avatarResize = Image::make($avatarString);
                    $avatarResize->resize(null, 140, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    $avatarName = $this->getUser()->getId() . '-' . date('YmdHis') . '.jpeg';
                    $avatarResize->save('../public/images/img-certificate/' . $avatarName);
                    $idt->setCertificate($avatarName);
                }else{
                    return GenBasic::send(202, ['msg' => 'Wajib Melampirkan Sertifikat']);
                }
                $this->em->persist($idt);
            }else{
                $identity->setNik($nik);
                if($certificate){
                    /** @var UploadedFile $certificate */
                    $avatarString = (string)Image::make($certificate->getRealPath())->encode('jpeg', '75');
                    $avatarResize = Image::make($avatarString);
                    $avatarResize->resize(null, 140, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    if(file_exists('../public/images/img-certificate/'.$identity->getCertificate())) {
                        unlink('../public/images/img-certificate/'.$identity->getCertificate());
                    }
                    $avatarName = $this->getUser()->getId() . '-' . date('YmdHis') . '.jpeg';
                    $avatarResize->save('../public/images/img-certificate/' . $avatarName);
                    $identity->setCertificate($avatarName);
                }
            }
            $this->em->flush();
            $code = 200;
            $response =
                [
                    'msg' => 'Berhasil Memperbarui Profil',
                ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Patch Identity' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
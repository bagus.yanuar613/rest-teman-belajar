<?php


namespace App\Controller\Api\Tentor;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Attendance;
use App\Repository\AttendanceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PoinController extends BaseController
{

    /**
     * PoinController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/point", methods="GET|HEAD")
     */
    public function getPoint()
    {
        try {
            /** @var AttendanceRepository $attendRepo */
            $attendRepo = $this->em->getRepository(Attendance::class);
            $point = $attendRepo->createQueryBuilder('a')
                ->select(
                    'COUNT(a.id) as point'
                )
                ->innerJoin('a.transaction', 't')
                ->innerJoin('t.tentorSubject', 'ts')
                ->innerJoin('ts.user', 'u')
                ->where('u.id = :user')
                ->andWhere('a.isPresence = :presence')
                ->andWhere('a.isActive = :active')
                ->setParameters(['user' => $this->getUser()->getId(), 'presence' => 1, 'active' => 1])
                ->getQuery()
                ->getSingleScalarResult();
            $code = 200;
            $response = [
                'msg' => 'Success To Fetch Tentor Point',
                'data' => $point
            ];
        }catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Transaction ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
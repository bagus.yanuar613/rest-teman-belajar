<?php


namespace App\Controller\Api\Tentor;


use App\Common\GenBasic;
use App\Common\GenFirebase;
use App\Controller\BaseController;
use App\Entity\Attendance;
use App\Entity\AttendancePlan;
use App\Entity\Cart;
use App\Entity\Member;
use App\Entity\Schedule;
use App\Entity\Subscription;
use App\Entity\Tentor;
use App\Entity\TentorSaldo;
use App\Entity\Transaction;
use App\Entity\User;
use App\Repository\AttendancePlanRepository;
use App\Repository\AttendanceRepository;
use App\Repository\ScheduleRepository;
use App\Repository\SubscriptionRepository;
use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class UtilityController extends BaseController
{
    /**
     * UtilityController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    public function getSubscriptionRequestList($user, $offset = 0, $limit = 5)
    {
        try {
            /** @var SubscriptionRepository $subsRepo */
            $subsRepo = $this->em->getRepository(Subscription::class);
            $subscription = $subsRepo->createQueryBuilder('s')
                ->innerJoin('s.tentorSubject', 'ts')
                ->where('ts.user = :user')
                ->andWhere('s.status = :status')
                ->setParameter('status', 0)
                ->setParameter('user', $user)
                ->orderBy('s.id', 'DESC')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()->getResult();
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($subscription, [
                    'id',
                    'subscriptionCode',
                    'tentorSubject' => [
                        'category' => ['name'],
                        'level' => ['name']
                    ],
                    'user' => [
                        'memberProfile' => ['fullName', 'generatedAvatar', 'avatar']
                    ],
                    'status',
                    'confirmedAt'
                ])
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Subscription List ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Subscription List ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    public function getSubscriptionRequestDetail($user, $id)
    {
        try {
            /** @var SubscriptionRepository $subsRepo */
            $subsRepo = $this->em->getRepository(Subscription::class);
            $subscription = $subsRepo->createQueryBuilder('s')
                ->innerJoin('s.tentorSubject', 'ts')
                ->where('ts.user = :user')
                ->andWhere('s.status = :status')
                ->andWhere('s.id = :id')
                ->setParameter('status', 0)
                ->setParameter('user', $user)
                ->setParameter('id', $id)
                ->getQuery()->getOneOrNullResult();
            if (!$subscription) {
                return GenBasic::send(202, ['msg' => 'Subscription Not Found!']);
            }
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($subscription, [
                    'id',
                    'subscriptionCode',
                    'tentorSubject' => [
                        'category' => ['name'],
                        'level' => ['name']
                    ],
                    'user' => [
                        'memberProfile' => [
                            'fullName',
                            'generatedAvatar',
                            'avatar',
                            'class' => ['name', 'memberClassParent' => ['name']]
                        ]
                    ],
                    'requestCart' => [
                        'method' => [
                            'id', 'name'
                        ],
                        'district' => [
                            'id',
                            'districtName',
                            'city' => [
                                'id',
                                'cityName'
                            ],
                            'subCity' => [
                                'id', 'name'
                            ]
                        ],
                        'duration',
                        'encounter',
                        'attendees',
                        'firstMeet',
                        'address',
                        'note',
                        'estimatedIncome',
                        'createdAt'
                    ],
                    'status',
                    'confirmedAt'
                ])
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Subscription List ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Subscription List ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    public function getSubscriptionList($user, $status, $offset = 0, $limit = 5)
    {
        try {
            /** @var SubscriptionRepository $subsRepo */
            $subsRepo = $this->em->getRepository(Subscription::class);
            $subscription = $subsRepo->createQueryBuilder('s')
                ->innerJoin('s.tentorSubject', 'ts')
                ->where('ts.user = :user')
                ->andWhere('s.confirmedAt is not null')
                ->andWhere('s.status != :except')
                ->setParameter('except', 0)
                ->setParameter('user', $user);
            if ($status === 1 || $status === '1') {
                $subscription->andWhere('s.status = :status')->setParameter('status', 1);
            } elseif ($status === 9 || $status === '9') {
                $subscription->andWhere('s.status = :status')->setParameter('status', 9);
            } elseif ($status === 6 || $status === '6') {
                $subscription->andWhere('s.status = :status')->setParameter('status', 6);
            }
            $results = $subscription
                ->orderBy('s.id', 'DESC')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()->getResult();
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($results, [
                    'id',
                    'tentorSubject' => [
                        'category' => ['name'],
                        'level' => ['name']
                    ], 'user' => [
                        'memberProfile' => ['fullName', 'generatedAvatar', 'avatar']
                    ],
                    'status',
                    'confirmedAt',
                    'subscriptionCode'
                ])
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Subscription List ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Subscription List ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    public function getSubscriptionDetail($user, $id)
    {
        try {
            /** @var SubscriptionRepository $subsRepo */
            $subsRepo = $this->em->getRepository(Subscription::class);
            $subscription = $subsRepo->createQueryBuilder('s')
                ->innerJoin('s.tentorSubject', 'ts')
                ->where('ts.user = :user')
                ->andWhere('s.id = :id')
                ->setParameter('user', $user)
                ->setParameter('id', $id)
                ->getQuery()->getOneOrNullResult();
            if (!$subscription) {
                return GenBasic::send(202, ['msg' => 'Subscription Not Found!']);
            }
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($subscription, [
                    'id',
                    'remains',
                    'tentorSubject' => [
                        'category' => ['name'],
                        'level' => ['name']
                    ], 'user' => [
                        'id',
                        'memberProfile' => ['fullName', 'generatedAvatar', 'avatar','class' => ['name', 'memberClassParent' => ['name']]]
                    ],
                    'cartActive' => [
                        'method' => [
                            'id', 'name'
                        ],
                        'district' => [
                            'id',
                            'districtName',
                            'city' => [
                                'id',
                                'cityName'
                            ],
                            'subCity' => [
                                'id', 'name', 'province' => ['id', 'provinceName']
                            ]
                        ],
                        'duration',
                        'encounter',
                        'attendees',
                        'firstMeet',
                        'address',
                        'note',
                        'createdAt'
                    ],
                    'status',
                    'confirmedAt',
                    'subscriptionCode'
                ])
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Subscription List ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Subscription List ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    public function confirmSubscription($user, $data = [])
    {
        try {
            $id = $data['id'];
            $status = $data['status'];
            /** @var SubscriptionRepository $subsRepo */
            $subsRepo = $this->em->getRepository(Subscription::class);
            /** @var Subscription $subscription */
            $subscription = $subsRepo->createQueryBuilder('s')
                ->innerJoin('s.tentorSubject', 'ts')
                ->where('s.id = :id')
                ->andWhere('ts.user = :user')
                ->andWhere('s.confirmedAt is null')
                ->andWhere('s.status = :status')
                ->setParameter('id', $id)
                ->setParameter('user', $user)
                ->setParameter('status', 0)
                ->getQuery()->getOneOrNullResult();
            if (!$subscription) {
                return GenBasic::send(202, ['msg' => 'Subscription Not Found']);
            }
            date_default_timezone_set('Asia/Jakarta');
            /** @var Cart $cart */
            $cart = $subscription->getCart()[0];
            $transStat = 6;
            $expiredPay = null;
            // request Accepted
            if ($status) {
                $transStat = 0;
                $interval = 2;
                $expiredPay = new \DateTime();
                $expiredPay->modify('+' . $interval . ' day');
            }
            $subscription->setStatus($transStat)->setConfirmedAt(new \DateTime());
            $cart->setExpiredPay($expiredPay);
            $this->em->flush();
            $memberFcm = $subscription->getUser()->getAppFcmToken();
            $type = $cart->getMethod()->getId() === 2 ? 'Online' : 'Tatap Muka';
            $category = $subscription->getTentorSubject()->getCategory()->getName();
            $level = $subscription->getTentorSubject()->getLevel()->getName();
            $tentor = $subscription->getTentorSubject()->getUser()->getTentorProfile()->getFullName();
            $notification = null;
            $data = [
                'type' => 'Pesanan',
            ];
            $message = 'Hai, Pesanan Les ' . $type . ' ' . $category . ' - ' . $level . ' Dengan Kak ' . $tentor . ' Sudah Di Konfirmasi Silahkan Cek Transaksi Kamu.';
            if ($memberFcm !== null) {
                $notification = GenFirebase::SendNotification($memberFcm, 'Konfirmasi Pesanan (TEMAN BELAJAR)', $message, $data);
            }
            $code = 200;
            $response = [
                'msg' => 'Success Confirm Subscription!',
                'notification' => $notification
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Confirm Subscription' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @param $user
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function checkActiveAttendance($user)
    {
        /** @var AttendanceRepository $attendRepo */
        $attendRepo = $this->em->getRepository(Attendance::class);
        /** @var Attendance $attendance */
        return $attendRepo->createQueryBuilder('a')
            ->innerJoin('a.subscription', 's')
            ->innerJoin('s.tentorSubject', 'ts')
            ->where('ts.user = :user')
            ->andWhere('a.isActive = :status')
            ->setParameters(['user' => $user, 'status' => 0])
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function createAttendance($user, $data)
    {
        try {
            $id = $data['id'];
            $type = $data['type'];
            $clientId = $data['clientId'];
            /** @var SubscriptionRepository $subsRepo */
            $subsRepo = $this->em->getRepository(Subscription::class);
            /** @var Subscription $subscription */
            $subscription = $subsRepo->createQueryBuilder('s')
                ->innerJoin('s.tentorSubject', 'ts')
                ->where('ts.user = :user')
                ->andWhere('s.status = :status')
                ->andWhere('s.id = :id')
                ->setParameters(['user' => $user, 'status' => 1, 'id' => $id])
                ->getQuery()->getOneOrNullResult();
            if (!$subscription) {
                return GenBasic::send(202, ['msg' => 'Data Berlangganan Tidak Tersedia']);
            }

            //Check If Any Attendance still active
            $attendance = $this->checkActiveAttendance($user);
            if ($attendance) {
                return GenBasic::send(202, [
                    'msg' => 'Gagal Membuat Absensi, Kamu Masih Mempunyai Absensi Aktif',
                    'data' => GenBasic::CustomNormalize($attendance, [
                        'id',
                        'subscription' => [
                            'user' => [
                                'memberProfile' => ['fullName', 'generatedAvatar']
                            ],
                            'tentorSubject' => [
                                'category' => ['name'],
                                'level' => ['name'],
                            ]
                        ]
                    ])
                ]);
            }
            $currentRemains = $subscription->getRemains();
            if ($currentRemains <= 0) {
                return GenBasic::send(202, ['msg' => 'Jumlah Pertemuan Tidak Cukup']);
            }
            /** @var Cart $cartActive */
            $cartActive = $subscription->getCartActive();
            if (!$cartActive) {
                return GenBasic::send(202, ['msg' => 'No Cart Active!']);
            }

            if ($cartActive->getRestAttendance() === 1) {
                $cartActive->setIsFinish(true);
            }

            $subscription->setRemains($currentRemains - 1);
            date_default_timezone_set('Asia/Jakarta');
            $duration = $cartActive->getDuration();
            $finishAt = new \DateTime();
            $finishAt->modify('+' . $duration . ' minutes');
            $attendance = new Attendance();
            $attendance->setSubscription($subscription)
                ->setIsPresence(0)
                ->setIsActive(0)
                ->setCart($cartActive)
                ->setClientId($clientId)
                ->setPoint(0)
                ->setFinishAt($finishAt)
                ->setStartAt(new \DateTime());
            $this->em->persist($attendance);
            $this->em->flush();
            $memberFcm = $subscription->getUser()->getAppFcmToken();
            $notification = null;
            $data = [
                'id' => $subscription->getId(),
                'type' => 'Konfirmasi',
                'clientId' => $clientId,
                'method' => $type
            ];
            //type 2 online, 1 Offline
            $message = $type === 2 ? 'Hai, Pelajaran Online Mu Akan Di Mulai. Silahkan Melakukan Konfirmasi Untuk Melakukan Video Call' : 'Hai, Pelajaran Tatap Muka Mu Akan Di Mulai.';
            if ($memberFcm !== null) {
                $notification = GenFirebase::SendNotification($memberFcm, 'Memulai Pelajaran (TEMAN BELAJAR)', $message, $data);
            }
            $code = 200;
            $response = [
                'msg' => 'Berhasil Membuat Data Absensi',
                'data' => [
                    'id' => $attendance->getId()
                ]
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To create Attendance ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To create Attendance ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @param $user
     * @param $id
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getAttendanceById($user, $id)
    {
        /** @var AttendanceRepository $attendRepo */
        $attendRepo = $this->em->getRepository(Attendance::class);
        /** @var Attendance $attendance */
        return $attendRepo->createQueryBuilder('a')
            ->innerJoin('a.subscription', 't')
            ->innerJoin('t.user', 'm')
            ->innerJoin('t.tentorSubject', 'ts')
            ->where('ts.user = :user')
            ->andWhere('a.id = :id')
            ->setParameters(['user' => $user, 'id' => $id])
            ->getQuery()->getOneOrNullResult();
    }

    public function setFinishAttendance($user, $id, $type, $note)
    {
        try {
            /** @var Attendance $attendance */
            $attendance = $this->getAttendanceById($user, $id);
            if (!$attendance) {
                return GenBasic::send(202, ['msg' => 'Attendance Not Found!']);
            }
            date_default_timezone_set('Asia/Jakarta');
            /** @var Cart $cart */
            $cart = $attendance->getCart();
            /** @var Subscription $subscription */
            $subscription = $attendance->getSubscription();
            //1 By Qr, 2 By Button
            $statusPresence = 0;
            if ($type === '1' || $type === 1) {
                $statusPresence = 1;
                $attendance->setPoint(1);
                $this->createReward($subscription->getUser(), $user, $cart, $subscription);
            }
            $attendance->setIsPresence($statusPresence)
                ->setIsActive(1)
                ->setFinishAt(new \DateTime())
                ->setNote($note);
            // Auto Renewal Subscription
            if ($subscription->getRemains() === 1) {
                $this->createAutoRenewal($user, $cart);
            }

            //If Attendance Remains = 0 set subscription to finish
            if ($subscription->getRemains() === 0) {
                $subscription->setStatus(9);
                $plan = $subscription->getAttendancePlan();
                /** @var AttendancePlan $v */
                foreach ($plan as $v) {
                    $v->setStatus(0);
                }
            }
            $this->em->flush();
            $code = 200;
            $response = [
                'msg' => 'Berhasil Memperbarui Data Absensi',
                'data' => [
                    'start' => $attendance->getStartAt()->format('Y-m-d H:i:s'),
                    'finish' => $attendance->getFinishAt()->format('Y-m-d H:i:s'),
                    'note' => $attendance->getNote(),
                    'rest' => $subscription->getRemains(),
                    'parentPhone' => $attendance->getSubscription()->getUser()->getMemberProfile()->getPhone(),
                    'startGenerated' => $attendance->getGeneratedStart(),
                    'finishGenerated' => $attendance->getGeneratedFinish(),
                ]
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Finish Attendance ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    public function createAutoRenewal($user, $cart)
    {
        /** @var Cart $newCart */
        $newCart = clone $cart;
        $code = 'TP';
        switch ($newCart->getMethod()->getId()) {
            case 1:
                $code = 'TP01';
                break;
            case 2:
                $code = 'TP02';
                break;
            case 3:
                $code = 'TK';
                break;
            default:
                break;
        }
        date_default_timezone_set('Asia/Jakarta');
        $now = new \DateTime();
        $expiredPay = $now->modify('+2 day');
        $newCart->setId(null)
            ->setTransaction(null)
            ->setIsActive(0)
            ->setExpiredPay($expiredPay)
            ->setCreatedAt(new \DateTime())
            ->setUpdatedAt(new \DateTime())
            ->setReferenceId($code . '/' . $user->getId() . '/' . date('YmdHis'));
        $this->em->persist($newCart);
    }

    public function createReward(User $member, User $tentor, Cart $cart, Subscription $subscription)
    {
        $category = $subscription->getTentorSubject()->getCategory()->getName();
        $level = $subscription->getTentorSubject()->getLevel()->getName();
        $method = $cart->getMethod()->getName();
        /** @var Tentor $tentorProfile */
        $tentorProfile = $tentor->getTentorProfile();
        /** @var Member $memberProfile */
        $memberProfile = $member->getMemberProfile();
        $currentBalance = $tentorProfile->getBalance();
        $currentPoint = $tentorProfile->getPoint();
        $medal = $memberProfile->getMedal() + 1;
        $newSaldo = $currentBalance + ((0.85 * $cart->getAmount()) * $cart->getAttendees());
        $newPoint = $currentPoint + 1;
        $balance = new TentorSaldo();
        $balance->setUser($tentor)
            ->setType(0)
            ->setAmount((0.85 * $cart->getAmount()) * $cart->getAttendees())
            ->setDescription($method . ' ' . $category . ' ' . $level);
        $tentorProfile->setBalance($newSaldo)->setPoint($newPoint);
        $memberProfile->setMedal($medal);
        $this->em->persist($balance);
    }

    public function validateQR($user, $id, $qrCode)
    {
        try {

            /** @var AttendanceRepository $attendRepo */
            $attendRepo = $this->em->getRepository(Attendance::class);
            /** @var Attendance $attendance */
            $attendance = $attendRepo->createQueryBuilder('a')
                ->innerJoin('a.subscription', 't')
                ->innerJoin('t.user', 'm')
                ->innerJoin('t.tentorSubject', 'ts')
                ->where('ts.user = :user')
                ->andWhere('a.id = :id')
                ->andWhere('m.id = :member')
                ->setParameters(['user' => $user, 'id' => $id, 'member' => $qrCode])
                ->getQuery()->getOneOrNullResult();
            if (!$attendance) {
                return GenBasic::send(202, ['msg' => 'Failed Qr Not Match!']);
            }

            $code = 200;
            $response = [
                'msg' => 'QR Code Cocok',
                'data' => ['status' => true]
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed While Matching Qr Code ' . $err
            ];
        }
        return GenBasic::send($code, $response);
    }

    public function getAllSchedule()
    {
        try {
            /** @var ScheduleRepository $schedule */
            $schedule = $this->em->getRepository(Schedule::class);
            $results = $schedule->createQueryBuilder('s')
                ->select([
                    's.id',
                    's.day',
                    's.time',
                    '\'0\' as flag'
                ])
                ->getQuery()
                ->getScalarResult();
            $normalize = GenBasic::serializeToJson($results);
            $newArray = [];
            foreach ($normalize as $key => $item) {
                $newArray[$item['day']][] = $item;
            }
            $code = 200;
            $response = [
                'msg' => 'Success Fetch Schedule!',
                'data' => $newArray
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch All Schedule ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    public function getMySchedule($user)
    {
        try {
            $schedule = $user->getSchedule();
            $code = 200;
            $response = [
                'msg' => 'Success Fetch Schedule!',
                'data' => GenBasic::CustomNormalize($schedule, [
                    'id',
                    'day'
                ])
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch My Schedule ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch My Schedule ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    public function createMySchedule($user, $s_id)
    {
        try {
            $schedule = $this->em->getRepository(Schedule::class)->findBy(['id' => $s_id]);
            if (!$schedule) {
                return GenBasic::send(202, 'Schedule Not Found!');
            }
            /** @var Schedule $item */
            foreach ($schedule as $item) {
                $user->addSchedule($item);
            }
            $this->em->flush();
            $code = 200;
            $response = [
                'msg' => 'Success create Schedule!',
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To create Schedule ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    public function patchMySchedule($user, $s_id)
    {
        try {
            $schedule = $this->em->getRepository(Schedule::class)->findBy(['id' => $s_id]);
            if (!$schedule) {
                return GenBasic::send(202, 'Schedule Not Found!');
            }
            $oldSchedule = $user->getSchedule()->toArray();
            foreach ($oldSchedule as $v) {
                $user->removeSchedule($v);
            }
            /** @var Schedule $item */
            foreach ($schedule as $item) {
                $user->addSchedule($item);
            }
            $this->em->flush();
            $code = 200;
            $response = [
                'msg' => 'Success patch Schedule!',
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To patch My Schedule ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    public function getSubscriptionSchedule($user, $id)
    {
        try {
            /** @var AttendancePlanRepository $planRepo */
            $planRepo = $this->em->getRepository(AttendancePlan::class);
            /** @var AttendancePlan $plan */
            $plan = $planRepo->createQueryBuilder('p')
                ->innerJoin('p.subscription', 's')
                ->innerJoin('s.tentorSubject', 'ts')
                ->where('ts.user = :user')
                ->andWhere('s.id = :id ')
                ->setParameters(['user' => $user, 'id' => $id])
                ->getQuery()->getResult();
//            $schedule = $subscription->getAttendancePlan();
            $code = 200;
            $response = [
                'msg' => 'Success Fetch Transaction Schedule',
                'data' => GenBasic::CustomNormalize($plan, [
                    'id',
                    'schedule' => [
                        'id',
                        'day'
                    ]
                ])
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Subscription Schedule ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Subscription Schedule ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    public function getExistingSubscriptionSchedule($user, $id)
    {
        try {
            /** @var AttendancePlanRepository $planRepo */
            $planRepo = $this->em->getRepository(AttendancePlan::class);
            /** @var AttendancePlan $plan */
            $plan = $planRepo->createQueryBuilder('p')
                ->innerJoin('p.subscription', 's')
                ->innerJoin('s.tentorSubject', 'ts')
                ->where('ts.user = :user')
                ->andWhere('s.id != :id ')
                ->andWhere('p.status = :status')
                ->setParameters(['user' => $user, 'status' => 1, 'id' => $id])
                ->getQuery()->getResult();
            $code = 200;
            $response = [
                'msg' => 'Success Fetch Existing Subscription Schedule',
                'data' => GenBasic::CustomNormalize($plan, [
                    'id',
                    'schedule' => ['id']
                ])
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Existing Subscription Schedule ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Existing Subscription Schedule ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    public function createSubscriptionSchedule($subscriptionId, $scheduleId)
    {
        try {
            /** @var Subscription $subscription */
            $subscription = $this->em->getRepository(Subscription::class)->find($subscriptionId);
            $schedule = $this->em->getRepository(Schedule::class)->findBy(['id' => $scheduleId]);
            if (!$subscription || !$schedule) {
                return GenBasic::send(202, ['msg' => 'Transaction OR Schedule Not Found!']);
            }
            $oldSchedule = $subscription->getAttendancePlan();
            /** @var AttendancePlan $v */
            foreach ($oldSchedule as $v) {
                $this->em->remove($v);
            }
            /** @var Schedule $item */
            foreach ($schedule as $item) {
                $plan = new AttendancePlan();
                $plan->setSubscription($subscription)->setSchedule($item)->setStatus(1);
                $this->em->persist($plan);
            }
            $this->em->flush();
            $code = 200;
            $response = [
                'msg' => 'Success Create Schedule!'
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Create Subscription Schedule ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
<?php


namespace App\Controller\Api\Tentor;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Schedule;
use App\Entity\Transaction;
use App\Entity\User;
use App\Repository\ScheduleRepository;
use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class ScheduleController extends BaseController
{

    private $utility;

    /**
     * ScheduleController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Api\Tentor\UtilityController($this->em);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/schedule", methods="GET|HEAD")
     */
    public function schedule()
    {
        return $this->utility->getAllSchedule();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/my-schedule", methods="GET|HEAD")
     */
    public function MySchedule()
    {
        return $this->utility->getMySchedule($this->getUser());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/schedule/create", methods="POST|HEAD")
     */
    public function createMySchedule()
    {
        /** @var User $user */
        $user = $this->getUser();
        $s_id = GenBasic::request($this->req)['schedule'] ?? [];
        return $this->utility->createMySchedule($user, $s_id);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/schedule/patch", methods="POST|HEAD")
     */
    public function patchMySchedule()
    {
        /** @var User $user */
        $user = $this->getUser();
        $s_id = GenBasic::request($this->req)['schedule'] ?? [];
        return $this->utility->patchMySchedule($user, $s_id);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/schedule/transaction", methods="GET|HEAD")
     */
    public function getSubscriptionSchedule()
    {
        $id = $this->req->query->get('id');
        return $this->utility->getSubscriptionSchedule($this->getUser(), $id);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/schedule/transaction-exist", methods="GET|HEAD")
     */
    public function getExistingSubscriptionSchedule()
    {
        $id = $this->req->query->get('id');
        return $this->utility->getExistingSubscriptionSchedule($this->getUser(), $id);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/schedule/transaction/submit")
     */
    public function createSubscriptionSchedule()
    {
        $scheduleId = GenBasic::request($this->req)['scheduleId'] ?? [];
        $subscriptionId = GenBasic::request($this->req)['subscriptionId'];
        return $this->utility->createSubscriptionSchedule($subscriptionId, $scheduleId);
    }
}
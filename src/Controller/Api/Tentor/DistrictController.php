<?php


namespace App\Controller\Api\Tentor;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\District;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class DistrictController
 * @package App\Controller\Api\Tentor
 */
class DistrictController extends BaseController
{
    /**
     * DistrictController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/district", methods="GET|HEAD")
     */
    public function getDistrict()
    {
        try {
            /** @var User $user */
            $user = $this->getUser();
            $district = $user->getDistrict();
            $code = 200;
            $response =
                [
                    'msg' => 'Success Fetch District List!',
                    'data' => GenBasic::CustomNormalize($district, ['id', 'districtName'])
                ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch District' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize District ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/district/create", methods="POST|HEAD")
     */
    public function createDistrict()
    {
        try {
            /** @var User $user */
            $user = $this->getUser();
            $d_id = GenBasic::request($this->req)['district'];
            $district = $this->em->getRepository(District::class)->findOneBy(['id' => $d_id]);
            if (!$district) {
                return GenBasic::send(202, ['msg' => 'Failed To Get District Data']);
            }
            $availableDistrict = $user->getDistrict();
            if (count($availableDistrict) > 0) {
                $currentCity = $availableDistrict[0]->getCity();
                $city = $district->getCity();
                if ($currentCity !== $city) {
                    return GenBasic::send(202, ['msg' => 'Failed Add District With Different City']);
                }
            }
            if (count($availableDistrict) >= 3) {
                return GenBasic::send(202, ['msg' => 'Max District = 3']);
            }
            $user->addDistrict($district);
            $this->em->flush();
            $code = 200;
            $response = [
                'msg' => 'Success Add District'
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/district/patch", methods="POST|HEAD")
     */
    public function patchDistrict()
    {
        try {
            /** @var User $user */
            $user = $this->getUser();
            $d_id = GenBasic::request($this->req)['district'] ?? [];
            $oldDistrict = $user->getDistrict()->toArray();
            foreach ($oldDistrict as $v) {
                $user->removeDistrict($v);
            }
            $district = $this->em->getRepository(District::class)->findBy(['id' => $d_id]);
            foreach ($district as $v) {
                $user->addDistrict($v);
            }
            $this->em->flush();
            $code = 200;
            $response = 'Success Patch District!';
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/district/delete", methods="POST|HEAD")
     */
    public function deleteDistrict()
    {
        try {
            /** @var User $user */
            $user = $this->getUser();
            $d_id = GenBasic::request($this->req)['district'];

            $district = $this->em->getRepository(District::class)->findOneBy(['id' => $d_id]);
            if (!$district) {
                return GenBasic::send(202, ['msg' => 'District Not Found!']);
            }
            $user->removeDistrict($district);
            $this->em->flush();
            $code = 200;
            $response = [
                'msg' => 'Success Delete District!'
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
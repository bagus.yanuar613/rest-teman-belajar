<?php


namespace App\Controller\Api\Tentor;

use App\Common\GenBasic;
use App\Common\GenFirebase;
use App\Controller\BaseController;
use App\Entity\Cart;
use App\Entity\Transaction;
use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class TransactionController
 * @package App\Controller\Api\Tentor
 */
class TransactionController extends BaseController
{

    /**
     * TransactionController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/transaction", methods="GET|HEAD")
     */
    public function getTransactions()
    {
        try {
            /** @var TransactionRepository $transaction */
            $status = $this->req->query->get('status');

            $transaction = $this->em->getRepository(Transaction::class);
            $temp = $transaction->createQueryBuilder('t')
                ->innerJoin('t.tentorSubject', 'ts')
                ->innerJoin('t.user', 'u')
                ->where('ts.user = :user')
                ->andWhere('t.status = :status')
                ->setParameter('user', $this->getUser()->getId())
                ->setParameter('status', $status)
                ->groupBy('t.subscriptionCode')
                ->orderBy('t.id', 'DESC')
                ->getQuery()
                ->getResult();

            $results = GenBasic::CustomNormalize($temp, [
                'id',
                'subscriptionCode',
                'tentorSubject' => [
                    'category' => ['name'],
                    'level' => ['name']
                ],
                'user' => [
                    'memberProfile' => ['fullName', 'generatedAvatar', 'avatar']
                ]
            ]);
            $code = 200;
            $msg = 'Success Fetch Transaction!';
            if (count($results) <= 0) {
                $msg = 'Maaf, Anda Belum Memiliki Siswa. Silahkan Lengkapi Data Terlebih Dahulu, Agar Sistem Dapat Mencarikan Anda Siswa.';
            }
            $response = [
                'msg' => $msg,
                'data' => $results
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Transaction ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Transaction ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/transaction/detail", methods="GET|HEAD")
     */
    public function getDetailTransaction()
    {
        try {
            $id = $this->req->query->get('id');
            /** @var TransactionRepository $transaction */
            $transaction = $this->em->getRepository(Transaction::class);
            $results = $transaction->createQueryBuilder('t')
                ->innerJoin('t.tentorSubject', 'ts')
                ->where('t.id = :id')
                ->andWhere('ts.user = :user')
                ->setParameter('id', $id)
                ->setParameter('user', $this->getUser())
                ->getQuery()->getOneOrNullResult();
            if (!$results) {
                return GenBasic::send(202,
                    [
                        'msg' => 'Transaction Not Found!'
                    ]
                );
            }
            $code = 200;
            $response = [
                'msg' => 'Success Fetch Detail Transaction!',
                'data' => GenBasic::CustomNormalize($results, [
                    'id',
                    'subscriptionCode',
                    'status',
                    'tentorSubject' => [
                        'category' => ['name'],
                        'level' => ['name']
                    ],
                    'lastCart' => [
                        'attendees',
                        'method' => ['id', 'name'],
                        'duration',
                        'encounter',
                        'district' => [
                            'id',
                            'districtName',
                            'city' => [
                                'id',
                                'cityName'
                            ]
                        ],
                        'address',
                        'note',
                        'generatedFirstMeet',
                        'status',
                        'generateAmount'
                    ],
                    'rest',
                    'user' => [
                        'memberProfile' => ['fullName', 'class' => ['name', 'memberClassParent' => ['name']], 'avatar']
                    ],
                    'generatedCreatedAt'
                ])
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Detail Transaction' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Detail Transaction' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }



    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/transaction/count", methods="GET|HEAD")
     */
    public function getCountTransactionActive()
    {
        try {
            /** @var TransactionRepository $transaction */
            $transaction = $this->em->getRepository(Transaction::class);
            $temp = $transaction->createQueryBuilder('t')
                ->select('COUNT(t.id) as qty')
                ->innerJoin('t.tentorSubject', 'ts')
                ->innerJoin('t.user', 'u')
                ->where('ts.user = :user')
                ->andWhere('t.status = :status')
                ->setParameter('user', $this->getUser()->getId())
                ->setParameter('status', 1)
                ->groupBy('t.subscriptionCode')
                ->orderBy('t.id', 'DESC')
                ->getQuery()
                ->getScalarResult();

            $code = 200;
            $msg = 'Success Fetch Transaction!';
            $response = [
                'msg' => $msg,
                'data' => $temp
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch District' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
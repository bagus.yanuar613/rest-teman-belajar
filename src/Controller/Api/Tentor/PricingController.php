<?php


namespace App\Controller\Api\Tentor;

use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\District;
use App\Entity\Level;
use App\Entity\Method;
use App\Entity\Pricing;
use App\Entity\Schedule;
use App\Entity\User;
use App\Repository\PricingRepository;
use App\Repository\ScheduleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class PricingController extends BaseController
{

    /**
     * PricingController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/cek-pricing", methods="GET|HEAD")
     */
    public function getTentorPricing()
    {
        try {
            /** @var User $user */
            $user = $this->getUser();
            $districts = $user->getDistrict();
            if(count($districts) <= 0 ){
                return GenBasic::send(202, 'You Have No District');
            }
            $city = $districts[0]->getCity();
            /** @var PricingRepository $pricing */
            $pricing  = $this->em->getRepository(Pricing::class);
            $results = $pricing->createQueryBuilder('p')
                ->select([
                    'p.id',
                    'c.cityName',
                    'l.name as level',
                    'g.name as grade',
                    'm.name as method',
                    'm.id as methodId',
                    'p.price'
                ])
                ->leftJoin('p.city', 'c')
                ->leftJoin('p.level', 'l')
                ->leftJoin('p.grade', 'g')
                ->innerJoin('p.method', 'm')
                ->where('c.id = :city or p.city is null')
                ->setParameter('city', $city)
                ->getQuery()->getScalarResult();
            ;
            $newArray = [];
            foreach ($results as $key => $item){
                $newArray[$item['methodId']][] = $item;
            }
            $code = 200;
            $response = [
                'msg' => 'Success Fetch All Pricing',
                'data' => $newArray
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch My Schedule '.$err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize My Schedule '.$e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
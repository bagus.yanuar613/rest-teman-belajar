<?php


namespace App\Controller\Api\Migration;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Grade;
use App\Entity\Member;
use App\Entity\Tentor;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends BaseController
{

    /**
     * AttendanceController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api-public/migration/account", methods="GET|HEAD")
     */
    public function getMainAccount()
    {
        try {
            $page = $this->req->query->get('page');
            $httpClient = HttpClient::create();
            $res = $httpClient->request('GET', 'http://localhost:8000/account?page=' . $page);
            $code = $res->getStatusCode();
            $temp = json_decode($res->getContent(), true);
            $payload = $temp['payload'];
            foreach ($payload as $key => $v) {
                $email = $v['email'];
                $password = $v['password'];
                $provider = $v['provider'];
                $role = $v['role'];
                $vProvider = null;
                $vRole = null;
                switch ($provider) {
                    case "0":
                        $vProvider = 'teman-belajar';
                        break;
                    case "1":
                        $vProvider = 'facebook';
                        break;
                    case "2":
                        $vProvider = 'google';
                        break;
                    default:
                        break;
                }
                switch ($role) {
                    case "0":
                        $vRole = ['ROLE_ADMIN'];
                        break;
                    case "1":
                        $vRole = ['ROLE_TENTOR'];
                        break;
                    case "2":
                    case "3":
                        $vRole = ['ROLE_MEMBER'];
                        break;
                    default:
                        break;
                }
                $user = new User();
                $user->setEmail($email)
                    ->setRoles($vRole)
                    ->setPassword($password)
                    ->setIsEmailVerified(true)
                    ->setProvider($vProvider);
                $this->em->persist($user);
            }
            $this->em->flush();
            $response = 'Success Insert ' . count($payload) . ' Rows';
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed send message' . $e->getMessage(),
            ];
        } catch (TransportExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed Transport ' . $e->getMessage(),
            ];
        } catch (ClientExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed ClientExceptionInterface ' . $e->getMessage(),
            ];
        } catch (RedirectionExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed RedirectionExceptionInterface ' . $e->getMessage(),
            ];
        } catch (ServerExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed ServerExceptionInterface ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api-public/migration/tentor", methods="GET|HEAD")
     */
    public function getDataTentor()
    {
        try {
            $page = $this->req->query->get('page');
            $httpClient = HttpClient::create();
            $res = $httpClient->request('GET', 'http://localhost:8000/tentor?page=' . $page);
            $code = $res->getStatusCode();
            $temp = json_decode($res->getContent(), true);
            $payload = $temp['payload'];
            foreach ($payload as $key => $v) {
                $email = $v['email'];
                $password = $v['password'];
                $source = $v['source'];
                $ava = $v['ava'];
                $phone = $v['phone'];
                $fullName = $v['tentorProfile'] === null ? 'Guru Teman Belajar' : $v['tentorProfile']['fullname'];
                $slugName = $v['tentorProfile'] === null ? 'Guru Teman Belajar' : $v['tentorProfile']['permalink'];
                $about = $v['tentorProfile'] === null ? null : $v['tentorProfile']['aboutMe'];
                $experience = $v['tentorProfile'] === null ? null : $v['tentorProfile']['experience'];
                $education = $v['tentorProfile'] === null ? null : $v['tentorProfile']['education'];
                $achievement = $v['tentorProfile'] === null ? null : $v['tentorProfile']['achievement'];
                $workAt = $v['tentorProfile'] === null ? null : $v['tentorProfile']['workAt'];
                $isVerified = $v['tentorProfile'] === null ? null : $v['tentorProfile']['isVerified'];
                $status = $v['tentorProfile'] === null ? null : $v['tentorProfile']['status'];
                $provider = null;
                switch ($source) {
                    case 0:
                        $provider = 'teman-belajar';
                        break;
                    case 1:
                        $provider = 'facebook';
                        break;
                    case 2:
                        $provider = 'google';
                        break;
                    default:
                        break;
                }
                $grade = $this->em->getRepository(Grade::class)->find(1);
                $user = new User();
                $user->setEmail($email)
                    ->setRoles(['ROLE_TENTOR'])
                    ->setPassword($password)
                    ->setIsEmailVerified(true)
                    ->setProvider($provider);
                $tentorProfile = new Tentor();
                $tentorProfile->setGrade($grade)
                    ->setFullName($fullName)
                    ->setSlugName($slugName)
                    ->setPhone($phone)
                    ->setAddress(null)
                    ->setAbout($about)
                    ->setExperience($experience)
                    ->setEducation($education)
                    ->setAchievement($achievement)
                    ->setCompany($workAt)
                    ->setAvatar($ava)
                    ->setIsActive($status)
                    ->setIsVerified($isVerified);
                $tentorProfile->setUser($user);
                $user->setTentorProfile($tentorProfile);
                $this->em->persist($user);
                $this->em->persist($tentorProfile);
            }
            $this->em->flush();
            $response = 'Success';
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed send message' . $e->getMessage(),
            ];
        } catch (TransportExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed Transport ' . $e->getMessage(),
            ];
        } catch (ClientExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed ClientExceptionInterface ' . $e->getMessage(),
            ];
        } catch (RedirectionExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed RedirectionExceptionInterface ' . $e->getMessage(),
            ];
        } catch (ServerExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed ServerExceptionInterface ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
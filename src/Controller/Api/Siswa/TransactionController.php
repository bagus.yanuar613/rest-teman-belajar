<?php


namespace App\Controller\Api\Siswa;


use App\Common\GenBasic;
use App\Common\GenFirebase;
use App\Controller\BaseController;
use App\Entity\Cart;
use App\Entity\District;
use App\Entity\Method;
use App\Entity\Pricing;
use App\Entity\TentorSubject;
use App\Entity\Transaction;
use App\Entity\User;
use App\Repository\CartRepository;
use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class TransactionController
 * @package App\Controller\Api\Member
 */
class TransactionController extends BaseController
{

    private $utility;
    /**
     * TransactionController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        date_default_timezone_set('Asia/Bangkok');
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/transaction", methods="GET|HEAD")
     */
    public function getTransaction()
    {
        try {
            $limit = $this->req->query->get('limit') ?? 5;
            $offset = $this->req->query->get('offset') ?? 0;
            $status = $this->req->query->get('status') ?? null;
            /** @var TransactionRepository $transaction */
            $transaction = $this->em->getRepository(Transaction::class);
            $qb = $transaction->createQueryBuilder('t')
                ->innerJoin('t.tentorSubject', 'ts')
                ->where('t.user = :user')
                ->setParameter('user', $this->getUser());
            if ($status !== null || $this->req->query->has('status')) {
                $qb->andWhere('t.status = :status')->setParameter('status', $status);
            }
            $results = $qb->setFirstResult($offset)
                ->setMaxResults($limit)
                ->orderBy('t.id', 'DESC')
                ->getQuery()
                ->getResult();
            $code = 200;
            $response = [
                'msg' => 'Success Fetch Transaction',
                'data' => GenBasic::CustomNormalize($results, [
                    'id',
                    'subscriptionCode',
                    'status',
                    'generatedStatus',
                    'tentorSubject' => [
                        'user' => [
                            'tentorProfile' => [
                                'fullName',
                                'avatar',
                                'generatedAvatar'
                            ]
                        ],
                        'category' => ['id', 'name'],
                        'level' => ['id', 'name'],
                    ],
                    'rest'
                ])
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Transaction List ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Transaction List ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/tentor/detail", methods="GET|HEAD")
     */
    public function getMyDetailTentor()
    {
        try {
            $transactionId = $this->req->query->get('id') ?? null;
            /** @var TransactionRepository $transaction */
            $transaction = $this->em->getRepository(Transaction::class);
            $results = $transaction->createQueryBuilder('t')
                ->innerJoin('t.tentorSubject', 'ts')
                ->where('t.user = :user')
                ->andWhere('t.id = :id')
                ->setParameter('user', $this->getUser())
                ->setParameter('id', $transactionId)
                ->getQuery()->getOneOrNullResult();
            if (!$results) {
                return GenBasic::send(202, ['msg' => 'Transaction Not Found!']);
            }
            $code = 200;
            $response = [
                'msg' => 'Success Fetch Transaction',
                'data' => GenBasic::CustomNormalize($results, [
                    'id',
                    'subscriptionCode',
                    'status',
                    'tentorSubject' => [
                        'user' => [
                            'tentorProfile' => [
                                'fullName',
                                'avatar',
                                'generatedAvatar'
                            ],
                            'avgRatings'
                        ],
                        'category' => ['id', 'name'],
                        'level' => ['id', 'name']
                    ],
                    'user' => [
                        'memberProfile' => [
                            'fullName', 'phone',
                        ]
                    ],
                    'lastCart' => [
                        'id',
                        'encounter',
                        'duration',
                        'method' => ['id', 'name'],
                        'district' => [
                            'id',
                            'districtName',
                            'city' => ['id', 'cityName']
                        ],
                        'address',
                        'status',
                        'isExpired'
                    ],
                    'rest'
                ])
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Transaction List ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Transaction List ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/transaction/create", methods="POST|HEAD")
     */
    public function sendToTransaction()
    {
            $tentorSubject = GenBasic::request($this->req)['tsId'];
            $duration = GenBasic::request($this->req)['duration'];
            $encounter = GenBasic::request($this->req)['encounter'];
            $attendees = GenBasic::request($this->req)['attendees'];
            $location = GenBasic::request($this->req)['districtId'] ?? null;
            $note = GenBasic::request($this->req)['note'];
            $method = GenBasic::request($this->req)['method'];
            $meet = GenBasic::request($this->req)['meet'];
            $address = GenBasic::request($this->req)['address'] ?? '';
            $data = [
                'tsId' => $tentorSubject,
                'duration' => $duration,
                'encounter' => $encounter,                             
                'attendees' =>$attendees,
                'location' => $location,
                'note' => $note,
                'method' => $method,
                'meet' => $meet,
                'address' => $address
            ];
            return $this->utility->sendSubscription($this->getUser(), $data);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/api/member/transaction/extend", methods="POST|HEAD")
     */
    public function extendTransaction()
    {
        try {
            $id = GenBasic::request($this->req)['id'];
            /** @var CartRepository $cart */
            $cart = $this->em->getRepository(Cart::class);
            /** @var Cart $result */
            $result = $cart->createQueryBuilder('c')
                ->innerJoin('c.transaction', 't')
                ->where('c.id = :id')
                ->andWhere('t.user = :user')
                ->setParameter('id', $id)
                ->setParameter('user', $this->getUser())
                ->getQuery()->getOneOrNullResult();
            if (!$result) {
                return GenBasic::send(202, 'Transaction Not Found');
            }

            /** @var Cart $new */
            $new = clone $result;
            $code = 'TP';
            switch ($new->getMethod()->getId()) {
                case 1:
                    $code = 'TP01';
                    break;
                case 2:
                    $code = 'TP02';
                    break;
                case 3:
                    $code = 'TK';
                    break;
                default:
                    break;
            }
            date_default_timezone_set('Asia/Jakarta');
            $newFirstMeet = $new->getFirstMeet()->modify('+' . $new->getEncounter() . ' week');
            $now = new \DateTime();
            $expiredPay = $now->modify('+2 day');
            $new->setId(null)
                ->setStatus(1)
                ->setFirstMeet($newFirstMeet)
                ->setExpiredPay($expiredPay)
                ->setCreatedAt(new \DateTime())
                ->setUpdatedAt(new \DateTime())
                ->setReferenceId($code . '/' . $this->getUser()->getId() . '/' . date('YmdHis'));
            $this->em->persist($new);
            $this->em->flush();
            $code = 200;
            $response = 'Success Extend Transaction';
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed Extend Transaction ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/transaction/cancel-extend", methods="POST|HEAD")
     */
    public function cancelExtendTransaction()
    {
        try {
            $id = GenBasic::request($this->req)['id'];
            /** @var Cart $cart */
            $cart = $this->em->getRepository(Cart::class)->find($id);
            /** @var Transaction $transaction */
            $transaction = $cart->getTransaction();
            if($transaction->getRest() <= 0){
                $transaction->setStatus(9);
            }
            $cart->setStatus(6);
            $this->em->flush();
            $code = 200;
            $response = 'Success Extend Transaction';
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/api/member/transaction/cancel", methods="POST|HEAD")
     */
    public function cancelTransaction()
    {
        try {
            $id = GenBasic::request($this->req)['id'];
            /** @var CartRepository $cart */
            $cart = $this->em->getRepository(Cart::class);
            /** @var Cart $result */
            $result = $cart->createQueryBuilder('c')
                ->innerJoin('c.transaction', 't')
                ->where('c.id = :id')
                ->andWhere('t.user = :user')
                ->setParameter('id', $id)
                ->setParameter('user', $this->getUser())
                ->getQuery()->getOneOrNullResult();
            if (!$result) {
                return GenBasic::send(202, 'Transaction Not Found');
            }
            /** @var Transaction $transaction */
            $transaction = $this->em->getRepository(Transaction::class)->find($result->getTransaction()->getId());
            if (!$transaction) {
                return GenBasic::send(202, 'Transaction Not Found');
            }
            $result->setStatus(6);
            $transaction->setStatus(6);
            $this->em->flush();
            $code = 200;
            $response = 'Success Cancel Transaction';
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Cancel Transaction ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/transaction/history", methods="GET|HEAD")
     */
    public function transactionHistory()
    {
        try {
            $status = $this->req->query->get('status') ?? 'all';
            $limit = $this->req->query->get('limit') ?? 5;
            $offset = $this->req->query->get('offset') ?? 0;
            /** @var CartRepository $transactions */
            $transactions = $this->em->getRepository(Cart::class);
            $qb = $transactions->createQueryBuilder('c')
                ->innerJoin('c.transaction', 't')
                ->where('t.user = :user')
                ->setParameter('user', $this->getUser());
            if (!$this->req->query->has('status') || $this->req->query->get('status') !== 'all') {
                $qb->andWhere('c.status = :status')->setParameter('status', $status);
            }
            $results = $qb
                ->orderBy('c.id', 'DESC')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult();
            $code = 200;
            $attribute = [
                'id',
                'status',
                'referenceId',
                'total',
                'expiredPay',
                'transaction' => [
                    'tentorSubject' => [
                        'user' => [
                            'tentorProfile' => ['fullName', 'avatar', 'generatedAvatar']
                        ],
                        'category' => [
                            'name',
                        ],
                        'level' => [
                            'name',
                        ]
                    ]
                ]
            ];
            $response = GenBasic::CustomNormalize($results, $attribute);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch History Transaction List ' . $err->getMessage()
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize History Transaction List ' . $e->getMessage()
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/transaction/history/detail/{id}", methods="GET|HEAD")
     */
    public function transactionHistoryDetail($id)
    {
        try {
            /** @var CartRepository $transactions */
            $transactions = $this->em->getRepository(Cart::class);
            $results = $transactions->createQueryBuilder('c')
                ->innerJoin('c.transaction', 't')
                ->where('t.user = :user')
                ->andWhere('c.id = :id')
                ->setParameter('user', $this->getUser())
                ->setParameter('id', $id)
                ->getQuery()->getOneOrNullResult();
            if (!$results) {
                return GenBasic::send(202, ['msg' => 'Cart Not Found!']);
            }
            $code = 200;
            $attribute = [
                'id',
                'status',
                'referenceId',
                'amount',
                'encounter',
                'duration',
                'total',
                'expiredPay',
                'transaction' => [
                    'tentorSubject' => [
                        'user' => [
                            'tentorProfile' => ['fullName', 'avatar', 'generatedAvatar']
                        ],
                        'category' => [
                            'name',
                        ],
                        'level' => [
                            'name',
                        ]
                    ],
                    'user' => [
                        'memberProfile' => [
                            'fullName', 'class' => ['name'], 'school', 'phone'
                        ]
                    ]
                ],
                'district' => [
                    'id',
                    'districtName',
                    'city' => [
                        'id',
                        'cityName'
                    ]
                ],
                'address'
            ];
            $response = GenBasic::CustomNormalize($results, $attribute);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch History Transaction List ' . $err->getMessage()
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize History Transaction List ' . $e->getMessage()
            ];
        }
        return GenBasic::send($code, $response);
    }
}
<?php


namespace App\Controller\Api\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\MemberClass;
use App\Entity\MemberClassParent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class MemberClassController extends BaseController
{

    /**
     * MemberClassController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/class", methods="GET|HEAD")
     */
    public function getMemberClass()
    {
        try {
            /** @var MemberClassParent $class */
            $class = $this->em->getRepository(MemberClassParent::class)->findAll();
            $code = 200;
            $response = [
                'msg' => 'Success Fetch Member Class Parent',
                'data' => GenBasic::CustomNormalize($class, [
                    'id',
                    'name',
                    'memberClass' => [
                        'id',
                        'name',
                        'level' => [
                            'id',
                            'name'
                        ]
                    ]
                ])
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data Member Class ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Data  Member Class ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
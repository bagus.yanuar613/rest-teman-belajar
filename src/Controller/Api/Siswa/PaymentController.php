<?php


namespace App\Controller\Api\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\BankAccount;
use App\Entity\Cart;
use App\Entity\Payment;
use App\Entity\Transaction;
use App\Repository\CartRepository;
use App\Repository\PaymentRepository;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class PaymentController
 * @package App\Controller\Api\Siswa
 */
class PaymentController extends BaseController
{

    private $utility;

    /**
     * PaymentController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        date_default_timezone_set('Asia/Jakarta');
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/payment/list", methods="GET|HEAD")
     */
    public function getPaymentList()
    {
        $limit = $this->req->query->get('limit');
        $offset = $this->req->query->get('offset');
        $status = $this->req->query->get('status') ?? 'all';
        return $this->utility->getPaymentList($this->getUser(), $status, $offset, $limit);
    }

    /**
     * @param $id
     * @return Response
     * @Route("/api/member/payment/detail/{id}")
     */
    public function paymentDetail($id)
    {
        try {
            /** @var Transaction $transaction */
            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $id, 'user' => $this->getUser(), 'isPaid' => null]);
            if (!$transaction) {
                return new Response('Pembayaran Tidak Ditemukan!');
            }
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($transaction, [
                    'id',
                    'amount',
                    'bank' => [
                        'id',
                        'number',
                        'holderName',
                        'bank' => [
                            'id', 'name'
                        ]
                    ],
                    'paymentExpiration',
                    'confirmationInfo'
                ])
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Payment Detail ' . $err->getMessage()
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Payment Detail ' . $e->getMessage()
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return Response
     * @Route("/api/member/payment/submit-confirm", methods="POST|HEAD")
     */
    public function submitConfirmation()
    {
        try {
            $id = GenBasic::request($this->req)['id'];
            $accountNumber = GenBasic::request($this->req)['accountNumber'];
            $accountHolder = GenBasic::request($this->req)['accountHolder'];
            /** @var Transaction $transaction */
            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $id, 'user' => $this->getUser(), 'isPaid' => null]);
            if (!$transaction) {
                return GenBasic::send(202, ['msg' => 'Payment Not Found']);
            }
            $confirmationInfo = [
                'accountHolder' => $accountHolder,
                'accountNumber' => $accountNumber
            ];
            $transaction->setConfirmationInfo($confirmationInfo);
            $this->em->flush();
            $code = 200;
            $response = [
                'msg' => 'Success'
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed Submit Confirmation ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

//    /**
//     * @return \Symfony\Component\HttpFoundation\JsonResponse
//     * @Route("/api/member/payment/submit", methods="POST|HEAD")
//     */
//    public function submitPayment()
//    {
//        try {
//            $invoice = GenBasic::request($this->req)['invoiceId'];
//            $vendorId = GenBasic::request($this->req)['vendorId'];
//            /** @var BankAccount $vendor */
//            $vendor = $this->em->getRepository(BankAccount::class)->find($vendorId);
//            /** @var CartRepository $transactions */
//            $transactions = $this->em->getRepository(Cart::class);
//            $qb = $transactions->createQueryBuilder('c')
//                ->innerJoin('c.transaction', 't');
//            $qb->where('c.status = :status')
//                ->andWhere('t.user = :user')
//                ->setParameter('status', 1)
//                ->setParameter('user', $this->getUser());
//            $orStatements = $qb->expr()->orX();
//            foreach ($invoice as $v) {
//                $orStatements->add(
//                    $qb->expr()->eq('c.id', $v)
//                );
//            }
//            $qb->andWhere($orStatements);
//            $results = $qb
//                ->orderBy('c.createdAt', 'DESC')
//                ->getQuery()
//                ->getResult();
//            if (!$vendor || !$results) {
//                return GenBasic::send(202, 'Vendor Or Carts Not Found');
//            }
//            $summary = 0;
//            /** @var Cart $v */
//            foreach ($results as $v) {
//                $summary += ($v->getTotal() - $v->getDiscount());
//                $v->setStatus(7);
//            }
//            $manualCode = rand(1, 1000);
//            $amount = $summary + $manualCode;
//            $now = new \DateTime();
//            $expiredPay = $now->modify('+1 day');
//            $payment = new Payment();
//            $payment->setBank($vendor)
//                ->setUser($this->getUser())
//                ->setPaymentReference('INV/' . $this->getUser()->getId() . '/' . date('YmdHis'))
//                ->setStatus(0)
//                ->setPaymentMethod('Manual Transfer')
//                ->setManualCode($manualCode)
//                ->setAmount($amount)
//                ->setExpiredPay($expiredPay)
//                ->setCart($results);
//            $this->em->persist($payment);
//            $this->em->flush();
//            $code = 200;
//            $response = [
//                'msg' => 'Success Submit Payment!',
//                'data' => [
//                    'invoice' => $payment->getId()
//                ]
//            ];
//        } catch (\Exception $err) {
//            $code = 500;
//            $response = [
//                'msg' => 'Failed To Fetch Transaction List ' . $err->getMessage()
//            ];
//        }
//        return GenBasic::send($code, $response);
//    }
//
//    /**
//     * @return \Symfony\Component\HttpFoundation\JsonResponse
//     * @Route("/api/member/payment/detail", methods="GET|HEAD")
//     */
//    public function paymentDetail()
//    {
//        try {
//            $id = $this->req->query->get('id');
//            /** @var PaymentRepository $paymentRepo */
//            $paymentRepo = $this->em->getRepository(Payment::class);
//            $payment = $paymentRepo->createQueryBuilder('p')
//                ->innerJoin('p.user', 'u')
//                ->where('p.user = :user')
//                ->andWhere('p.id = :id')
//                ->setParameter('user', $this->getUser())
//                ->setParameter('id', $id)
//                ->getQuery()->getOneOrNullResult();
//            if(!$payment){
//                return GenBasic::send(202, ['msg' => 'Payment Not Found!']);
//            }
//            $code = 200;
//            $response = [
//                'msg' => 'Success Get Payment Detail!',
//                'data' => GenBasic::CustomNormalize($payment, [
//                    'id',
//                    'bank' => [
//                        'bank' => [
//                            'name', 'code', 'icon'
//                        ],
//                        'number',
//                        'holderName'
//                    ],
//                    'paymentReference',
//                    'paymentMethod',
//                    'status',
//                    'manualCode',
//                    'amount',
//                    'generatedCreatedAt',
//                    'cart' => [
//                        'status',
//                        'referenceId',
//                        'total',
//                        'discount',
//                        'expiredPay',
//                        'transaction' => [
//                            'tentorSubject' => [
//                                'user' => [
//                                    'tentorProfile' => [
//                                        'fullName', 'avatar', 'generatedAvatar'
//                                    ]
//                                ],
//                                'category' => ['name'],
//                                'level' => ['name']
//                            ]
//                        ]
//                    ]
//                ])
//            ];
//        } catch (\Exception $err) {
//            $code = 500;
//            $response = [
//                'msg' => 'Failed To Fetch Transaction List ' . $err->getMessage()
//            ];
//        } catch (ExceptionInterface $e) {
//            $code = 500;
//            $response = [
//                'msg' => 'Failed To Fetch Transaction List ' . $e->getMessage()
//            ];
//        }
//        return GenBasic::send($code, $response);
//    }
//
//    /**
//     * @return Response
//     * @Route("/api/member/payment/data", methods="GET|HEAD")
//     */
//    public function paymentList()
//    {
//        try {
//            /** @var PaymentRepository $paymentRepo */
//            $paymentRepo = $this->em->getRepository(Payment::class);
//            $qb = $paymentRepo->createQueryBuilder('p')
//                ->innerJoin('p.user', 'u')
//                ->where('p.user = :user')
//                ->setParameter('user', $this->getUser());
//            $results = $qb
//                ->orderBy('p.createdAt', 'DESC')
//                ->getQuery()
//                ->getResult();
//
//            $code = 200;
//            $attribute = [
//                'id',
//                'status',
//                'totalCart',
//                'paymentMethod',
//                'paymentReference',
//                'amount',
//                'cart' => [
//                    'transaction' => [
//                        'tentorSubject' => [
//                            'user' => [
//                                'tentorProfile' => ['fullName', 'avatar', 'generatedAvatar']
//                            ],
//                            'category' => [
//                                'name',
//                            ],
//                            'level' => [
//                                'name',
//                            ]
//                        ]
//                    ],
//
//                ]
//            ];
//            $response = GenBasic::CustomNormalize($results, $attribute);
//        } catch (\Exception $err) {
//            $code = 500;
//            $response = [
//                'msg' => 'Terjadi kesalahan dalam pengambilan data ' . $err->getMessage(),
//            ];
//        } catch (ExceptionInterface $e) {
//            $code = 500;
//            $response = [
//                'msg' => 'Failed To Normalize Transactions ' . $e->getMessage(),
//            ];
//        }
//        return GenBasic::send($code, $response);
//    }
//
//    /**
//     * @return Response
//     * @Route("/api/member/transfer/detail", methods="GET|HEAD")
//     */
//    public function transferDetail()
//    {
//        try {
//            $id = $this->req->query->get('id');
//            /** @var PaymentRepository $paymentRepo */
//            $paymentRepo = $this->em->getRepository(Payment::class);
//            $payment = $paymentRepo->createQueryBuilder('p')
//                ->innerJoin('p.user', 'u')
//                ->where('p.user = :user')
//                ->andWhere('p.id = :id')
//                ->setParameter('user', $this->getUser())
//                ->setParameter('id', $id)
//                ->getQuery()->getResult();
//            $code = 200;
//            $response = [
//                'msg' => 'Success Get Payment Detail!',
//                'data' => GenBasic::CustomNormalize($payment, [
//                    'id',
//                    'bank' => [
//                        'bank' => [
//                            'name', 'code', 'icon'
//                        ],
//                        'number',
//                        'holderName'
//                    ],
//                    'paymentReference',
//                    'paymentMethod',
//                    'status',
//                    'manualCode',
//                    'amount',
//                    'generatedExpiredPay'
//                ])
//            ];
//        } catch (\Exception $err) {
//            $code = 500;
//            $response = [
//                'msg' => 'Failed To Fetch Transaction List ' . $err->getMessage()
//            ];
//        } catch (ExceptionInterface $e) {
//            $code = 500;
//            $response = [
//                'msg' => 'Failed To Fetch Transaction List ' . $e->getMessage()
//            ];
//        }
//        return GenBasic::send($code, $response);
//    }
//
//    /**
//     * @return Response
//     * @Route("/api/member/transfer/confirm", methods="POST|HEAD")
//     */
//    public function transferConfirmation()
//    {
//        try {
//            $id = GenBasic::request($this->req)['id'];
//            $holderName = GenBasic::request($this->req)['holderName'];
//            $rekening = GenBasic::request($this->req)['rekening'];
//            /** @var Payment $payment */
//            $payment = $this->em->getRepository(Payment::class)->findOneBy(['user' => $this->getUser(), 'id' => $id]);
//            if (!$payment) {
//                return GenBasic::send(202, ['msg' => 'Payment Not Found!']);
//            }
//            date_default_timezone_set('Asia/Jakarta');
//            $payment->setHolderName($holderName)->setRekening($rekening)->setConfirmedAt(new \DateTime());
//            $this->em->flush();
//            $code = 200;
//            $response = [
//                'msg' => 'Success Confirm',
//            ];
//        } catch (\Exception $err) {
//            $code = 500;
//            $response = [
//                'msg' => 'Failed To Fetch Transaction List ' . $err->getMessage()
//            ];
//        } catch (ExceptionInterface $e) {
//            $code = 500;
//            $response = [
//                'msg' => 'Failed To Fetch Transaction List ' . $e->getMessage()
//            ];
//        }
//        return GenBasic::send($code, $response);
//    }
}
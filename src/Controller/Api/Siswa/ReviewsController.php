<?php


namespace App\Controller\Api\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Subscription;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Routing\Annotation\Route;

class ReviewsController extends BaseController
{

    private $utility;

    /**
     * ReviewsController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/reviews/list", methods="GET|HEAD")
     */
    public function getSubscriptionListToReview()
    {
        return $this->utility->getSubscriptionListToReview($this->getUser());
    }

    /**
     * @param $id
     * @return Response
     * @Route("/api/member/reviews/detail/{id}")
     */
    public function getReviewDetail($id)
    {
        try {
            $subscription = $this->utility->getSubscriptionReviewDetail($this->getUser(), $id);
            $code = 200;
            $response = [
                'msg' => 'Berhasil Mengunduh Data Review',
                'data' => GenBasic::CustomNormalize($subscription, [
                    'id',
                    'subscriptionCode',
                    'tentorSubject' => [
                        'user' => [
                            'id',
                            'tentorProfile' => [
                                'fullName', 'avatar', 'generatedAvatar'
                            ]
                        ],
                        'level' => ['id', 'name'],
                        'category' => ['id', 'name']
                    ],
                    'rating' => [
                        'rating',
                        'review',
                        'isAnonymous',
                    ]
                ])
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Subscription To Review List ' . $e->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Subscription To Review Detail ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/reviews/create", methods="POST|HEAD")
     */
    public function sendReviews()
    {
        $data = [
            'id' => GenBasic::request($this->req)['id'],
            'rating' => GenBasic::request($this->req)['rating'],
            'review' => GenBasic::request($this->req)['review'],
            'isAnonymous' => GenBasic::request($this->req)['isAnonymous'],
            'tentorId' => GenBasic::request($this->req)['tentorId']
        ];
        return $this->utility->sendReviews($this->getUser(), $data);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/reviews/tentor/{id}", methods="GET|HEAD")
     */
    public function getTentorReviews($id)
    {
        return $this->utility->getTentorReviews($id);
    }
}
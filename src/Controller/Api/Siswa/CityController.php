<?php


namespace App\Controller\Api\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\City;
use App\Entity\District;
use App\Entity\User;
use App\Repository\DistrictRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CityController extends BaseController
{

    /**
     * CityController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/myDistrict", methods="GET|HEAD")
     */
    public function getDistrict()
    {
        try {
            /** @var User $user */
            $user = $this->getUser();
            /** @var City $city */
            $city = $user->getMemberProfile()->getDistrict()->getCity();
            /** @var DistrictRepository $district */
            $district = $this->em->getRepository(District::class);
            $results = $district->createQueryBuilder('d')
                ->select([
                    'd.id',
                    'd.districtName as district',
                    'c.cityName as city',
                    'p.provinceName as province'
                ])
                ->innerJoin('d.city', 'c')
                ->innerJoin('c.province', 'p')
                ->where('c.id = :city')
                ->setParameter('city', $city->getId())
                ->getQuery()->getScalarResult();
            $code = 200;
            $response = GenBasic::serializeToJson($results);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data District ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
<?php


namespace App\Controller\Api\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class CartController extends BaseController
{

    private $utility;

    /**
     * CartController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }

    /**
     * @return Response
     * @Route("/api/member/cart/list", methods="GET|HEAD")
     */
    public function getCartList()
    {
        $limit = $this->req->query->get('limit');
        $offset = $this->req->query->get('offset');
        return $this->utility->getCartList($this->getUser(), $limit, $offset);
    }

    /**
     * @param $id
     * @return Response
     * @Route("/api/member/cart/detail/{id}", methods="GET|HEAD")
     */
    public function cartDetail($id)
    {

        try {
            $cart = $this->utility->getCartById($this->getUser(), $id);
            if (!$cart) {
                return GenBasic::send(202, ['Cart Not Found!']);
            }
            $attribute = [
                'id',
                'referenceId',
                'district' => [
                    'id',
                    'districtName',
                    'city' => [
                        'id', 'cityName'
                    ]
                ],
                'encounter',
                'duration',
                'attendees',
                'firstMeet',
                'address',
                'note',
                'amount',
                'total',
                'discount',
                'method' => ['id', 'name'],
                'subscription' => [
                    'tentorSubject' => [
                        'user' => [
                            'tentorProfile' => [
                                'fullName', 'avatar', 'generatedAvatar'
                            ],
                        ],
                        'category' => ['id', 'name'],
                        'level' => ['id', 'name'],
                    ],
                    'user' => [
                        'memberProfile' => [
                            'fullName',
                            'school',
                            'class' => [
                                'name'
                            ],
                            'phone'
                        ]
                    ]
                ]
            ];
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($cart, $attribute)
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Selected Cart ' . $e->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Selected Cart ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return Response
     * @Route("/api/member/cart/count", methods="GET|HEAD")
     */
    public function getCartCount()
    {
        return $this->utility->getCartCount($this->getUser());
    }

    /**
     * @return Response
     * @Route("/api/member/cart/selected", methods="GET|HEAD")
     */
    public function getSelectedCart()
    {
        try {
            $id = $this->req->query->get('id') ?? [];
            $cart = $this->utility->getCartListById($this->getUser(), $id);
            if ($cart === null) {
                return GenBasic::send(202, 'No Selected Cart!');
            }
            $code = 200;
            $attribute = [
                'id',
                'referenceId',
                'method' => ['id', 'name'],
                'encounter',
                'attendees',
                'amount',
                'total',
                'discount',
                'subscription' => [
                    'tentorSubject' => [
                        'user' => [
                            'tentorProfile' => [
                                'fullName', 'avatar', 'generatedAvatar'
                            ]
                        ],
                        'category' => ['id', 'name'],
                        'level' => ['id', 'name'],
                    ]
                ]
            ];
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($cart, $attribute)
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Selected Cart ' . $e->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Selected Cart ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
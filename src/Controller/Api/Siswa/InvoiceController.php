<?php


namespace App\Controller\Api\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\BankAccount;
use App\Entity\Cart;
use App\Entity\Transaction;
use App\Repository\CartRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class InvoiceController
 * @package App\Controller\Api\Siswa
 */
class InvoiceController extends BaseController
{

    private $utility;

    /**
     * InvoiceController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        date_default_timezone_set('Asia/Jakarta');
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }

    /**
     * @return Response
     * @Route("/api/member/invoice/submit", methods="POST|HEAD")
     */
    public function submitInvoice()
    {
        $session = new Session();
        $bankId = GenBasic::request($this->req)['vid'];
        $cartId = GenBasic::request($this->req)['cartId'];
        $bank = $this->em->getRepository(BankAccount::class)->find($bankId);
        if (!$bank) {
            return GenBasic::send(202, 'Failed To Create New Transaction. Bank Not Found');
        }
        $payment = $this->utility->sendCheckOut($this->getUser(), $bank, $cartId);
        if (!$payment) {
            return GenBasic::send(500, 'Failed To Create New Transaction');
        }
        $transactionId = $payment[0]->getTransaction()->getId();
        return GenBasic::send(200, [
            'invoice' => $transactionId
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/invoice/list", methods="GET|HEAD")
     */
    public function getInvoiceList()
    {
        $limit = $this->req->query->get('limit');
        $offset = $this->req->query->get('offset');
        return $this->utility->getInvoiceList($this->getUser(), $offset, $limit);
    }

    /**
     * @param $id
     * @return Response
     * @Route("/api/member/invoice/detail/{id}", methods="GET|HEAD")
     */
    public function getInvoiceById($id)
    {
        try {
            /** @var Transaction $invoice */
            $invoice = $this->utility->getInvoiceById($this->getUser(), $id);
            if (!$invoice) {
                return GenBasic::send(202, 'Invoice Not Found!');
            }
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($invoice, [
                    'id',
                    'paymentExpiration',
                    'amount',
                    'slug',
                    'isPaid',
                    'confirmationInfo',
                    'bank' => [
                        'bank' => ['name']
                    ],
                    'cart' => [
                        'id',
                        'referenceId',
                        'subscription' => [
                            'tentorSubject' => [
                                'user' => [
                                    'tentorProfile' => [
                                        'fullName', 'avatar', 'generatedAvatar'
                                    ]
                                ],
                                'category' => ['id', 'name'],
                                'level' => ['id', 'name']
                            ]
                        ],
                        'total',
                        'amount',
                        'discount'
                    ]
                ])
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Invoice By Id' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Invoice By Id ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return Response
     * @Route("/api/member/invoice/count", methods="GET|HEAD")
     */
    public function getInvoiceCount()
    {
        return $this->utility->getInvoiceCount($this->getUser());
    }
}
<?php


namespace App\Controller\Api\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class SubscriptionController extends BaseController
{

    private $utility;

    /**
     * SubscriptionController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }

    /**
     * @return Response
     * @Route("/api/member/subscription/list", methods="GET|HEAD")
     */
    public function getSubscriptionList()
    {
        $status = $this->req->query->get('status') ?? 'all';
        $limit = $this->req->query->get('limit') ?? 5;
        $offset = $this->req->query->get('offset') ?? 0;
        return $this->utility->getSubscriptionList($this->getUser(), $status, $offset, $limit);
    }

    /**
     * @param $id
     * @Route("/api/member/subscription/detail/{id}", methods="GET|HEAD")
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getSubscriptionDetail($id)
    {
        try {
            $subscription = $this->utility->getMySubscriptionDetail($this->getUser(), $id);
            if (!$subscription) {
                return GenBasic::send(202, [
                    'msg' => 'Failed',
                    'data' => 'Subscription Not Found!'
                ]);
            }
            $code = 200;
            $attribute = [
                'id',
                'tentorSubject' => [
                    'id',
                    'user' => [
                        'id',
                        'tentorProfile' => [
                            'fullName', 'generatedAvatar'
                        ],
                        'avgRatings'
                    ],
                    'category' => ['id', 'name'],
                    'level' => ['id', 'name'],
                ],
                'requestCart' => [
                    'method' => ['id', 'name'],
                    'isExpired',
                    'district' => ['id', 'districtName', 'city' => ['cityName', 'id']],
                ],
                'lastCart' => [
                    'referenceId',
                    'method' => ['id', 'name'],
                    'district' => ['id', 'districtName', 'city' => ['cityName', 'id']],
                    'duration',
                    'encounter',
                    'attendees',
                    'firstMeet',
                    'address',
                    'note',
                    'amount',
                    'total',
                    'discount',
                ],
                'remains',
                'subscriptionCode',
                'status',
                'confirmedAt'
            ];
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($subscription, $attribute)
            ];
        } catch (NonUniqueResultException $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Subscription Cause Non Unique Result',
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Subscription ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Subscription ' . $e->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/api/member/subscription-create", methods="POST|HEAD")
     */
    public function sendToTransaction()
    {
        $tentorSubject = GenBasic::request($this->req)['tsId'];
        $duration = GenBasic::request($this->req)['duration'];
        $encounter = GenBasic::request($this->req)['encounter'];
        $attendees = GenBasic::request($this->req)['attendees'];
        $location = GenBasic::request($this->req)['location'];
        $note = GenBasic::request($this->req)['note'] ?? '';
        $method = GenBasic::request($this->req)['method'];
        $meet = GenBasic::request($this->req)['meet'];
        $address = GenBasic::request($this->req)['address'] ?? '';
        $data = [
            'tsId' => $tentorSubject,
            'duration' => $duration,
            'encounter' => $encounter,
            'attendees' => $attendees,
            'location' => $location,
            'note' => $note,
            'method' => $method,
            'meet' => $meet,
            'address' => $address
        ];
        return $this->utility->sendSubscription($this->getUser(), $data);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/api/member/subscription-cancel", methods="POST|HEAD")
     */
    public function cancelSubscription()
    {
        $id = $this->req->request->get('id');
        return $this->utility->cancelSubscription($this->getUser(), $id);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/subscription/not-confirmed/count", methods="GET|HEAD")
     */
    public function getSubscriptionNotConfirmed()
    {
        return $this->utility->getSubscriptionNotConfirmed($this->getUser());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/subscription-renewal", methods="POST|HEAD")
     */
    public function createManualRenewal()
    {
        $id = GenBasic::request($this->req)['id'];
        return $this->utility->createManualRenewal($this->getUser(), $id);
    }
}
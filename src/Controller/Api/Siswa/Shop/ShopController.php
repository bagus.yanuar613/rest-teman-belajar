<?php


namespace App\Controller\Api\Siswa\Shop;


use App\Controller\BaseController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ShopController  extends BaseController
{

    private $ct;

    /**
     * TransactionController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->ct = new \App\Controller\siswa\Shop\ShopController($this->em);
    }

    /**
     * @return JsonResponse
     * @Route("/api/shop", name="api-shop")
     */
    public function growthTryoutApi()
    {
        return $this->ct->shopApi();
    }
}
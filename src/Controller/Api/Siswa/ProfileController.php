<?php


namespace App\Controller\Api\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\District;
use App\Entity\Member;
use App\Entity\MemberClass;
use App\Entity\Tentor;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Intervention\Image\ImageManagerStatic as Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;


/**
 * Class ProfileController
 * @package App\Controller\Api\Siswa
 */
class ProfileController extends BaseController
{


    /**
     * ProfileController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/profile", methods="GET|HEAD")
     */
    public function getProfile()
    {
        try {
            /** @var User $user */
            $user = $this->getUser();
            /** @var Member $profile */
            $profile = $user->getMemberProfile();
            $code = 200;
            $response =
                [
                    'msg' => 'Berhasil Mengunduh Data Profil',
                    'data' => GenBasic::CustomNormalize($profile,
                        [
                            'id',
                            'fullName',
                            'phone',
                            'school',
                            'class' => [
                                'id',
                                'name',
                                'memberClassParent' => [
                                    'name'
                                ],
                                'level' => ['id', 'name']
                            ],
                            'address',
                            'medal',
                            'coin',
                            'avatar',
                            'generatedAvatar',
                            'gender',
                            'parentName',
                            'parentPhone',
                            'district' => [
                                'id',
                                'districtName',
                                'city' => ['id', 'cityName']
                            ],
                            'user' => [
                              'id'
                            ],
                            'myQrCode'
                        ]
                    )
                ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Profile' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Profile ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/profile/patch", methods="POST|HEAD")
     */
    public function patchProfile()
    {
        try {
            $fullName = GenBasic::request($this->req)['name'];
            $parentName = GenBasic::request($this->req)['parentName'];
            $phone = GenBasic::request($this->req)['phone'];
            $parentPhone = GenBasic::request($this->req)['parentPhone'];
            $school = GenBasic::request($this->req)['school'];
            $class = GenBasic::request($this->req)['class'];
            $gender = GenBasic::request($this->req)['gender'];
            $districtId = GenBasic::request($this->req)['districtId'];
            $address = GenBasic::request($this->req)['address'];
            /** @var Member $member */
            $member = $this->em->getRepository(Member::class)->findOneBy(['user' => $this->getUser()]);
            $district = $this->em->getRepository(District::class)->find($districtId);
            $memberClass = $this->em->getRepository(MemberClass::class)->find($class);
            if (!$district && !$class) {
                return GenBasic::send(202, ['msg' => 'Data Kecamatan Tidak Di Temukan']);
            }


            $member->setFullName($fullName)
                ->setPhone($phone)
                ->setSchool($school)
                ->setClass($memberClass)
                ->setDistrict($district)
                ->setAddress($address)
                ->setParentPhone($parentPhone)
                ->setParentName($parentName)
                ->setGender($gender);
            $this->em->flush();
            $code = 200;
            $response =
                [
                    'msg' => 'Berhasil Memperbarui Profil Pengguna',
                ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Patch Profile' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/profile/patch-picture", methods="POST|HEAD")
     */
    public function patchPictureProfile()
    {
        try {
            $avatar = $this->req->files->get('avatar');
            /** @var Member $member */
            $member = $this->em->getRepository(Member::class)->findOneBy(['user' => $this->getUser()]);
            if ($avatar) {
                /** @var UploadedFile $avatar */
                $avatarString = (string)Image::make($avatar->getRealPath())->encode('jpeg', '75');
                $avatarResize = Image::make($avatarString);
                $avatarResize->resize(null, 140, function ($constraint) {
                    $constraint->aspectRatio();
                });
                if(!empty($member->getAvatar()) && file_exists('../public/images/img-account/'.$member->getAvatar())) {
                    unlink('../public/images/img-account/'.$member->getAvatar());
                }

                $avatarName = $this->getUser()->getId() . '-' . date('YmdHis') . '.jpeg';
                $avatarResize->save('../public/images/img-account/' . $avatarName);
                $member->setAvatar($avatarName);
            }else{
                return GenBasic::send(202,
                    [
                        'msg' => 'Tidak Ada Gambar Yang Terlampir',
                    ]
                );
            }
            $this->em->flush();
            $code = 200;
            $response =
                [
                    'msg' => 'Berhasil Memperbarui Foto Profil Pengguna',
                ];
        }catch (\Exception $err){
            $code = 500;
            $response = [
                'msg' => 'Failed To Patch Profile' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);

    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/profile/check", methods="GET|HEAD")
     */
    public function checkProfile()
    {
        try {
            $userProfile = $this->em->getRepository(Member::class)->findOneBy(['user' => $this->getUser()]);
            $criteria    = ['fullName', 'class', 'address', 'phone', 'district'];
            foreach ($criteria as $v) {
                $getter = 'get'.ucfirst($v);
                if ($userProfile->$getter() === null) {
                    return GenBasic::send(200, [
                        'msg' => 'Berhasil Mengunduh Data Profil',
                        'data' => ['status' => false]
                    ]);
                }
            }
            return GenBasic::send(200, [
                'msg' => 'Berhasil Mengunduh Data Profil',
                'data' => ['status' => true]
            ]);
        }catch (\Exception $err){
            $code = 500;
            $response = [
                'msg' => 'Failed To Patch Profile' . $err->getMessage(),
            ];
            return GenBasic::send($code, $response);
        }
    }
}
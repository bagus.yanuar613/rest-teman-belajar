<?php


namespace App\Controller\Api\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\CategoryToParentToLevel;
use App\Entity\Method;
use App\Entity\Pricing;
use App\Entity\TentorSubject;
use App\Entity\User;
use App\Repository\CategoryToParentToLevelRepository;
use App\Repository\TentorSubjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class SubjectController extends BaseController
{

    /**
     * SubjectController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api-public/level/category")
     */
    public function getCategoryLevel()
    {
        try {
            $categoryId = $this->req->query->get('category');
            /** @var CategoryToParentToLevelRepository $level */
            $level = $this->em->getRepository(CategoryToParentToLevel::class);
            $result = $level->createQueryBuilder('p')
                ->select([
                    'l.id',
                    'l.name'
                ])
                ->innerJoin('p.category', 'c')
                ->innerJoin('p.level', 'l')
                ->where('c.id = :id')
                ->setParameter('id', $categoryId)
                ->groupBy('l.id')
                ->getQuery()->getScalarResult();
            $code = 200;
            $response = [
                'msg' => 'Success Fetch Category Level',
                'data' => $result
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed Fetch Category Level ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/tentor-subject", methods="GET|HEAD")
     */
    public function tentorSubjectDetail()
    {
        try {
            $id = $this->req->query->get('id');
            $method = $this->req->query->get('method');
            $vMethod = $this->em->getRepository(Method::class)->find($method);
            if (!$vMethod) {
                return GenBasic::send(202, ['msg' => 'Method Not Found!']);
            }
            //response ketika memilih metode belajar secara konsultasi
            if ($vMethod->getId() === 3) {
                return GenBasic::send(202, ['msg' => 'Method Not Support!']);
            }
            $city = 'is null';
            if ($vMethod->getId() === 1) {
                $city = '= ds.city';
            }
            /** @var TentorSubjectRepository $subject */
            $subject = $this->em->getRepository(TentorSubject::class);
            $result = $subject->createQueryBuilder('ts')
                ->addSelect('p.price as price')
                ->innerJoin('ts.user', 'u')
                ->innerJoin('u.tentorProfile', 'tp')
                ->leftJoin('u.district', 'ds')
                ->leftJoin('ts.method', 'm')
                ->innerJoin(Pricing::class, 'p', Join::WITH, 'p.level = ts.level and p.grade = tp.grade and p.city ' . $city . ' and p.method = :mid')
                ->where('ts.id = :id')
                ->andWhere('m.id = :method')
                ->andWhere('ts.isAvailable = :status')
                ->setParameter('id', $id)
                ->setParameter('method', $method)
                ->setParameter('status', true)
                ->setParameter('mid', $vMethod->getId())
                ->getQuery()->getOneOrNullResult();

            if (!$result) {
                return GenBasic::send(202, ['msg' => 'Tentor Subject Not Found!']);
            }
            $attribute =
                [
                    'id',
                    'user' => [
                        'id',
                        'tentorProfile' => [
                            'fullName',
                            'avatar',
                            'gender',
                            'phone',
                            'address',
                            'about',
                            'experience',
                            'education',
                            'achievement',
                            'company',
                            'isVerified',
                            'grade' => ['id', 'name']
                        ],
                        'avgRatings',
                        'district' => [
                            'id',
                            'districtName',
                            'city' => ['id', 'cityName']
                        ],
                        'schedule' => [
                            'id',
                            'day',
                            'time'
                        ],
                        'tentorSubject' => [
                            'id',
                            'category' => ['id', 'name'],
                            'level' => ['id', 'name']
                        ]
                    ],
                    'category' => ['id', 'name'],
                    'level' => ['id', 'name'],
                    'method' => ['id', 'name']
                ];
            $code = 200;
            $response = [
                'msg' => 'Success Fetch Tentor Subject',
                'data' => GenBasic::CustomNormalize($result, $attribute)
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed Fetch Tentor Subject ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed Normalize Tentor Subject ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/tentor-subject/other", methods="GET|HEAD")
     */
    public function getTentorSkill()
    {
        try {
            $id = $this->req->query->get('id');
            /** @var TentorSubject $tentor */
            $tentor = $this->em->getRepository(TentorSubject::class)->find($id);
            if (!$tentor) {
                return GenBasic::send(202, ['msg' => 'Skill Not Found!']);
            }
            /** @var User $user */
            $user = $tentor->getUser();
            $skill = $user->getTentorSubject();
            $code = 200;
            $response = [
                'msg' => 'Success Fetch Tentor Subject',
                'data' => GenBasic::CustomNormalize($skill, [
                    'category' => ['name', 'id'],
                    'level' => ['name', 'id'],
                ])
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed Fetch Tentor Subject ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed Normalize Tentor Subject ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

}
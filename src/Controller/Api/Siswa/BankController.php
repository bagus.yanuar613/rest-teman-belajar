<?php


namespace App\Controller\Api\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\BankAccount;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class BankController extends BaseController
{

    /**
     * BankController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        date_default_timezone_set('Asia/Jakarta');
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/bank", methods="GET|HEAD")
     */
    public function getListBank()
    {
        try {
            $bank = $this->em->getRepository(BankAccount::class)->findAll();
            $code = 200;
            $response = [
                'msg' => "success Fetch Bank Account",
                'data' => GenBasic::CustomNormalize($bank, [
                    'id',
                    'holderName',
                    'number',
                    'bank' => ['id', 'name', 'icon']
                ])
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Bank List ' . $err->getMessage()
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Bank List ' . $e->getMessage()
            ];
        }
        return GenBasic::send($code, $response);
    }



}
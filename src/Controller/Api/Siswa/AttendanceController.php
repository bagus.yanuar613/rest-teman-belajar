<?php


namespace App\Controller\Api\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Attendance;
use App\Entity\Cart;
use App\Entity\Member;
use App\Entity\Tentor;
use App\Entity\TentorSaldo;
use App\Entity\Transaction;
use App\Entity\User;
use App\Repository\AttendanceRepository;
use App\Repository\TransactionRepository;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class AttendanceController extends BaseController
{
    private $utility;
    /**
     * AttendanceController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/attendance/list/{id}", methods="GET|HEAD")
     */
    public function getMySubscriptionAttendance($id)
    {
        return $this->utility->getAttendance($this->getUser(), $id);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/attendance/active", methods="GET|HEAD")
     */
    public function getActiveAttendance()
    {
        return $this->utility->getActiveAttendance($this->getUser());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/attendance/presence", methods="POST|HEAD")
     */
    public function setPresence()
    {
        $id = GenBasic::request($this->req)['id'];
        return $this->utility->setManualPresence($this->getUser(), $id);
    }

    /**
     * @param $id
     * @return Response
     * @Route("/api/member/attendance/detail/{id}", methods="GET|HEAD")
     */
    public function detailAttendance($id)
    {
        try {
            /** @var Attendance $attendance */
            $attendance = $this->utility->getDetailAttendance($this->getUser(), $id);
            if (!$attendance) {
                return GenBasic::send(202, ['msg' => 'Attendance Not Found']);
            }
            $code = 200;
            $response = [
                'msg' => 'Berhasil Mengunduh Data Absensi',
                'data' => GenBasic::CustomNormalize(
                    $attendance,
                    [
                        'id',
                        'subscription' => [
                            'id',
                            'tentorSubject' => [
                                'user' => [
                                    'id',
                                    'tentorProfile' => [
                                        'fullName', 'avatar', 'generatedAvatar'
                                    ]
                                ],
                                'category' => ['id', 'name'],
                                'level' => ['id', 'name']
                            ]
                        ],
                        'startAt',
                        'finishAt',
                    ]
                ),
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Load My Detail Attendance ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize My Detail Attendance ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

//    /**
//     * @return \Symfony\Component\HttpFoundation\JsonResponse
//     * @Route("/api/member/attendance", methods="GET|HEAD")
//     */
//    public function getAttendance()
//    {
//        try {
//            $user = $this->getUser();
//            $transactionId = $this->req->query->get('id');
//            /** @var AttendanceRepository $attendRepo */
//            $attendRepo = $this->em->getRepository(Attendance::class);
//            $attendance = $attendRepo->createQueryBuilder('a')
//                ->innerJoin('a.transaction', 't')
//                ->where('t.user = :user')
//                ->andWhere('t.id = :id')
//                ->andWhere('t.status = :status')
//                ->setParameters(['user' => $user, 'id' => $transactionId, 'status' => 1])
//                ->getQuery()->getResult();;
//
//            $code = 200;
//            $response = [
//                'msg' => 'Success Fetch Attendance!',
//                'data' => GenBasic::CustomNormalize($attendance, [
//                    'id',
//                    'isPresence',
//                    'isActive',
//                    'generatedStart'
//                ])
//            ];
//        } catch (\Exception $err) {
//            $code = 500;
//            $response = [
//                'msg' => 'Failed To Check Attendance ' . $err->getMessage(),
//            ];
//        } catch (ExceptionInterface $e) {
//            $code = 500;
//            $response = [
//                'msg' => 'Failed To Normalize Attendance ' . $e->getMessage(),
//            ];
//        }
//        return GenBasic::send($code, $response);
//    }
//
//    /**
//     * @return \Symfony\Component\HttpFoundation\JsonResponse
//     * @Route("/api/member/attendance/presence", methods="POST|HEAD")
//     */
//    public function setPresence()
//    {
//        try {
//            $id = GenBasic::request($this->req)['id'];
//            /** @var AttendanceRepository $attendRepo */
//            $attendRepo = $this->em->getRepository(Attendance::class);
//            /** @var Attendance $attendance */
//            $attendance = $attendance = $attendRepo->createQueryBuilder('a')
//                ->innerJoin('a.transaction', 't')
//                ->where('t.user = :user')
//                ->andWhere('a.isPresence = :status')
//                ->andWhere('a.isActive = :active')
//                ->andWhere('a.id = :id')
//                ->setParameter('status', 0)
//                ->setParameter('active', 1)
//                ->setParameter('id', $id)
//                ->setParameter('user', $this->getUser())
//                ->getQuery()->getOneOrNullResult();
//            if (!$attendance) {
//                return GenBasic::send(202, ['msg' => 'Attendance Not Found!']);
//            }
//            date_default_timezone_set('Asia/Jakarta');
//            /** @var Cart $cart */
//            $cart = $attendance->getCart();
//            /** @var Transaction $transaction */
//            $transaction = $attendance->getTransaction();
//            /** @var User $user */
//            $user = $attendance->getTransaction()->getTentorSubject()->getUser();
//            $category = $transaction->getTentorSubject()->getCategory()->getName();
//            $level = $transaction->getTentorSubject()->getLevel()->getName();
//            $method = $cart->getMethod()->getName();
//            /** @var Tentor $tentor */
//            $tentor = $user->getTentorProfile();
//            /** @var Member $member */
//            $member = $this->getUser()->getMemberProfile();
//            $currentBalance = $tentor->getBalance();
//            $currentPoint = $tentor->getPoint();
//            $newSaldo = $currentBalance + ((0.85 * $cart->getAmount()) * $cart->getAttendees());
//            $newPoint = $currentPoint + 1;
//            $medal = $member->getMedal() +1;
//            $balance = new TentorSaldo();
//            $balance->setUser($user)
//                ->setType(0)
//                ->setAmount((0.85 * $cart->getAmount()) * $cart->getAttendees())
//                ->setDescription($method . ' ' . $category . ' ' . $level);
//            $tentor->setBalance($newSaldo)->setPoint($newPoint);
//
//            $this->em->persist($balance);
//            $member->setMedal($medal);
//            $attendance->setIsPresence(true);
//            $this->em->flush();;
//            $code = 200;
//            $response = 'Success Set Presence';
//        } catch (\Exception $err) {
//            $code = 500;
//            $response = [
//                'msg' => 'Failed To Set Presence ' . $err->getMessage(),
//            ];
//        }
//        return GenBasic::send($code, $response);
//    }
//
//    /**
//     * @return \Symfony\Component\HttpFoundation\JsonResponse
//     * @Route("/api/member/attendance/active", methods="GET|HEAD")
//     */
//    public function getActiveAttendance()
//    {
//        try {
//            /** @var AttendanceRepository $attendRepo */
//            $attendRepo = $this->em->getRepository(Attendance::class);
//            $attendance = $attendRepo->createQueryBuilder('a')
//                ->innerJoin('a.transaction', 't')
//                ->where('t.user = :user')
//                ->andWhere('a.isPresence = :status')
//                ->setParameter('status', 0)
//                ->setParameter('user', $this->getUser())
//                ->getQuery()->getResult();
//            $code = 200;
//            $response = [
//                'msg' => 'Success Fetch Attendance!',
//                'data' => GenBasic::CustomNormalize($attendance, [
//                    'id',
//                    'isPresence',
//                    'isActive',
//                    'generatedStart',
//                    'transaction' => [
//                        'tentorSubject' => [
//                            'category' => ['name'],
//                            'level' => ['name'],
//                            'user' => [
//                                'tentorProfile' => [
//                                    'fullName', 'avatar', 'generatedAvatar'
//                                ]
//                            ]
//                        ]
//                    ]
//                ])
//            ];
//
//        }catch (\Exception $err) {
//            $code = 500;
//            $response = [
//                'msg' => 'Failed To Check Attendance ' . $err->getMessage(),
//            ];
//        } catch (ExceptionInterface $e) {
//            $code = 500;
//            $response = [
//                'msg' => 'Failed To Normalize Attendance ' . $e->getMessage(),
//            ];
//        }
//        return GenBasic::send($code, $response);
//    }

//    /**
//     * @return \Symfony\Component\HttpFoundation\JsonResponse
//     * @Route("/api/member/attendance/active-online", methods="GET|HEAD")
//     */
//    public function getOnlineAttendance()
//    {
//        try {
//            /** @var AttendanceRepository $attendRepo */
//            $attendRepo = $this->em->getRepository(Attendance::class);
//            $attendance = $attendRepo->createQueryBuilder('a')
//                ->innerJoin('a.transaction', 't')
//                ->innerJoin('a.cart', 'c')
//                ->innerJoin('c.method', 'm')
//                ->where('t.user = :user')
//                ->andWhere('a.isPresence = :status')
//                ->andWhere('a.isActive = :active')
//                ->andWhere('m.id = :method')
//                ->setParameter('status', 0)
//                ->setParameter('active', 0)
//                ->setParameter('method', 2)
//                ->setParameter('user', $this->getUser())
//                ->getQuery()->getResult();
//            $code = 200;
//            $response = [
//                'msg' => 'Success Fetch Attendance!',
//                'data' => GenBasic::CustomNormalize($attendance, [
//                    'id',
//                    'isPresence',
//                    'isActive',
//                    'clientId',
//                    'generatedStart',
//                    'transaction' => [
//                        'tentorSubject' => [
//                            'category' => ['name'],
//                            'level' => ['name'],
//                            'user' => [
//                                'tentorProfile' => [
//                                    'fullName', 'avatar', 'generatedAvatar'
//                                ]
//                            ]
//                        ]
//                    ],
//                    'cart' => [
//                        'method' => [
//                            'id', 'name'
//                        ]
//                    ]
//                ])
//            ];
//        } catch (\Exception $err) {
//            $code = 500;
//            $response = [
//                'msg' => 'Failed To Patch Attendance ' . $err->getMessage(),
//            ];
//        } catch (ExceptionInterface $e) {
//            $code = 500;
//            $response = [
//                'msg' => 'Failed To Normalize Attendance ' . $e->getMessage(),
//            ];
//        }
//        return GenBasic::send($code, $response);
//    }
}
<?php


namespace App\Controller\Api\Siswa\TryOut;


use App\Controller\BaseController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RaporController extends BaseController
{

    private $ct;

    /**
     * TransactionController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->ct = new \App\Controller\siswa\TryOut\RaporController($this->em);
    }

    /**
     * @return JsonResponse
     * @Route("/api/tryout-chart", name="api-chart-tryout")
     */
    public function growthTryoutApi()
    {
        $this->ct->setUser($this->getUser());
        return $this->ct->growthTryoutApi();
    }

    /**
     * @return JsonResponse
     * @Route("/api/tryout-result", name="api-result-tryout")
     */
    public function resultTryoutApi()
    {
        $this->ct->setUser($this->getUser());
        return $this->ct->resultTryoutApi();
    }

    /**
     * @return JsonResponse
     * @Route("/api/tryout-result-detail/{to}", name="api-result-tryout-detail")
     */
    public function resultDetailTryoutApi($to)
    {
        $this->ct->setUser($this->getUser());
        return $this->ct->resultDetailTryoutApi($to);
    }

    /**
     * @return JsonResponse
     * @Route("/api/tryout-result-rank/{to}", name="api-result-tryout-rank")
     */
    public function resultRankTryoutApi($to)
    {
        $this->ct->setUser($this->getUser());
        return $this->ct->resultRankTryoutApi($to);
    }
}
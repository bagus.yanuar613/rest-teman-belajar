<?php


namespace App\Controller\Api\Siswa\TryOut;


use App\Common\GenBasic;
use App\Controller\Admin\TransactionController;
use App\Controller\BaseController;
use App\Controller\siswa\TryOut\DetailController;
use App\Entity\BankAccount;
use App\Entity\TryOut;
use App\Entity\TryOutPricing;
use App\Entity\TryOutRegistrant;
use App\Entity\TryOutSubject;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends BaseController
{

    private $ct;

    /**
     * TransactionController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->ct = new \App\Controller\siswa\TryOut\SearchController($this->em);
    }

    /**
     *
     * @Route("/api/pilih-tryout", name="Api-TryOut")
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function pilihTryoutApi()
    {
        return $this->ct->pilihTryoutApi();
    }

    /**
     * @return Response
     * @Route("/api/tryout-code-access/{cat}/{codeAccess}", name="apiTryoutCodeAccess", methods="POST|HEAD")
     */
    public function apiAccessTryCodeByCode($cat, $codeAccess)
    {
        $this->ct = new DetailController($this->em);
        $this->ct->setUser($this->getUser());
        return $this->ct->tryoutCodeAccessApi($cat, $codeAccess);
    }

    /**
     *
     * @Route("/api/pilih-soal-tryout/{cat}", name="Api-Soal-TryOut")
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function pilihTryoutSoalApi($cat)
    {
        $this->ct->setUser($this->getUser());
        return $this->ct->pilihTryoutSoalApi($cat);
    }

    /**
     *
     * @Route("/api/detail-soal-tryout/{cat}", name="Api-detail-Soal-TryOut")
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function detailTryoutSoalApi($cat)
    {
        try {
            $data = $this->em->createQueryBuilder()
                ->from(TryOut::class, "a")
                ->select(["a"])
                ->addSelect("(SELECT SUM(s.duration) FROM " . TryOutSubject::class . " s WHERE s.tryout = a) as durationTotal")
                ->addSelect("(SELECT SUM(s1.quizNumber) FROM " . TryOutSubject::class . " s1 WHERE s1.tryout = a) as number")
                ->where("a.id = :id")
                ->setParameter('id', $cat)
                ->getQuery()
                ->getResult();
            $code = 200;
            $data = [
                "msg" => "success",
                "data" => GenBasic::CustomNormalize($data, [
                    'id',
                    'title',
                    'banner',
                    'isFree',
                    'codeAccess',
                    'estimatedTime',
                    'startDay',
                    'finalDay',
                    'isActive',
                    'rules',
                    'direction',
                    'price',
                    'isStrict',
                    'restTime',
                    'description',
                    'level' => [
                        "id",
                        "name"
                    ],
                    'pricing' => [
                        "id",
                        "price",
                        "min",
                        "max",
                        'duration'
                    ],
                    'reward' => [
                        'minRegistrant',
                        'freeAccess',
                        'isActive'
                    ],
                    'durationTotal',
                    'number'
                ])];
            $single = $data['data'][0][0];
            $single['durationTotal'] = $data['data'][0]['durationTotal'];
            $single['number'] = $data['data'][0]['number'];
            $data['data'] = $single;

        } catch (\Exception $e) {
            $code = 501;
            $data = ["msg" => "Internal server error " . $e->getMessage()];
        }

        return GenBasic::send($code, $data);
    }


    /**
     *
     * @Route("/api/get-amount-tryout/{cat}/{number}", name="Api-get-amount-TryOut")
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getAmount($cat, $number)
    {
        /** @var DetailController ct */
        $this->ct = new DetailController($this->em);
        return $this->ct->getTotalAmountApi($cat, $number, true);
    }

    /**
     *
     * @Route("/api/confirmation-tryout", name="Api-post-confirmation-TryOut", methods="POST|HEAD")
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function paymentConfirmation()
    {
        try {

            $id = $this->req->get("id");
            /** @var TryOutRegistrant $data */
            $data = $this->em->getRepository(TryOutRegistrant::class)->findOneBy(["id" => $id, 'user' => $this->getUser()]);
            $trans = $data->getTransaction();
            $account = [
                "accountHolder" => $this->req->get("accountHolder"),
                "accountNumber" => $this->req->get("accountNumber")
            ];
            $trans->setConfirmationInfo($account);
            $this->em->flush();
            $code = 200;
            $data = [
                "msg" => "Transaksi berhasil",
                "data" => GenBasic::CustomNormalize($data, [
                    'id',
                    'registrantNumber',
                    'confirmationInfo',
                    'tryOut' => [
                        "id",
                        "total",
                        "totalPrice"
                    ]

                ])
            ];
        } catch (\Exception $e) {
            $code = 501;
            $data = [
                "msg" => "Terjadi galat, transaksi gagal " . $e->getMessage(),
            ];
        }

        return GenBasic::send($code, $data);

    }

    /**
     *
     * @Route("/api/checkout-tryout", name="Api-post-checkout-TryOut", methods="POST|HEAD")
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */

    public function checkOut()
    {
        $pricing = json_decode($this->getAmount($this->req->get('ref'), $this->req->get('total'))->getContent());
        $pricingObj = $pricing->payload->data->ref ? $this->em->getRepository(TryOutPricing::class)->findOneBy(["id" => $pricing->payload->data->ref]) : NULL;
        try {
            /** @var TryOut $tryOut */
            $tryOut = $this->em->getRepository(TryOut::class)->findOneBy(["id" => $this->req->get('ref')]);
            $bank = $this->em->getRepository(BankAccount::class)->findOneBy(['id' => $this->req->get('paymentMethod')]);
            if ($bank) {
                $finalPrice = ($pricing->payload->data->price / $this->req->get('total'));

                $newRegistrant = new TryOutRegistrant();
                $newRegistrant->setUser($this->getUser())
                    ->setIsPaid(false)
                    ->setBasePrice($tryOut->getPrice())
                    ->setFinalPrice($finalPrice)
                    ->setTotalPrice(($pricing->payload->data->price + $this->req->get('digits')))
                    ->setRegistrantNumber($this->req->get('total'))
                    ->setPaymentMethod($bank)
                    ->setTryOut($tryOut)
                    ->setIrtChecked(false)
                    ->setTryOutPricing($pricingObj);
                $this->em->persist($newRegistrant);

                $transaction = new TransactionController($this->em);
                $transaction->setUser($this->getUser());
                $newTransaction = $transaction->newTransaction($newRegistrant, $finalPrice, "tryOutRegistrant", $bank);

                if (!$newTransaction) {
//                    $this->em->rollback();
                    throw new \Exception("Error while creating new transaction");
                }

                $this->em->flush();
                $code = 200;
                $data = [
                    "msg" => "Transaksi berhasil",
                    "data" => GenBasic::CustomNormalize($newRegistrant, [
                        'id',
                        'registrantNumber',
                        'tryOut' => [
                            "id",
                            "total",
                            "totalPrice"
                        ],
                        "transaction" => [
                            "id"
                        ]

                    ])
                ];
            } else {
                throw  new \Exception("Invalid Bank Account");
            }
        } catch (\Exception $e) {
            $code = 501;
            $data = [
                "msg" => "Terjadi galat, transaksi gagal " . $e->getMessage(),
            ];
        }

        return GenBasic::send($code, $data);
    }


    /**
     * @return JsonResponse
     * @Route("/api/history-tryout", name="api-history-tryout")
     */
    public function historyTryoutApi()
    {
        $this->ct = new DetailController($this->em);
        $this->ct->setUser($this->getUser());
        return $this->ct->historyTryoutApi();
    }

    /**
     * @return JsonResponse
     * @Route("/api/add-registrant-tryout/{reg}", name="api-add-registrant-tryout")
     */
    public function addRegistrantTryoutApi($reg)
    {
        $this->ct->setUser($this->getUser());
        return $this->ct->tambahPesertaApi($reg);
    }

    /**
     * @return JsonResponse
     * @Route("/api/remove-registrant-tryout/{reg}", name="api-remove-registrant-tryout")
     */
    public function removeRegistrantTryoutApi($reg)
    {
        $this->ct->setUser($this->getUser());
        return $this->ct->removePesertaApi($reg);
    }

    /**
     * @return JsonResponse
     * @Route("/api/activate-registrant-tryout/{reg}", name="api-activate-registrant-tryout")
     */
    public function activateRegistrantTryoutApi($reg)
    {
        $this->ct->setUser($this->getUser());
        return $this->ct->aktivasiPesertaApi($reg);
    }


}
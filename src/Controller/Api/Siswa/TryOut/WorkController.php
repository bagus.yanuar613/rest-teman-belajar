<?php


namespace App\Controller\Api\Siswa\TryOut;


use App\Controller\BaseController;
use App\Controller\siswa\TryOut\ReportQuizController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class WorkController extends BaseController
{
    private $ct;

    /**
     * TransactionController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->ct = new \App\Controller\siswa\TryOut\WorkController($em);
    }

    /**
     *
     * @Route("/api/get-tab-menu-do/{cat}/{reg}", name="api_get_tab_menu_work_tryout")
     */
    public function getTabMenu($cat, $reg)
    {
        $this->ct->setUser($this->getUser());
        return $this->ct->getTabMenu($cat, $reg);
    }

    /**
     *
     * @Route("/api/get-all-quizes/{cat}/{reg}", name="api_get_all_quizes")
     */
    public function getALlQuizes($cat, $reg)
    {
        $this->ct->setUser($this->getUser());
        return $this->ct->getALlQuizes($cat, $reg);
    }

    /**
     *
     * @Route("/api/save-quiz-answer/{cat}/{reg}", name="api_save_answer_quiz", methods="POST|HEAD")
     */
    public function saveAnswer($cat, $reg)
    {
        $this->ct->setUser($this->getUser());
        return $this->ct->saveAnswer($cat, $reg);
    }

    /**
     *
     * @Route("/api/end-quiz/{cat}/{reg}", name="api_end_quiz_session", methods="POST|HEAD")
     */
    public function endQuiz($cat, $reg)
    {
        $this->ct->setUser($this->getUser());
        return $this->ct->endQuiz($cat, $reg);
    }

    /**
     *
     * @Route("/api/end-tryout/{reg}", name="api_end_tryout_session", methods="POST|HEAD")
     */
    public function endTryOut($reg)
    {
        $this->ct->setUser($this->getUser());
        return $this->ct->endTryOut($reg);
    }

    /**
     *
     * @Route("/api/report-quiz/{quiz}/{reg}", name="api_report_quiz", methods="POST|HEAD")
     */
    public function reportQuiz($quiz, $reg)
    {
        $this->ct = new ReportQuizController($this->em);
        $this->ct->setUser($this->getUser());
        return $this->ct->laporkanSoalDetailApi($quiz, $reg);
    }

}
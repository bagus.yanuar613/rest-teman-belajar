<?php


namespace App\Controller\Api\Siswa;


use App\Common\GenBasic;
use App\Common\GenFirebase;
use App\Controller\BaseController;
use App\Entity\Member;
use App\Entity\TentorSubject;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Google\Cloud\Firestore\DocumentSnapshot;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Firestore;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ChatController extends BaseController
{

    protected $firestore;
    protected $factory;

    /**
     * ChatController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        date_default_timezone_set('Asia/Jakarta');
        $this->req = Request::createFromGlobals();
        $this->factory = (new Factory())->withServiceAccount('../config/teman-belajar-mo-1564386173622-firebase-adminsdk-d7pd1-6bc8bacfa9.json');
        $this->firestore = $this->factory->createFirestore();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/chat", methods="POST|HEAD")
     */
    public function memberChat()
    {
        try {
            $user = $this->getUser();
            $uid = $user->getId();
            $db = $this->firestore->database();
            $msg = GenBasic::request($this->req)['msg'];
            $tsId = GenBasic::request($this->req)['tsId'];
            /** @var TentorSubject $ts */
            $ts = $this->em->getRepository(TentorSubject::class)->find($tsId);
            $tentorId = $ts->getUser()->getId();
            $now = new \DateTime();
            $docId = str_replace(" ", "_", $now->format("Y-m-d H:i:s"));
            $docRef = $db->collection('chat_room')->document($uid . '_' . $tentorId);
            $docSnap = $docRef->snapshot()->data();
            if (empty($docSnap)) {
                $singleData = [
                    'lastUpdate' => $now,
                    'message' => $msg,
                    'memberId' => $uid,
                    'tentorId' => $tentorId,
                    'senderIndex' => 0,
                    'recipientIndex' => 1,
                    'member' => 'send',
                    'tentor' => 'receive'
                ];
                $docRef->set($singleData);
            } else {
                $singleData = [
                    ['path' => 'lastUpdate', 'value' => $now],
                    ['path' => 'message', 'value' => $msg],
                    ['path' => 'member', 'value' => 'send'],
                    ['path' => 'tentor', 'value' => 'receive'],
                    ['path' => 'recipientIndex', 'value' => $docRef->snapshot()['recipientIndex'] + 1]
                ];
                $docRef->update($singleData);
            }
            $docRef->collection('room')
                ->document($docId)->set([
                    'createdAt' => $now,
                    'message' => $msg,
                    'memberId' => $uid,
                    'tentorId' => $tentorId,
                    'member' => 'send',
                    'tentor' => 'receive'
                ]);
            $tentorToken = $ts->getUser()->getAppFcmToken();
            $memberName = $user->getMemberProfile()->getFullName();
            $notification = GenFirebase::SendNotification($tentorToken, $memberName, $msg, [
                'type' => 'chat',
                'docId' => $uid . '_' . $tentorId,
                'name' => $memberName
            ]);
            $code = 200;
            $response = 'success';
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Send Message ' . $err->getMessage()
            ];
        }
        return GenBasic::send($code, $response);
    }


    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/tentor/chat", methods="POST|HEAD")
     */
    public function tentorChat()
    {
        try {
            $db = $this->firestore->database();
            $msg = GenBasic::request($this->req)['msg'];
            $userId = GenBasic::request($this->req)['userId'];
            $tentorId = $this->getUser()->getId();
            $now = new \DateTime();
            $docId = str_replace(" ", "_", $now->format("Y-m-d H:i:s"));
            $docRef = $db->collection('chat_room')->document($userId . '_' . $tentorId);
            $docSnap = $docRef->snapshot()->data();
            if (empty($docSnap)) {
                $singleData = [
                    'lastUpdate' => $now,
                    'message' => $msg,
                    'memberId' => $userId,
                    'tentorId' => $tentorId,
                    'senderIndex' => 0,
                    'recipientIndex' => 1,
                    'member' => 'receive',
                    'tentor' => 'send'
                ];
                $docRef->set($singleData);
            } else {
                $singleData = [
                    ['path' => 'lastUpdate', 'value' => $now],
                    ['path' => 'message', 'value' => $msg],
                    ['path' => 'member', 'value' => 'receive'],
                    ['path' => 'tentor', 'value' => 'send'],
                    ['path' => 'recipientIndex', 'value' => $docRef->snapshot()['recipientIndex'] + 1]
                ];
                $docRef->update($singleData);
            }
            $docRef->collection('room')
                ->document($docId)->set([
                    'createdAt' => $now,
                    'message' => $msg,
                    'memberId' => $userId,
                    'tentorId' => $tentorId,
                    'member' => 'receive',
                    'tentor' => 'send'
                ]);
            $tentorName = $this->getUser()->getTentorProfile()->getFullName();
            /** @var User $member */
            $member = $this->em->getRepository(User::class)->find($userId);
            $memberToken = $member->getAppFcmToken();
            $notification = GenFirebase::SendNotification($memberToken, $tentorName, $msg, [
                'type' => 'chat',
                'name' => $tentorName
            ]);
            $code = 200;
            $response = 'success';
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Send Message ' . $err->getMessage()
            ];
        }
        return GenBasic::send($code, $response);
    }
}
<?php


namespace App\Controller\Api\v2\Publish;


use App\Controller\BaseController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class LevelController extends BaseController
{

    private $utility;
    /**
     * LevelController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Api\v2\UtilityController($this->em);
        header('Access-Control-Allow-Origin: http://localhost:3000');
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api-public/v2/level/category/by/{id}", methods="GET|HEAD")
     */
    public function getLevelByCategory($id)
    {
        return $this->utility->getLevelByCategory($id);
    }
}
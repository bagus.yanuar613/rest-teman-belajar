<?php


namespace App\Controller\Api\v2\Publish;


use App\Controller\BaseController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DistrictController extends BaseController
{

    private $utility;
    /**
     * DistrictController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Api\v2\UtilityController($this->em);
        header('Access-Control-Allow-Origin: http://localhost:3000');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api-public/v2/district", methods="GET|HEAD")
     */
    public function getDistrictByName()
    {
        $name = $this->req->query->get('name');
        return $this->utility->getDistrictByName($name);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api-public/v2/district/by/{id}", methods="GET|HEAD")
     */
    public function getDistrictById($id)
    {
        return $this->utility->getDistrictById($id);
    }
}
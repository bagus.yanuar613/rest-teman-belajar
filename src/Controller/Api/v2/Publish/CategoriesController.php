<?php


namespace App\Controller\Api\v2\Publish;


use App\Controller\BaseController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CategoriesController
 * @package App\Controller\Api\v2\Publish
 */
class CategoriesController extends BaseController
{

    private $utility;

    /**
     * CategoriesController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Api\v2\UtilityController($this->em);
        header('Access-Control-Allow-Origin: http://localhost:3000');
        header('Content-Type: application/json');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api-public/v2/categories/reference", methods="GET|HEAD")
     */
    public function getReferenceCategory()
    {
        return $this->utility->getReferenceCategory();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api-public/v2/categories/popular", methods="GET|HEAD")
     */
    public function getPopularCategories()
    {
        return $this->utility->getPopularCategories();
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api-public/v2/categories/by/{id}", methods="GET|HEAD")
     */
    public function getCategoryById($id)
    {
        return $this->utility->getCategoryById($id);
    }
}
<?php


namespace App\Controller\Api\v2;


use App\Common\GenBasic;
use App\Controller\QueryController;
use App\Entity\Categories;
use App\Entity\CategoryToParentToLevel;
use App\Entity\District;
use App\Repository\CategoriesRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class UtilityController extends QueryController
{
    /**
     * UtilityController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    //Categories
    public function getReferenceCategory()
    {
        try {
            $categories = $this->em->getRepository(Categories::class)->findOneBy(['isReference' => true]);
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($categories, [
                    'id',
                    'name',
                    'slug'
                ])
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Reference Categories ' . $e->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Reference Categories ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    public function getPopularCategories()
    {
        try {
            $categories = $this->em->getRepository(Categories::class)->findBy(['isPopular' => true]);
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($categories, [
                    'id',
                    'name',
                    'slug'
                ])
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Reference Categories ' . $e->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Reference Categories ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    public function getCategoryById($id)
    {
        try {
            $categories = $this->em->getRepository(Categories::class)->find($id);
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($categories, [
                    'id',
                    'name',
                    'slug'
                ])
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Category ' . $e->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Category ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    //Level
    public function getLevelByCategory($id)
    {
        try {
            /** @var ServiceEntityRepository $repo */
            $repo = $this->em->getRepository(CategoryToParentToLevel::class);
            $level = $repo->createQueryBuilder('p')
                ->innerJoin('p.category', 'c')
                ->innerJoin('p.level', 'l')
                ->where('c.id = :id')
                ->setParameter('id', $id)
                ->groupBy('l.id')
                ->getQuery()->getResult();
            $code = 200;
            $response = [
              'msg' => 'Success',
              'data' => GenBasic::CustomNormalize($level, [
                  'id',
                  'level' => [
                      'id',
                      'name'
                  ]
              ])
            ];
        }catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Level ' . $e->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Level ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    //District
    public function getDistrictByName($name)
    {
        try {

            /** @var ServiceEntityRepository $repo */
            $repo = $this->em->getRepository(District::class);
            $district = $repo->createQueryBuilder('d')
                ->where('d.districtName LIKE :name')
                ->setParameter('name', '%' . $name . '%')
                ->getQuery()->getResult();
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($district, [
                    'id',
                    'districtName',
                    'subCity' => [
                        'name',
                        'province' => [
                            'provinceName'
                        ]
                    ],
                    'slug'
                ])
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Reference Categories ' . $e->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Reference Categories ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    public function getDistrictById($id)
    {
        try {
            $district = $this->em->getRepository(District::class)->find($id);
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($district, [
                    'id',
                    'districtName',
                    'slug',
                    'subCity' => [
                        'name'
                    ]
                ])
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch District ' . $e->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize District ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
<?php


namespace App\Controller\Api\Publish;


use App\Common\GenBasic;
use App\Common\TestNormalize;
use App\Controller\BaseController;
use App\Entity\Categories;
use App\Entity\City;
use App\Entity\District;
use App\Entity\Grade;
use App\Entity\Level;
use App\Entity\Method;
use App\Entity\Pricing;
use App\Entity\Rating;
use App\Entity\Schedule;
use App\Entity\Subject;
use App\Entity\Tentor;
use App\Entity\TentorSubject;
use App\Entity\User;
use App\Repository\CategoriesRepository;
use App\Repository\SubjectRepository;
use App\Repository\TentorSubjectRepository;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;


/**
 * Class SubjectController
 * @package App\Controller\Api\Publish
 * @Route("/api-public/subject")
 */
class SubjectController extends BaseController
{

    private $utility;
    /**
     * SubjectController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/find-tentor", methods="GET|HEAD")
     */
    public function findTentor()
    {
            $category = $this->req->query->get('category') ?? null;
            $method = $this->req->query->get('method') ?? null;
            $district = $this->req->query->get('district') ?? null;
            $level = $this->req->query->get('level') ?? null;
            $limit = $this->req->query->get('limit') ?? 5;
            $offset = $this->req->query->get('offset') ?? 0;
            $data = [
                'category' => $category,
                'method' => $method,
                'district' => $district,
                'level' => $level
            ];
            return $this->utility->findTentor($data, $offset, $limit);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/auto-find-tentor", methods="GET|HEAD")
     */
    public function autoFindTentor()
    {
        try {
            $category = $this->req->query->get('category') ?? null;
            $method = $this->req->query->get('method') ?? null;
            $meet = $this->req->query->get('meet') ?? null;
            $district = $this->req->query->get('district') ?? null;
            $level = $this->req->query->get('level') ?? null;
            dump($this->req->query->all());
            die();

            $timestamp = strtotime($meet);
            $day = date('w', $timestamp) - 1;
            $hour = date('H:i:s', $timestamp);

            if ($category === null || $level === null || ($method === '1' && $district === null)) {
                return GenBasic::send(500, 'Wrong Parameter!');
            }
            /** @var TentorSubjectRepository $ts */
            $ts = $this->em->getRepository(TentorSubject::class);
            $qb = $ts->createQueryBuilder('ts')
                ->addSelect([
                    'p.price as price',
                    'COUNT(t.id) as transactionQty'
                ])
                ->innerJoin('ts.user', 'u')
                ->innerJoin('u.tentorProfile', 'tp')
                ->leftJoin('ts.method', 'm')
                ->leftJoin('u.district', 'ds')
                ->leftJoin('u.ratings', 'r')
                ->leftJoin('u.schedule', 's')
                ->leftJoin('ts.transaction', 't', Join::WITH, 't.status = 1')
                ->leftJoin('t.schedule', 'tc', Join::WITH, '(tc.day != :day) and (tc.time != :time)')
                ->where('ts.isAvailable = :status')
                ->andWhere('tp.isActive = :active')
                ->andWhere('s.day = :day')
                ->andWhere('s.time = :time')
                ->setParameter('status', true)
                ->setParameter('active', true)
                ->setParameter('day', $day)
                ->setParameter('time', $hour)
                ->groupBy('t.id')
                ->addGroupBy('ts.id')
                ->addOrderBy('COUNT(t.id)', 'ASC');


            if ($category !== null) {
                $qb->andWhere('ts.category = :category')->setParameter('category', $category);
            }
            if ($level !== null) {
                $qb->andWhere('ts.level = :level')->setParameter('level', $level);
            }
            if ($method !== null) {
                $vMethod = [$method];
                $qb->andWhere('m.id IN (:method)')->setParameter('method', $vMethod);
            }
            if ($district !== null) {
                $v = [$district];
                $qb->andWhere('ds.id IN(:district)')->setParameter('district', $v);
            }

            $city = 'is null';
            if ($method === '1') {
                $city = '= ds.city';
            }
            $qb->innerJoin(Pricing::class, 'p', Join::WITH, 'p.level = ts.level and p.grade = tp.grade and p.city ' . $city . ' and p.method = :mid');
            $qb->setParameter('mid', $method);
            $tempResults = $qb
                ->setFirstResult(0)
                ->setMaxResults(1)
                ->getQuery()
                ->getResult();
            $attributes = [
                'id',
                'slug',
                'user' => [
                    'tentorProfile' => [
                        'fullName',
                        'about',
                        'slugName',
                        'generatedAvatar',
                        'grade' => ['id', 'name']
                    ],
                    'email',
                    'district' => ['id', 'districtName'],
                    'avgRatings',
                    'schedule' => [
                        'id',
                        'day',
                        'time'
                    ]
                ],
                'transaction' => [
                    'id'
                ]
            ];
            $code = 200;
            $response = GenBasic::CustomNormalize($tempResults, $attributes);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data Level Subject ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Data Level Subject ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/find-by-name", methods="GET|HEAD")
     */
    public function findSubject()
    {
        try {
            $name = $this->req->query->get('name');
            /** @var CategoriesRepository $subject */
            $subject = $this->em->getRepository(Categories::class);
            $results = $subject->createQueryBuilder('c')
                ->select([
                    'c.name',
                    'c.id as categoryId',
                ])
                ->where('c.name LIKE :name')
                ->setParameter('name', '%' . $name . '%')
                ->getQuery()
                ->getScalarResult();
            $code = 200;
            $response = [
                'msg' => 'Success Fetch Subject!',
                'data' => $results
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Load Subject' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
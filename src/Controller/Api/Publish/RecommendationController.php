<?php


namespace App\Controller\Api\Publish;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Categories;
use App\Entity\District;
use App\Entity\MemberClass;
use App\Entity\Method;
use App\Entity\Pricing;
use App\Entity\TentorSubject;
use App\Repository\TentorSubjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class RecommendationController extends BaseController
{

    /**
     * RecommendationController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/recommendation/data", methods="GET|HEAD")
     */
    public function getRecommendation()
    {
        try {
            $category = $this->req->query->get('category') ?? null;
            $method = $this->req->query->get('method') ?? null;
            $district = $this->req->query->get('district') ?? null;
            $level = $this->req->query->get('level') ?? null;
            $limit = $this->req->query->get('limit') ?? 5;
            $offset = $this->req->query->get('offset') ?? 0;

            if ($category === null || $level === null || ($method === '1' && $district === null)) {
                return GenBasic::send(500, 'Wrong Parameter!');
            }
            /** @var TentorSubjectRepository $ts */
            $ts = $this->em->getRepository(TentorSubject::class);
            $qb = $ts->createQueryBuilder('ts')
                ->addSelect('p.price as price')
                ->innerJoin('ts.user', 'u')
                ->innerJoin('u.tentorProfile', 'tp')
                ->leftJoin('ts.method', 'm')
                ->leftJoin('u.district', 'ds')
                ->leftJoin('u.ratings', 'r')
                ->where('ts.isAvailable = :status')
                ->andWhere('tp.isActive = :active')
                ->setParameter('status', true)
                ->setParameter('active', true)
                ->groupBy('ts.id')
                ->orderBy('AVG(r.rating)', 'DESC');

            if ($category !== null) {
                $qb->andWhere('ts.category = :category')->setParameter('category', $category);
            }
            if ($level !== null) {
                $qb->andWhere('ts.level = :level')->setParameter('level', $level);
            }
            if ($method !== null) {
                $vMethod = [$method];
                $qb->andWhere('m.id IN (:method)')->setParameter('method', $vMethod);
            }
            if ($district !== null) {
                $v = [$district];
                $qb->andWhere('ds.id IN(:district)')->setParameter('district', $v);
            }

            $city = 'is null';
            if ($method === '1') {
                $city = '= ds.city';
            }
            $qb->innerJoin(Pricing::class, 'p', Join::WITH, 'p.level = ts.level and p.grade = tp.grade and p.city ' . $city . ' and p.method = :mid');
            $qb->setParameter('mid', $method);
            $tempResults = $qb
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult();
            $attributes = [
                'id',
                'slug',
                'user' => [
                    'tentorProfile' => [
                        'fullName',
                        'about',
                        'slugName',
                        'generatedAvatar',
                        'grade' => ['id', 'name']
                    ],
                    'email',
                    'district' => ['id', 'districtName'],
                    'avgRatings'
                ],
            ];
            $code = 200;
            $response = GenBasic::CustomNormalize($tempResults, $attributes);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data Level Subject ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Data Level Subject ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);

    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/member/recommendation/data", methods="GET|HEAD")
     */
    public function getRecommendationWeb()
    {
        try {
            $category = $this->req->query->get('category') ?? null;
            $method = $this->req->query->get('method') ?? null;
            $district = $this->req->query->get('district') ?? null;
            $level = $this->req->query->get('level') ?? null;
            $limit = $this->req->query->get('limit') ?? 5;
            $offset = $this->req->query->get('offset') ?? 0;

            if ($category === null || $level === null || ($method === '1' && $district === null)) {
                return GenBasic::send(500, 'Wrong Parameter!');
            }
            /** @var TentorSubjectRepository $ts */
            $ts = $this->em->getRepository(TentorSubject::class);
            $qb = $ts->createQueryBuilder('ts')
                ->addSelect('p.price as price')
                ->innerJoin('ts.user', 'u')
                ->innerJoin('u.tentorProfile', 'tp')
                ->leftJoin('ts.method', 'm')
                ->leftJoin('u.district', 'ds')
                ->leftJoin('u.ratings', 'r')
                ->where('ts.isAvailable = :status')
                ->andWhere('tp.isActive = :active')
                ->setParameter('status', true)
                ->setParameter('active', true)
                ->groupBy('ts.id')
                ->orderBy('AVG(r.rating)', 'DESC');

            if ($category !== null) {
                $qb->andWhere('ts.category = :category')->setParameter('category', $category);
            }
            if ($level !== null) {
                $qb->andWhere('ts.level = :level')->setParameter('level', $level);
            }
            if ($method !== null) {
                $vMethod = [$method];
                $qb->andWhere('m.id IN (:method)')->setParameter('method', $vMethod);
            }
            if ($district !== null) {
                $v = [$district];
                $qb->andWhere('ds.id IN(:district)')->setParameter('district', $v);
            }

            $city = 'is null';
            if ($method === '1') {
                $city = '= ds.city';
            }
            $qb->innerJoin(Pricing::class, 'p', Join::WITH, 'p.level = ts.level and p.grade = tp.grade and p.city ' . $city . ' and p.method = :mid');
            $qb->setParameter('mid', $method);
            $tempResults = $qb
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult();
            $attributes = [
                'id',
                'slug',
                'user' => [
                    'tentorProfile' => [
                        'fullName',
                        'about',
                        'slugName',
                        'generatedAvatar',
                        'grade' => ['id', 'name'],
                        'slugName'
                    ],
                    'email',
                    'district' => ['id', 'districtName'],
                    'avgRatings'
                ],
            ];
            $code = 200;
            $response = GenBasic::CustomNormalize($tempResults, $attributes);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data Level Subject ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Data Level Subject ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);

    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api/member/related-categories", methods="GET|HEAD")
     */
    public function getRelatedCategories()
    {
        try {
            if (!$this->getUser()) {
                return GenBasic::send(202, 'Unauthenticated!');
            }
            /** @var MemberClass $class */
            $class = $this->getUser()->getMemberProfile()->getClass();
            $categories = $class->getCategory();
            $code = 200;
            $response = [
                'msg' => 'Success Fetch Recommendation!',
                'data' => GenBasic::CustomNormalize($categories, [
                    'id',
                    'name'
                ])
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Get Related Categories ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Related Categories ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/member/related-categories", methods="GET|HEAD")
     */
    public function getRelatedCategoriesWeb()
    {
        try {
            if (!$this->getUser()) {
                return GenBasic::send(202, 'Unauthenticated!');
            }
            /** @var MemberClass $class */
            $class = $this->getUser()->getMemberProfile()->getClass();
            $categories = $class->getCategory();
            $code = 200;
            $response = [
                'msg' => 'Success Fetch Recommendation!',
                'data' => GenBasic::CustomNormalize($categories, [
                    'id',
                    'name'
                ])
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Get Related Categories ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Related Categories ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
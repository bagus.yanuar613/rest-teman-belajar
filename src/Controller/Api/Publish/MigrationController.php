<?php


namespace App\Controller\Api\Publish;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Member;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Component\HttpClient\HttpClient;

class MigrationController extends BaseController
{

    /**
     * MigrationController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api-public/migration/user", methods="GET|HEAD")
     */
    public function getFromClient()
    {
        try {
            $httpClient = HttpClient::create();
            $res = $httpClient->request('GET', 'http://localhost:8000/user');
            $code = $res->getStatusCode();
            $temp = json_decode($res->getContent(), true);
            $payload = $temp['payload'];
            foreach ($payload as $key => $v) {
                $email = $v['email'];
                $password = $v['password'];
                $source = $v['source'];
                $ava = $v['ava'];
                $fullName = $v['memberProfile'] === null ? 'Siswa Teman Belajar' : $v['memberProfile']['fullname'];
                $phone = $v['memberProfile'] === null ? null : $v['memberProfile']['phone'];
                $school = $v['memberProfile'] === null ? null : $v['memberProfile']['school'];
                $fullAddress = $v['memberProfile'] === null ? null : $v['memberProfile']['fullAddress'];
                $class = null;
                $provider = null;
                switch ($source) {
                    case 0:
                        $provider = 'teman-belajar';
                        break;
                    case 1:
                        $provider = 'facebook';
                        break;
                    case 2:
                        $provider = 'google';
                        break;
                    default:
                        break;
                }
                $user = new User();
                $user->setEmail($email)
                    ->setRoles(['ROLE_MEMBER'])
                    ->setPassword($password)
                    ->setIsEmailVerified(true)
                    ->setProvider($provider);
                $memberProfile = new Member();
                $memberProfile->setFullName($fullName)
                    ->setPhone($phone)
                    ->setClass(null)
                    ->setSchool($school)
                    ->setDistrict(null)
                    ->setAddress($fullAddress)
                    ->setAvatar($ava)
                    ->setIsActive(true)
                    ->setParentName(null)
                    ->setParentPhone(null)
                    ->setDeviceID(null)
                    ->setDeviceModel(null)
                    ->setGender(null)
                    ->setDateOfBirth(null)
                    ->setMedal(0)
                    ->setCoin(0);
                $memberProfile->setUser($user);
                $user->setMemberProfile($memberProfile);
                $this->em->persist($user);
                $this->em->persist($memberProfile);
            }
            $this->em->flush();
            $response = 'Success';
//            $response = $payload;
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed send message' . $e->getMessage(),
            ];
        } catch (TransportExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed Transport ' . $e->getMessage(),
            ];
        } catch (ClientExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed ClientExceptionInterface ' . $e->getMessage(),
            ];
        } catch (RedirectionExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed RedirectionExceptionInterface ' . $e->getMessage(),
            ];
        } catch (ServerExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed ServerExceptionInterface ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
<?php
declare(strict_types=1);

namespace App\Controller\Api\Publish;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Categories;
use App\Entity\CategoriesParent;
use App\Entity\Subject;
use App\Repository\CategoriesParentRepository;
use App\Repository\CategoriesRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CategoriesController
 * @package App\Controller\Api\Publish
 * @Route("/api-public/categories")
 */
class CategoriesController extends BaseController
{
    /**
     * CategoriesController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/default", methods="GET|HEAD")
     */
    public function getDefaultCategories()
    {
        try {
            /** @var CategoriesRepository $categories */
            $categories = $this->em->getRepository(Categories::class);
            $results = $categories->createQueryBuilder('c')
                ->select([
                    'c.id',
                    'c.name',
                    'c.slug'
                ])
                ->where('c.isDefault = :default')
                ->setParameter('default', true)
                ->getQuery()->getScalarResult();
            $code = 200;
            $response = GenBasic::serializeToJson($results);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data Categories ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/popular", methods="GET|HEAD")
     */
    public function getPopularCategories()
    {
        try {
            /** @var CategoriesRepository $categories */
            $categories = $this->em->getRepository(Categories::class);
            $results = $categories->createQueryBuilder('c')
                ->select([
                    'c.id',
                    'c.name',
                    'c.slug'
                ])
                ->where('c.isPopular = :popular')
                ->setParameter('popular', true)
                ->getQuery()->getScalarResult();
            $code = 200;
            $response = GenBasic::serializeToJson($results);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data Categories ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }


    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/name", methods="GET|HEAD")
     */
    public function getCategoryByName()
    {
        try {
            $param = $this->req->query->get('name');
            /** @var CategoriesRepository $categories */
            $categories = $this->em->getRepository(Categories::class);
            $results = $categories->createQueryBuilder('c')
                ->select([
                    'c.id',
                    'c.name',
                    'c.slug'
                ])
                ->where('c.name LIKE :name')
                ->andWhere('c.isTryOut = :tryout')
                ->setParameter('name', '%' . $param . '%')
                ->setParameter('tryout', false)
                ->orderBy('c.isPopular', 'DESC')
                ->getQuery()->getScalarResult();
            $code = 200;
            $response = GenBasic::serializeToJson($results);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data Categories ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/reference", methods="GET|HEAD")
     */
    public function getReferenceCategories()
    {
        try {
            /** @var CategoriesRepository $categories */
            $categories = $this->em->getRepository(Categories::class);
            $results = $categories->createQueryBuilder('c')
                ->select([
                    'c.id',
                    'c.name',
                    'c.slug'
                ])
                ->where('c.isReference = :isReference')
                ->setParameter('isReference', true)
                ->getQuery()->getScalarResult();
            if (!$results) {
                return GenBasic::send(202, 'Reference Category Not Found!');
            }
            $code = 200;
            $response = GenBasic::serializeToJson($results[0]);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data Categories ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

}
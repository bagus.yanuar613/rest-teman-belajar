<?php


namespace App\Controller\Api\Publish;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\District;
use App\Repository\DistrictRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CityController
 * @package App\Controller\Api\Publish
 * @Route("/api-public/cities")
 */
class CityController extends BaseController
{

    /**
     * CityController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/district", methods="GET|HEAD")
     */
    public function getDistrict()
    {
        try {
            $name = $this->req->query->get('name');
            /** @var DistrictRepository $district */
            $district = $this->em->getRepository(District::class);
            $results = $district->createQueryBuilder('d')
                ->select([
                    'd.id',
                    'd.districtName as district',
                    'c.cityName as city',
                    'p.provinceName as province'
                ])
                ->innerJoin('d.city', 'c')
                ->innerJoin('c.province', 'p')
                ->where('d.districtName LIKE :name')
                ->setParameter('name', '%' . $name . '%')
                ->getQuery()->getScalarResult();
            $code = 200;
            $response = GenBasic::serializeToJson($results);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data District ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @param $city
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/district/{city}", methods="GET|HEAD")
     */
    public function getDistrictByCity($city)
    {
        try {

            /** @var DistrictRepository $district */
            $district = $this->em->getRepository(District::class);
            $qb = $district->createQueryBuilder('d')
                ->select([
                    'd.id',
                    'd.districtName as name'
                ])
                ->innerJoin('d.city', 'c')
                ->where('c.id = :city')
                ->setParameter('city', $city)
                ->getQuery()->getScalarResult();

            $code = 200;
            $response = GenBasic::serializeToJson($qb);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data District ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
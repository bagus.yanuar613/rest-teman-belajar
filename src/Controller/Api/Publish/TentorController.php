<?php


namespace App\Controller\Api\Publish;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Rating;
use App\Entity\Schedule;
use App\Entity\Tentor;
use App\Entity\User;
use App\Repository\RatingRepository;
use App\Repository\TentorRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class TentorController
 * @package App\Controller\Api\Publish
 * @Route("/api-public/tentor")
 */
class TentorController extends BaseController
{

    /**
     * TentorController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/schedule", methods="GET|HEAD")
     */
    public function getSchedule()
    {
        try {
            /** @var UserRepository $user */
            $user = $this->em->getRepository(User::class);
            $results = $user->createQueryBuilder('u')
                ->select([
                    'u.id',
                    's.day',
                    's.time',
                ])
                ->leftJoin('u.schedule', 's')
                ->where('u.id = :id')
                ->setParameter('id', 1)
                ->getQuery()->getScalarResult();
            $code = 200;
            $response = GenBasic::serializeToJson($results);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data Schedule ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @param $slugName
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/reviews/{slugName}", methods="GET|HEAD")
     */
    public function getReviews($slugName)
    {
        try {
            /** @var RatingRepository $ratings */
            $ratings = $this->em->getRepository(Rating::class);
            $qb = $ratings->createQueryBuilder('r')
                ->select([
                    'up.fullName as name',
                    'up.avatar',
                    'r.rating',
                    'r.review'
                ])
                ->innerJoin('r.tentor', 't')
                ->innerJoin('t.tentorProfile', 'tp')
                ->innerJoin('r.user', 'u')
                ->innerJoin('u.memberProfile', 'up')
                ->where('tp.slugName = :slugName')
                ->setParameter('slugName', $slugName)
                ->setFirstResult(0)
                ->setMaxResults(3)
                ->orderBy('r.rating', 'DESC')
                ->getQuery()
                ->getScalarResult();
            $code = 200;
            $response = GenBasic::serializeToJson($qb);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data Tentor ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/reviews-sample", methods="GET|HEAD")
     */
    public function getSampleReview()
    {
        try {
            /** @var RatingRepository $reviews */
            $reviews = $this->em->getRepository(Rating::class);
            $results = $reviews->createQueryBuilder('r')
                ->setFirstResult(0)
                ->setMaxResults(3)
                ->getQuery()
                ->getResult();
            $code = 200;
            $response = GenBasic::CustomNormalize($results,
                    [
                        'user' => [
                            'memberProfile' => [
                                'fullName',
                                'generatedAvatar'
                            ]
                        ],
                        'rating',
                        'review'
                    ]
                );
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data Reviews ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Data Reviews ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/schedule-by-day", methods="GET|HEAD")
     */
    public function getScheduleByDay()
    {
        try {
            $u_id = $this->req->query->get('u_id');
            $d_id = $this->req->query->get('d_id');
            /** @var UserRepository $user */
            $user = $this->em->getRepository(User::class);
            $results = $user->createQueryBuilder('u')
                ->select([
                    'u.id',
                    's.day',
                    's.time',
                ])
                ->leftJoin('u.schedule', 's')
                ->where('u.id = :id')
                ->andWhere('s.day = :day')
                ->setParameter('id', $u_id)
                ->setParameter('day', $d_id)
                ->getQuery()->getScalarResult();
            $code = 200;
            $response = $results;
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data Schedule ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
<?php


namespace App\Controller\Api\Publish;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Bank;
use App\Entity\Tentor;
use App\Entity\User;
use App\Repository\BankRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class BankController extends BaseController
{

    /**
     * BankController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api-public/bank", methods="GET|HEAD")
     */
    public function getListBank()
    {
        try {
            $name = $this->req->query->get('name');
            /** @var BankRepository $bankRepo */
            $bankRepo = $this->em->getRepository(Bank::class);
            $bank = $bankRepo->createQueryBuilder('b')
                ->where('b.name LIKE :name')
                ->setParameter('name', '%' . $name . '%')
                ->getQuery()
                ->getResult();
            $code = 200;
            $response =
                [
                    'msg' => 'Success Fetch Bank List!',
                    'data' => GenBasic::CustomNormalize($bank,
                        [
                            'id',
                            'code',
                            'name',
                            'icon'
                        ]
                    )
                ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Bank List!' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Bank List! ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
<?php


namespace App\Controller\Api\Publish;


use App\Common\GenBasic;
use App\Common\GenFirebase;
use App\Controller\BaseController;
use App\Entity\Cart;
use App\Entity\Promo;
use App\Entity\Subscription;
use App\Entity\Transaction;
use App\Repository\PromoRepository;
use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class PromoController
 * @package App\Controller\Web\Siswa
 */
class PromoController extends BaseController
{

    /**
     * PromoController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }


    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route ("/api-public/promo", methods="GET|HEAD")
     */
    public function getPromo()
    {
        try {
            /** @var PromoRepository $promo */
            $promo = $this->em->getRepository(Promo::class);
            $qb = $promo->createQueryBuilder('p')
                ->select([
                    'p.title',
                    'p.image',
                    'p.url',
                ])
                ->where('p.isActive = :status')
                ->setParameter('status', true)
                ->getQuery()->getScalarResult();
            $code = 200;
            $response = GenBasic::serializeToJson($qb);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data Categories ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route ("/api-public/test-fcm", methods="POST|HEAD")
     */
    public function cekMessagingFB()
    {
        try {
            $data = [
                'type' => 'Konfirmasi',
                'method' => 2
            ];
//            $token = 'do76ksiOxVa8YVLLA2sSeq:APA91bFTG70z-aLg8uvtyEVvACcK16oJqtBZmOkYqACIdect4hP8n-5am762q33As6X8_76h4228DC7qpnLO5IsRvtfEEZa0cNAebg-z5uo4X-mCKIXMuwpIWmyU2AhuPbNjnwD3K3ln';
            $token = 'dSVheEbbLAXMZngSELQ0kx:APA91bFHgkbrD2Zyr9cvs_ah88534SgrvsEhOT1pIJQ5uGvJQ6wY5p6VV98GRWDzJ-aCIPbSlSfiK0p7OZklatlpmymNmGFJ-rxQgCMf52xIV8lAHt27vJ2FMPZKZuqHLoMuCDCdBBR3';
            $message = 'Hai, Guru Mu Menelpon Ulang Silahkan Konfirmasi.';
            $notification = GenFirebase::SendNotification($token, 'Memulai Pelajaran (TEMAN BELAJAR)', $message, $data);
            $code = 200;
            $response = $notification;
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed send message' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api-public/admin-confirm", methods="POST|HEAD")
     */
    public function adminConfirmPayment()
    {
        try {
            //status Accept = true, Reject = False
            $id = GenBasic::request($this->req)['id'];
            $status = GenBasic::request($this->req)['status'];
            /** @var TransactionRepository $transRepo */
            $transRepo = $this->em->getRepository(Transaction::class);
            /** @var Transaction $transaction */
            $transaction = $transRepo->createQueryBuilder('t')
                ->where('t.isPaid is null')
                ->andWhere('t.confirmationInfo is not null')
                ->andWhere('t.id = :id')
                ->setParameter('id', $id)
                ->getQuery()->getOneOrNullResult();
            if (!$transaction) {
                return GenBasic::send(202, ['Transaction Not Found!']);
            }
            if ($status === true) {
                $transaction->setIsPaid(true);
            } else {
                $transaction->setIsPaid(false);
            }
            if ($transaction->getSlug() === 'cart') {
                /** @var Cart $cart */
                foreach ($transaction->getCart() as $cart) {
                    if ($status === true) {
                        /** @var Subscription $subscription */
                        $subscription = $cart->getSubscription();
                        $cart->setIsActive(true);
                        $encounter = $cart->getEncounter();
                        $currentRemains = $subscription->getRemains();
                        $current = $encounter + $currentRemains;
                        if ($subscription->getStatus() === 9) {
                            foreach ($subscription->getAttendancePlan() as $v) {
                                $v->setStatus(1);
                            }
                        }
                        $subscription->setStatus(1);
                        $subscription->setRemains($current);
                    } else {
                        $cart->setIsActive(false);
                        if ($cart->getSubscription()->getStatus() === 0) {
                            $cart->getSubscription()->setStatus(6);
                        }
                    }
                }
            }
            $this->em->flush();
            $code = 200;
            $response = [
                'msg' => 'Success'
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed send message' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/api-public/admin-confirm-reset", methods="POST|HEAD")
     */
    public function adminResetConfirmPayment()
    {
        try {
            //status Accept = true, Reject = False
            $id = GenBasic::request($this->req)['id'];
            $status = GenBasic::request($this->req)['status'];
            /** @var TransactionRepository $transRepo */
            $transRepo = $this->em->getRepository(Transaction::class);
            /** @var Transaction $transaction */
            $transaction = $transRepo->createQueryBuilder('t')
                ->where('t.id = :id')
                ->setParameter('id', $id)
                ->getQuery()->getOneOrNullResult();
            if (!$transaction) {
                return GenBasic::send(202, ['Transaction Not Found!']);
            }
            if ($status === true) {
                $transaction->setIsPaid(true);
            } else {
                $transaction->setIsPaid(false);
            }
            if ($transaction->getSlug() === 'cart') {
                /** @var Cart $cart */
                foreach ($transaction->getCart() as $cart) {
                    if ($status === true) {
                        $cart->setIsActive(true);
                        $encounter = $cart->getEncounter();
                        $currentRemains = $cart->getSubscription()->getRemains();
                        $current = $encounter + $currentRemains;
                        $cart->getSubscription()->setStatus(1);
                        $cart->getSubscription()->setRemains($current);
                    } else {
                        $cart->setIsActive(false);
                        if ($cart->getSubscription()->getStatus() === 0) {
                            $cart->getSubscription()->setStatus(6);
                        }
                    }
                }
            }
            $this->em->flush();
            $code = 200;
            $response = [
                'msg' => 'Success'
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed send message' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }




}
<?php

namespace App\Controller\Auth;

use App\Controller\BaseController;
use App\Entity\Member;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends BaseController
{

    private $utility;
    /** @var UserPasswordEncoderInterface */
    protected $encoder;

//    /** @var EntityManagerInterface */
//    protected $em;

    /**
     * LoginController constructor.
     *
     * @param UserPasswordEncoderInterface $encoder
     * @param EntityManagerInterface $em
     */
    public function __construct(UserPasswordEncoderInterface $encoder, EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->encoder = $encoder;
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }

    /**
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     * @Route("/login", name="login")
     */
    public function loginPage(AuthenticationUtils $authenticationUtils)
    {
        $this->denyAccessUnlessGranted('IS_ANONYMOUS');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastEmail = $authenticationUtils->getLastUsername();
        return $this->render('siswa/auth/login.html.twig', ['last_email' => $lastEmail, 'error' => $error]);
    }

    /**
     * @return Response
     * @Route("/login-tentor", name="login_tentor")
     */
    public function loginTentorPage()
    {
        $this->denyAccessUnlessGranted('IS_ANONYMOUS');
        return $this->render('siswa/auth/DaftarGuru.html.twig');
    }

    /**
     * Link to this controller to start the "connect" process
     *
     * @Route("/connect/google", name="connect_google")
     * @param ClientRegistry $clientRegistry
     * @param Request $req
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function connectAction(ClientRegistry $clientRegistry, Request $req)
    {
        $role = $req->query->get('role');
        $fcm = $req->query->get('fcm');
        $provider = $req->query->get('provider');
        if (!$req->query->has('role') || !$req->query->has('fcm') || !$req->query->has('provider')) {
            return $this->redirect('/');
        }
        $session = new Session();
        $session->set('role', $role);
        $session->set('fcm', $fcm);
        $session->set('provider', $provider);
        return $clientRegistry
            ->getClient('google')
            ->redirect(['profile', 'email']);
    }

    /**
     * Link to this controller to start the "connect" process
     *
     * @Route("/connect/facebook", name="connect_facebook_start")
     * @param ClientRegistry $clientRegistry
     * @param Request $req
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function connectFacebookAction(ClientRegistry $clientRegistry, Request $req)
    {
        $role = $req->query->get('role');
        $fcm = $req->query->get('fcm');
        $provider = $req->query->get('provider');
        if (!$req->query->has('role') || !$req->query->has('fcm') || !$req->query->has('provider')) {
            return $this->redirect('/');
        }
        $session = new Session();
        $session->set('role', $role);
        $session->set('fcm', $fcm);
        $session->set('provider', $provider);
        return $clientRegistry
            ->getClient('facebook_main') // key used in config/packages/knpu_oauth2_client.yaml
            ->redirect([
                'public_profile', 'email' // the scopes you want to access
            ]);
    }

    /**
     * After going to Google, you're redirected back here
     * because this is the "redirect_route" you configured
     * in config/packages/knpu_oauth2_client.yaml
     *
     * @param Request $request
     * @param ClientRegistry $clientRegistry
     *
     * @Route("/connect/google/check", name="connect_google_check")
     * @return RedirectResponse
     */
    public function connectCheckAction(Request $request, ClientRegistry $clientRegistry)
    {
        $session = new Session();
        $redirect = $session->get('redirectTo');
        if ($session->get('role') === 'member') {
            //cek apakah sudah melengkapi data profil
            if (!$this->utility->checkProfile($this->getUser())) {
                return $this->redirect('/member/profile');
            }
        } elseif ($session->get('role') === 'tentor') {
            $redirect = '/tentor';
        } else {
            $redirect = '/admin';
        }
        if (!$session->has('redirectTo')) {
            $redirect = '/';
        }
        return $this->redirect($redirect);
    }

    /**
     * After going to Google, you're redirected back here
     * because this is the "redirect_route" you configured
     * in config/packages/knpu_oauth2_client.yaml
     *
     * @param Request $request
     * @param ClientRegistry $clientRegistry
     *
     * @Route("/connect/facebook/check", name="connect_facebook_check")
     * @return RedirectResponse
     */
    public function connectFacebookCheckAction(Request $request, ClientRegistry $clientRegistry)
    {
        $session = new Session();
        $redirect = $session->get('redirectTo');
        if ($session->get('role') === 'member') {
            //cek apakah sudah melengkapi data profil
            if (!$this->utility->checkProfile($this->getUser())) {
                return $this->redirect('/member/profile');
            }
        } elseif ($session->get('role') === 'tentor') {
            $redirect = '/tentor';
        } else {
            $redirect = '/admin';
        }
        if (!$session->has('redirectTo')) {
            $redirect = '/';
        }
        return $this->redirect($redirect);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \Exception('Don\'t forget to activate logout in security.yaml');
    }


    /**
     * @return Response
     * @Route("/forgot-password", name="forgot_password")
     */
    public function forgotPassword(\Swift_Mailer $mailer)
    {
        $this->denyAccessUnlessGranted('IS_ANONYMOUS');
        if ($this->req->getMethod() === "POST") {
            $email = $this->req->request->get('email');
            /** @var User $user */
            $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);
            if (!$user) {
                $this->addFlash('failed', 'Email Not Found!');
            }
            $message = (new \Swift_Message('Email'))
                ->setFrom('bagus.yanuar613@gmail.com')
                ->setTo('damn.john88@gmail.com')
                ->setBody(
                    $this->renderView('siswa/auth/lengkapidata1.html.twig', ['param' => bin2hex($email)]), 'text/html'
                );
            $mailer->send($message);
            $this->addFlash('success', 'Hai, Permintaan Reset Password mu Sudah Kami Proses Silahkan Klik Tautan Di Email Yang Sudah Kami Kirimkan');
        }
        return $this->render('siswa/auth/ForgotPassword.html.twig');
    }


    /**
     * @param $code
     * @return RedirectResponse|Response
     * @Route("/reset-password/{code}", name="reset_password")
     */
    public function resetPassword($code, UserPasswordEncoderInterface $encoder)
    {
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findOneBy(['email' => hex2bin($code)]);
        if(!$user){
            return new Response('Forbiden Page!');
        }
        if($this->req->getMethod() === "POST"){
            $password = $this->req->request->get('password');
            $user->setPassword($encoder->encodePassword($user, $password));
            $this->em->flush();
            return $this->redirect('/login');
        }
        return $this->render('siswa/auth/ResetPassword.html.twig');
    }
}
<?php


namespace App\Controller\Auth;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Grade;
use App\Entity\Member;
use App\Entity\Tentor;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Firebase\Auth\Token\Exception\InvalidToken;
use Kreait\Firebase\Auth\UserRecord;
use Kreait\Firebase\Exception\Auth\RevokedIdToken;
use Kreait\Firebase\Exception\AuthException;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Factory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ApiLoginController extends BaseController
{

    protected $auth;
    private $passwordEncoder;

    /**
     * ApiLoginController constructor.
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->passwordEncoder = $passwordEncoder;
    }


    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Kreait\Firebase\Exception\AuthException
     * @throws \Kreait\Firebase\Exception\FirebaseException
     * @Route("/api-login", methods="POST|HEAD")
     */
    public function login()
    {
        try {
            $role = GenBasic::request($this->req)['role'];
            $provider = GenBasic::request($this->req)['provider'];
            $token = GenBasic::request($this->req)['idToken'];
            $fcmtoken = GenBasic::request($this->req)['fcmToken'];
            $deviceId = GenBasic::request($this->req)['deviceId'];
            $deviceModel = GenBasic::request($this->req)['deviceModel'];
            $availableProvider = ['google', 'facebook', 'teman-belajar'];
            if (!in_array($provider, $availableProvider, true)) {
                return GenBasic::send(202, ['msg' => 'Provider ' . $provider . ' Tidak Tersedia']);
            }
            $user = null;
            switch ($role) {
                case 'member':
                    $role = ['ROLE_MEMBER'];
                    break;
                case 'tentor':
                    $role = ['ROLE_TENTOR'];
                    break;
                default:
                    $role = null;
                    break;
            }
            //Auth By Email & Password
            if ($provider === 'teman-belajar') {
                $email = GenBasic::request($this->req)['email'];
                $password = GenBasic::request($this->req)['password'];
                $socialVerify = GenBasic::request($this->req)['socialVerify'] ?? true;
                /** @var User $user */
                $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);

                if (!$user) {
                    return GenBasic::send(202, ['msg' => 'Pengguna Tidak Di Temukan']);
                }

                if ($socialVerify) {
                    $firebaseUser = $this->verifyFirebaseToken($token);
                    if (!$firebaseUser) {
                        return GenBasic::send(202, ['msg' => 'Akun Tidak Terverifikasi']);
                    }
                    if($email !== $firebaseUser->email){
                        return GenBasic::send(202, ['msg' => 'Email Pengguna Tidak Cocok Dengan Email Social']);
                    }
                }

                if ($user->getProvider() !== $provider) {
                    return GenBasic::send(202, ['msg' => 'Akun Sudah Di Gunakan']);
                }
                $isAuth = $this->passwordEncoder->isPasswordValid($user, $password);
                if (!$isAuth) {
                    return GenBasic::send(202, ['msg' => 'Kata Sandi Tidak Cocok']);
                }
                $user->setApiToken($token)
                    ->setAppFcmToken($fcmtoken);
                if ($role[0] === 'ROLE_TENTOR' && in_array('ROLE_TENTOR', $user->getRoles(), true)) {
                    if ($socialVerify) {
                        /** @var Tentor $profile */
                        $profile = $this->em->getRepository(Tentor::class)->findOneBy(['user' => $user->getId()]);
                        $profile->setDeviceID($deviceId)
                            ->setDeviceModel($deviceModel);
                        $this->em->flush();
                        $code = 200;
                        $response = [
                            'msg' => 'Berhasil Melanjutkan Sebagai Guru',
                            'data' => [
                                'email' => $user->getEmail(),
                                'name' => $user->getTentorProfile()->getFullName(),
                                'avatar' => $user->getTentorProfile()->getAvatar(),
                                'token' => $token,
                                'fcmToken' => $fcmtoken
                            ]
                        ];
                    } else {
                        $code = 200;
                        $response = [
                            'msg' => 'Autentikasi Berhasil Email dan Password Cocok'
                        ];
                    }
                } elseif ($role[0] === 'ROLE_MEMBER' && in_array('ROLE_MEMBER', $user->getRoles(), true)) {
                    if ($socialVerify) {
                        /** @var Member $profile */
                        $profile = $this->em->getRepository(Member::class)->findOneBy(['user' => $user->getId()]);
                        $profile->setDeviceID($deviceId)
                            ->setDeviceModel($deviceModel);
                        $this->em->flush();
                        $code = 200;
                        $response = [
                            'msg' => 'Berhasil Melanjutkan Sebagai Member',
                            'data' => [
                                'email' => $user->getEmail(),
                                'name' => $user->getMemberProfile()->getFullName(),
                                'avatar' => $user->getMemberProfile()->getAvatar(),
                                'token' => $token,
                                'fcmToken' => $fcmtoken
                            ]
                        ];
                    } else {
                        $code = 200;
                        $response = [
                            'msg' => 'Autentikasi Berhasil Email dan Password Cocok'
                        ];
                    }
                } else {
                    $code = 202;
                    $response = [
                        'msg' => 'Akun Anda Sudah Di Gunakan',
                    ];
                }
            } else {
                //Auth By Social Media
                $firebaseUser = $this->verifyFirebaseToken($token);
                if (!$firebaseUser) {
                    return GenBasic::send(202, ['msg' => 'Akun Sosial Media Tidak Di Temukan']);
                }
                /** @var User $user */
                $user = $this->em->getRepository(User::class)->findOneBy(['email' => $firebaseUser->email]);
                if (!$user) {
                    // Register New User
                    $data = [
                        'email' => $firebaseUser->email,
                        'fullName' => $firebaseUser->displayName,
                        'avatar' => $firebaseUser->photoUrl,
                        'phone' => $firebaseUser->phoneNumber,
                        'token' => $token,
                        'fcmtoken' => $fcmtoken,
                        'deviceId' => $deviceId,
                        'deviceModel' => $deviceModel,
                        'role' => $role,
                        'provider' => $provider
                    ];
                    $register = $this->createNewUser($data);
                    $code = $register['code'];
                    $response = $register['response'];
                } else {
                    // Login
                    $data = [
                        'user' => $user,
                        'token' => $token,
                        'fcmtoken' => $fcmtoken,
                        'deviceId' => $deviceId,
                        'deviceModel' => $deviceModel,
                        'role' => $role,
                        'provider' => $provider
                    ];
                    $login = $this->findUser($data);
                    $code = $login['code'];
                    $response = $login['response'];
                }
            }
        } catch (RevokedIdToken $e) {
            $code = 500;
            $response = [
                'code' => 500,
                'msg' => 'The token invalid: ' . $e->getMessage(),
            ];
        } catch (InvalidToken $e) {
            $code = 500;
            $response = [
                'code' => 500,
                'msg' => 'The token invalid: ' . $e->getMessage(),
            ];
        } catch (\InvalidArgumentException $e) {
            $code = 500;
            $response = [
                'code' => 500,
                'msg' => 'The token could not be parsed: ' . $e->getMessage(),
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => $err->getMessage(),
            ];
        }
        return GenBasic::send(
            $code,
            $response
        );
    }

    /**
     * @param $token
     * @return UserRecord
     * @throws AuthException
     * @throws FirebaseException
     */
    private function verifyFirebaseToken($token)
    {
        $factory = (new Factory())->withServiceAccount('../config/teman-belajar-mo-1564386173622-firebase-adminsdk-d7pd1-6bc8bacfa9.json');
        $this->auth = $factory->createAuth();
        $frb = $this->auth->verifyIdToken($token, $checkIfRevoked = true);
        $uid = $frb->claims()->get('sub');
        /** @var UserRecord $userFb */
        return $this->auth->getUser($uid);
    }

    private function createNewUser(array $data)
    {
        $email = $data['email'];
        $role = $data['role'];
        $provider = $data['provider'];
        $token = $data['token'];
        $fcmtoken = $data['fcmtoken'];
        $fullName = $data['fullName'];
        $phone = $data['phone'];
        $avatar = $data['avatar'];
        $deviceId = $data['deviceId'];
        $deviceModel = $data['deviceModel'];
        $user = new User();
        $user
            ->setEmail($email)
            ->setRoles($role)
            ->setProvider($provider)
            ->setApiToken($token)
            ->setIsEmailVerified(true)
            ->setAppFcmToken($fcmtoken);
        if (in_array('ROLE_TENTOR', $role, true)) {
            $slugName = null;
            if ($fullName !== null) {
                $slugName = str_replace(' ', '-', $fullName) . '-' . strtotime("now");
            }
            $grade = $this->em->getRepository(Grade::class)->find(1);
            $tentor = new Tentor();
            $tentor
                ->setGrade($grade)
                ->setFullName($fullName)
                ->setSlugName($slugName)
                ->setPhone($phone)
                ->setAvatar($avatar)
                ->setDeviceID($deviceId)
                ->setDeviceModel($deviceModel)
                ->setPoint(0)
                ->setBalance(0)
                ->setIsActive(false)
                ->setIsVerified(false)
                ->setUser($user);
            $this->em->persist($user);
            $this->em->persist($tentor);
            $this->em->flush();
            return [
                'code' => 200,
                'response' => [
                    'msg' => 'Berhasil Melanjutkan Sebagai Guru',
                    'data' => [
                        'email' => $email,
                        'name' => $fullName,
                        'avatar' => $avatar,
                        'token' => $token,
                        'fcmToken' => $fcmtoken
                    ]
                ]
            ];
        } elseif (in_array('ROLE_MEMBER', $role, true)) {
            $member = new Member();
            $member
                ->setFullName($fullName)
                ->setPhone($phone)
                ->setAvatar($avatar)
                ->setDeviceID($deviceId)
                ->setDeviceModel($deviceModel)
                ->setMedal(0)
                ->setCoin(0)
                ->setIsActive(true)
                ->setUser($user);
            $this->em->persist($user);
            $this->em->persist($member);
            $this->em->flush();
            return [
                'code' => 200,
                'response' => [
                    'msg' => 'Berhasil Melanjutkan Sebagai Member',
                    'data' => [
                        'email' => $email,
                        'name' => $fullName,
                        'avatar' => $avatar,
                        'token' => $token,
                        'fcmToken' => $fcmtoken
                    ]
                ]
            ];
        } else {
            return [
                'code' => 200,
                'response' => [
                    'msg' => 'Role Cannot Be Null!',
                ]
            ];
        }
    }

    private function findUser(array $data)
    {
        $user = $data['user'];
        $provider = $data['provider'];
        $token = $data['token'];
        $fcmtoken = $data['fcmtoken'];
        $role = $data['role'];
        $deviceId = $data['deviceId'];
        $deviceModel = $data['deviceModel'];
        if ($user->getProvider() !== $provider) {
            return [
                'code' => 202,
                'response' => [
                    'msg' => 'Akun Sudah Di Gunakan Pada Provider Lain',
                ]
            ];
        }
        $user->setApiToken($token)
            ->setAppFcmToken($fcmtoken);
        if ($role[0] === 'ROLE_TENTOR' && in_array("ROLE_TENTOR", $user->getRoles(), true)) {
            /** @var Tentor $profile */
            $profile = $this->em->getRepository(Tentor::class)->findOneBy(['user' => $user->getId()]);
            $profile->setDeviceID($deviceId)
                ->setDeviceModel($deviceModel);
            $this->em->flush();
            return [
                'code' => 200,
                'response' => [
                    'msg' => 'Berhasil Melanjutkan Sebagai Guru',
                    'data' => [
                        'email' => $user->getEmail(),
                        'name' => $user->getTentorProfile()->getFullName(),
                        'avatar' => $user->getTentorProfile()->getAvatar(),
                        'token' => $token,
                        'fcmToken' => $fcmtoken
                    ]
                ]
            ];
        } elseif ($role[0] === 'ROLE_MEMBER' && in_array("ROLE_MEMBER", $user->getRoles(), true)) {
            /** @var Member $profile */
            $profile = $this->em->getRepository(Member::class)->findOneBy(['user' => $user->getId()]);
            $profile->setDeviceID($deviceId)
                ->setDeviceModel($deviceModel);
            $this->em->flush();
            return [
                'code' => 200,
                'response' => [
                    'msg' => 'Berhasil Melanjutkan Sebagai Member',
                    'data' => [
                        'email' => $user->getEmail(),
                        'name' => $user->getMemberProfile()->getFullName(),
                        'avatar' => $user->getMemberProfile()->getAvatar(),
                        'token' => $token,
                        'fcmToken' => $fcmtoken
                    ]
                ]
            ];
        } else {
            return [
                'code' => 200,
                'response' => [
                    'msg' => 'Akun Anda Sudah Di Gunakan',
                ]
            ];
        }
    }
}
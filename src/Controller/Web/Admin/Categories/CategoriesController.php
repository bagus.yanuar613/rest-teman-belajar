<?php


namespace App\Controller\Web\Admin\Categories;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Categories;
use App\Entity\CategoriesParent;
use App\Entity\Subject;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CategoriesController
 * @package App\Controller\Web\Admin\Categories
 */
class CategoriesController extends BaseController
{
    /**
     * CategoriesController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/ajax/categories", methods="GET|HEAD")
     */
    public function getParent()
    {
        try {
            $parent = $this->em->getRepository(CategoriesParent::class)->findAll();
            $ar = [];
            /**
             * @var CategoriesParent $dataParent
             */
            foreach ($parent as $key => $dataParent){
                $ar[$key]['id'] = $dataParent->getId();
                $ar[$key]['name'] = $dataParent->getName();
                $ar[$key]['child'] = [];

                /**
                 * @var Categories $dataChild
                 */
                foreach ($dataParent->getChildren() as $keyChild => $dataChild){
                    $ar[$key]['child'][$keyChild]['id'] = $dataChild->getId();
                    $ar[$key]['child'][$keyChild]['name'] = $dataChild->getName();
                    $ar[$key]['child'][$keyChild]['subject'] = [];
                    /**
                     * @var Subject $dataSubject
                     */
                    foreach ($dataChild->getSubject() as $keySubject => $dataSubject){
                        $ar[$key]['child'][$keyChild]['subject'][$keySubject]['id'] = $dataSubject->getId();
                        $ar[$key]['child'][$keyChild]['subject'][$keySubject]['name'] = $dataSubject->getName();
                    }

                }
            }
            $code = 200;
            $response = GenBasic::serializeToJson($ar);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Data Categories ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
<?php


namespace App\Controller\Web\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Attendance;
use App\Entity\Cart;
use App\Entity\Categories;
use App\Entity\CategoriesParent;
use App\Entity\CategoryToParentToLevel;
use App\Entity\City;
use App\Entity\District;
use App\Entity\Level;
use App\Entity\Member;
use App\Entity\MemberClass;
use App\Entity\Method;
use App\Entity\Pricing;
use App\Entity\Subject;
use App\Entity\Tentor;
use App\Entity\TentorSubject;
use App\Entity\Transaction;
use App\Repository\AttendanceRepository;
use App\Repository\CartRepository;
use App\Repository\CategoriesParentRepository;
use App\Repository\CategoryToParentToLevelRepository;
use App\Repository\LevelRepository;
use App\Repository\SubjectRepository;
use App\Repository\TentorSubjectRepository;
use App\Repository\TransactionRepository;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class MainController extends BaseController
{

    private $utility;

    /**
     * MainController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="beranda")
     */
    public function landingPage()
    {
        if ($this->getUser()) {
            if (in_array('ROLE_ADMIN', $this->getUser()->getRoles(), true)) {
                return $this->redirect('/admin');
            }
            if (!$this->utility->checkProfile($this->getUser())) {
                return $this->redirect('/member/profile');
            }

            $class = null;
            $district = null;
            $method = null;
            $categories = null;
            $ai = false;
            if($this->getUser()->getMemberProfile()->getClass() !== null && $this->getUser()->getMemberProfile()->getDistrict() !== null){
                /** @var MemberClass $class */
                $class = $this->getUser()->getMemberProfile()->getClass();
                $district = $this->getUser()->getMemberProfile()->getDistrict();
                $categories = $class->getCategory();
                /** @var Method $method */
                $method = $this->em->getRepository(Method::class)->find(1);
                $ai = true;
            }

            $data = [
                'user' => $this->getUser(),
                'memberClass' => $class,
                'district' => $district,
                'categories' => $categories,
                'method' => $method,
                'ai' => $ai
            ];
        } else {
            $category = $this->em->getRepository(Categories::class)->findOneBy(['isReference' => true]);
//            setlocale(LC_ALL, 'IND');
//            date_default_timezone_set("Asia/Bangkok");
//            $firstMeet = strftime('%A, %d %B %Y %H', strtotime(date('Y-m-d H:i:s'))) . ':00';
//            $vFirstMeet = strftime('%Y-%m-%d %H', strtotime(date('Y-m-d H:i:s'))) . ':00';
            $data = [
                'category' => $category,
//                'firstMeet' => $firstMeet,
//                'vFirstMeet' => $vFirstMeet
            ];
        }
        return $this->render('siswa/landingpage.html.twig', $data);
    }

    /**
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/kota/{slug}")
     */
    public function landingPageByCity($slug)
    {
        if ($this->getUser()) {
            if (!$this->utility->checkProfile($this->getUser())) {
                return $this->redirect('/member/profile');
            }
            /** @var MemberClass $class */
            $class = $this->getUser()->getMemberProfile()->getClass();
            $district = $this->getUser()->getMemberProfile()->getDistrict();
            $categories = $class->getCategory();
            $data = [
                'user' => $this->getUser(),
                'memberClass' => $class,
                'district' => $district,
                'categories' => $categories
            ];
            return $this->render('siswa/landingpage.html.twig', $data);
        } else {
            $category = $this->em->getRepository(Categories::class)->findOneBy(['isReference' => true]);
            $city = $this->em->getRepository(City::class)->findOneBy(['slug' => $slug]);
            setlocale(LC_ALL, 'IND');
            date_default_timezone_set("Asia/Bangkok");
            $firstMeet = strftime('%A, %d %B %Y %H', strtotime(date('Y-m-d H:i:s'))) . ':00';
            $vFirstMeet = strftime('%Y-%m-%d %H', strtotime(date('Y-m-d H:i:s'))) . ':00';
            $data = [
                'category' => $category,
                'firstMeet' => $firstMeet,
                'vFirstMeet' => $vFirstMeet,
                'city' => $city,
            ];
            return $this->render('siswa/landingPageKota.html.twig', $data);
        }
    }


    /**
     * @return Response
     * @Route("/pencarian", methods="GET|HEAD")
     */
    public function searchPage()
    {
        $categoryId = $this->req->query->get('category');
        $districtId = $this->req->query->get('district');
//        $meet = $this->req->query->get('meet');
        $methodId = (int)$this->req->query->get('method');

//        if (!$this->req->query->has('category') || !$this->req->query->has('meet') || !$this->req->query->has('method') || $meet === '') {
//            return new Response('Wrong Parameter!');
//        }
        if (!$this->req->query->has('category') || !$this->req->query->has('method')) {
            return new Response('Wrong Parameter!');
        }
        $session = new Session();
        $session->set('category', $categoryId);
        $session->set('district', $districtId);
        $session->set('method', $methodId);
//        $session->set('meet', $meet);
        $category = $this->em->getRepository(Categories::class)->find($categoryId);
        if (!$category) {
            return $this->redirect('/');
        }
        $level = $this->utility->getSubjectLevel($categoryId);
        /** @var Method $method */
        $method = $this->em->getRepository(Method::class)->find($methodId);
        if (count($level) <= 0 || !$method) {
            return new Response('Ooops, Sepertinya Data Yang Kamu Cari Belum Tersedia');
        }

        $district = null;
        if ($method->getId() === 1) {
            if (!$this->req->query->has('district')) {
                return new Response('Ooops, Sepertinya Yang Kamu Belum Memilih Tempat Belajar Mu');
            }
            $district = $this->em->getRepository(District::class)->find($districtId);
        }

//        setlocale(LC_ALL, 'IND');
//        $vMeet = strftime('%Y-%m-%d %H', strtotime($meet)) . ':00';
//        $vMeetString = strftime('%A, %d %B %Y %H', strtotime($meet)) . ':00';
        return $this->render('siswa/pencarian/hasilPencarian.html.twig', [
            'level' => $level,
            'category' => $category,
            'district' => $district,
//            'meet' => $vMeet,
//            'meetString' => $vMeetString,
            'method' => $method,
        ]);
    }

    /**
     * @param $slugMethod
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/guru/{slugMethod}/{slug}", methods="GET|HEAD")
     */
    public function searchBySubjectAndTeacher($slugMethod, $slug)
    {
        try {
            $session = new Session();
            $session->set('redirectTo', $this->req->getPathInfo());
            /** @var Method $method */
            $method = $this->em->getRepository(Method::class)->findOneBy(['slug' => $slugMethod]);
            $detail = $this->utility->findTentorBySlug($method, $slug);
            if ($detail !== null) {
                return $this->render('siswa/detailGuru.html.twig', [
                    'detail' => $detail[0],
                    'price' => $detail['price'],
                    'method' => $method
                ]);
            }
            return new Response('Halaman Yang Kamu Cari Tidak Di Temukan');
        } catch (NonUniqueResultException $e) {
            return new Response('Something Wrong There Are Multiple Unique Result');
        }
    }

    /**
     * @return Response
     * @Route("/member/pencarian", methods="GET|HEAD")
     */
    public function authSearchPage()
    {
        $category = $this->em->getRepository(Categories::class)->findOneBy(['isReference' => true]);
//        setlocale(LC_ALL, 'IND');
//        date_default_timezone_set("Asia/Bangkok");
//        $firstMeet = strftime('%A, %d %B %Y %H', strtotime(date('Y-m-d H:i:s'))) . ':00';
//        $vFirstMeet = strftime('%Y-%m-%d %H', strtotime(date('Y-m-d H:i:s'))) . ':00';
        $data = [
            'category' => $category,
//            'firstMeet' => $firstMeet,
//            'vFirstMeet' => $vFirstMeet
        ];
        return $this->render('siswa/beranda/pencarian.html.twig', $data);
    }
    //Detail Pencarian Keahlian Guru

    /**
     * @return Response
     * @Route("/member/notification")
     */
    public function notifikasi()
    {
        return $this->render('siswa/notifikasi/notifikasi.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/pencarian-kategori", name="pencarianKategori")
     */
    public function searchByCategories()
    {
        $parent = $this->utility->getParentCategories();
//        dump($parent);
//        die();
        return $this->render('siswa/pencarian/pencarianKategori.html.twig', ['parent' => $parent]);
    }

    /**
     * @param $slug
     * @return Response
     * @Route("/les-privat/{slug}", name="landingpageMapel")
     */
    public function landingPageByCategory($slug)
    {
        $category = $this->em->getRepository(Categories::class)->findOneBy(['slug' => $slug]);
        if(!$category){
            return new Response('Category Not Found');
        }
        return $this->render('siswa/landingPageMapel.html.twig', ['firstMeet' => '', 'vFirstMeet' => '', 'category' => $category]);
    }


}
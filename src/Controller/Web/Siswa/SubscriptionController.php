<?php


namespace App\Controller\Web\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Cart;
use App\Entity\Subscription;
use App\Entity\Transaction;
use App\Repository\CartRepository;
use App\Repository\SubscriptionRepository;
use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class SubscriptionController extends BaseController
{

    private $utility;

    /**
     * SubscriptionController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }

    /**
     * @return Response
     * @Route("/member/subscription", methods="GET|HEAD")
     */
    public function subscription()
    {
        return $this->render('siswa/Subscription/Subscription.html.twig');
    }

    /**
     * @return Response
     * @Route("/member/subscription/list", methods="GET|HEAD")
     */
    public function getSubscriptionList()
    {
        $status = $this->req->query->get('status');
        $limit = $this->req->query->get('limit') ?? 3;
        $offset = $this->req->query->get('offset') ?? 0;
        return $this->utility->getSubscriptionList($this->getUser(), $status, $offset, $limit);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/member/subscription-create", methods="POST|HEAD")
     */
    public function sendToTransaction()
    {
        if (!$this->getUser()) {
            return GenBasic::send(401, 'Unauthenticated');
        }
        $tentorSubject = $this->req->request->get('tsId');
        $duration = $this->req->request->get('duration');
        $encounter = $this->req->request->get('encounter');
        $attendees = $this->req->request->get('attendees');
        $location = $this->req->request->get('location');
        $note = $this->req->request->get('note');
        $method = $this->req->request->get('method');
        $meet = $this->req->request->get('meet');
        $address = $this->req->request->get('address') ?? '';
        $data = [
            'tsId' => $tentorSubject,
            'duration' => $duration,
            'encounter' => $encounter,
            'attendees' =>$attendees,
            'location' => $location,
            'note' => $note,
            'method' => $method,
            'meet' => $meet,
            'address' => $address
        ];
        return $this->utility->sendSubscription($this->getUser(), $data);
    }

    /**
     * @param $id
     * @return Response
     * @Route("/member/subscription/detail/{id}", methods="GET|HEAD")
     */
    public function getSubscriptionDetail($id)
    {
        try {
            $subscription = $this->utility->getMySubscriptionDetail($this->getUser(), $id);
            if(!$subscription){
                return new Response('Subscription Not Found!');
            }
            return $this->render('siswa/Subscription/SubscriptionDetail.html.twig', ['subscription' => $subscription]);
        } catch (NonUniqueResultException $e) {
            return new Response('Subscription Non Unique Result!');
        }

    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/member/subscription/not-confirmed/count", methods="GET|HEAD")
     */
    public function getSubscriptionNotConfirmed()
    {
        return $this->utility->getSubscriptionNotConfirmed($this->getUser());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/member/subscription-cancel", methods="POST|HEAD")
     */
    public function cancelSubscription()
    {
        $id = $this->req->request->get('id');
        return $this->utility->cancelSubscription($this->getUser(), $id);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/member/subscription-renewal", methods="POST|HEAD")
     */
    public function createManualRenewal()
    {
        $id = $this->req->request->get('id');
        return $this->utility->createManualRenewal($this->getUser(), $id);
    }
}
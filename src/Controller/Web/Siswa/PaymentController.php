<?php


namespace App\Controller\Web\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\BankAccount;
use App\Entity\Cart;
use App\Entity\Payment;
use App\Entity\Transaction;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class PaymentController extends BaseController
{

    private $utility;

    /**
     * PaymentController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }

    /**
     * @return Response
     * @Route("/member/payment")
     */
    public function paymentPage()
    {
        return $this->render('siswa/Payment/Payment.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/member/payment/list", methods="GET|HEAD")
     */
    public function getPaymentList()
    {
        $limit = $this->req->query->get('limit');
        $offset = $this->req->query->get('offset');
        $status = $this->req->query->get('status');
        return $this->utility->getPaymentList($this->getUser(), $status, $offset, $limit);
    }

    /**
     * @param $id
     * @return Response
     * @Route("/member/payment/detail/{id}")
     */
    public function paymentDetail($id)
    {
        /** @var Transaction $transaction */
        $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $id, 'user' => $this->getUser(), 'isPaid' => null]);
        if (!$transaction) {
            return new Response('Pembayaran Tidak Ditemukan!');
        }
        setlocale(LC_ALL, 'IND');
        date_default_timezone_set("Asia/Jakarta");
        $tempExpiration = $transaction->getPaymentExpiration()->format('Y-m-d H:i:s');
        $expiration = strftime('%A, %d %B %Y %H:%I:%S', strtotime($tempExpiration));
        $summary = 0;
        /** @var Cart $v */
        foreach ($transaction->getCart() as $v) {
            $summary += $v->getTotal();
        }
        return $this->render('siswa/Payment/PaymentDetail.html.twig', ['transaction' => $transaction, 'expiration' => $expiration, 'summary' => $summary]);
    }

    /**
     * @param $id
     * @return Response
     * @Route("/member/payment/confirm/{id}")
     */
    public function paymentConfirmPage($id)
    {

        $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $id, 'user' => $this->getUser(), 'isPaid' => null]);

        if (!$transaction) {
            return new Response('Pembayaran Tidak Ditemukan!');
        }
        return $this->render('siswa/Payment/PaymentConfirmation.html.twig', ['transaction' => $transaction]);
    }

    /**
     * @return Response
     * @Route("/member/payment/submit-confirm", methods="POST|HEAD")
     */
    public function submitConfirmation()
    {
        try {
            $reference = $this->req->request->get('reference');
            $rekening = $this->req->request->get('rekening');
            $holderName = $this->req->request->get('holderName');
            /** @var Transaction $transaction */
            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $reference, 'user' => $this->getUser(), 'isPaid' => null]);
            if (!$transaction) {
                return GenBasic::send(202, ['msg' => 'Payment Not Found']);
            }

            $confirmationInfo = [
                'accountHolder' => $holderName,
                'accountNumber' => $rekening
            ];
            $transaction->setConfirmationInfo($confirmationInfo);
            $this->em->flush();
            $code = 200;
            $response = [
                'msg' => 'Success'
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed Submit Confirmation ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
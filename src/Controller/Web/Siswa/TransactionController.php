<?php


namespace App\Controller\Web\Siswa;


use App\Common\GenBasic;
use App\Common\GenFirebase;
use App\Controller\BaseController;
use App\Entity\BankAccount;
use App\Entity\Cart;
use App\Entity\District;
use App\Entity\Member;
use App\Entity\Method;
use App\Entity\Payment;
use App\Entity\Pricing;
use App\Entity\Tentor;
use App\Entity\TentorSubject;
use App\Entity\Transaction;
use App\Repository\CartRepository;
use App\Repository\PaymentRepository;
use App\Repository\TentorSubjectRepository;
use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\Expr\Join;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class TransactionController extends BaseController
{

    private $utility;
    /**
     * TransactionController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }

    /**
     * @param $slugMethod
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/order/{slugMethod}/{slug}", methods="GET|HEAD")
     */
    public function orderPage($slugMethod, $slug)
    {
        try {
            $session = new Session();
            $session->set('redirectTo', $this->req->getPathInfo());
            if (!$this->getUser()) {
                return $this->redirect('/login');
            } else {
                if (!$this->utility->checkProfile($this->getUser())) {
                    return $this->redirect('/member/profile');
                }
            }
            setlocale(LC_ALL, 'IND');
            date_default_timezone_set("Asia/Jakarta");
            $vMeetString = strftime('%A, %d %B %Y %H', strtotime(date('Y-m-d H:i:s'))) . ':00';
            $vMeet = strftime('%Y-%m-%d %H', strtotime(date('Y-m-d H:i:s'))) . ':00';
            if ($session->has('meet')) {
                $vMeet = strftime('%Y-%m-%d %H', strtotime(date('Y-m-d H:i:s'))) . ':00';
                $vMeetString = strftime('%A, %d %B %Y %H', strtotime($session->get('meet'))) . ':00';
            }

            /** @var Method $method */
            $method = $this->em->getRepository(Method::class)->findOneBy(['slug' => $slugMethod]);
            if (!$method || $method->getId() === 3) {
                return new Response('Ooops, Halaman Yang Kamu Cari Tidak Ada!');
            }

            /** @var TentorSubject $detail */
            $detail = $this->utility->findTentorBySlug($method, $slug);
            $district = null;
            $inArea = false;
            $address = '';
            $districtSession = false;
            if ($session->has('district')) {
                $district = $this->em->getRepository(District::class)->find((int)$session->get('district'));
                $districtSession = true;
            } else {
                $district = $this->getUser()->getMemberProfile()->getDistrict();
                $address = $this->getUser()->getMemberProfile()->getAddress();
            }
            if ($detail !== null) {

                //cek guru sudah mempunyai area mengajar atau belum
                if (!$detail[0]->getUser()->getDistrict()[0]) {
                    return new Response('Pengajar Belum Mempunyai Wilayah Mengajar');
                }

                //cek area order dan area mengajar tentor
                if ($district !== null && $detail[0]->getUser()->getDistrict()->contains($district)) {
                    $inArea = true;
                }

                return $this->render('siswa/pesan/pesanstepsatu.html.twig', [
                    'detail' => $detail[0],
                    'price' => $detail['price'],
                    'method' => $method,
                    'meet' => $vMeet,
                    'meetString' => $vMeetString,
                    'district' => $district,
                    'districtSession' => $districtSession,
                    'address' => $address,
                    'inArea' => $inArea
                ]);
            }
            return new Response('Guru Yang Kamu Cari Tidak Ditemukan');
        } catch (NonUniqueResultException $e) {
            return new Response('Terjadi Kesalahan');
        }

    }



    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/member/transaction/extend", methods="POST|HEAD")
     */
    public function extendTransaction()
    {
        try {
            $id = $this->req->request->get('id');
            /** @var CartRepository $cart */
            $cart = $this->em->getRepository(Cart::class);
            /** @var Cart $result */
            $result = $cart->createQueryBuilder('c')
                ->innerJoin('c.transaction', 't')
                ->where('c.id = :id')
                ->andWhere('t.user = :user')
                ->setParameter('id', $id)
                ->setParameter('user', $this->getUser())
                ->getQuery()->getOneOrNullResult();
            if (!$result) {
                return GenBasic::send(202, 'Transaction Not Found');
            }

            /** @var Cart $new */
            $new = clone $result;
            $code = 'TP';
            switch ($new->getMethod()->getId()) {
                case 1:
                    $code = 'TP01';
                    break;
                case 2:
                    $code = 'TP02';
                    break;
                case 3:
                    $code = 'TK';
                    break;
                default:
                    break;
            }
            date_default_timezone_set('Asia/Jakarta');
            $newFirstMeet = $new->getFirstMeet()->modify('+' . $new->getEncounter() . ' week');
            $now = new \DateTime();
            $expiredPay = $now->modify('+2 day');
            $new->setId(null)
                ->setStatus(1)
                ->setFirstMeet($newFirstMeet)
                ->setExpiredPay($expiredPay)
                ->setCreatedAt(new \DateTime())
                ->setUpdatedAt(new \DateTime())
                ->setReferenceId($code . '/' . $this->getUser()->getId() . '/' . date('YmdHis'));
//            $this->em->detach($new);
            $this->em->persist($new);
            $this->em->flush();
            $code = 200;
            $response = 'Success Extend Transaction';
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/member/transaction/cancel-extend", methods="POST|HEAD")
     */
    public function cancelExtendTransaction()
    {
        try {
            $id = $this->req->request->get('id');
            /** @var Cart $cart */
            $cart = $this->em->getRepository(Cart::class)->find($id);
            /** @var Transaction $transaction */
            $transaction = $cart->getTransaction();
            if ($transaction->getRest() <= 0) {
                $transaction->setStatus(9);
            }
            $cart->setStatus(6);
            $this->em->flush();
            $code = 200;
            $response = 'Success Extend Transaction';
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return Response
     * @Route("/member/transaction-finish", methods="GET|HEAD")
     * @throws \Exception
     */
    public function finish()
    {
        if (!$this->getUser()) {
            return $this->redirect('/login');
        }
        $class = $this->getUser()->getMemberProfile()->getClass();
        $categories = $class->getCategory();
        $district = $this->getUser()->getMemberProfile()->getDistrict()->getId();
        $session = new Session();
        if ($session->has('district')) {
            $district = $session->get('district');
        }
        $method = 1;
        $level = $session->get('level');
        $session->remove('district');
        $session->remove('meet');
        $session->remove('category');
        $session->remove('method');
        return $this->render('siswa/pesan/selesaiPesan.html.twig', ['related' => $categories, 'memberClass' => $class, 'district' => $district, 'method' => $method, 'level' => $level]);
    }

    /**
     * @return Response
     * @Route("/member/transaction", methods="GET|HEAD")
     */
    public function transaction()
    {
        return $this->render('siswa/pesanan/pesanan.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/member/transaction/data", methods="GET|HEAD")
     */
    public function getTransaction()
    {
        try {
            $status = $this->req->query->get('status');
            /** @var TransactionRepository $transaction */
            $transaction = $this->em->getRepository(Transaction::class);
            $qb = $transaction->createQueryBuilder('t')
                ->where('t.user = :user')
                ->setParameter('user', $this->getUser());
            if (!$this->req->query->has('status') || $this->req->query->get('status') !== 'all') {
                $qb->andWhere('t.status = :status')->setParameter('status', $status);
            }
//            if($status === 0){
//                $qb->innerJoin('t.cart', 'c')->where('c.expiredPay > :expired')->setParameter('expired', );
//            }
            $results = $qb->getQuery()->getResult();
            $code = 200;
            $response = GenBasic::CustomNormalize($results, [
                'id',
                'tentorSubject' => [
                    'user' => [
                        'tentorProfile' => ['fullName', 'generatedAvatar']
                    ],
                    'category' => ['name'],
                    'level' => ['name']
                ],
                'status'
            ]);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam normalisasi data ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }




    /**
     * @return Response
     * @Route("/member/cart/data", methods="GET|HEAD")
     */
    public function getCart()
    {
        try {
            $status = $this->req->query->get('status');
            /** @var CartRepository $transactions */
            $transactions = $this->em->getRepository(Cart::class);
            $qb = $transactions->createQueryBuilder('c')
                ->innerJoin('c.transaction', 't')
                ->where('t.user = :user')
                ->setParameter('user', $this->getUser());
            if (!$this->req->query->has('status') || $this->req->query->get('status') !== 'all') {
                $qb->andWhere('c.status = :status')->setParameter('status', $status);
            }
            $results = $qb
                ->orderBy('c.createdAt', 'DESC')
                ->getQuery()
                ->getResult();

            $code = 200;
            $attribute = [
                'id',
                'status',
                'referenceId',
                'total',
                'expiredPay',
                'transaction' => [
                    'tentorSubject' => [
                        'user' => [
                            'tentorProfile' => ['fullName', 'generatedAvatar']
                        ],
                        'category' => [
                            'name',
                        ],
                        'level' => [
                            'name',
                        ]
                    ]
                ]
            ];
            $response = GenBasic::CustomNormalize($results, $attribute);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Transactions ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return Response
     * @Route("/member/payment/data", methods="GET|HEAD")
     */
    public function paymentList()
    {
        try {
            /** @var PaymentRepository $paymentRepo */
            $paymentRepo = $this->em->getRepository(Payment::class);
            $qb = $paymentRepo->createQueryBuilder('p')
                ->innerJoin('p.user', 'u')
                ->where('p.user = :user')
                ->setParameter('user', $this->getUser());
            $results = $qb
                ->orderBy('p.createdAt', 'DESC')
                ->getQuery()
                ->getResult();

            $code = 200;
            $attribute = [
                'id',
                'status',
                'totalCart',
                'paymentMethod',
                'paymentReference',
                'amount',
                'cart' => [
                    'transaction' => [
                        'tentorSubject' => [
                            'user' => [
                                'tentorProfile' => ['fullName', 'generatedAvatar']
                            ],
                            'category' => [
                                'name',
                            ],
                            'level' => [
                                'name',
                            ]
                        ]
                    ],

                ]
            ];
            $response = GenBasic::CustomNormalize($results, $attribute);
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Transactions ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @param $id
     * @return Response
     * @Route("/member/transaction/payment/detail/{id}")
     */
    public function paymentListDetail($id)
    {
        /** @var Payment $payment */
        $payment = $this->em->getRepository(Payment::class)->findOneBy(['user' => $this->getUser(), 'id' => $id]);
        if (!$payment) {
            return new Response('Detail Pembayaran Tidak Di Temukan');
        }
        $summary = 0;
        /** @var Cart $v */
        foreach ($payment->getCart() as $v) {
            $summary += ($v->getTotal() - $v->getDiscount());
        }
        $temp = $payment->getCreatedAt();
        $temp2 = $temp->format('Y-m-d H:i:s');
        setlocale(LC_ALL, 'IND');
        date_default_timezone_set("Asia/Bangkok");
        $paymentDate = strftime('%d %B %Y', strtotime($temp2));
        return $this->render('siswa/pesanan/pembayaranDetail.html.twig', ['payment' => $payment, 'paymentDate' => $paymentDate, 'summary' => $summary, 'pId' => bin2hex($payment->getId())]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/member/transaction/cancel", methods="POST|HEAD")
     */
    public function cancelTransaction()
    {
        try {
            $id = $this->req->request->get('id');
            /** @var CartRepository $cart */
            $cart = $this->em->getRepository(Cart::class);
            /** @var Cart $result */
            $result = $cart->createQueryBuilder('c')
                ->innerJoin('c.transaction', 't')
                ->where('c.id = :id')
                ->andWhere('t.user = :user')
                ->setParameter('id', $id)
                ->setParameter('user', $this->getUser())
                ->getQuery()->getOneOrNullResult();
            if (!$result) {
                return GenBasic::send(202, 'Transaction Not Found');
            }
            /** @var Transaction $transaction */
            $transaction = $this->em->getRepository(Transaction::class)->find($result->getTransaction()->getId());
            if (!$transaction) {
                return GenBasic::send(202, 'Transaction Not Found');
            }
            $result->setStatus(6);
            $transaction->setStatus(6);
            $this->em->flush();
            $code = 200;
            $response = 'Success Cancel Transaction';
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi kesalahan dalam pengambilan data ' . $err->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}
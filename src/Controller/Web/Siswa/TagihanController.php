<?php


namespace App\Controller\Web\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Categories;
use App\Entity\CategoriesParent;
use App\Entity\District;
use App\Entity\Subject;
use App\Entity\Tentor;
use App\Entity\TentorSubject;
use App\Repository\CategoriesParentRepository;
use App\Repository\SubjectRepository;
use App\Repository\TentorSubjectRepository;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class TagihanController extends BaseController
{
    /**
     * MainController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }



//    /**
//     * @param $slugSubject
//     * @param $slugName
//     * @param $method
//     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
//     * @Route("/guru/{method}/{slugSubject}/{slugName}", methods="GET|HEAD")
//     */
//    public function searchBySubjectAndTeacher($slugSubject, $slugName, $method)
//    {
//        try {
//            $vMethod = $method === 'private' ? 0 : 1;
//            /** @var TentorSubjectRepository $subject */
//            $subject = $this->em->getRepository(TentorSubject::class);
//            $qb = $subject->createQueryBuilder('ts')
//                ->innerJoin('ts.subject', 's')
//                ->innerJoin('ts.user', 'u')
//                ->innerJoin(Tentor::class, 't', Join::WITH, 'u.id = t.user')
//                ->where('s.slug = :slugSubject')
//                ->andWhere('t.slugName = :slugName')
//                ->setParameter('slugSubject', $slugSubject)
//                ->setParameter('slugName', $slugName);
//
//            $orStatement = $qb->expr()->orX();
//            $orStatement->add(
//                $qb->expr()->eq('ts.method', ':defaultMethod')
//            );
//            $orStatement->add(
//                $qb->expr()->eq('ts.method', ':method')
//            );
//            $qb->andWhere($orStatement)->setParameter('method', $vMethod)->setParameter('defaultMethod', 2);
//            $detail = $qb->getQuery()->getOneOrNullResult();
//            if ($detail !== null) {
//                return $this->render('siswa/detailGuru.html.twig', ['detail' => $detail, 'method' => $vMethod]);
//            }
//            return new Response('Teacher Or Subject Not Found');
//        } catch (\Exception $err) {
//            $code = 500;
//            $response = [
//                'msg' => 'Failed To Fetch Data Tentor Subject ' . $err->getMessage(),
//            ];
//            return GenBasic::send($code, $response);
//        }
//    }


}
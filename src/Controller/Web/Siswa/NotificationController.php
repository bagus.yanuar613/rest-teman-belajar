<?php


namespace App\Controller\Web\Siswa;


use App\Controller\BaseController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class NotificationController extends BaseController
{

    private $utility;

    /**
     * NotificationController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }

    /**
     * @return Response
     * @Route("/member/notification/list", methods="GET|HEAD")
     */
    public function getNotificationList()
    {
        $limit = $this->req->query->get('limit') ?? 10;
        $offset = $this->req->query->get('offset') ?? 0;
        return $this->utility->getNotificationList($this->getUser(), $offset, $limit);
    }
}
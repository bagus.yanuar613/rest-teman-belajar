<?php


namespace App\Controller\Web\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Attendance;
use App\Entity\Cart;
use App\Entity\District;
use App\Entity\Member;
use App\Entity\MemberClass;
use App\Entity\MemberClassParent;
use App\Entity\Transaction;
use App\Repository\AttendanceRepository;
use App\Repository\CartRepository;
use App\Repository\TransactionRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Proxies\__CG__\App\Entity\Subscription;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountController extends BaseController
{

    /**
     * AccountController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/member/account")
     */
    public function index()
    {
        $user = $this->getUser();
        return $this->render('siswa/akunSaya/akunSaya.html.twig', ['user' => $user]);
    }



    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/member/settings")
     */
    public function settings()
    {
        return $this->render('siswa/akunSaya/pengaturanAkun.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/member/profiles")
     */
    public function profiles()
    {
        $user = $this->getUser()->getMemberProfile();
        return $this->render('siswa/akunSaya/akunProfil.html.twig', ['user' => $user]);
    }

    /**
     * @return Response
     * @Route("/member/profile/edit")
     */
    public function akunEditProfil()
    {
        $user = $this->getUser()->getMemberProfile();
        $kelas = $this->em->getRepository(MemberClassParent::class)->findAll();
        return $this->render('siswa/akunSaya/akunEditProfil.html.twig', ['user' => $user, 'kelas' => $kelas]);
    }

    /**
     * @return Response
     * @Route("/member/profile/patch", methods="POST")
     */
    public function patchProfile(){
        $fullName = $this->req->request->get('name');
        $parentName =  $this->req->request->get('parentName');
        $phone = $this->req->request->get('phone');
        $parentPhone = $this->req->request->get('parentPhone');
        $school = $this->req->request->get('school');
        $class = $this->req->request->get('class');
        $dob = $this->req->request->get('dob');
        $gender = $this->req->request->get('gender');
        $districtId = $this->req->request->get('districtId');
        $address = $this->req->request->get('address');
        /** @var Member $member */
        $member = $this->em->getRepository(Member::class)->findOneBy(['user' => $this->getUser()]);
        $district = $this->em->getRepository(District::class)->find($districtId);
        $memberClass = $this->em->getRepository(MemberClass::class)->find($class);
//        if (!$district && !$class) {
//            return GenBasic::send(202, ['msg' => 'District Not Found!']);
//        }
        $member->setFullName($fullName)
            ->setPhone($phone)
            ->setSchool($school)
            ->setClass($memberClass)
            ->setDistrict($district)
            ->setAddress($address)
            ->setParentPhone($parentPhone)
            ->setParentName($parentName)
            ->setDateOfBirth(date_create_from_format('d/m/Y', $dob))
            ->setGender($gender);
        $this->em->flush();
//        dump($member);
//        die();
        return $this->redirect('/member/profiles');
    }



    /**
     * @return Response
     * @Route("/member/qrcode", name="qrcode")
     */
    public function qrcode()
    {
        $user = $this->getUser();
        $id = bin2hex($user->getId());
        return $this->render('siswa/qrcode/qrcode.html.twig', ['user' => $id]);
    }

}
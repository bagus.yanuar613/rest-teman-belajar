<?php


namespace App\Controller\Web\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\BankAccount;
use App\Entity\Cart;
use App\Entity\Transaction;
use App\Repository\CartRepository;
use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class InvoiceController extends BaseController
{

    private $utility;
    /**
     * InvoiceController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }

    /**
     * @return Response
     * @Route("/member/invoice", methods="GET|HEAD")
     */
    public function invoicePage()
    {
        return $this->render('siswa/Invoice/Invoice.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/member/invoice/list", methods="GET|HEAD")
     */
    public function getInvoiceList()
    {
        $limit = $this->req->query->get('limit');
        $offset = $this->req->query->get('offset');
        return $this->utility->getInvoiceList($this->getUser(), $offset, $limit);
    }

    /**
     * @param $id
     * @return Response
     * @Route("/member/invoice/detail/{id}", methods="GET|HEAD")
     */
    public function getInvoiceById($id)
    {
        try {
            /** @var Transaction $invoice */
            $invoice = $this->utility->getInvoiceById($this->getUser(), $id);
            if (!$invoice) {
                return new Response('Failed To Load Detail. Invoice Detail Not Found!');
            }
            setlocale(LC_ALL, 'IND');
            date_default_timezone_set("Asia/Jakarta");
            $tempExpiration = $invoice->getCreatedAt();
            $expiration = strftime('%A, %d %B %Y %H:%I:%S', strtotime($tempExpiration));
            $summary = 0;
            /** @var Cart $v */
            foreach ($invoice->getCart() as $v) {
                $summary += $v->getTotal();
            }
            return $this->render('siswa/Invoice/InvoiceDetail.html.twig', ['invoice' => $invoice, 'expiration' => $expiration, 'summary' => $summary]);
        } catch (NonUniqueResultException $e) {
            return new Response('Failed To Load Detail Because Non Unique Result');
        }
    }
    /**
     * @return Response
     * @Route("/member/invoice/count", methods="GET|HEAD")
     */
    public function getInvoiceCount()
    {
        return $this->utility->getInvoiceCount($this->getUser());
    }

    /**
     * @return Response
     * @Route("/member/invoice/submit", methods="POST|HEAD")
     */
    public function submitInvoice()
    {
        $session = new Session();
        $bankId = $this->req->request->get('vid');
        $cartId = $session->get('cart_id');
        $bank = $this->em->getRepository(BankAccount::class)->find($bankId);
        if (!$session->has('cart_id') || !$bank) {
            return GenBasic::send(202, 'Failed To Create New Transaction.');
        }
        $payment = $this->utility->sendCheckOut($this->getUser(), $bank, $cartId);
        if (!$payment) {
            return GenBasic::send(500, 'Failed To Create New Transaction');
        }
        $transactionId = $payment[0]->getTransaction()->getId();
        return GenBasic::send(200, [
            'invoice' => $transactionId
        ]);
    }
}
<?php


namespace App\Controller\Web\Siswa;


use App\Common\GenBasic;
use App\Common\GenFirebase;
use App\Controller\BaseController;
use App\Entity\TentorSubject;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Google\Cloud\Firestore\DocumentSnapshot;
use Kreait\Firebase\Firestore;
use Kreait\Firebase\Factory;

class ChatController extends BaseController
{

    private $utility;
    protected $firestore;
    protected $factory;
    /**
     * ChatController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        date_default_timezone_set('Asia/Jakarta');
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
        $this->factory = (new Factory())->withServiceAccount('../config/teman-belajar-mo-1564386173622-firebase-adminsdk-d7pd1-6bc8bacfa9.json');
        $this->firestore = $this->factory->createFirestore();
    }

    /**
     * @return Response
     * @Route("/member/chat", name="chat")
     */
    public function chatListPage()
    {
        return $this->render('siswa/notifikasi/chat.html.twig');
    }

    /**
     * @param $id
     * @return Response
     * @Route("/member/chat-with/{id}", name="isiChat")
     */
    public function isiChat($id)
    {
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->find($id);
        if(!$user){
            return new Response('User Not Found!');
        }
        return $this->render('siswa/notifikasi/isiChat.html.twig', ['id' => $id, 'user' => $user]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/member/chat/send", methods="POST|HEAD")
     */
    public function memberChat()
    {
        try {
            $user = $this->getUser();
            $uid = $user->getId();
            $db = $this->firestore->database();
            $msg = $this->req->request->get('msg');
            $tentorId = $this->req->request->get('tentorId');
            $tentorUser = $this->em->getRepository(User::class)->find($tentorId);
            if(!$tentorUser){
                return GenBasic::send(202, ['Tentor Not Found']);
            }
            $now = new \DateTime();
            $docId = str_replace(" ", "_", $now->format("Y-m-d H:i:s"));
            $docRef = $db->collection('chat_room')->document($uid . '_' . $tentorId);
            $docSnap = $docRef->snapshot()->data();
            if (empty($docSnap)) {
                $singleData = [
                    'lastUpdate' => $now,
                    'message' => $msg,
                    'memberId' => $uid,
                    'tentorId' => $tentorId,
                    'senderIndex' => 0,
                    'recipientIndex' => 1,
                    'member' => 'send',
                    'tentor' => 'receive'
                ];
                $docRef->set($singleData);
            } else {
                $singleData = [
                    ['path' => 'lastUpdate', 'value' => $now],
                    ['path' => 'message', 'value' => $msg],
                    ['path' => 'member', 'value' => 'send'],
                    ['path' => 'tentor', 'value' => 'receive'],
                    ['path' => 'recipientIndex', 'value' => $docRef->snapshot()['recipientIndex'] + 1]
                ];
                $docRef->update($singleData);
            }
            $docRef->collection('room')
                ->document($docId)->set([
                    'createdAt' => $now,
                    'message' => $msg,
                    'memberId' => $uid,
                    'tentorId' => $tentorId,
                    'member' => 'send',
                    'tentor' => 'receive'
                ]);
            $tentorToken = $tentorUser->getAppFcmToken();
            $memberName = $user->getMemberProfile()->getFullName();
            $notification = GenFirebase::SendNotification($tentorToken, $memberName, $msg, [
                'type' => 'chat',
                'docId' => $uid . '_' . $tentorId,
                'name' => $memberName
            ]);
            $code = 200;
            $response = 'success';
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Send Message ' . $err->getMessage()
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/member/chat/list", methods="GET|HEAD")
     */
    public function getChatList()
    {
        try {
            $id = $this->req->query->get('id') ?? [];
            /** @var User $user */
            $user = $this->em->getRepository(User::class)->findBy(['id' => $id]);
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($user, [
                    'id',
                    'tentorProfile' => [
                        'fullName', 'generatedAvatar'
                    ]
                ])
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Send Message ' . $e->getMessage()
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize ' . $e->getMessage()
            ];
        }

        return GenBasic::send($code, $response);
    }
}
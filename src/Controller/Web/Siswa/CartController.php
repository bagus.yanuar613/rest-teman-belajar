<?php


namespace App\Controller\Web\Siswa;


use App\Controller\BaseController;
use App\Entity\BankAccount;
use App\Entity\Cart;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class CartController extends BaseController
{

    private $utility;

    /**
     * CartController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }

    /**
     * @return Response
     * @Route("/member/cart", methods="GET|HEAD")
     */
    public function cart()
    {
        return $this->render('siswa/Cart/Cart.html.twig');
    }

    /**
     * @param $id
     * @return Response
     * @Route("/member/cart/detail/{id}", methods="GET|HEAD")
     */
    public function cartDetail($id)
    {

        try {
            $cart = $this->utility->getCartById($this->getUser(), $id);
            if (!$cart) {
                return new Response('Failed To Load Detail. Cart Detail Not Found!');
            }
            return $this->render('siswa/Cart/CartDetail.html.twig', ['cart' => $cart]);
        } catch (NonUniqueResultException $e) {
            return new Response('Failed To Load Detail Because Non Unique Result');
        }

    }


    /**
     * @return Response
     * @Route("/member/cart/list", methods="GET|HEAD")
     */
    public function getCartList()
    {
        $limit = $this->req->query->get('limit');
        $offset = $this->req->query->get('offset');
        return $this->utility->getCartList($this->getUser(), $limit, $offset);
    }

    /**
     * @return Response
     * @Route("/member/cart/count", methods="GET|HEAD")
     */
    public function getCartCount()
    {
        return $this->utility->getCartCount($this->getUser());
    }

    /**
     * @return Response
     * @Route("/member/cart/checkout", methods="POST|HEAD")
     */
    public function checkout()
    {
        $id = $this->req->request->get('invoice-check');
        $session = new Session();
        $session->set('cart_id', $id);
        return $this->redirectToRoute('cart_checkout');
    }

    /**
     * @return Response
     * @Route("/member/cart/cart-checkout", methods="GET|HEAD", name="cart_checkout")
     */
    public function cartCheckout()
    {
        $session = new Session();
        $cart = $this->utility->getCartListById($this->getUser(), $session->get('cart_id'));
        if (!$session->has('cart_id') || count($cart) <= 0) {
            return $this->redirect('/member/cart');
        }
        $summary = 0;
        /** @var Cart $v */
        foreach ($cart as $v) {
            $summary += $v->getTotal();
        }

        $bank = $this->em->getRepository(BankAccount::class)->findAll();
        return $this->render('siswa/Cart/CartShipment.html.twig', ['cart' => $cart, 'summary' => $summary, 'bank' => $bank]);
    }


}
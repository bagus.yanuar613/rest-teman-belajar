<?php


namespace App\Controller\Web\Siswa;


use App\Controller\BaseController;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UnsubscriptionController extends BaseController
{

    private $utility;

    /**
     * UnsubscriptionController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }


    /**
     * @param $id
     * @return Response|\Symfony\Component\HttpFoundation\Response
     * @Route("/member/unsubscription/{id}")
     */
    public function unsubscribePage($id)
    {
        try {
            $subscription = $this->utility->getMySubscriptionActive($this->getUser(), $id);
            if(!$subscription){
                return new Response('Subscription Not Found!');
            }
            return $this->render('siswa/masalah/alasan.html.twig');
        }catch (\Exception $e){
            return new Response('Subscription Not Found!');
        }
    }
}
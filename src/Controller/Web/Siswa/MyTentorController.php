<?php


namespace App\Controller\Web\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\TentorSubject;
use App\Entity\Transaction;
use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class MyTentorController extends BaseController
{

    private $utility;
    /**
     * MyTentorController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/member/tentor/transaction-dashboard", methods="GET|HEAD")
     */
    public function getMyTentorInDashboard()
    {
        try {
            /** @var TransactionRepository $transRepo */
            $transRepo = $this->em->getRepository(Transaction::class);
            $transaction = $transRepo->createQueryBuilder('t')
                ->where('t.user = :user')
                ->setParameter('user', $this->getUser())
                ->orderBy('t.id', 'DESC')
                ->setFirstResult(0)
                ->setMaxResults(3)
                ->getQuery()->getResult();
            $code = 200;
            $response = [
                'msg' => 'Success To Fetch My Tentor',
                'data' => [
                    'count' => count($transaction),
                    'transaction' => GenBasic::CustomNormalize($transaction, [
                        'id',
                        'tentorSubject' => [
                            'user' => [
                                'tentorProfile' => [
                                    'fullName', 'avatar', 'generatedAvatar'
                                ]
                            ],
                            'category' => ['name'],
                            'level' => ['name']
                        ],
                        'rest',
                        'status'
                    ])
                ]
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch My Tentor ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize My Tentor ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/member/tentor")
     */
    public function myTentor()
    {
        $trans = $this->em->getRepository(Transaction::class)->findOneBy(['user' => $this->getUser()]);
        return $this->render('siswa/guruku/guruku.html.twig', ['trans' => $trans]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/member/tentor/schedule", methods="GET|HEAD")
     */
    public function getTentorSchedule()
    {
        $tsId = $this->req->query->get('tsId');
        $day = $this->req->query->get('day');
        return $this->utility->getTentorSchedule($tsId, $day);
    }

//    /**
//     * @param $id
//     * @return \Symfony\Component\HttpFoundation\Response
//     * @Route("/member/tentor/detail/{id}")
//     */
//    public function myTentorDetail($id)
//    {
//        /** @var TransactionRepository $transaction */
//        $transaction = $this->em->getRepository(Transaction::class);
//        try {
//            /** @var Transaction $result */
//            $result = $transaction->createQueryBuilder('t')
//                ->innerJoin('t.user', 'u')
//                ->where('u.id = :user')
//                ->andWhere('t.id = :id')
//                ->setParameters(['user' => $this->getUser()->getId(), 'id' => $id])
//                ->getQuery()->getOneOrNullResult();
//            $paymentIdHex = null;
//            if($result->getLastCart()->getLastPaymentId() !== false){
//                $paymentIdHex = bin2hex($result->getLastCart()->getLastPaymentId()->getId());
//            }
//            if ($result !== null) {
//                return $this->render('siswa/guruku/gurukuDetail.html.twig', ['transaction' => $result, 'pId' => $paymentIdHex]);
//            }
//            return new Response('Data Tidak Di Temukan!');
//        } catch (NonUniqueResultException $e) {
//            return new Response('Data Tidak Di Temukan!');
//        }
//    }
}
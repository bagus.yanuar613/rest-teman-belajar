<?php


namespace App\Controller\Web\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Attendance;
use App\Entity\Cart;
use App\Entity\Member;
use App\Entity\Tentor;
use App\Entity\TentorSaldo;
use App\Entity\Transaction;
use App\Entity\User;
use App\Repository\AttendanceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Routing\Annotation\Route;

class AttendanceController extends BaseController
{
    private $utility;

    /**
     * AttendanceController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/member/attendance/active", methods="GET|HEAD")
     */
    public function getActiveAttendance()
    {
        return $this->utility->getActiveAttendance($this->getUser());
    }


    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/member/attendance/data/{id}", methods="GET|HEAD")
     */
    public function getMySubscriptionAttendance($id)
    {
        return $this->utility->getAttendance($this->getUser(), $id);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/member/attendance/presence", methods="POST|HEAD")
     */
    public function setPresence()
    {
        $id = $this->req->request->get('id');
        return $this->utility->setManualPresence($this->getUser(), $id);
    }

    /**
     * @param $id
     * @return Response
     * @Route("/member/attendance/detail/{id}")
     */
    public function detailAttendance($id)
    {
        try {
            /** @var Attendance $attendance */
            $attendance = $this->utility->getDetailAttendance($this->getUser(), $id);
            if (!$attendance) {
                return new Response('Attendance Not Found!');
            }
            setlocale(LC_ALL, 'IND');
            date_default_timezone_set("Asia/Jakarta");
            $start = strftime('%A, %d %B %Y', strtotime($attendance->getStartAt()->format('Y-m-d')));
            $diff = date_diff($attendance->getStartAt(), $attendance->getFinishAt());
            $diffFormat = $diff->format("%H:%I:%S");
            return $this->render('siswa/Attendance/AttendanceDetail.html.twig', ['attendance' => $attendance, 'start' => $start, 'diff' => $diffFormat]);
        } catch (NonUniqueResultException $e) {
            return new Response('Attendance Non Unique!');
        }
    }
}
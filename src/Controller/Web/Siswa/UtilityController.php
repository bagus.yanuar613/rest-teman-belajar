<?php

namespace App\Controller\Web\Siswa;

use App\Common\GenBasic;
use App\Common\GenFirebase;
use App\Controller\BaseController;
use App\Entity\Attendance;
use App\Entity\AttendancePlan;
use App\Entity\Cart;
use App\Entity\CategoriesParent;
use App\Entity\CategoryToParentToLevel;
use App\Entity\District;
use App\Entity\Member;
use App\Entity\Method;
use App\Entity\Notification;
use App\Entity\Pricing;
use App\Entity\Rating;
use App\Entity\Schedule;
use App\Entity\Subscription;
use App\Entity\Tentor;
use App\Entity\TentorSaldo;
use App\Entity\TentorSubject;
use App\Entity\Transaction;
use App\Entity\User;
use App\Repository\AttendancePlanRepository;
use App\Repository\AttendanceRepository;
use App\Repository\CartRepository;
use App\Repository\CategoryToParentToLevelRepository;
use App\Repository\NotificationRepository;
use App\Repository\RatingRepository;
use App\Repository\ScheduleRepository;
use App\Repository\SubscriptionRepository;
use App\Repository\TentorSubjectRepository;
use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class UtilityController extends BaseController
{

    /**
     * UtilityController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    public function checkProfile($user)
    {
        $userProfile = $this->em->getRepository(Member::class)->findOneBy(['user' => $user]);
        $criteria = ['fullName', 'phone'];
        foreach ($criteria as $v) {
            $getter = 'get' . ucfirst($v);
            if ($userProfile->$getter() === null || $userProfile->$getter() === '') {
                return false;
            }
        }

        return true;
    }

    public function getParentCategories()
    {
        return $this->em->getRepository(CategoriesParent::class)->findAll();
    }
    public function getSubjectLevel($id)
    {
        /** @var CategoryToParentToLevelRepository $levelRepo */
        $levelRepo = $this->em->getRepository(CategoryToParentToLevel::class);

        return $levelRepo->createQueryBuilder('p')
            ->select(
                [
                    'l.id',
                    'l.name',
                ]
            )
            ->innerJoin('p.category', 'c')
            ->innerJoin('p.level', 'l')
            ->where('c.id = :id')
            ->setParameter('id', $id)
            ->groupBy('l.id')
            ->getQuery()->getScalarResult();
    }

    public function findTentor($data, $offset = 0, $limit = 5)
    {
        try {
            $category = $data['category'];
            $method = $data['method'];
            $district = $data['district'];
            $level = $data['level'];
            if ($category === null || $level === null || ($method === '1' && $district === null)) {
                return GenBasic::send(202, 'Invalid Parameter!');
            }
            /** @var TentorSubjectRepository $ts */
            $ts = $this->em->getRepository(TentorSubject::class);
            $qb = $ts->createQueryBuilder('ts')
                ->addSelect('p.price as price')
                ->innerJoin('ts.user', 'u')
                ->innerJoin('u.tentorProfile', 'tp')
                ->leftJoin('u.district', 'ds')
                ->leftJoin('u.ratings', 'r')
                ->leftJoin('ts.method', 'm')
                ->where('ts.isAvailable = :status')
                ->andWhere('tp.isActive = :active')
                ->setParameter('status', true)
                ->setParameter('active', true);

            if ($category !== null) {
                $qb->andWhere('ts.category = :category')->setParameter('category', $category);
            }
            if ($level !== null) {
                $qb->andWhere('ts.level = :level')->setParameter('level', $level);
            }
            if ($method !== null) {
                $vMethod = [$method];
                $qb->andWhere('m.id IN (:method)')->setParameter('method', $vMethod);
            }
            if ($district !== null) {
                $v = [$district];
                $qb->andWhere('ds.id IN(:district)')->setParameter('district', $v);
            }

            $city = 'is null';
            if ($method === '1') {
                $city = '= ds.city';
            }
            $qb->innerJoin(Pricing::class, 'p', Join::WITH, 'p.level = ts.level and p.grade = tp.grade and p.city ' . $city . ' and p.method = :mid')
                ->setParameter('mid', $method);
            $tempResults = $qb
                ->groupBy('ts.id')
                ->orderBy('AVG(r.rating)', 'DESC')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult();
            $attributes = [
                'id',
                'slug',
                'user' => [
                    'tentorProfile' => [
                        'fullName',
                        'about',
                        'slugName',
                        'generatedAvatar',
                        'grade' => ['id', 'name'],
                    ],
                    'email',
                    'district' => ['id', 'districtName'],
                    'avgRatings',
                ],
            ];
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($tempResults, $attributes),
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Find Tentor ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Find Tentor ' . $e->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    public function findTentorBySlug($method, $slug)
    {

        $city = 'is null';
        if ($method->getId() === 1) {
            $city = '= ds.city';
        }
        /** @var TentorSubjectRepository $tsRepo */
        $tsRepo = $this->em->getRepository(TentorSubject::class);

        return $tsRepo->createQueryBuilder('ts')
            ->addSelect('p.price as price')
            ->innerJoin('ts.user', 'u')
            ->innerJoin('u.tentorProfile', 'tp')
            ->leftJoin('u.district', 'ds')
            ->leftJoin('ts.method', 'm')
            ->innerJoin(Pricing::class, 'p', Join::WITH, 'p.level = ts.level and p.grade = tp.grade and p.city ' . $city . ' and p.method = :mid')
            ->where('ts.slug = :slug')
            ->andWhere('m.slug = :method')
            ->andWhere('ts.isAvailable = :status')
            ->setParameter('slug', $slug)
            ->setParameter('method', $method->getSlug())
            ->setParameter('status', true)
            ->setParameter('mid', $method->getId())
            ->getQuery()->getOneOrNullResult();
    }

    public function sendSubscription($user, $params = [])
    {
        try {

            $tentorSubject = $params['tsId'];
            $duration = $params['duration'];
            $encounter = $params['encounter'];
            $attendees = $params['attendees'];
            $location = $params['location'];
            $note = $params['note'];
            $method = $params['method'];
            $meet = new \DateTime($params['meet']);
            $address = $params['address'];
            $district = null;
            /** @var Method $vMethod */
            $vMethod = $this->em->getRepository(Method::class)->find($method);
            if (!$vMethod) {
                return GenBasic::send(202, 'Method Not Found!');
            }
            switch ($vMethod->getId()) {
                case 1:
                    $vDuration = $duration / 60;
                    $district = $this->em->getRepository(District::class)->find($location);
                    break;
                case 2:
                    $vDuration = $duration / 30;
                    break;
                default:
                    return GenBasic::send(202, 'Invalid Method Type!');
                    break;
            }

            /** @var TentorSubject $skill */
            $skill = $this->em->getRepository(TentorSubject::class)->find($tentorSubject);

            if (!$skill) {
                return GenBasic::send(202, 'Tentor Subject Not Found!');
            }
            $city = $vMethod->getId() === 1 ? $skill->getUser()->getDistrict()[0]->getCity() : null;
            $grade = $skill->getUser()->getTentorProfile()->getGrade();
            /** @var Pricing $pricing */
            $pricing = $this->em->getRepository(Pricing::class)->findOneBy(['city' => $city, 'level' => $skill->getLevel(), 'grade' => $grade, 'method' => $vMethod]);
            $price = $pricing->getPrice();
            if ($price === null) {
                return GenBasic::send(202, 'Pricing Not Found!');
            }
            $code = 'TP';
            switch ($vMethod->getId()) {
                case 1:
                    $code = 'TP01';
                    break;
                case 2:
                    $code = 'TP02';
                    break;
                default:
                    break;
            }
            date_default_timezone_set('Asia/Jakarta');
            $total = $price * $encounter * $vDuration * $attendees;
            $subscription = new Subscription();
            $subscription->setUser($user)
                ->setStatus(0)
                ->setTentorSubject($skill)
                ->setSubscriptionCode('TB-SUB/' . $user->getId() . '/' . date('YmdHis'));
            $cart = new Cart();
            $cart->setMethod($vMethod)
                ->setDistrict($district)
                ->setDuration($duration)
                ->setEncounter($encounter)
                ->setAttendees($attendees)
                ->setFirstMeet($meet)
                ->setNote($note)
                ->setAmount($price)
                ->setAddress($address)
                ->setDiscount(0)
                ->setExpiredPay(null)
                ->setReferenceId($code . '/' . $user->getId() . '/' . date('YmdHis'))
                ->setTotal($total)
                ->setTransaction(null);
            $cart->setSubscription($subscription);
            $this->em->persist($subscription);
            $this->em->persist($cart);
            $this->em->flush();
            $tentorFcm = $skill->getUser()->getAppFcmToken();
            $notification = null;
            $data = [
                'id' => $subscription->getId(),
                'type' => 'Konfirmasi',
            ];
            if ($tentorFcm !== null) {
                $message = 'Hai, Kamu Dapat Siswa Baru. Silahan Konfirmasi Pesanan!';
                $notification = GenFirebase::SendNotification($tentorFcm, 'Mendapatkan Siswa Baru (TEMAN BELAJAR)', $message, $data);
            }
            $code = 200;
            $response = [
                'order' => 'Success Send Subscription Request',
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Send Subscription Request ' . $err->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    public function cancelSubscription($user, $id)
    {
        try {
            /** @var Subscription $subscription */
            $subscription = $this->getMySubscriptionDetail($user, $id);
            if (!$subscription) {
                return GenBasic::send(202, ['Subscription Not Found!']);
            }
            $subscription->setStatus(6);
            $this->em->flush();
            $code = 200;
            $response = [
                'msg' => 'Success',
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Cancel Subscription Request ' . $err->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    public function getSubscriptionList($user, $status, $offset = 0, $limit = 5)
    {
        try {
            /** @var SubscriptionRepository $subsRepo */
            $subsRepo = $this->em->getRepository(Subscription::class);
            /** @var CartRepository $cartRepo */

            $subscription = $subsRepo->createQueryBuilder('s')
                ->innerJoin('s.cart', 'c')
//                ->leftJoin('c.transaction', 't')
                ->where('s.user = :user')
                ->setParameter('user', $user);
            if ($status !== 'all') {
                $subscription->andWhere('s.status = :status')->setParameter('status', $status);
            }
            $results = $subscription
                ->orderBy('s.id', 'DESC')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()->getResult();
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize(
                    $results,
                    [
                        'id',
                        'tentorSubject' => [
                            'user' => [
                                'tentorProfile' => ['fullName', 'generatedAvatar'],
                            ],
                            'category' => ['name'],
                            'level' => ['name'],
                        ],
                        'requestCart' => [
                            'isExpired',
                        ],
                        'status',
                        'confirmedAt',
                        'subscriptionCode',
                        'hasRenewal',
                        'hasPayment'
                    ]
                ),
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Subscription List ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Subscription List ' . $e->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @param $user
     * @param $id
     *
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getMySubscriptionDetail($user, $id)
    {
        /** @var SubscriptionRepository $subscriptionRepo */
        $subscriptionRepo = $this->em->getRepository(Subscription::class);

        return $subscriptionRepo->createQueryBuilder('t')
            ->innerJoin('t.user', 'u')
            ->where('u.id = :user')
            ->andWhere('t.id = :id')
            ->setParameters(['user' => $user->getId(), 'id' => $id])
            ->getQuery()->getOneOrNullResult();
    }

    /**
     * @param $user
     * @param $id
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getMySubscriptionActive($user, $id)
    {
        /** @var SubscriptionRepository $subscriptionRepo */
        $subscriptionRepo = $this->em->getRepository(Subscription::class);

        return $subscriptionRepo->createQueryBuilder('t')
            ->innerJoin('t.user', 'u')
            ->where('u.id = :user')
            ->andWhere('t.id = :id')
            ->andWhere('t.status = :status')
            ->setParameters(['user' => $user->getId(), 'id' => $id, 'status' => 1])
            ->getQuery()->getOneOrNullResult();
    }

    public function getSubscriptionNotConfirmed($user)
    {
        try {
            /** @var SubscriptionRepository $subsRepo */
            $subsRepo = $this->em->getRepository(Subscription::class);
            $subscription = $subsRepo->createQueryBuilder('s')
                ->select('COUNT(s.id) as count')
                ->where('s.user = :user')
                ->andWhere('s.confirmedAt is null')
                ->setParameter('user', $user)
                ->getQuery()->getSingleScalarResult();
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => $subscription,
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Subscription Notification ' . $err->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    public function getCartList($user, $limit = 1, $offset = 0)
    {
        try {
            /** @var CartRepository $cartRepo */
            $cartRepo = $this->em->getRepository(Cart::class);
            $qb = $cartRepo->createQueryBuilder('c')
                ->innerJoin('c.subscription', 's')
                ->where('s.user = :user')
                ->andWhere('c.transaction is null')
                ->andWhere('c.expiredPay is not null and c.expiredPay > :now');
            date_default_timezone_set('Asia/Jakarta');
            $now = new \DateTime();
            $cart = $qb->setParameter('user', $user)
                ->setParameter('now', $now)
                ->orderBy('c.expiredPay', 'ASC')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult();
            $code = 200;
            $attribute = [
                'id',
                'status',
                'total',
                'expiredPay',
                'subscription' => [
                    'tentorSubject' => [
                        'user' => [
                            'tentorProfile' => ['fullName', 'generatedAvatar'],
                        ],
                        'category' => [
                            'name',
                        ],
                        'level' => [
                            'name',
                        ],
                    ],
                ],
            ];
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($cart, $attribute),
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Cart List ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Cart List ' . $e->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    public function getCartListById($user, $id = [])
    {
//        if (!is_array($id)) {
//            return null;
//        }
        /** @var CartRepository $cartRepo */
        $cartRepo = $this->em->getRepository(Cart::class);
        $qb = $cartRepo->createQueryBuilder('c')
            ->innerJoin('c.subscription', 's')
            ->where('s.user = :user')
            ->andWhere('c.transaction is null')
            ->andWhere('c.expiredPay is not null and c.expiredPay > :now');
        date_default_timezone_set('Asia/Jakarta');
        $now = new \DateTime();
        $orStatements = $qb->expr()->orX();
        foreach ($id as $v) {
            $orStatements->add(
                $qb->expr()->eq('c.id', $v)
            );
        }
        $qb->andWhere($orStatements);

        return $qb->setParameter('user', $user)
            ->setParameter('now', $now)
            ->orderBy('c.id', 'DESC')
            ->getQuery()
            ->getResult();

    }

    public function getCartCount($user)
    {
        try {
            /** @var CartRepository $cartRepo */
            $cartRepo = $this->em->getRepository(Cart::class);
            $qb = $cartRepo->createQueryBuilder('c')
                ->select('COUNT(c.id) as count')
                ->innerJoin('c.subscription', 's')
                ->where('s.user = :user')
                ->andWhere('c.transaction is null')
                ->andWhere('c.expiredPay is not null and c.expiredPay > :now');
            date_default_timezone_set('Asia/Jakarta');
            $now = new \DateTime();
            $cart = $qb->setParameter('user', $user)
                ->setParameter('now', $now)
                ->getQuery()
                ->getSingleScalarResult();
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => $cart,
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Count Cart ' . $err->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @param $user
     * @param $id
     *
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getCartById($user, $id)
    {
        /** @var CartRepository $cartRepo */
        $cartRepo = $this->em->getRepository(Cart::class);
        $qb = $cartRepo->createQueryBuilder('c')
            ->innerJoin('c.subscription', 's')
            ->where('s.user = :user')
            ->andWhere('c.transaction is null')
            ->andWhere('c.id = :id')
            ->andWhere('c.expiredPay is not null or c.expiredPay > :now');
        date_default_timezone_set('Asia/Jakarta');
        $now = new \DateTime();

        return $qb->setParameter('user', $user)
            ->setParameter('now', $now)
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function sendCheckOut($user, $bank, $id = [])
    {
        $cart = $this->getCartListById($user, $id);
        if (count($cart) <= 0) {
            return false;
        }

        $summary = 0;
        /** @var Cart $v */
        foreach ($cart as $v) {
            $summary += $v->getTotal();
        }
        $random = rand(10, 999);
        $total = $summary + $random;
        $transaction = new \App\Controller\Admin\TransactionController($this->em);
        $transaction->setUser($user);
        $newTransaction = $transaction->newTransaction($cart, $total, 'cart', $bank);
        if (!$newTransaction) {
            $this->em->rollback();

            return false;
        }
        $this->em->flush();

        return $cart;
    }

    public function getInvoiceList($user, $offset = 0, $limit = 5)
    {
        try {
            date_default_timezone_set('Asia/Jakarta');
            $now = new \DateTime();
            /** @var TransactionRepository $transRepo */
            $transRepo = $this->em->getRepository(Transaction::class);
            $transaction = $transRepo->createQueryBuilder('t')
                ->where('t.user = :user')
                ->andWhere('t.isPaid is null')
                ->andWhere('t.paymentExpiration > :now')
                ->andWhere('t.confirmationInfo is null')
                ->setParameter('user', $user)
                ->setParameter('now', $now)
                ->orderBy('t.paymentExpiration', 'ASC')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult();
            $code = 200;
            $attribute = [
                'id',
                'amount',
                'slug',
                'paymentExpiration',
                'isPaid',
                'tryOutRegistrant' => [
                    'id',
                    'tryOut' => [
                        'id',
                        'title',
                        'startDay',
                        'finalDay',
                    ],
                ],
                'cart' => [
                    'id',
                    'subscription' => [
                        'tentorSubject' => [
                            'user' => [
                                'tentorProfile' => [
                                    'fullName',
                                    'generatedAvatar',
                                ],
                            ],
                            'category' => ['name'],
                            'level' => ['name'],
                        ],
                    ],
                    'total',
                ],
            ];
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($transaction, $attribute),
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Load Invoice List ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Invoice List ' . $e->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @param $user
     * @param $id
     *
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getInvoiceById($user, $id)
    {
        /** @var TransactionRepository $transRepo */
        $transRepo = $this->em->getRepository(Transaction::class);

        return $transRepo->createQueryBuilder('t')
            ->where('t.user = :user')
            ->andWhere('t.id = :id')
            ->setParameter('user', $user)
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getInvoiceCount($user)
    {
        try {
            date_default_timezone_set('Asia/Jakarta');
            $now = new \DateTime();
            /** @var TransactionRepository $transRepo */
            $transRepo = $this->em->getRepository(Transaction::class);
            $transaction = $transRepo->createQueryBuilder('t')
                ->select('COUNT(t.id) as count')
                ->where('t.user = :user')
                ->andWhere('t.isPaid is null')
                ->andWhere('t.paymentExpiration > :now')
                ->andWhere('t.confirmationInfo is null')
                ->setParameter('user', $user)
                ->setParameter('now', $now)
                ->orderBy('t.paymentExpiration', 'ASC')
                ->getQuery()
                ->getSingleScalarResult();
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => $transaction,
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Load Invoice Count ' . $err->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    public function getPaymentList($user, $status, $offset = 0, $limit = 5)
    {
        try {
            /** @var TransactionRepository $transRepo */
            $transRepo = $this->em->getRepository(Transaction::class);
            $qb = $transRepo->createQueryBuilder('t')
                ->where('t.user = :user')
                ->andWhere('t.confirmationInfo is not null')
                ->setParameter('user', $user);
            if ($status === '6') {
                $qb->andWhere('t.isPaid = :isPaid')->setParameter('isPaid', 0);
            } elseif ($status === '1') {
                $qb->andWhere('t.isPaid = :isPaid')->setParameter('isPaid', 1);
            } elseif ($status === '0') {
                $qb->andWhere('t.isPaid is null');
            }
            $transaction = $qb->orderBy('t.id', 'DESC')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult();
            $code = 200;
            $attribute = [
                'id',
                'amount',
                'slug',
                'paymentExpiration',
                'isPaid',
                'confirmationInfo',
                'tryOutRegistrant' => [
                    'id',
                    'tryOut' => [
                        'id',
                        'title',
                        'startDay',
                        'finalDay',
                    ],
                ],
                'cart' => [
                    'id',
                    'subscription' => [
                        'tentorSubject' => [
                            'user' => [
                                'tentorProfile' => [
                                    'fullName',
                                    'generatedAvatar',
                                ],
                            ],
                            'category' => ['name'],
                            'level' => ['name'],
                        ],
                    ],
                    'total',
                ],

            ];
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize($transaction, $attribute),
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Load Invoice List ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Invoice List ' . $e->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    public function getAttendance($user, $id)
    {
        try {
            /** @var AttendanceRepository $attendance */
            $attendance = $this->em->getRepository(Attendance::class);
            $results = $attendance->createQueryBuilder('a')
                ->innerJoin('a.subscription', 's')
                ->where('s.id = :id')
                ->andWhere('s.user = :user')
                ->setParameter('id', $id)
                ->setParameter('user', $user)
                ->getQuery()->getResult();
            $code = 200;
            $response = [
                'msg' => 'Berhasil Mengunduh Data Absensi',
                'data' => GenBasic::CustomNormalize(
                    $results,
                    [
                        'id',
                        'generatedStart',
                        'startAt',
                        'isPresence',
                        'isActive',
                    ]
                ),
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Load My Attendance ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize My Attendance ' . $e->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    public function getActiveAttendance($user)
    {
        try {
            /** @var AttendanceRepository $attendRepo */
            $attendRepo = $this->em->getRepository(Attendance::class);
            $attendance = $attendRepo->createQueryBuilder('a')
                ->innerJoin('a.subscription', 's')
                ->where('s.user = :user')
                ->andWhere('a.isPresence = :status')
                ->setParameter('status', 0)
                ->setParameter('user', $user)
                ->getQuery()->getResult();
            $code = 200;
            $response = [
                'msg' => 'Berhasil Mengunduh Data Absensi Yang Sedang Berjalan',
                'data' => GenBasic::CustomNormalize(
                    $attendance,
                    [
                        'id',
                        'isPresence',
                        'isActive',
                        'generatedStart',
                        'generatedFinish',
                        'subscription' => [
                            'tentorSubject' => [
                                'category' => ['name'],
                                'level' => ['name'],
                                'user' => [
                                    'tentorProfile' => [
                                        'fullName',
                                        'avatar',
                                        'generatedAvatar',
                                    ],
                                ],
                            ],
                        ],
                    ]
                ),
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Patch Attendance ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Attendance ' . $e->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    /**
     * @param $user
     * @param $id
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getDetailAttendance($user, $id)
    {
        /** @var AttendanceRepository $attendanceRepo */
        $attendanceRepo = $this->em->getRepository(Attendance::class);
        return $attendanceRepo->createQueryBuilder('a')
            ->innerJoin('a.subscription', 's')
            ->where('s.user = :user')
            ->andWhere('a.id = :id')
            ->setParameters(['user' => $user, 'id' => $id])
            ->getQuery()->getOneOrNullResult();
    }

    public function setManualPresence($user, $id)
    {
        try {
            /** @var AttendanceRepository $attendRepo */
            $attendRepo = $this->em->getRepository(Attendance::class);
            /** @var Attendance $attendance */
            $attendance = $attendance = $attendRepo->createQueryBuilder('a')
                ->innerJoin('a.subscription', 's')
                ->where('s.user = :user')
                ->andWhere('a.isPresence = :status')
                ->andWhere('a.isActive = :active')
                ->andWhere('a.id = :id')
                ->setParameter('status', 0)
                ->setParameter('active', 1)
                ->setParameter('id', $id)
                ->setParameter('user', $user)
                ->getQuery()->getOneOrNullResult();
            if (!$attendance) {
                return GenBasic::send(202, ['msg' => 'Absensi Tidak Di Temukan']);
            }
            date_default_timezone_set('Asia/Jakarta');
            /** @var Cart $cart */
            $cart = $attendance->getCart();
            /** @var Subscription $subscription */
            $subscription = $attendance->getSubscription();
            /** @var User $user */
            $tentorUser = $subscription->getTentorSubject()->getUser();
            $category = $subscription->getTentorSubject()->getCategory()->getName();
            $level = $subscription->getTentorSubject()->getLevel()->getName();
            $method = $cart->getMethod()->getName();
            /** @var Tentor $tentor */
            $tentor = $tentorUser->getTentorProfile();
            /** @var Member $member */
            $member = $user->getMemberProfile();
            $currentBalance = $tentor->getBalance();
            $currentPoint = $tentor->getPoint();
            $newSaldo = $currentBalance + ((0.85 * $cart->getAmount()) * $cart->getAttendees());
            $newPoint = $currentPoint + 1;
            $medal = $member->getMedal() + 1;
            $balance = new TentorSaldo();
            $balance->setUser($tentorUser)
                ->setType(0)
                ->setAmount((0.85 * $cart->getAmount()) * $cart->getAttendees())
                ->setDescription($method . ' ' . $category . ' ' . $level);
            $tentor->setBalance($newSaldo)->setPoint($newPoint);
            $this->em->persist($balance);
            $attendance->setIsPresence(true)->setPoint(1);
            $member->setMedal($medal);
            $this->em->flush();;
            $code = 200;
            $response = [
                'msg' => 'Berhasil Melakukan Absensi',
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Set Presence ' . $err->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    public function getTentorSchedule($tsId, $day)
    {
        try {
            /** @var TentorSubject $tentorSubject */
            $tentorSubject = $this->em->getRepository(TentorSubject::class)->findOneBy(['id' => $tsId]);
            /** @var User $tentor */
            $tentor = $tentorSubject->getUser();
            /** @var ScheduleRepository $scheduleRepo */
            $scheduleRepo = $this->em->getRepository(Schedule::class);
            $schedule = $scheduleRepo->createQueryBuilder('s')
                ->leftJoin('s.user', 'u')
                ->where('u.id = :user')
                ->andWhere('s.day = :day')
                ->setParameters(['user' => $tentor->getId(), 'day' => $day])
                ->getQuery()->getResult();

            /** @var AttendancePlanRepository $schedulePlanRepo */
            $schedulePlanRepo = $this->em->getRepository(AttendancePlan::class);

            $plan = $schedulePlanRepo->createQueryBuilder('p')
                ->innerJoin('p.subscription', 's')
                ->innerJoin('s.tentorSubject', 'ts')
                ->where('ts.user = :user')
                ->andWhere('p.status = :status')
                ->setParameters(['user' => $tentor, 'status' => 1])
                ->getQuery()->getResult();
            $tempSchedule = [];
            $tempPlan = [];
            /** @var Schedule $v */
            foreach ($schedule as $key => $v) {
                $tempSchedule[$key]['id'] = $v->getId();
                $tempSchedule[$key]['day'] = $v->getDay();
                $tempSchedule[$key]['time'] = $v->getGeneratedTime();
                $tempSchedule[$key]['flag'] = true;
            }
            /** @var AttendancePlan $vPlan */
            foreach ($plan as $key => $vPlan) {
                $tempPlan[$key]['id'] = $vPlan->getSchedule()->getId();
            }

            foreach ($tempSchedule as $key => $temp) {
                $keyMatch = array_search($temp['id'], array_column($tempPlan, 'id'));
                if ($keyMatch !== false) {
                    $tempSchedule[$key]['flag'] = false;
                }
            }
            $code = 200;
            $response = [
                'msg' => 'Berhasil Mengunduh Data Jadwal Guru',
                'data' => $tempSchedule,
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Tentor Schedule ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Tentor Schedule ' . $e->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    public function getNotificationList($user, $offset, $limit)
    {
        try {
            /** @var NotificationRepository $notifRepo */
            $notifRepo = $this->em->getRepository(Notification::class);
            $notification = $notifRepo->createQueryBuilder('n')
                ->where('n.recipient = :user')
                ->setParameter('user', $user)
                ->orderBy('n.id', 'DESC')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()->getResult();
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize(
                    $notification,
                    [
                        'id',
                        'message',
                        'sender' => [
                            'id',
                        ],
                        'slug',
                        'identifier',
                        'status',
                        'createdAt',
                    ]
                ),
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Notification List ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Notification List ' . $e->getMessage(),
            ];
        }

        return GenBasic::send($code, $response);
    }

    public function getSubscriptionListToReview($user)
    {
        try {
            /** @var SubscriptionRepository $subscriptionRepo */
            $subscriptionRepo = $this->em->getRepository(Subscription::class);
            $subscription = $subscriptionRepo->createQueryBuilder('s')
                ->innerJoin('s.attendance', 'a')
                ->where('s.user = :user')
                ->setParameter('user', $user)
                ->orderBy('a.createdAt', 'DESC')
                ->getQuery()->getResult();
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize(
                    $subscription,
                    [
                        'id',
                        'tentorSubject' => [
                            'user' => [
                                'tentorProfile' => ['fullName', 'generatedAvatar'],
                            ],
                            'category' => ['name'],
                            'level' => ['name'],
                        ],
                        'subscriptionCode',
                        'rating' => [
                            'rating',
                            'review'
                        ],
                        'hasReview'
                    ]
                ),
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Subscription To Review List ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Subscription To Review List ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @param $user
     * @param $id
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getSubscriptionReviewDetail($user, $id)
    {
        /** @var SubscriptionRepository $subsRepo */
        $subsRepo = $this->em->getRepository(Subscription::class);
        return $subsRepo->createQueryBuilder('s')
            ->where('s.user = :user')
            ->andWhere('s.id = :id')
            ->setParameter('user', $user)
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function sendReviews($user, $data = [])
    {
        $id = $data['id'];
        $rating = $data['rating'];
        $review = $data['review'];
        $isAnonymous = $data['isAnonymous'];
        $tentorId = $data['tentorId'];
        try {
            $subscription = $this->em->getRepository(Subscription::class)->find($id);
            if (!$subscription) {
                return GenBasic::send(202, ['msg' => 'Paket Berlangganan Tidak Di Temukan']);
            }
            /** @var Rating $reviewRepo */
            $reviewRepo = $this->em->getRepository(Rating::class)->findOneBy(['subscription' => $subscription]);

            if (!$reviewRepo) {
                $tentor = $this->em->getRepository(User::class)->find($tentorId);
                if (!$tentor) {
                    return GenBasic::send(202, ['msg' => 'Guru Tidak Di Temukan']);
                }
                $newRating = new Rating();
                $newRating->setRating($rating)
                    ->setReview($review)
                    ->setIsAnonymous($isAnonymous)
                    ->setSubscription($subscription)
                    ->setTentor($tentor)
                    ->setUser($user);
                $this->em->persist($newRating);
            } else {
                $reviewRepo->setRating($rating)
                    ->setReview($review)
                    ->setIsAnonymous($isAnonymous);
            }
            $this->em->flush();
            $code = 200;
            $response = [
                'msg' => 'Berhasil Memberikan Ulasan Kepada Guru'
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Gagal Memberikan Ulasan ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    public function getTentorReviews($id)
    {
        try {
            /** @var RatingRepository $ratingRepo */
            $ratingRepo = $this->em->getRepository(Rating::class);
            $rating = $ratingRepo->createQueryBuilder('r')
                ->innerJoin('r.tentor', 't')
                ->where('t.id = :id')
                ->setParameter('id', $id)
                ->getQuery()->getResult();
            $code = 200;
            $response = [
                'msg' => 'Success',
                'data' => GenBasic::CustomNormalize(
                    $rating,
                    [
                        'id',
                        'tentor' => [
                            'tentorProfile' => ['fullName', 'avatar','generatedAvatar'],
                            'avgRatings',
                        ],
                        'review',
                    ]
                ),
            ];
        } catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Fetch Tentor Reviews ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Tentor Reviews ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    public function createManualRenewal($user, $id)
    {
        try {
            /** @var Subscription $subscription */
            $subscription = $this->getMySubscriptionDetail($user, $id);
            if (!$subscription) {
                return GenBasic::send(202, ['msg' => 'Detail Berlangganan Tidak Di Temukan']);
            }
            if ($subscription->getHasRenewal()) {
                return GenBasic::send(202, ['msg' => 'Kamu Masih Memiliki Perpanjangan Yang Belum Di Checkout. Silahkan Lakukan Checkout dan Melanjutkan Ke Pembayaran']);
            }
            $cart = $subscription->getCartActive();
            if (!$cart) {
                return GenBasic::send(202, ['msg' => 'Paket Aktif Tidak Di Temukan']);
            }

            /** @var Cart $newCart */
            $newCart = clone $cart;
            $code = 'TP';
            switch ($newCart->getMethod()->getId()) {
                case 1:
                    $code = 'TP01';
                    break;
                case 2:
                    $code = 'TP02';
                    break;
                case 3:
                    $code = 'TK';
                    break;
                default:
                    break;
            }
            date_default_timezone_set('Asia/Jakarta');
            $now = new \DateTime();
            $expiredPay = $now->modify('+2 day');
            $newCart->setId(null)
                ->setTransaction(null)
                ->setIsActive(0)
                ->setExpiredPay($expiredPay)
                ->setCreatedAt(new \DateTime())
                ->setUpdatedAt(new \DateTime())
                ->setReferenceId($code . '/' . $user->getId() . '/' . date('YmdHis'));
            $this->em->persist($newCart);
            $this->em->flush();
            $code = 200;
            $response = [
                'msg' => 'Paket Berlangganan Kamu Berhasil Di Perpanjang. Segera Lakukan Pembayaran Sebelum Perpanjangan Kamu Usang'
            ];
        } catch (\Exception $e) {
            $code = 500;
            $response = [
                'msg' => 'Terjadi Kesalahan. Gagal Melakukan Perpanjangan ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }


}
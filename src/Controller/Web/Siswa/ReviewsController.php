<?php


namespace App\Controller\Web\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\Rating;
use App\Entity\Subscription;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReviewsController extends BaseController
{

    private $utility;

    /**
     * ReviewsController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
        $this->utility = new \App\Controller\Web\Siswa\UtilityController($this->em);
    }

    /**
     * @return Response
     * @Route("/member/reviews")
     */
    public function reviews()
    {
        return $this->render('siswa/ulasan/Ulasan.html.twig');
    }

    /**
     * @param $id
     * @return Response
     * @Route("/member/reviews/detail/{id}")
     */
    public function createReviewsPage($id)
    {
        try {
            $subscription = $this->utility->getSubscriptionReviewDetail($this->getUser(), $id);
            if (!$subscription) {
                return new Response('Subscription Not Found!');
            }
            return $this->render('siswa/ulasan/beriUlasan.html.twig', ['subscription' => $subscription]);
        } catch (NonUniqueResultException $e) {
            return new Response('Subscription Non Unique');
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/member/reviews/list", methods="GET|HEAD")
     */
    public function getSubscriptionListToReview()
    {
        return $this->utility->getSubscriptionListToReview($this->getUser());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/member/reviews/create", methods="POST|HEAD")
     */
    public function sendReviews()
    {
        $data = [
            'id' => $this->req->request->get('id'),
            'rating' => $this->req->request->get('rating'),
            'review' => $this->req->request->get('review'),
            'isAnonymous' => $this->req->request->get('isAnonymous') === "true" ? true : false,
            'tentorId' => $this->req->request->get('tentorId')
        ];
        return $this->utility->sendReviews($this->getUser(), $data);
    }


}
<?php


namespace App\Controller\Web\Siswa;


use App\Common\GenBasic;
use App\Controller\BaseController;
use App\Entity\District;
use App\Entity\Member;
use App\Entity\MemberClass;
use App\Entity\MemberClassParent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProfileController
 * @package App\Controller\Web\Siswa
 */
class ProfileController extends BaseController
{
    /**
     * ProfileController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }


    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/member/profile", methods="GET|HEAD")
     */
    public function profile()
    {
        if (!$this->getUser()) {
            return $this->render('/');
        }

        $classMember = $this->em->getRepository(MemberClassParent::class)->findAll();
        $profile = $this->em->getRepository(Member::class)->findOneBy(['user' => $this->getUser()]);
        return $this->render('siswa/auth/profile.html.twig', ['profile' => $profile, 'kelas' => $classMember]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/member/profile/update", methods="POST|HEAD")
     */
    public function profileUpdate()
    {
        $session = new Session();
        $redirect = $session->get('redirectTo');
        if (!$session->has('redirectTo') || $redirect === null) {
            $redirect = '/';
        }
        $this->denyAccessUnlessGranted('ROLE_MEMBER');
        $fullName = $this->req->request->get('fullName');
        $phone = $this->req->request->get('phone');
        $class = $this->req->request->get('class');
        $districtId = $this->req->request->get('districtId');
        $address = $this->req->request->get('address');
        if($fullName === '' || $phone === ''){
            $this->addFlash('failed', 'Mohon Lengkapi Data Nama Dan Nomor Handphone');
            return $this->redirect('/member/profile');
        }

        $memberClass = $this->em->getRepository(MemberClass::class)->find($class);
//        if(!$memberClass){
//            $this->addFlash('failed', 'Mohon Lengkapi Semua Data!');
//            return $this->redirect('/member/profile');
//        }
        /** @var Member $profile */
        $profile = $this->em->getRepository(Member::class)->findOneBy(['user' => $this->getUser()]);
        $district = $this->em->getRepository(District::class)->find($districtId);
        $profile->setFullName($fullName);
        $profile->setPhone($phone);
        $profile->setClass($memberClass);
        $profile->setAddress($address);
        $profile->setDistrict($district);
        $this->em->flush();
        return $this->redirect($redirect);
    }
}
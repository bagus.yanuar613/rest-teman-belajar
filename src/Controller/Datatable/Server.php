<?php
declare(strict_types=1);

namespace App\Controller\Datatable;

use App\Controller\Admin\FasilitasController;
use App\Entity\FacilityCategory;
use App\Entity\FacilityProduct;
use App\Entity\Image;
use App\Repository\FacilityCategoryRepository;
use Carbon\Carbon;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class Server
 * @package App\Controller\Datatable
 */
class Server extends AbstractController
{
    protected $em;

    protected $controller;

    /**
     * Server constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @Route("/uploads-data", name="get-filemanager")
     *
     *
     * @return array|JsonResponse
     */
    public function getFileMan(Request $request)
    {
        $images = $this->em->getRepository(Image::class)->getGallery($this->getUser());

        return $this->json($images);
    }

    /**
     * @param Request $request
     * @Route("/uploads-delete", name="delete-filemanager")
     *
     *
     * @return array|JsonResponse
     */
    public function deleteFileMan(Request $request)
    {
        try {
            $image = $this->em->getRepository(Image::class)->findOneBy(
                [
                    'id' => $request->get('id'),
                    'author' => $this->getUser(),
                ]
            );
            $this->em->remove($image);
            $this->em->flush();
            $this->addFlash('success', 'Gambar berhasil dihapus');
            $response = [
                'code' => 200,
                'status' => 'Berhasil dihapus',
                'label' => 'success',
            ];
        } catch (\Exception $e) {
            $response = [
                'code' => 403,
                'status' => 'Terjadi kesalahan saat menghapus data ' . $e->getMessage(),
                'label' => 'danger',
            ];
            $this->addFlash('info', 'Gambar gagal dihapus');

        }

        return $this->json($response, $response['code']);
    }

    /**
     * @param Request $request
     * @Route("/images-upload", name="image-upload")
     *
     *
     * @return array|JsonResponse
     */
    public function uploadImage(Request $request)
    {
        $data = [
            'code' => 501,
            "message" => "Error",
        ];
        try {
            $file = $request->files->get('file');
            $targetDir = '../public/images/upload';
            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = str_replace(' ', '-', $originalFilename);
            $fileName = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();
            $file->move($targetDir, $fileName);
            $data = [
                'code' => 200,
                "file" => "/images/upload/$fileName",
            ];

            $image = new Image();
            $image->setImagePath($data['file']);
            $image->setAuthor($this->getUser());
            $this->em->persist($image);
            $this->em->flush();

        } catch (\Exception $e) {
            $data = [
                'code' => 501,
                "file" => $e->getMessage(),
            ];
        }

        return $this->json($data, $data['code']);
    }

    /**
     * @return string
     */
    protected function deleteButton($data)
    {
        return '<a name="action" data-cred="' . $data['id'] . '" id="deleteData" class="btn btn-sm btn-danger btn-rose" ><i class="fa fa-trash"></i></a>';
    }

    /**
     * @return string
     */
    protected function deleteAccountButton($data)
    {
        return '<a name="action" data-cred="' . $data['id'] . '" id="deleteAccount" class="btn btn-sm btn-danger btn-rose" ><i class="fa fa-trash"></i></a>';
    }

    /**
     * @return string
     */
    protected function deletePricingWilayahButton($data)
    {
        return '<a name="action" data-cred="'.$data['id'].'" data-cityname="'.$data['cityName'].'" data-city="'.$data['cityid'].'" id="deletePricingData" class="btn btn-sm btn-danger btn-rose" ><i class="fa fa-trash"></i></a>';
    }

    /**
     * @return string
     */
    protected function deletePricingButton($data)
    {
        return '<a name="action" data-cred="'.$data['id'].'" data-subject="'.$data['subjectId'].'" data-parent="'.str_replace(' ','',$data['parentName']).'" data-name="'.$data['name'].' " id="deletePricingData" class="btn btn-sm btn-danger btn-rose" ><i class="fa fa-trash"></i></a>';
    }

    /**
     * @return string
     */
    protected function deleteSubjectButton($data)
    {
        return '<a name="action" data-cred="'.$data['id'].'" data-category="'.$data['category'].'" data-tbid="'.str_replace(' ','',$data['parentName']).''.$data['id'].'" data-idcategory="'.$data['categoryId'].'" id="deleteSubject" class="btn btn-sm btn-danger btn-rose" ><i class="fa fa-trash"></i></a>';
    }

    /**
     * @return string
     */
    protected function editButton($data)
    {
        if(isset($data['expiredPay'])){
            $data['expiredPay'] = Carbon::parse($data['expiredPay'])->isoFormat('LL, HH:mm');
        }
        if(isset($data['paymentExpiration'])){
            $data['paymentExpiration'] = Carbon::parse($data['paymentExpiration'])->isoFormat('LL, HH:mm');
        }
        if (isset($data['createdAt'])){
            $data['createdAt'] = Carbon::parse($data['createdAt'], 'Asia/Jakarta')->isoFormat('LL, HH:mm');
        }
        if (isset($data['updatedAt'])){
            $data['updatedAt'] = Carbon::parse($data['updatedAt'], 'Asia/Jakarta')->isoFormat('LL, HH:mm');
        }
        $dataAttr = '';
        foreach ($data as $key => $val) {
            $dataAttr .= ' data-' . $key . '="' . $val . '" ';
        }

        return '<a name="action" data-cred="' . $data['id'] . '"  ' . $dataAttr . ' id="editData"  class="btn btn-sm btn-warning btn-orange"><i class="fa fa-edit"></i></a>';
    }

    /**
     * @param $data
     *
     * @return string
     */
    protected function editBankButton($data)
    {
        if(isset($data['expiredPay'])){
            $data['expiredPay'] = Carbon::parse($data['expiredPay'])->isoFormat('LL, HH:mm');
        }
        if(isset($data['paymentExpiration'])){
            $data['paymentExpiration'] = Carbon::parse($data['paymentExpiration'])->isoFormat('LL, HH:mm');
        }
        if (isset($data['createdAt'])){
            $data['createdAt'] = Carbon::parse($data['createdAt'], 'Asia/Jakarta')->isoFormat('LL, HH:mm');
        }
        if (isset($data['updatedAt'])){
            $data['updatedAt'] = Carbon::parse($data['updatedAt'], 'Asia/Jakarta')->isoFormat('LL, HH:mm');
        }
        $dataAttr = '';
        foreach ($data as $key => $val) {
            $dataAttr .= ' data-' . $key . '="' . $val . '" ';
        }

        return '<a name="action" data-cred="' . $data['id'] . '"  ' . $dataAttr . ' id="editBank"  class="btn btn-sm btn-warning btn-orange"><i class="fa fa-edit"></i></a>';
    }


    /**
     * @return string
     */
    protected function detailPenarikanButton($data)
    {
        return '<a name="action" data-cred="' . $data['userid'] . '" data-penarikan="'.$data['id'].'"  id="detailData"  class="btn btn-sm btn-warning btn-orange" style="border-radius: 5px">Detail</a>';
    }

    /**
     * @return string
     */
    protected function detailButton($data)
    {
//        if(isset($data['createdAt'])){
//            $data['createdAt'] = Carbon::parse($data['createdAt'])->isoFormat('LL, HH:mm');
//        }
//        $dataAttr = '';
//        foreach ($data as $key => $val) {
//            $dataAttr .= ' data-'.$key.'="'.$val.'" ';
//        }

        return '<a name="action" data-cred="' . $data['id'] . '"  id="detailData"  class="btn btn-sm btn-warning btn-orange" style="border-radius: 5px">Detail</a>';
    }

    /**
     * @param $data
     *
     * @return string
     */
    protected function detailTransButton($data)
    {
//        if(isset($data['createdAt'])){
//            $data['createdAt'] = Carbon::parse($data['createdAt'])->isoFormat('LL, HH:mm');
//        }
//        $dataAttr = '';
//        foreach ($data as $key => $val) {
//            $dataAttr .= ' data-'.$key.'="'.$val.'" ';
//        }

        return '<a name="action" data-cred="' . $data['id'] . '"  id="detailData"  class="btn btn-sm btn-warning btn-orange" style="border-radius: 5px">Detail</a>';
    }

    /**
     * @param $data
     *
     * @return string
     */
    protected function activeButton($data)
    {
        if ($data['isActive'] === false) {
            return '<a name="action" data-cred="' . $data['id'] . '" data-name="' . $data['name'] . '" data-isActive="' . $data['isActive'] . '" id="activeData"  style="border-radius: 5px" class="btn waves-effect blue  waves-light btn-small tooltipped" data-position="top" data-tooltip="Ganti Status Aktif"><i class="material-icons">check</i></a>';
        } else {
            return '<a name="action" data-cred="' . $data['id'] . '" data-name="' . $data['name'] . '" data-isActive="' . $data['isActive'] . '" id="activeData"  style="border-radius: 5px" class="btn waves-effect red waves-light btn-small tooltipped" data-position="top" data-tooltip="Ganti Status Tidak Aktif"><i class="material-icons">clear</i></a>';
        }
    }

    /**
     *
     * @return string
     */
    protected function showButton($data)
    {
        return '<a name="action" data-cred="' . $data['id'] . '" id="showData" style="border-radius: 5px" class="btn waves-effect green waves-light btn-small tooltipped" data-position="top" data-tooltip="Lihat Detail"><i class="material-icons">check</i></a>';
    }

    /**
     * @param $data
     *
     * @return string
     */
    protected function checkButton($data)
    {
        return '<a name="action" data-cred="' . $data['id'] . '" data-slug="' . $data['slug'] . '" id="checkData" style="border-radius: 5px" class="btn waves-effect green waves-light btn-small tooltipped" data-position="top" data-tooltip="Lihat Detail"><i class="material-icons">remove_red_eye</i></a>';
    }

    /**
     * @param $data
     *
     * @return string
     */
    protected function confirmButton($data)
    {
        if ($data['isConfirmed'] == '0') {
            return '<a name="action" data-cred="' . $data['id'] . '" id="confirmData" data-confirm="' . $data['isConfirmed'] . '" style="border-radius: 5px" class="btn waves-effect green waves-light btn-small tooltipped" data-position="top" data-tooltip="Lihat Detail"><i class="material-icons">event</i></a>';
        } elseif ($data['isConfirmed'] == '1') {
            return '<a name="action" data-cred="' . $data['id'] . '" id="confirmData" data-confirm="' . $data['isConfirmed'] . '" style="border-radius: 5px" class="btn waves-effect blue waves-light btn-small tooltipped" data-position="top" data-tooltip="Lihat Detail"><i class="material-icons">check</i></a>';
        } else {
            return '<a name="action" data-cred="' . $data['id'] . '" id="confirmData" data-confirm="' . $data['isConfirmed'] . '" style="border-radius: 5px" class="btn waves-effect red waves-light btn-small tooltipped" data-position="top" data-tooltip="Lihat Detail"><i class="material-icons">clear</i></a>';
        }
    }

    /**
     *
     * @return string
     */
    protected function viewButton($data)
    {
        return '<a name="action" data-cred="' . $data['userId'] . '" id="showData" style="border-radius: 5px" class="btn waves-effect green waves-light btn-small tooltipped" data-position="top" data-tooltip="Lihat Detail"><i class="material-icons">remove_red_eye</i></a>';
    }

    /**
     * @param $data
     *
     * @return string
     */
    protected function viewMButton($data)
    {
        return '<a name="action" data-cred="' . $data['id'] . '" id="showData" style="border-radius: 5px" class="btn waves-effect green waves-light btn-small tooltipped" data-position="top" data-tooltip="Lihat Detail"><i class="material-icons">remove_red_eye</i></a>';
    }

    /**
     * @param Request $request
     * @Route("/datatable/get", name="datatable-get")
     *
     *
     * @return array|JsonResponse
     */
    public
    function getData(
        Request $request
    )
    {
        try {
            $ctrl = 'App\\Controller\\' . $request->get('space') . '\\' . $request->get('class');
            $this->controller = new $ctrl($this->em);
            $orderDatatable = $request->get('order');
            $orderColumn = $orderDatatable[0]['column'] - 1;
            $columnOrder = $this->controller->tableFields[$orderColumn < 0 ? 0 : $orderColumn]['sql'];
            $orderDir = $orderDatatable[0]['dir'];

            $repo = $this->em->getRepository($this->controller->class);

            $selectString = '';
            foreach ($this->controller->tableFields as $val) {
                $selectString .= $val['sql'] . ',';
            }

            $selectString = substr($selectString, 0, -1);
            $where = $this->controller->requiredWhere;
            $joint = $this->controller->joint;
            $groupBy = $request->get('groupBy');
            $addSelect = $this->controller->addSelect;
            if ($request->get('where')) {
                if ($this->controller->requiredWhereParam !== null) {
                    $where = $this->controller->requiredWhereParam;
                    $clause = json_decode($request->get('where'));
                    foreach ($clause as $key => $value) {
                        $where = str_replace(':' . $key, $value, $where);
                    }
                }
            }

//            dump($selectString);
//            dump($request->get('start'));
//            dump($request->get('length'));
//            dump($columnOrder);
//            dump($orderDir);
//            die();
            $data = $repo->dataTable(
                explode(',', $selectString),
                $request->get('start'),
                $request->get('length'),
                $columnOrder,
                $orderDir,
                $request->get('search')['value'],
                $where,
                $joint,
                $groupBy,
                $addSelect
            );

//            dump($request->get('actions'));die();
//            if($request->get('actions')) {
            if ($data['data'] !== null) {
                if ($request->get('actions')) {

                    $data['data'] = $this->injectButton($data['data'], $request->get('actions'));
                }

                if ($this->controller->formatDTResult) {
                    $data['data'] = $this->reformat($data['data'], $this->controller);
                }
            }

            return new JsonResponse(
                [
                    'data' => $data['data'],
                    'draw' => $request->get('draw'),
                    'recordsTotal' => $data['count'],
                    'recordsFiltered' => $data['count'],
                    'all' => $data,
                ], 200
            );
        } catch (\Exception $e) {
            dump($e);
            die();
        }
    }

    /**
     * @param Request $request
     * @Route("/datatable/delete", name="datatable-delete")
     *
     * @return JsonResponse
     */
    public function deleteDatatable(Request $request)
    {
        $response = [
            'code' => 101,
            'status' => 'Initiate',
            'label' => 'danger',
        ];
        $ctrl = 'App\\Controller\\' . $request->get('space') . '\\' . $request->request->get('class');
        $this->controller = new $ctrl($this->em);

        try {
            if (method_exists($this->controller->class, 'getAuthor')) {
                $data = $this->em->getRepository($this->controller->class)->findOneBy(
                    [
                        'id' => $request->request->get('id'),
                        'author' => $this->getUser(),
                    ]
                );
            } else {
                $data = $this->em->getRepository($this->controller->class)->findOneBy(
                    [
                        'id' => $request->request->get('id'),
                    ]
                );
            }

//            if ($this->controller->notifyUpload !== null) {
//                list($dataGetter, $link) = explode(':', $this->controller->notifyUpload);
//                $dataGetter = 'get' . ucfirst($dataGetter);
//                $link = 'get' . ucfirst($link);
//                $uploads = $data->$dataGetter()->toArray();
//                foreach ($uploads as $file) {
//                    if (file_exists('../public' . $file->$link())) {
//                        unlink('../public' . $file->$link());
//                    }
//                }
//            }
            $this->em->remove($data);
            $this->em->flush();
            $response = [
                'code' => 200,
                'status' => 'Berhasil dihapus',
                'label' => 'success',
            ];
        } catch (\Exception $e) {
            $response = [
                'code' => 403,
                'status' => 'Terjadi kesalahan saat menghapus data ' . $e->getMessage(),
                'label' => 'danger',
            ];
        }

        return new JsonResponse(
            $response,
            200
        );
    }

    /**
     * @param $data
     */
    protected function reformat($data, $class)
    {
        $newData = [];
        foreach ($data as $single) {
            $result = [];
            foreach ($class->tableFields as $col) {
                if (isset($col['formatter'])) {
                    $methods = explode("-", $col['formatter']);
                    $method = $methods[0];
                    $n1class = $class;
                    if (!empty($methods[1])) {
                        $n1class = new $methods[1]($this->em);
                    }
                    $result[$col['field']] = $n1class->$method($single);
                } else {

                    $result[$col['field']] = $single[$col['field']];
                }
            }
            $result['id'] = $single['id'];
            if (isset($single['action'])) {
                $result['action'] = $single['action'];
            }
            $newData[] = $result;
        }

        return $newData;
    }

    /**
     * @param $data
     * @param $buttons
     *
     * @return mixed
     */
    protected function injectButton($data, $buttons)
    {
        if ($buttons !== null && !empty($buttons)) {
            $buttons = explode(',', $buttons);
            foreach ($data as &$single) {
                $buttonInject = [];
                foreach ($buttons as $button) {
                    $getter = $button . 'Button';
                    $buttonInject[] = method_exists($this->controller, "$getter") ? $this->controller->$getter($single) : $this->$getter($single);
                }
                $single['action'] = implode(' ', $buttonInject);
            }
        }

        return $data;
    }

}
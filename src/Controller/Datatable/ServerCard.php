<?php

namespace App\Controller\Datatable;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ServerCard
 * @package App\Controller\Datatable
 */
class ServerCard extends AbstractController
{
    protected $em;

    /**
     * Server constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @Route("/card/get", name="card-get")
     */
    public function getData(Request $request){
        try {
            $ctrl = 'App\\Controller\\' . $request->get('space') . '\\' . $request->get('class');
            $nw = new $ctrl($this->em);

            $repo = $this->em->getRepository($nw->class);
            $selectString = '';
            foreach ($request->get('sql') as $key => $val) {
                $selectString .= $val. ',';
            }
//            dump($request->get('sql'));die();
            $selectString = substr($selectString, 0, -1);
            $where = $nw->requiredWhere;
            $joint        = $nw->joint;
            $addSelect    = $nw->addSelectCard;
            if ($nw->requiredWhereParam !== null) {
                $where = $nw->requiredWhereParam;
                $clause = json_decode($request->get('where'));
                foreach ($clause as $key => $value) {
                    $where = str_replace(':' . $key, $value, $where);
                }
            }
            $data = $repo->card(
                explode(',', $selectString),
                $where,
                $joint,
                $addSelect
            );

            if ($data['data'] !== null) {
                if ($nw->formatDTResult) {
                    $data['data'] = $this->reformat($data['data'], $nw, $request);
                }
            }

            return new JsonResponse(
                [
                    'detail' => $data,
//                    'all' => $data
                ]
            );

        }catch (\Exception $e){
            dump('error = '.$e);
            die();
        }
    }

    /**
     * @param $data
     * @param $class
     *
     * @return array
     */
    protected function reformat($data, $class, Request $request)
    {
        $newData = [];
        foreach ($data as $single) {
            foreach ($request->get('sql') as $key => $col) {
                if (strpos($col, ' as ')) {
                    $field = explode('as ',$col);
                }else{
                    $field = explode('.',$col);
                }

                if (isset($request->get('formatter')[$key])) {
                    if ($request->get('formatter')[$key] == $field[1]) {
                        $method = $request->get('formatter')[$key];
                        $result[$field[1]] = $class->$method($single);
                    } else {
                        $result[$field[1]] = $single[$field[1]];
                    }
                }else{
                    $result[$field[1]] = $single[$field[1]];
                }
            }
            $result['id'] = $single['id'];
            $newData[] = $result;
        }
        return $newData;
    }

}
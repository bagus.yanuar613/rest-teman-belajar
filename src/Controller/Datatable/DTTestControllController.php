<?php

namespace App\Controller\Datatable;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class DTTestControllController extends AbstractController
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/dt-view", methods="GET|HEAD")
     */
    public function index(){
        return $this->render('TestDatatables/DTAjaxObject.html.twig');
    }
    /**
     * @Route("/dt-test/{row}", methods="GET|HEAD")
     */
    public function getData($row)
    {
        $res = [];
        for($i=0; $i < $row; $i++){
            $obj['col1'] = 'vcol1-'.$i;
            $obj['col2'] = 'vcol2-'.$i;
            $obj['col3'] = 'vcol3-'.$i;
            $obj['col4'] = '<button class="btn">test</button>';
            array_push($res,$obj);
        }

        return new JsonResponse(['data' => $res], 200);
    }
}

// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/8.0.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.0.0/firebase-messaging.js');
// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
firebase.initializeApp({
    apiKey: "AIzaSyASzro8lPtYHw-9iTbMkVaQVHX3iDf3xXw",
    authDomain: "teman-belajar-mo-1564386173622.firebaseapp.com",
    databaseURL: "https://teman-belajar-mo-1564386173622.firebaseio.com",
    projectId: "teman-belajar-mo-1564386173622",
    storageBucket: "teman-belajar-mo-1564386173622.appspot.com",
    messagingSenderId: "183874983613",
    appId: "1:183874983613:web:e100f19094d2d3821389d4",
    measurementId: "G-5HXYFWHV8W"
});
// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();
// [END initialize_firebase_in_sw]

messaging.setBackgroundMessageHandler(function(payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const notificationTitle = 'Background Message Title';
    const notificationOptions = {
        body: 'Background Message body.',
        icon: '/firebase-logo.png'
    };

    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});

messaging.usePublicVapidKey('BNBxUJ7pzk0dpzWN5frBT9oi0Wf9iqcu0gw5BtwOwOPlNFDGjKNXAndeZ1k5uQ4ZI1RJwCyqdqIlNs5eN7uwyLQ');

var today = new Date();

var datatest = {
    parameter: 'month',
    start: String(today.getMonth() + 1).padStart(2, '0'),
    end: String(today.getMonth()).padStart(2, '0'),
};

$(function () {

    var start = moment().subtract(29, 'days');
    var end = moment();
    var param1 = 'Last 30 Days';

    function cb(start, end, param1) {
        $('#reportrange span').html(start.format('D MMMM YYYY') + ' - ' + end.format('D MMMM YYYY'));
        param(start.format('YYYY-MM-D HH:mm:ss'), end.format('YYYY-MM-D HH:mm:ss'), param1);
        console.log(start.format('YYYY-MM-D HH:mm:ss'))
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        tipe: param1,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end, param1);

});
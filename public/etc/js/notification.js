
var notif = 0;
if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
        firebase.initializeApp({
            apiKey: "AIzaSyASzro8lPtYHw-9iTbMkVaQVHX3iDf3xXw",
            authDomain: "teman-belajar-mo-1564386173622.firebaseapp.com",
            databaseURL: "https://teman-belajar-mo-1564386173622.firebaseio.com",
            projectId: "teman-belajar-mo-1564386173622",
            storageBucket: "teman-belajar-mo-1564386173622.appspot.com",
            messagingSenderId: "183874983613",
            appId: "1:183874983613:web:e100f19094d2d3821389d4",
            measurementId: "G-5HXYFWHV8W"
        });
// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
        const messaging = firebase.messaging();
        messaging.usePublicVapidKey('BNBxUJ7pzk0dpzWN5frBT9oi0Wf9iqcu0gw5BtwOwOPlNFDGjKNXAndeZ1k5uQ4ZI1RJwCyqdqIlNs5eN7uwyLQ');

        navigator.serviceWorker.register('/firebase-messaging-sw.js');

        // [START get_messaging_object]
        // Retrieve Firebase Messaging object.
        // [END get_messaging_object]
        // [START set_public_vapid_key]
        // Add the public key generated from the console here.

        // [END set_public_vapid_key]
        // const messaging = firebase.messaging();

        // messaging.usePublicVapidKey('BKq1LPepjNymqWRaRCz0IlLrbMivEr0hNoIEdARoVzwVRsjSkaBFFdAgTiz8WT0YiR1k00Wai6ZRlaHI7d85hxc');

        // IDs of divs that display Instance ID token UI or request permission UI.
        // const tokenDivId = 'token_div';
        // const permissionDivId = 'permission_div';
        //
        // // [START refresh_token]
        // // Callback fired if Instance ID token is updated.
        // messaging.onTokenRefresh(() => {
        //     messaging.getToken().then((refreshedToken) => {
        //         console.log('Token refreshed.');
        //         // Indicate that the new Instance ID token has not yet been sent to the
        //         // app server.
        //         setTokenSentToServer(false);
        //         // Send Instance ID token to app server.
        //         sendTokenToServer(refreshedToken);
        //         // [START_EXCLUDE]
        //         // Display new Instance ID token and clear UI of all previous messages.
        //         resetUI();
        //         // [END_EXCLUDE]
        //     }).catch((err) => {
        //         console.log('Unable to retrieve refreshed token ', err);
        //         showToken('Unable to retrieve refreshed token ', err);
        //     });
        // });
        // [END refresh_token]

        // [START receive_message]
        // Handle incoming messages. Called when:
        // - a message is received while the app has focus
        // - the user clicks on an app notification created by a service worker
        //   `messaging.setBackgroundMessageHandler` handler.
        messaging.onMessage((payload) => {
            console.log('Message received. ', payload);
            notif += 1;
            let title = payload['notification']['title'];
            let body = payload['notification']['body'];
            // $('#notif-title').html(title);
            // $('#notif-body').html(body);
            // $('#toast-notif').toast('show');
            notifklik(title,body);
            // [START_EXCLUDE]
            // Update the UI to include the received message.
            // appendMessage(payload);
            // [END_EXCLUDE]
        });

        // [END receive_message]

        // function resetUI() {
        //     // clearMessages();
        //     showToken('loading...');
        //     // [START get_token]
        //     // Get Instance ID token. Initially this makes a network call, once retrieved
        //     // subsequent calls to getToken will return from cache.
        messaging.getToken().then((currentToken) => {
            if (currentToken) {
                console.log(currentToken)
            } else {
                // Show permission request.
                console.log('No Instance ID token available. Request permission to generate one.');
                // Show permission UI.
                // updateUIForPushPermissionRequired();
                // setTokenSentToServer(false);
            }
        }).catch((err) => {
            console.log('An error occurred while retrieving token. ', err);
            // showToken('Error retrieving Instance ID token. ', err);
            // setTokenSentToServer(false);
        });
        //     // [END get_token]
        // }
        //
        //
        // function showToken(currentToken) {
        //     console.log("token show", currentToken);
        //     // Show token in console and UI.
        //     // const tokenElement = document.querySelector('#token');
        //     // tokenElement.textContent = currentToken;
        // }
        //
        // // Send the Instance ID token your application server, so that it can:
        // // - send messages back to this app
        // // - subscribe/unsubscribe the token from topics
        // function sendTokenToServer(currentToken) {
        //     if (!isTokenSentToServer()) {
        //         console.log('Sending token to server...');
        //         // TODO(developer): Send the current token to your server.
        //         setTokenSentToServer(true);
        //     } else {
        //         console.log('Token already sent to server so won\'t send it again ' +
        //             'unless it changes');
        //     }
        //
        // }
        //
        // function isTokenSentToServer() {
        //     return window.localStorage.getItem('sentToServer') === '1';
        // }
        //
        // function setTokenSentToServer(sent) {
        //     window.localStorage.setItem('sentToServer', sent ? '1' : '0');
        // }

        // function requestPermission() {
        //     console.log('Requesting permission...');
        //     // [START request_permission]
        //     Notification.requestPermission().then((permission) => {
        //         if (permission === 'granted') {
        //             console.log('Notification permission granted.');
        //
        //
        //             // TODO(developer): Retrieve an Instance ID token for use with FCM.
        //             // [START_EXCLUDE]
        //             // In many cases once an app has been granted notification permission,
        //             // it should update its UI reflecting this.
        //             resetUI();
        //             // [END_EXCLUDE]
        //         } else {
        //             console.log('Unable to get permission to notify.');
        //         }
        //     });
        //     // [END request_permission]
        // }
        //
        // function deleteToken() {
        //     // Delete Instance ID token.
        //     // [START delete_token]
        //     messaging.getToken().then((currentToken) => {
        //         messaging.deleteToken(currentToken).then(() => {
        //             console.log('Token deleted.');
        //             setTokenSentToServer(false);
        //             // [START_EXCLUDE]
        //             // Once token is deleted update UI.
        //             resetUI();
        //             // [END_EXCLUDE]
        //         }).catch((err) => {
        //             console.log('Unable to delete token. ', err);
        //         });
        //         // [END delete_token]
        //     }).catch((err) => {
        //         console.log('Error retrieving Instance ID token. ', err);
        //         showToken('Error retrieving Instance ID token. ', err);
        //     });
        //
        // }
        //
    });
}

function notifklik(title, message) {

    //
    $.notify({
        // options
        icon: 'fa fa-bell',
        title: title,
        message: message,
        url: '',
        target: '_blank'
    },{
        // settings
        element: 'body',
        position: null,
        type: "info",
        allow_dismiss: true,
        newest_on_top: true,
        showProgressbar: false,
        placement: {
            from: "bottom",
            align: "right"
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
        delay: 5000,
        timer: 1000,
        url_target: '_blank',
        mouse_over: 'pause',
        animate: {
            enter: 'animated fadeInRight',
            exit: 'animated fadeOutRight'
        },
        onShow: null,
        onShown: null,
        onClose: null,
        onClosed: null,
        icon_type: 'class',
        template: '<a data-notify="container" href="{3}" target="_blank" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<span data-notify="title" class="font-weight-bold" style="display: block">{1}</span>' +
            '<span data-notify="message">{2}</span>' +
            '</a>'
    });
}

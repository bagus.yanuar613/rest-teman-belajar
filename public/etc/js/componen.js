var url = window.location.pathname.split('/');
var lok2 = url[2];
var lok1 = url[1];
var lok3 = url[3];
$(document).ready(function () {
    setActive();
    // tgl_mulai();
    var clas = localStorage.getItem('sidebar');

    if (clas === '') {
        $('#sidebar').addClass('active');
    } else {
        $('#sidebar').removeClass('active');
    }

    $('#tableData_wrapper .dataTables_filter').html('asd');

    $('.checktext').click(function () {
        if ($(this).prop("checked") == true) {
            $('#text' + $(this).attr('id')).removeAttr('disabled');
        } else if ($(this).prop("checked") == false) {
            $('#text' + $(this).attr('id')).attr('disabled', '');
            $('#text' + $(this).attr('id')).val('');

        }
    });
});

function setActive() {
    console.log(lok3);
    if (lok2 === undefined || lok2 === '') {
        $('#sidebar #dashboard').addClass('active');
    } else {
        $('#sidebar #' + lok2).addClass('active');
    }
    if (lok3) {
        console.log('lok2 ' + lok2);
        $('#sub-' + lok2).collapse();
        $('#sub-' + lok2 + ' #' + lok3).css('color', 'var(--blueColor)')
    }
    // if (lok2 === 'info') {
    //     $('#colapse-profile').addClass('active');
    //     $('.collapsible-body').attr('style', 'display:block');
    //     $('#' + lok3).addClass('active');
    // } else if (lok2 === 'kost' || lok2 === 'kontrakan' || lok2 === 'persewaan' || lok2 === 'guest-house') {
    //     // $('#colapse_property').addClass('active');
    //     $('.collapsible-body').attr('style', 'display:block');
    //     // $('#'+lok3).addClass('active');
    // }
}

function showToast(message, label) {
    var warna = "";
    if (label === 'success') {
        warna = "green";
    } else if (label === 'danger') {
        warna = "red";
    } else if (label === 'warning') {
        warna = "yellow"
    } else if (label === 'info') {
        warna = "blue"
    } else {
        warna = "";
    }
    M.toast({html: message, classes: 'rounded ' + warna});
}

async function getDataAjax(url, data = {}) {
    let resToJson = [];
    await $.get(url, data, function (data) {
        if (data['code'] === 200) {
            resToJson = data['payload'];
        }
    });
    return resToJson;
}

async function handleImageUpload(event) {
    // console.log(event);
    // const files = event.target.files;
    const files = event[0].files;
    var data = '';
    for (var i = 0; i < files.length; i++) {
        // console.log("FILE ", imageFile)
        const imageFile = files[i];
        console.log('originalFile instanceof Blob', imageFile instanceof Blob); // true
        console.log(`originalFile size ${imageFile.size / 1024 / 1024} MB`);

        const options = {
            maxSizeMB: 0.5,
            maxWidthOrHeight: 1000,
            useWebWorker: true
        };
        try {
            const compressedFile = await imageCompression(imageFile, options);// smaller than maxSizeMB
            data = compressedFile;

        } catch (error) {
            console.log(error);
            data = 'error'
        }

    }
    return data;

}

function currency(field) {
    $('#' + field).on({
        keyup: function () {
            formatCurrency($(this));
        },
        blur: function () {
            formatCurrency($(this), "blur");
        }
    });

}

function currencyClass(field) {
    $('.' + field).on({
        keyup: function () {
            formatCurrency($(this));
        },
        blur: function () {
            formatCurrency($(this), "blur");
        }
    });

}

function getCodeBank(select1, bank_id) {
    $('#' + select1).empty().select2();
    // var bank_id = parseInt($('#bank_id').val());
    $.get('/admin/get-bank',
        function (data) {
            console.log(data);
            if (data['code'] === 200) {

                var a = data['payload'];
                var awal = "Pilih Bank";
                var select = $('#' + select1);
                select.append('<option value="" selected="selected" disabled>' + awal + '</option>');

                $.each(a, function (key, value) {
                    if (value['id'] === bank_id) {
                        select.append('<option selected value="' + value['id'] + '">' + value['name'] + ' (' + value['code'] + ')</option>');
                    } else {
                        select.append('<option value="' + value['id'] + '">' + value['name'] + ' (' + value['code'] + ')</option>');
                    }
                });
            } else {
                console.log('Gagal ambil data bank');
            }
        })
}

function getCodeBankAccount(select1, bank_id) {
    $('#' + select1).select2();
    // var bank_id = parseInt($('#bank_id').val());
    $.get('/admin/get-bank-account',
        function (data) {
            console.log(data);
            if (data['code'] === 200) {

                var a = data['payload'];
                var awal = "Pilih Nomor Rekening";
                var select = $('#' + select1);
                select.append('<option value="" selected="selected" disabled>' + awal + '</option>');

                $.each(a, function (key, value) {
                    if (value['id'] === bank_id) {
                        select.append('<option selected value="' + value['id'] + '">' + value['holderName'] + '' + value['name'] + ' (' + value['code'] + ')</option>');
                    } else {
                        select.append('<option value="' + value['id'] + '">' + value['holderName'] + ' ' + value['name'] + ' (' + value['code'] + ')</option>');
                    }
                });
            } else {
                console.log('Gagal ambil data bank');
            }
        })
}






(function($) {

	"use strict";

	var fullHeight = function() {

		$('.js-fullheight').css('height', $(window).height());
		$(window).resize(function(){
			$('.js-fullheight').css('height', $(window).height());
		});

	};
	fullHeight();

	$('#sidebarCollapse').on('click', function () {
      // var sidebar = $('#sidebar').toggleClass('active');
      var clas =  $('#sidebar').attr('class');
      if(clas === ''){
		  $('#sidebar').addClass('active');
	  }else{
		  $('#sidebar').removeClass('active');
	  }
      localStorage.setItem("sidebar", clas);
      console.log(clas);

	});

})(jQuery);

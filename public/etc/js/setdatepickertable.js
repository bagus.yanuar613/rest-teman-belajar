var today = new Date();
var mulai, selesai, awal, akhir;
var url = window.location.href.split('?');
const months = ["January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
];
if (url[1]) {
        url = url[1].split('&');
        mulai = new Date(url[0].split('=')[1]);
        selesai = new Date(url[1].split('=')[1]);
        awal = String(mulai.getDate()).padStart(2, 0) + ' ' + months[mulai.getMonth()] + ' ' + mulai.getFullYear();
        akhir = String(selesai.getDate()).padStart(2, 0) + ' ' + months[selesai.getMonth()] + ' ' + selesai.getFullYear();
        console.log(awal);
        console.log(akhir);
}

$(function () {
    var start = mulai ?? moment().subtract(29, 'days');
    var tglawal = awal ?? moment().subtract(29, 'days').format('D MMMM YYYY');
    var tglAkhir = akhir ?? moment().format('D MMMM YYYY');
    var end = selesai ?? moment();

    $('#reportrange span').html(tglawal + ' - ' + tglAkhir);

    function cb(start, end, param1) {
        $('#reportrange span').html(start.format('D MMMM YYYY') + ' - ' + end.format('D MMMM YYYY'));
        getTable(start.format('YYYY-MM-D'), end.format('YYYY-MM-D'));
        console.log(start.format('YYYY-MM-D HH:mm:ss'))
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    // cb(start, end, param1);

});

function getTable(start, end) {
    window.location = '?dateStart=' + start + '&dateEnd=' + end;

}
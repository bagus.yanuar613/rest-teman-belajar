var cacheName = 'Temporas';

// Cache all the files to make a PWA
self.addEventListener('install', e => {
    e.waitUntil(
        caches.open(cacheName).then(cache => {
            return cache.addAll([
                // './',
                // './index.html',
                '/Manifest.json'
            ]);
        })
    );
});


self.addEventListener('fetch', event => {
    event.respondWith(
        caches.open(cacheName)
            .then(cache => cache.match(event.request, { ignoreSearch: true }))
            .then(response => {
                return response || fetch(event.request);
            }).catch(function(err) {       // fallback mechanism
            return caches.open('CACHE_CONTAINING_ERROR_MESSAGES')
                .then(function(cache) {
                    return cache.match('/offline.html');
                });
        })
    );
});
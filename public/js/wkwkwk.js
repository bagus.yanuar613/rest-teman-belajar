function getCurrentDate(date) {
    let year = String(date.getFullYear());
    let month = String(date.getMonth() + 1).padStart(2, '0');
    let day = String(date.getDate()).padStart(2, '0');
    // return year+'-'+month+'-'+day+' '+today.getHours()+':'+today.getMinutes()+':'+today.getSeconds();
    return year + '-' + month + '-' + day;
}

function dateToStringWHour(date) {
    let year = String(date.getFullYear());
    let month = String(date.getMonth() + 1).padStart(2, '0');
    let day = String(date.getDate()).padStart(2, '0');
    let hour = String(date.getHours()).padStart(2, '0');
    let minute = String(date.getMinutes()).padStart(2, '0');
    let seconds = String(date.getSeconds()).padStart(2, '0');
    return day+'-'+month+'-'+year+' '+hour+':'+minute+':'+seconds;
}

function getCurrentDateString(date) {
    return date.toLocaleDateString('id-ID', {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'})
}

function formatUang(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function createLoading(text = 'Sedang Memuat....') {
    return '<div class="w-100 text-center d-flex align-items-center justify-content-center" style="min-height: 200px">' +
        '<div class="text-center">' +
        '<div class="spinner-border text-primary" role="status">' +
        '<span class="sr-only">Loading...</span>' +
        '</div>' +
        '<div class="text-center w-100">' + text + '</div>' +
        '</div>' +
        '</div>';
}

function generateRateStar(value) {
    let result = '';
    for (let i = 0; i < Math.floor(value); i++) {
        result += '<i class="fa fa-star"></i>';
    }
    let tempValue = value - Math.floor(value);
    if (tempValue < 1 && tempValue > 0) {
        result += '<i class="fa fa-star-half-o"></i>';
        for (let i = 0; i < 5 - (Math.floor(value) + 1); i++) {
            result += '<i class="fa fa-star-o"></i>';
        }
    } else {
        for (let i = 0; i < 5 - (Math.floor(value)); i++) {
            result += '<i class="fa fa-star-o"></i>';
        }
    }
    return result;
}

function genAlert(title, msg) {
    $('#alert').html('<div class="modal fade" id="modalAlert" tabindex="-1" role="dialog" aria-hidden="true">\n' +
        '    <div class="modal-dialog modal-dialog-centered " role="document">\n' +
        '        <div class="modal-content " style="border-radius: 15px">\n' +
        '\n' +
        '            <div class="modal-body p-3">\n' +
        '\n' +
        '                <p class="text-center font-weight-bold">' + title + '</p>\n' +
        '                <p class="text-center f08">' + msg + '</p>\n' +
        '\n' +
        '                <div class="d-flex justify-content-center align-items-center mt-4">\n' +
        '                    <a class="bt-primary shadow-md mdl-alert" style="padding: 0.2em 0.8em; width: 6em" data-dismiss="modal"\n' +
        '                       aria-label="Close">Oke</a>\n' +
        '\n' +
        '                </div>\n' +
        '            </div>\n' +
        '\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>')
    $('#modalAlert').modal('show');
    $('.mdl-alert').on('click', function () {
        $('#modalAlert').modal('hide');
    })
}

function genConfirm(title, msg, accept) {
    $('#alert').html('<div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-hidden="true">\n' +
        '    <div class="modal-dialog modal-dialog-centered " role="document">\n' +
        '        <div class="modal-content " style="border-radius: 15px">\n' +
        '\n' +
        '            <div class="modal-body p-3">\n' +
        '\n' +
        '                <p class="text-center font-weight-bold">' + title + '</p>\n' +
        '                <p class="text-center f08">' + msg + '</p>\n' +
        '\n' +
        '                <div class="d-flex justify-content-center align-items-center mt-4">\n' +
        '                    <a class="bt-primary shadow-md  mr-3"  onclick=' + accept + ' style="padding: 0.2em 0.8em; width: 6em" data-dismiss="modal"\n' +
        '                       aria-label="Close">Ya</a>\n' +
        '\n' +
        '                    <a class="bt-outline-grey-a shadow-md" style="padding: 0.2em 0.8em; width: 6em" data-dismiss="modal"\n' +
        '                       aria-label="Close">Tidak</a>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>')
    $('#modalConfirm').modal('show');
}


function genLoading(text = 'Tunggu Sebentar Ya...') {
    $('#alert').html('<div class="modal fade" id="modalLoading" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">\n' +
        '    <div class="modal-dialog modal-dialog-centered " role="document">\n' +
        '        <div class="modal-content " style="border-radius: 15px">\n' +
        '\n' +
        '            <div class="modal-body p-3">\n' +
        '<div style="margin-left: calc(50% - 3em)">' +
        '<div class="loading">\n' +
        '        <span></span>\n' +
        '        <span></span>\n' +
        '        <span></span>\n' +
        '        <span></span>\n' +
        '        <span></span>\n' +
        '        <span></span>\n' +
        '\n' +
        '    </div>\n' +
        '            </div>\n' +
        '<p id="alertText" class="text-center" style="margin-top: 60px">' + text +
        '</p>' +
        '            </div>\n' +
        '\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>'
    )
    $('#modalLoading').modal('show');
}
function closeLoading() {
    $('#modalLoading').modal('hide', {backdrop: "false", keyboard: true});
    $('#modalLoading').css('overflow-y', 'scroll');
    $(".modal-backdrop").remove();
}

function genLoadingDestroy() {
    $("body #modalLoading").remove();
    $("body").removeClass("modal-open");
    $("body .modal-backdrop.fade.show ").remove();
}
function genLoadingHide() {
    $('#alert').html('');
    $('#modalLoading').modal('hide');
}

function genLoadingShowError(txt) {
    $("#alertText").html('<div class="alert alert-danger" role="alert">' +  txt + '<button type="button" class="close" data-dismiss="modal" aria-label="Close">\n' +
        '          <span aria-hidden="true">&times;</span>\n' +
        '        </button></div> ');
}

function typeDelay(callback, ms) {
    var timer = 0;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}

function timeSince(date) {
    let seconds = Math.floor((new Date() - date) / 1000);
}

function closeInfoInstallApp() {
    $("#infoInstalApp").html("<div></div>").removeClass("infoInstalApp");
}
function setSlickPromo() {
    $('.promo').slick({
        arrows: false,
        centerMode: true,
        centerPadding: '20%',
        infinite: false,
        slidesToShow: 1,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    infinite: false,
                    centerPadding: '15%',
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    mobileFirst: true,
                    infinite: false,
                    centerPadding: '10%',
                    slidesToShow: 1
                }
            }
        ]
    });
}

$(document).ready(function () {

    var mc = {
        '0-60': 'red',
        '61-75': 'orange',
        '76-100': 'green'
    };

    var cm = {
        '0-0': 'text-black-50',
        '1-60': 't-red',
        '61-75': 't-orange',
        '76-100': 't-green'
    };

    function between(x, min, max) {
        return x >= min && x <= max;
    }

    var dc;
    var first;
    var second;
    var th;

    $('p').each(function (index) {
        th = $(this);
        dc = parseInt($(this).attr('data-color'), 0);
        $.each(cm, function (name, value) {

            first = parseInt(name.split('-')[0], 10);
            second = parseInt(name.split('-')[1], 10);
            if (between(dc, first, second)) {
                th.addClass(value);

            }
        });

    });

    $('div').each(function (index) {
        th = $(this);
        dc = parseInt($(this).attr('data-color'), 0);
        $.each(mc, function (name, value) {
            first = parseInt(name.split('-')[0], 10);
            second = parseInt(name.split('-')[1], 10);
            if (between(dc, first, second)) {
                th.addClass(value);
                th.width(dc + '%');
            }
        });

    });
});

$.fn.onDataLoading = function (text = 'Sedang Menguduh Data...', params) {
    let options = $.extend({
        minHeight: 200
    }, params);
    this.empty();
    let content = '<div class="w-100 text-center d-flex align-items-center justify-content-center" style="min-height: ' + options.minHeight + 'px">' +
        '<div class="text-center">' +
        '<div class="spinner-border text-primary" role="status">' +
        '<span class="sr-only">Loading...</span>' +
        '</div>' +
        '<div class="text-center w-100">' + text + '</div>' +
        '</div>' +
        '</div>';
    this.append(content);
};

$.fn.onEmptyData = function (text = 'Oops, Data Yang kamu Cari Tidak Di Temukan', params) {
    let options = $.extend({
        minHeight: 200
    }, params);
    this.empty();
    let content = '<div class="d-flex justify-content-center align-items-center" style="min-height: ' + options.minHeight + 'px">' + text + '</div>';
    this.append(content);
};

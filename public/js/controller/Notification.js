var notification = 0;
var cart_notif = 0;
var subs_not_confirmed_notif = 0;
var invoice_notif = 0;

async function getCartNotif() {
    cart_notif = 0;
    let response = await $.get('/member/cart/count');
    if (response['code'] === 200) {
        let v = response['payload']['data'];
        cart_notif = parseInt(v);
        if(cart_notif > 0){
            $('#cart-notification').html(cart_notif);
            $('#cart-notification').removeClass('d-none');
        }else{
            $('#cart-notification').addClass('d-none');
        }
        console.log('cart', cart_notif);
    }
}

async function getCountSubscriptionNotConfirmed()
{
    subs_not_confirmed_notif = 0;
    let response = await $.get('/member/subscription/not-confirmed/count');
    if (response['code'] === 200) {
        let v = response['payload']['data'];
        subs_not_confirmed_notif = parseInt(v);
        if(subs_not_confirmed_notif > 0){
            $('#not-confirmed-notiication').html(subs_not_confirmed_notif);
            $('#not-confirmed-notiication').removeClass('d-none');
        }else{
            $('#not-confirmed-notiication').addClass('d-none');
        }
        console.log('subs', subs_not_confirmed_notif);
    }
}

async function getCountInvoice()
{
    invoice_notif = 0;
    let response = await $.get('/member/invoice/count');
    if (response['code'] === 200) {
        let v = response['payload']['data'];
        invoice_notif = parseInt(v);
        if(invoice_notif > 0){
            $('#invoice-notification').html(invoice_notif);
            $('#invoice-notification').removeClass('d-none');
        }else{
            $('#invoice-notification').addClass('d-none');
        }
        console.log('invoice', invoice_notif);
    }
}


$(document).ready(async function () {
    notification = 0;
    await getCartNotif();
    await getCountSubscriptionNotConfirmed();
    await getCountInvoice();
    notification = cart_notif + subs_not_confirmed_notif + invoice_notif;
    if(notification > 0){
        $('#my-account-notif').html(notification);
        $('#my-account-notif').removeClass('d-none');
    }else{
        $('#my-account-notif').addClass('d-none');
    }
});
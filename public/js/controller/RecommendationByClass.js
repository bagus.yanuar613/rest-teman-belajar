function setSlickRekomendasi() {
    $('.slick-rekomendasi').slick({
        arrows: false,
        centerMode: true,
        centerPadding: '20%',
        infinite: false,
        slidesToShow: 1,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    infinite: false,
                    centerPadding: '15%',
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    mobileFirst: true,
                    infinite: false,
                    centerPadding: '10%',
                    slidesToShow: 1
                }
            }
        ]
    });
}

function recommendWrapper(v) {
    let avatar = v[0]['user']['profile']['generatedAvatar'] || location.origin + '/images/no-image.png';
    let name = v[0]['user']['profile']['name'];
    let about = v[0]['user']['profile']['about'];
    let slug = v[0]['slug'];
    let rating = v[0]['user']['avgRatings'];
    let price = v['price'];
    return '<div data-slug="' + slug + '" class="gen-card r-wrapper p-3" style="height: 15em">\n' +
        '<div class="header-profile">\n' +
        '                                        <div class="gambar-div"><img src="' + avatar + '"></div>\n' +
        '                                        <div class="content">\n' +
        '                                            <p class="title">' + name + '</p>\n' +
        '                                            <div class="star-div">' + generateRateStar(rating) + '</div>\n' +
        '                                            <p class="f08 fivelines">' + about + '</p>\n' +
        '                                            <p class="text-right t-primary f12">Rp.' + formatUang(price) + ' /Jam</p>\n' +
        '                                        </div>\n' +
        '                                    </div>\n' +
        '                                </div>';
}

async function getRecommendation(c, v) {
    let el = $('#' + v);
    let data = {
        category: c,
        method: 1,
        district: d_id,
        level: l_id
    };
    let response = await $.get('/member/recommendation/data', data);
    $(".slick-rekomendasi").slick('unslick');
    el.empty();
    if (response['code'] === 200) {
        if (response['payload'].length > 0) {
            $.each(response['payload'], function (k, v) {
                el.append(recommendWrapper(v));
            });
            $('.r-wrapper').on('click', function () {
                let slug = this.dataset.slug;
                window.location.href = '/guru/' + m_id + '/' + slug;
            })
        } else {
            // let content = '<div class="d-flex justify-content-center align-items-center" style="min-height: 200px">Tidak Ada Rekomendasi Untuk Kamu</div>';
            // el.append(content)
            $('#title-main-related-'+c).remove();
            $('#panel-main-related-'+c).remove();
        }
        setSlickRekomendasi();
    }
}
$(document).ready(function () {
    setSlickRekomendasi();
    $.each($('.related'), function (k, v) {
        let id = v.dataset.id;
        let elId = v.id;
        getRecommendation(id, elId)
    });
});
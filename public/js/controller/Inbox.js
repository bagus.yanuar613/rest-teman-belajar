function elInbox(v) {
    const {sender, identifier, message, slug, status, createdAt} = v;
    let senderId = sender['id']
    let tempDate = new Date(createdAt);
    let dateString = String(tempDate.getHours()).padStart(2, '0') + ':' + String(tempDate.getMinutes()).padStart(2, '0');
    let type = '';
    let redirectTo = '/';
    switch (slug) {
        case 'subscription':
            type = 'Pemesanan';
            redirectTo = '/member/subscription/detail/' + identifier;
            break;
        case 'chat':
            type = 'Pesan';
            redirectTo = '/member/chat-with/' + senderId;
            break;
        default:
            break;
    }
    return '<a class="d-block" href="' + redirectTo + '">\n' +
        '            <div class="notifikasi-untuk-kamu">\n' +
        '                <div class="header">\n' +
        '                    <div class="d-flex align-items-center">\n' +
        '                        <i class="fa fa-shopping-cart mr-2 t-primary"></i>\n' +
        '                        <p class="f08 mr-4 mb-0 text-black-50">' + type + '</p>\n' +
        '                        <i class="fal fa-stopwatch"></i>\n' +
        '                        <p class="f08 mr-4 mb-0 text-black-50">Hari ini - ' + dateString + '</p>\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '                <div class="content">\n' +
        '                    <div class="left-content">\n' +
        '                        <p class="f10 font-weight-bold">Yeay, pesanan kamu diterima guru</p>\n' +
        '                        <p class="f08">' + message + '</p>\n' +
        '                    </div>\n' +
        '                    <div class="right-content">\n' +
        '                        <img src="/images/common/icons/mengajar.png">\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </a>'
}

async function getNotification() {
    let el = $('#panel-inbox');
    el.onDataLoading('Sedang Mengunduh Data Pesan Masuk..')
    let response = await $.get('/member/notification/list');
    console.log(response);
    if (response['code'] === 200) {
        el.empty()
        if (response['payload']['data'].length > 0) {
            $.each(response['payload']['data'], function (k, v) {
                el.append(elInbox(v));
            })
        }
    }else{
        el.onEmptyData('Tidak Ada Pesan Masuk');
    }
}

$(document).ready(function () {
    getNotification()
});
async function getReviews() {
    const url = window.location.pathname;
    let path = url.split("/");
    let slugName = path[4];
    let el = $('#review-box');
    let response = await $.get('/api-public/tentor/reviews/' + slugName);
    if (response['code'] === 200) {
        el.empty();
        if (response['payload'].length > 0) {
            $.each(response['payload'], function (k, v) {
                const {name, rating, review} = v;
                let avatar = v['avatar'] || location.origin + '/images/no-image.png';
                let content = '<div class="header-profile">' +
                    '<div class="gambar-div"><img src="' + avatar + '"></div>' +
                    '<div class="content">' +
                    '<p class="title">' + name + '</p>' +
                    '<div class="star-div">' + generateRateStar(rating) + rating +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<p class="f10">' + review + '</p>';
                el.append(content);
            })
        } else {
            el.append('Belum Ada Reviews');
        }
    }
}

function splitTime(time) {
    let temp = time.split(':');
    return temp[0] + ':' + temp[1];
}

async function getScheduleByDay(day) {
    let param = {u_id: u_id, d_id: day};
    let el = $('#panel-schedule');
    el.empty();
    el.append(createLoading('Sedang Memuat Daftar Jadwal..'))
    let response = await $.get('/api-public/tentor/schedule-by-day', param);
    el.empty();
    if (response['code'] === 200) {
        let payload = response['payload'];
        if (payload.length > 0) {
            $.each(payload, function (k, v) {
                let {id, day, time} = v;
                let content = '<input class="checkbox-jam" type="radio" name="sch" id="sch-' + id + '"' +
                    'value="' + id + '" disabled/>' +
                    '<label class="for-checkbox-tab "' +
                    'for="sch-' + id + '">' +
                    '<p class="mb-0 font-weight-bold p-0 f06">' + splitTime(time) + '</p>\n' +
                    '</label>'
                el.append(content)
            })
        } else {
            el.append('<div>Pengajar Tidak Ada Membuka Jadwal</div>')
        }
    }
}

$(document).ready(function () {
    getReviews().then(r => {
    });
    let day = $('input[name=day]:checked').val();
    // getScheduleByDay(day);
    $('#btn-order').on('click', function (k, v) {
        let currentUrl = window.location.pathname;
        window.location.href = currentUrl.replace('guru', 'order');
    })
    $('.day-choice').on('click', function (e) {
        let day = $('input[name=day]:checked').val();
        getScheduleByDay(day);
    })
});
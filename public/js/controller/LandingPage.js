async function getReviews() {
    let el = $('#review-box');
    let response = await $.get('/api-public/tentor/reviews-sample');
    if (response['code'] === 200) {
        el.empty();
        if (response['payload'].length > 0) {
            $.each(response['payload'], function (k, v) {
                const {review, rating} = v;
                let avatar = v['user']['profile']['generatedAvatar'];
                let name = v['user']['profile']['fullName'];
                let generatedAvatar = avatar || location.origin + '/images/no-image.png';
                let content = '<div class="gen-card p-3"><div class="header-profile">' +
                    '<div class="gambar-div"><img src="' + generatedAvatar + '" alt="' + generatedAvatar + '"></div>' +
                    '<div class="content">' +
                    '<p class="title">' + name + '</p>' +
                    '<div class="star-div f08">' + generateRateStar(rating) + "(" + rating + ")" +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<p class="f08">' + review + '</p></div>';
                el.append(content);
            })
        } else {
            el.append('<p class="text-center w-100 text-black-50 ">Belum Ada Reviews</p>');
        }
    }
}

async function getDefaultCategories() {
    let el = $('#panel-category');
    let response = await $.get('/api-public/categories/default');
    el.empty();
    if (response['code'] === 200) {
        if (response['payload'].length > 0) {
            $.each(response['payload'], function (k, v) {
                const {name, slug, id} = v;
                let content = '<div class="col-sm col-3">' +
                    '<div class="bt-mini" data-id="' + id + '" data-slug="' + slug + '">' +
                    '<div class="imagebutton">' +
                    '</div>' +
                    '<p class="label">' + name + '</p>' +
                    '</div>' +
                    '</div>';
                el.append(content);
            });
            let addContent = '<div class="col-sm col-3">' +
                '<div class="bt-mini" data-id="other" data-slug="other">' +
                '<div class="imagebutton">' +
                '</div>' +
                '<p class="label">Kategori Lainnya s</p>' +
                '</div>' +
                '</div>';
            el.append(addContent);
            $('.bt-mini').on('click', function () {
                let slug = this.dataset.slug;
                if(slug === 'other'){
                    window.location.href = '/pencarian-kategori';
                }
            })
        } else {
            el.append('<p class="text-center w-100 text-black-50">Belum Ada Mata Pelajaran</p>')
        }
    }

}
$(document).ready(function () {
    getDefaultCategories().then(r => {
    });
    getReviews()

});
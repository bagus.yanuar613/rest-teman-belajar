$.fn.createFilterList = function (params) {
    let options = $.extend({
        classTarget: 'list-target',
        name: '',
        id: '',
    }, params);
    let content = '<div class="hasil-pencarian">' +
        '<a data-id="' + options.id + '" data-name="' + options.name + '" class="title ' + options.classTarget + '">' + options.name +
        '</a>' +
        '</div>';
    this.append(content);
};

async function searchCategories(value) {
    let el = $('#category-results');
    if (value !== '') {
        el.onDataLoading('Tunggu Sebentar Sedang Mencarikan Mata Pelajaran Yang Kamu Cari...');
        let param = {name: value}
        let response = await $.get('/api-public/categories/name', param);
        el.empty();
        if (response['code'] === 200) {
            let payload = response['payload'];
            let classTarget = 'category-target';
            if (payload.length > 0) {
                $.each(response['payload'], function (k, v) {
                    const {name, id} = v;
                    el.createFilterList({
                        classTarget: classTarget,
                        name: name,
                        id: id
                    })
                });
                $('.category-target').click(function () {
                    let el = $('#category');
                    el.html(this.dataset.name);
                    el.attr('data-category', this.dataset.id);
                    $('#modalPencarianPelajaran').modal('hide');
                });
            } else {
                el.onEmptyData()
            }
        }
    } else {
        el.empty();
    }
}

async function searchDistricts(value) {
    let el = $('#district-content');
    if (value !== '') {
        let param = {name: value};
        el.onDataLoading('Tunggu Sebentar Sedang Mencarikan Wilayah Yang Kamu Cari...');
        let response = await $.get('/api-public/cities/district', param);
        el.empty();
        if (response['code'] === 200) {
            let payload = response['payload'];
            let classTarget = 'district-target';
            if (payload.length > 0) {
                $.each(response['payload'], function (k, v) {
                    const {district, id, city} = v;
                    el.createFilterList({
                        classTarget: classTarget,
                        name: district + ' (' + city + ')',
                        id: id
                    });
                });
                $('.district-target').click(function () {
                    let el = $('#district');
                    el.html(this.dataset.name);
                    el.attr('data-district', this.dataset.id);
                    $('#modalPencarianLokasi').modal('hide');
                });
            } else {
                el.onEmptyData();
            }
        }
    } else {
        el.empty();
    }
}

function getGeoLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else {
        console.log("Geolocation is not supported by this browser.");
    }
}

async function showPosition(position) {
    console.log(position.coords.latitude, position.coords.longitude);
}

function showError(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            console.log("User denied the request for Geolocation.")
            break;
        case error.POSITION_UNAVAILABLE:
            console.log("Location information is unavailable.")
            break;
        case error.TIMEOUT:
            console.log("The request to get user location timed out.")
            break;
        case error.UNKNOWN_ERROR:
            console.log("An unknown error occurred.")
            break;
    }
}

function changeDateTime() {
    let el = $('#pencarianTanggal');
    let elChild = $('#tanggal-dipilih');
    let vDate = $('#datepicker').datepicker('getDate');
    let vTime = $('input[name=time]:checked').val();
    let date = new Date(vDate);
    let time = String(vTime).padStart(2, '0') + ':00';
    elChild.html(getCurrentDateString(date) + ' ' + time);
    el.html(getCurrentDateString(date) + ' ' + time);
    el.attr('data-meet', getCurrentDate(date) + ' ' + time);
}

function setDisableTime(selectedDate, today) {
    if (selectedDate.getDate() !== today.getDate()) {
        $('input[name=time]').prop('checked', false);
        $('#jam9').prop('checked', true);
        for (let i = 9; i <= 20; i++) {
            $('#jam' + i).attr('disabled', false);
            $('#label-jam' + i).removeClass('disabled');
        }
    } else {
        $('input[name=time]').prop('checked', false);
        $('#jam' + today.getHours()).prop('checked', true);
        for (let i = 9; i < today.getHours(); i++) {
            $('#jam' + i).attr('disabled', true);
            $('#label-jam' + i).addClass('disabled');
        }
    }
}



function findTentor() {
    let category = $('#category').attr('data-category');
    let district = $('#district').attr('data-district');
    // let meet = $('#pencarianTanggal').attr('data-meet');
    let method = $('input[name=jenis]:checked').val();
    if (method === '1') {
        if (district === undefined) {
            genAlert('Ooops', 'Kamu Belum Memilih Wilayah Belajar Kamu.')
        } else {
            window.location.href = '/pencarian?category=' + category + '&district=' + district +  '&method=' + method;
        }
    } else {
        window.location.href = '/pencarian?category=' + category + '&method=' + method;
    }
}

function setMethod() {
    let method = $('input[name=jenis]:checked').val();
    if (method === '2') {
        $('.panel-location').addClass('d-none');

    } else {
        $('.panel-location').removeClass('d-none');
    }
}



async function findMeTentor() {
    let category = $('#category').attr('data-category');
    let district = $('#district').attr('data-district');
    let meet = $('#pencarianTanggal').attr('data-meet');
    let method = $('input[name=jenis]:checked').val();
    let param = {};
    if (method === '1') {
        if (district === undefined) {
            genAlert('Ooops', 'Kamu Belum Memilih Wilayah Belajar Kamu.')
        } else {
            param = {
                category: category,
                method: method,
                meet: meet,
                district: district
            };
            let response = await $.get('/api-public/subject/auto-find-tentor', param);
            console.log(response)
        }
    } else {
        param = {
            category: category,
            method: method,
            meet: meet,
            district: null
        };
        let response = await $.get('/api-public/subject/auto-find-tentor', param);
        console.log(response)
    }
}


$(document).ready(function () {
    setMethod();
    $('#modalPencarianPelajaran').on('show.bs.modal', function (e) {
        $('#category-results').empty();
        $('#category-content').empty();
        $('#find-category-field').val('');
    });
    $('#modalPencarianPelajaran').on('shown.bs.modal', async function (e) {
        let el = $('#category-content');
        el.empty();
        el.append('<div class="d-flex align-items-center justify-content-center" style="min-height: 200px">' +
            createLoading('Sedang Memuat Pelajaran Terpopuler...') +
            '</div>');
        let response = await $.get('/api-public/categories/popular');
        el.empty();
        let content = '<div class="d-flex flex-wrap" id="category-child"></div>';
        el.append(content);
        $.each(response['payload'], function (k, v) {
            const {name, id} = v;
            let child = '<a class="chip-primary mr-1 mb-1 chip-category-target" href="#" data-id="' + id + '" data-name="' + name + '">' + name + '</a>'
            $('#category-child').append(child);
        })
        $('.chip-category-target').on('click', function (e) {
            e.preventDefault();
            let elTarget = $('#category');
            elTarget.html(this.dataset.name);
            elTarget.attr('data-category', this.dataset.id);
            $('#modalPencarianPelajaran').modal('hide');
        });

    });

    $('#modalPencarianTanggal').on('show.bs.modal', function (e) {
        let el = $('#pencarianTanggal');
        let elChild = $('#tanggal-dipilih');
        let dataMeet = el.attr('data-meet');
        let today = new Date(dataMeet);
        let hourString = String(today.getHours()).padStart(2, '0') + ':00';
        let dateString = getCurrentDateString(today) + ' ' + hourString;
        $('#datepicker').datepicker({
            todayHighlight: true,
            format: "yyyy-mm-dd",
            language: "id",
            startDate: today,
        }).datepicker("setDate", today).on('changeDate', function (e) {
            setDisableTime(e.date, today);
            changeDateTime();
        });
        elChild.html(dateString);
        let elTime = $('#jam' + today.getHours());
        elTime.attr('checked', true);
        setDisableTime(today, today);
    });

    $('#find-category-field').keyup(typeDelay(async function (e) {
        await searchCategories(this.value);
    }, 500));

    $('#btn-find-location').on('click', async function () {
        let val = $('#find-location-field').val();
        await searchDistricts(val);
    });

    $('#btn-find-category').on('click', async function () {
        let val = $('#find-category-field').val();
        await searchCategories(val);
    });

    $('#modalPencarianLokasi').on('show.bs.modal', async function (e) {
        $('#district-content').empty();
        $('#find-location-field').val('');
        getGeoLocation();
    });


    $('#find-location-field').keyup(typeDelay(async function (e) {
        await searchDistricts(this.value);
    }, 500));

    $('.time-choice').on('click', function (e) {
        changeDateTime();
    });

    $('.method').on('click', function (e) {
        setMethod();
    });

    $('#find-tentor').on('click', function () {
        findTentor();
    });
    $('#auto-find-tentor').on('click', function () {
        genAlert('Ooops', 'Sabar Ya Fitur Ini Sedang Kami Perbaiki')
    })
});


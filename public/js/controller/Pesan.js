function setSlickPaket() {
    $('.paket').slick({
        arrows: false,
        centerMode: true,
        centerPadding: '20%',
        infinite: false,
        slidesToShow: 2,
        initialSlide: 1,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    infinite: false,
                    centerPadding: '15%',
                    slidesToShow: 2,
                    initialSlide: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    mobileFirst: true,
                    infinite: false,
                    centerPadding: '10%',
                    slidesToShow: 2,
                    initialSlide: 1
                }
            }
        ]
    });
}

function initPackage() {
    let duration = $('input[name=duration]:checked').val();
    let attendees = $('input[name=attendees]:checked').val();
    let paket = [4, 8, 12, 16];
    let price = $('#field-price').data('price');
    let vDuration = 0;
    switch (method) {
        case 1:
            vDuration = duration / 60;
            break;
        case 2:
            vDuration = duration / 30;
            break;
        default:
            vDuration = duration / 60;
            break;
    }
    $.each(paket, function (k, v) {
        let key = k + 1;
        let vPrice = v * vDuration.toFixed(1) * attendees * price;
        let priceString = formatUang(vPrice);
        $('#price-tag-' + key).html(String(priceString));
    });
    let vPaket = $('input[name=encounter]:checked').val();
    let tarif = vDuration.toFixed(1) * price;
    $('#label-price').data('price', tarif);
    $('#label-price').html('Rp' + String(formatUang(tarif)));
    $('#label-count').data('count', vPaket);
    $('#label-count').html(String(vPaket));
    $('#label-attend').data('attend', String(attendees));
    $('#label-attend').html(String(attendees));
    let summary = vPaket * vDuration.toFixed(1) * attendees * price;
    $('#label-summary').data('summary', String(summary));
    $('#label-summary').html('Rp ' + String(formatUang(summary)));
}

function changeDateTime() {
    let el = $('#pencarianTanggal');
    let elChild = $('#tanggal-dipilih');
    let vDate = $('#datepicker').datepicker('getDate');
    let vTime = $('input[name=time]:checked').val();
    let date = new Date(vDate);
    let time = String(vTime).padStart(2, '0') + ':00';
    elChild.html(getCurrentDateString(date) + ' ' + time);
    el.html(getCurrentDateString(date) + ' ' + time);
    el.attr('data-meet', getCurrentDate(date) + ' ' + time);
}

function timeElement(v) {
    const {id, day, time, flag} = v;
    let flagClass = flag === false ? 'disabled' : '';
    // let tempTime = new Date(time);
    // let hours = String(tempTime.getHours()).padStart(2, '0') + ':00';
    return '<div class="col-4">' +
        '<input ' + flagClass + ' class="checkbox-jam time-choice" type="radio" name="time" id="jam-' + id + '">' +
        '<label class="for-checkbox-jam ' + flagClass + '" for="jam-' + id + '">' + time + '</label>' +
        '</div>';
}

// function setDisableTime(selectedDate, today) {
//     if (selectedDate.getDate() !== today.getDate()) {
//         $('input[name=time]').prop('checked', false);
//         $('#jam9').prop('checked', true);
//         for (let i = 9; i <= 20; i++) {
//             $('#jam' + i).attr('disabled', false);
//             $('#label-jam' + i).removeClass('disabled');
//         }
//     } else {
//         $('input[name=time]').prop('checked', false);
//         $('#jam' + today.getHours()).prop('checked', true);
//         for (let i = 9; i < today.getHours(); i++) {
//             $('#jam' + i).attr('disabled', true);
//             $('#label-jam' + i).addClass('disabled');
//         }
//     }
// }

async function getTentorSchedule(day) {
    let timeContainer = $('#time-container');
    timeContainer.onDataLoading('Sedang Mengunduh Data Jadwal Tentor');
    let response = await $.get('/member/tentor/schedule/', {tsId: ts_id, day: day});
    timeContainer.empty();
    if (response['code'] === 200) {
        if (response['payload']['data'].length > 0) {
            $.each(response['payload']['data'], function (k, v) {
                timeContainer.append(timeElement(v));
            })
        } else {
            timeContainer.onEmptyData('Guru Tidak Ada Jadwal');
        }
    } else {
        timeContainer.onEmptyData('Terjadi Kesalahan');
    }
    console.log(response)
}

$(document).ready(function () {
    if (!inArea && parseInt(method) === 1) {
        $('#myModal').modal('show', {backdrop: 'static', keyboard: false});
    }

    setSlickPaket();
    initPackage();
    $('#modalPencarianTanggal').on('shown.bs.modal', function (e) {
        let el = $('#pencarianTanggal');
        let elChild = $('#tanggal-dipilih');

        let dataMeet = el.attr('data-meet');
        let today = new Date(dataMeet);
        let hourString = String(today.getHours()).padStart(2, '0') + ':00';
        let dateString = getCurrentDateString(today) + ' ' + hourString;
        $('#datepicker').datepicker({
            todayHighlight: true,
            format: "yyyy-mm-dd",
            language: "id",
            startDate: today,
        }).datepicker("setDate", today).on('changeDate', function (e) {
            let currentDayInt = e.date.getDay() - 1;
            getTentorSchedule(currentDayInt);
        });
        elChild.html(dateString);

        let currentDayInt = today.getDay() - 1;
        getTentorSchedule(currentDayInt);
        // console.log('hari ke ', today.getDay());
        // let elTime = $('#jam' + today.getHours());
        // elTime.attr('checked', true);
        // setDisableTime(today, today);
    });

    $('.time-choice').on('click', function (e) {
        changeDateTime();
    });

    $('.jumlah-choice').on('click', function (e) {
        initPackage();
    });
    $('.duration-choice').on('click', function (e) {
        initPackage();
    });
    $('.paket-choice').on('click', function (e) {
        initPackage();
    });

    $('#btn-order').on('click', function (e) {
        $('#form-order').submit();
    });

    $('#form-order').on('submit', function (e) {
        e.preventDefault();
        var address, note;

        if ($('#address').val() === '') {
            $('html, body').animate({
                scrollTop: $("#address").offset().top - 200
            }, 200);
            $("#address").addClass("is-invalid");
            $("#alamatDetailHelp").addClass("t-red").html('Kamu harus memberikan alamat detailmu ke tentor');
            address = false;
        } else {
            $("#address").removeClass("is-invalid");
            $("#alamatDetailHelp").addClass("t-red").html('');
            address = true;
        }

        if (check_for_phone()) {
            $('html, body').animate({
                scrollTop: $("#note").offset().top - 200
            }, 200);
            $("#note").addClass("is-invalid");
            $("#noteHelp").addClass("t-red").html('Kamu tidak bisa memasukan nomor handphone');
            note = false;
        } else {
            $("#note").removeClass("is-invalid");
            $("#noteHelp").addClass("t-red").html('');
            note = true;
        }

        if (address && note) {
            genLoading();


            let meet = $('#pencarianTanggal').attr('data-meet');
            let location = $('#pencarianLokasi').attr('data-location');
            let methodId = method;
            var form = $('#form-order')[0];
            var data = new FormData(form);
            data.append('tsId', ts_id);
            data.append('meet', meet);
            data.append('location', location);
            data.append('method', methodId);
            $.ajax({
                type: 'POST',
                url: '/member/subscription-create',
                processData: false,
                contentType: false,
                data: data,
                success: function (d) {
                    if (d['code'] === 200) {
                        console.log(d);
                        window.location.href = '/member/transaction-finish'
                    } else {
                        genAlert('Ooops', 'Sepertinya Sedang Terjadi Masalah Pada Server Kami!')
                    }
                },
                error: function (d) {
                    genAlert('Ooops', 'Sepertinya Sedang Terjadi Masalah Pada Server Kami!')
                }
            });
            return true;
        }

    });

    $('#btn-switch-online').on('click', function (e) {
        e.preventDefault();
        let currentUrl = window.location.pathname;
        window.location.href = currentUrl.replace('les-private', 'les-online');
    })

    function check_for_phone() {

        var message = document.getElementById("note").value;
        var Regex = /\b[\+]?[(]?[0-9]{2,6}[)]?[-\s\.]?[-\s\/\.0-9]{3,15}\b/m;

        // alert(Regex.test(message));
        return Regex.test(message);
        // This will alert 'true' if the textarea contains a phonenumber
    }
});
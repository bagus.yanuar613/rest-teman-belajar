var limit = 2;
var offset = 0;

async function getTentor() {
    let el = $('#content-tentor');
    offset = 0;
    el.empty();
    el.append(
        '<div class="d-flex justify-content-center align-items-center" style="min-height: 300px">' +
        createLoading('Sedang Memuat Data Tentor') +
        '</div>'
    );
    let response = await $.get('/api-public/subject/find-tentor', getParameter());
    el.empty();
    let payload = response['payload']['data'];
    let code = response['code'];
    let len = payload.length;
    if (code === 200) {
        if (payload.length > 0) {
            offset += len;
            $.each(payload, function (k, v) {
                el.append(cardResult(v));
            });
            if (payload.length === limit) {
                el.append(buttonLoadMore());
                $('#btn-load').on('click', function () {
                    loadMoreTentor();
                })
            }
        } else {
            el.append(cardEmptyResult());
        }
        goToDetailTentor();
    } else {
        el.append(cardEmptyResult('Terjadi Kesalahan!'));
    }
}

async function loadMoreTentor() {
    $('#load-more-wrapper').empty();
    $('#load-more-wrapper').append(createLoading());
    let response = await $.get('/api-public/subject/find-tentor', getParameter());
    $('#load-more-wrapper').remove();
    let payload = response['payload']['data'];
    let code = response['code'];
    let len = payload.length;
    if (code === 200) {
        offset += len;
        if (len > 0) {
            $.each(payload, function (k, v) {
                $('#content-tentor').append(cardResult(v));
            });
            if (payload.length === limit) {
                $('#content-tentor').append(buttonLoadMore());
                $('#btn-load').on('click', function () {
                    loadMoreTentor();
                })
            }
            goToDetailTentor();
        }
    } else {
        $('#content-tentor').append(cardEmptyResult('Terjadi Kesalahan!'));
    }
}

function goToDetailTentor() {
    $('.card-tentor').on('click', function (e) {
        let slug = this.dataset.slug;
        let method = this.dataset.method;
        window.location.href = '/guru/' + method + '/' + slug;
    })
}

function getParameter() {
    const search = new URLSearchParams(window.location.search);
    let category = search.get('category');
    let district = search.get('district');
    let meet = search.get('meet');
    let method = search.get('method');
    let level = $('input[name=level]:checked').val();
    let param = {category, district, level, method, meet, offset, limit};
    if (method === '2') {
        param = {category, level, method, meet, offset, limit}
    }
    return param;
}

function cardResult(v) {
    let slug = v[0]['slug'];
    let tentorAvatar = v[0]['user']['profile']['generatedAvatar'] || location.origin + '/images/no-image.png';
    let tentorName = v[0]['user']['profile']['name'];
    let tentorGrade = v[0]['user']['profile']['grade']['id'] === 2 ? '<a class="pro">PRO</a>' : '';
    let tentorAbout = v[0]['user']['profile']['about'] || '';
    let rating = v[0]['user']['avgRatings'];
    let price = v['price'];
    let district = v[0]['user']['district'] || [];
    let elDistrict = '';
    $.each(district, function (k, v) {
        elDistrict += '<a class="area bt-outline-grey-a">' + v['districtName'] + '</a>';
    });
    return '<div class="gen-card-guru card-tentor" data-method="' + method + '" data-slug="' + slug + '">' +
        '<div class="content-guru">' +
        '<div class="image-div"><img src="' + tentorAvatar + '" ></div>' +
        '<div class="data-div">' +
        '<div class="nama-div">' +
        '<p class="nama">' + tentorName + '</p>' + tentorGrade +
        '</div>' +
        '<div class="star-div">' + generateRateStar(rating) +
        '</div>' +
        '<p class="deskripsi">' + tentorAbout + '</p></div>' +
        '</div>' +
        '<div class="area-div">' + elDistrict +
        '</div>' +
        '<div class="text-right mt-2">' +
        '<a class="t-primary font-weight-bold">Rp. ' + formatUang(price) + '/Jam</a>' +
        '</div>' +
        '</div>';
}

function buttonLoadMore() {
    return '<div id="load-more-wrapper" class="text-right mb-1">' +
        '<a id="btn-load" class="bt-primary">Muat Lebih</a>' +
        '</div>';
}

function cardEmptyResult(text = 'Hasil Kosong') {
    return '<div class="d-flex justify-content-center align-items-center" style="min-height: 300px">' + text + '</div>';
}

$(document).ready(function () {
    $('.level-choice').on('click', function (e) {
        getTentor().then(r => {
        });
    });
    getTentor().then(r => {
    });


});
async function setPresence(id) {
    let param = {id};
    genLoading('Tunggu Sebentar, Sedang Melakukan Absen')
    let response = await $.post('/member/attendance/presence', param);
    if (response['code'] === 200) {
        genAlert('Yeay Kamu Berhasil Absen')
        closeLoading();
        window.location.reload();
    } else {
        genAlert('Aduh Absensi Kamu Gagal, Coba Deh Hubungi Admin!')
        closeLoading();
    }
}

function elNoInformation() {
    return '<section class="mt-3 container-fluid">\n' +
        '<p class="font-weight-bold text-center">Kamu Belum Berlangganan</p>\n' +
        '<p class="text-center mb-3">Guru Les Privatmu akan muncul disini setelah kamu berlangganan les privat</p>\n' +
        '<a href="/member/pencarian" class="btn bt-accent w-100 mt-1">Cari Guru Les Privat</a>\n' +
        '</section>';
}

async function getAttendance() {
    let el = $('#panel-presence');
    let response = await $.get('/member/attendance/active');
    if (response['code'] === 200) {
        el.empty();
        if (response['payload']['data'].length > 0) {
            el.append(elPresenceHeader());
            $.each(response['payload']['data'], function (k, v) {
                el.append(elMyPresence(v))
            });
            $('.btn-presence').on('click', async function () {
                let id = this.dataset.presence;
                await setPresence(id);
            });
        } else {
            el.append('')
        }
    }
}

function elPresenceHeader() {
    return '<p class="f12 font-weight-bold">Presensi Belajar</p>';
}
function elMyPresence(v) {
    let avatar = v['subscription']['skill']['user']['profile']['generatedAvatar'] || location.origin + '/images/no-image.png';
    let name = v['subscription']['skill']['user']['profile']['name'];
    let category = v['subscription']['skill']['category']['name'];
    let level = v['subscription']['skill']['level']['name'];
    let isActive = v['isActive'];
    let isPresence = v['isPresence'];
    let buttonPresence = '<div class="w-100"><p>Pelajaran Sedang Berlangsung</p></div>';
    if (isActive === 1 && isPresence === 0) {
        buttonPresence = '<div class="w-100 mb-2"><a data-presence="' + v['id'] + '" class="bt-primary btn-presence">Konfirmasi Presensi</a></div>'
    }
    return '<div class="gen-card p-3">' +
        '<div class="d-flex justify-content-between">' +
        '<p class="f12">Presenesi Les ' + category + ' - ' + level + ' Dengan Kak ' + name + '</p>' +
        '<img src="' + avatar + '" style="border-radius: 50%; height: 6em; width: 6em; object-fit: cover">' +
        '</div>' + buttonPresence +
        '</div>';
}



function elMyInvoice(v) {
    return '<div class="div-color" style="background-color: var(--primaryColorLight); border: 1px solid var(--primaryColor)">' +
        '<p class="mb-0 mr-2 text-left">Kamu Memiliki ' + v + ' Tagihan Yang Belum Di Bayar. Segera Lakukan Pembayaran Sebelum Tagihan Usang.</p>' +
        '<p class="text-right mb-0"><a href="/member/invoice" class="text-right">Tagihan</a></p>' +
        '</div>';
}
async function getInvoice() {
    let parentEl = $('#panel-invoice');
    let response = await $.get('/member/invoice/count');
    if (response['code'] === 200 && parseInt(response['payload']['data']) > 0) {
        let v = response['payload']['data'];
        let vInt = parseInt(v);
        parentEl.append(elMyInvoice(vInt));
    } else {
        parentEl.append('')
    }
}

function elMyCart(v) {
    return '<div class="div-color" style="background-color: var(--primaryColorLight); border: 1px solid var(--primaryColor)">' +
        '<p class="mb-0 mr-2 text-left">Kamu Memiliki ' + v + ' Pesanan Yang Belum Di Checkout. Segera Lakukan Checkout Sebelum Pesananmu Usang.</p>' +
        '<p class="text-right mb-0"><a href="/member/cart" class="text-right">Keranjang</a></p>' +
        '</div>';
}
async function getCart() {
    let parentEl = $('#panel-cart');
    let response = await $.get('/member/cart/count');
    if (response['code'] === 200 && parseInt(response['payload']['data']) > 0) {
        let v = response['payload']['data'];
        let vInt = parseInt(v);
        parentEl.append(elMyCart(vInt));
    } else {
        parentEl.append('')
    }
}

function elSubscriptionHeader() {
    return '<div class="mt-5">' +
        '<div class="d-flex justify-content-between ">' +
        '<p class="f12 font-weight-bold">Guruku</p>' +
        '<a class="f12  text-primary" href="/member/subscription">Lihat Semua</a>' +
        '</div>' +
        '</div>';
}

function elMySubscription(v) {
    let avatar = v['skill']['user']['profile']['generatedAvatar'] || location.origin + '/images/no-image.png';
    let name = v['skill']['user']['profile']['name'];
    let category = v['skill']['category']['name'];
    let level = v['skill']['level']['name'];
    let isExpired = v['requestCart']['isExpired'];
    let status = '';
    let classStatus = '';
    switch (v['status']) {
        case 0:
            status = 'Menunggu';
            classStatus = 'menunggu-konfirmasi-pengajar';
            break;
        case 1:
            status = 'Aktif';
            classStatus = 'aktif';
            break;
        case 6:
            status = 'Pesanan Di Batalkan';
            classStatus = 'menunggu-konfirmasi-pengajar';
            break;
        case 9:
            status = 'Selesai';
            classStatus = 'selesai';
            break;
        default:
            break;
    }

    if (v['status'] === 0 && v['confirmedAt'] === null) {
        status = 'Menunggu Konfirmasi Pengajar';
        classStatus = 'menunggu-konfirmasi-pengajar';
    }

    if (v['status'] === 0 && isExpired === true) {
        status = 'Pesanan Expired';
        classStatus = 'menunggu-konfirmasi-pengajar';
    }

    return '<div class="gen-card p-3 card-tentor" data-id="' + v['id'] + '">' +
        '<div class="header-profile mb-0" style="border-radius: 15px">' +
        '<div class="gambar-div-md mr-3">' +
        '<img src="' + avatar + '">' +
        '</div>' +
        '<div class="w-100">' +
        '<div class="justify-content-between d-flex w-100">' +
        '<div>' +
        '<p class="title mb-0">' + name + '</p>' +
        '<p class="subtitle mb-0">' + category + ' - ' + level + '</p>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<hr>' +
        '<div class="w-100 text-right">' +
        '<a class="' + classStatus + '">' + status + '</a>' +
        '</div>' +
        '</div>';
}

async function getSubscription() {
    let el = $('#panel-subscription');
    el.onDataLoading('Sedang Mengunduh Informasi Transaksi...');
    let response = await $.get('/member/subscription/list', {status: 'all', limit: 3, offset: 0});
    if (response['code'] === 200) {
        if (response['payload']['data'].length > 0) {
            el.empty();
            el.append(elSubscriptionHeader());
            $.each(response['payload']['data'], function (k, v) {
                el.append(elMySubscription(v))
            });
            $('.card-tentor').on('click', function () {
                let id = this.dataset.id;
                window.location.href = '/member/subscription/detail/' + id;
            });
        } else {
            el.empty();
            el.append(elNoInformation())
        }
    }
}

$(document).ready(function () {
    getSubscription();
    getCart();
    getInvoice();
    getAttendance();

});
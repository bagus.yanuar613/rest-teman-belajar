async function getPromo() {
    let el = $('#panel-promo');
    let response = await $.get('/api-public/promo');
    el.slick('unslick')
    el.empty();
    if (response['code'] === 200) {
        let payload = response['payload'];
        if (payload.length > 0) {
            $.each(payload, function (k, v) {
                el.append(setPanelPromo(v));
            });
            $('.gen-card-promo').on('click', function () {

            });
            setSlickPromo();
        } else {
            el.append('<p class="text-center w-100 text-black-50">Belum Ada Promo</p>')
        }
    } else {
        alert('failed');
    }
}

function setPanelPromo(v) {
    const {title, image, url} = v;
    let generatedImage = image !== null ? window.location.origin + '/images/promo/' + image : '';
    return '<div class="gen-card-promo" data-url="' + url + '">' +
        '<img src="' + generatedImage + '" alt="' + generatedImage + '">' +
        '</div>';
}

$(document).ready(function () {
    setSlickPromo();
    getPromo().then(r =>{});
});
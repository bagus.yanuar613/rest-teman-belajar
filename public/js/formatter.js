function currency(number, decPlaces, decSep, thouSep) {
    const formatter = new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR',
        minimumFractionDigits: 2
    })

    return formatter.format(number);
}

function ajaxCall(fetch, url, method, loading = true, data = {}) {
    if (loading) {
        $("body").append(
            '<div class="modal fade" id="modalLoading" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">\n' +
            '        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 50%">\n' +
            '            <div class="modal-content" id="alert">\n' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>'
        );
    }
    $.ajax({
            url: url,
            method: method,
            data : data,
            beforeSend: function (xhr) {
                if (loading) {
                    genLoading();
                }
            },
            success: function (resultList) {
                if (loading) {
                    if (resultList['code'] === 200) {
                        if (resultList['payload']['data'] != null) {
                            genLoadingDestroy();
                            window[fetch](resultList['payload']['data']);
                        } else {
                            genLoadingShowError("Oops, data tidak tersedia untuk saat ini");
                        }
                    } else {
                        // genLoadingDestroy();
                        genLoadingShowError(resultList['payload']['msg']);
                    }
                }else{
                    window[fetch](resultList['payload']['data']);
                }
                // console.log(resultList);
            }
        }
    );
}

function stringToDate(str)
{
    var reggie = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/;
    var dateArray = reggie.exec(str);
    return  new Date(
        (+dateArray[1]),
        (+dateArray[2])-1, // Careful, month starts at 0!
        (+dateArray[3]),
        (+dateArray[4]),
        (+dateArray[5]),
        (+dateArray[6])
    );
}